<?php

//Nav Menu Heads
$text_common['overview'] = 'Overview';
$text_common['so'] = 'Sales';
$text_common['agent_invoice'] = 'Agent Invoices';
$text_common['po'] = 'Purchaser';
$text_common['ro'] = 'Return Order';
$text_common['report'] = 'Reports';

// Sales Order - Nav Menu
$text_common['so_view'] = 'View Order';
$text_common['so_client'] = 'Clients';
$text_common['return_order'] = 'Return Orders';

// Agent Invoice - Nav Menu
$text_common['agent_invoice_view'] = 'View';
$text_common['agent_invoice_history'] = 'History';

// PO - Nav Menu
$text_common['po_view'] = 'View Order';
$text_common['po_vendor'] = 'Vendor';
// RO - Nav Menu
$text_common['ro_vendor'] = 'RO To Vendor';
$text_common['ro_client'] = 'RO From Client';

//Report - Nav Menu
$text_common['report_view_vendor'] = 'Vendor Wise';
$text_common['report_view_client'] = 'Client Wise';
