<?php

//sales order view start
//table headings
$text_salesOrder['sales_order'] = 'Sales Order';
$text_salesOrder['search'] = 'Search';
$text_salesOrder['sr'] = 'Sr. no.';
$text_salesOrder['so'] = 'SO#';
$text_salesOrder['client_name'] = 'Client Name';
$text_salesOrder['order_date'] = 'Order Date';
$text_salesOrder['sales_agent'] = 'Sales Agent';
$text_salesOrder['so'] = 'SO#';
$text_salesOrder['amount'] = 'Amount';
$text_salesOrder['ro'] = 'RO#';
$text_salesOrder['order_id'] = 'SO#';
$text_salesOrder['htt'] = 'Amount(HT)';
$text_salesOrder['ttc'] = 'Amount(TTC)';
//actions
$text_salesOrder['set_time_tracker'] = 'Set Time Tracker';
$text_salesOrder['alerts'] = 'Alerts/Reminder';
$text_salesOrder['details'] = 'Details';
$text_salesOrder['action'] = 'Action';
//sales details
$text_salesOrder['sales_details'] = 'Sales Details';
$text_salesOrder['so'] = 'SO#';
$text_salesOrder['grand_total'] = 'Grand Total';
$text_salesOrder['client_address'] = 'Client Address';
$text_salesOrder['delivery_date'] = 'Delivery Date';
$text_salesOrder['delivery_address'] = 'Delivery Address';

//items
$text_salesOrder['item_id'] = 'Item Id';
$text_salesOrder['item_name'] = 'Item Name';
$text_salesOrder['brand_name'] = 'Brand Name';
$text_salesOrder['sell_rate'] = 'Sell Rate';
$text_salesOrder['quantity'] = 'Quantity';
$text_salesOrder['total'] = 'Total';
$text_salesOrder['reference'] = 'Reference';
$text_salesOrder['mrp'] = 'MRP';
//sales order view end

//--------------------------------------------//

//client view start
$text_salesOrder['client_details'] = 'Client Details';
$text_salesOrder['client_id'] = 'Client Id';
$text_salesOrder['client_name'] = 'Client Name';
$text_salesOrder['client_email'] = 'Client Email';
$text_salesOrder['client_contact'] = 'Client Contact';
$text_salesOrder['address'] = 'Address';

//client details
$text_salesOrder['total_balance'] = 'Total Balance';
$text_salesOrder['credit_limit'] = 'Credit Limit';
$text_salesOrder['credit_limit_used'] = 'Credit Limit Used';
$text_salesOrder['status_c'] = 'Status';
$text_salesOrder['client_address'] = 'Client Address';
$text_salesOrder['approval_status'] = 'Approval Status';
$text_salesOrder['assigned_agent'] = 'Assigned Agent';

//ro details
$text_salesOrder['ro'] = 'Return Order';
$text_salesOrder['return_number'] = 'Return Number';
$text_salesOrder['return_date'] = 'Return Date';
$text_salesOrder['reason'] = 'Reason';
$text_salesOrder['return_amount'] = 'Return Amount';
$text_salesOrder['return_order'] = 'Return Order Details';
//item details
$text_salesOrder['item_id'] = 'Item Id';
$text_salesOrder['item_name'] = 'Item Name';
$text_salesOrder['msrp'] = 'MSRP <br> Per Unit';
$text_salesOrder['quantity'] = 'Quantity';
$text_salesOrder['amount'] = 'Amount';

//so details
$text_salesOrder['sales_order'] = 'Sales Order';
$text_salesOrder['so'] = 'SO#';
$text_salesOrder['date'] = 'Order Date';
$text_salesOrder['status'] = 'Order Status';
$text_salesOrder['amount_pending'] = 'Amount Pending';
$text_salesOrder['person_responsible'] = 'Person Responsible';
