<?php

//table headings
$text_returnOrder['return_number'] = 'Return Number';
$text_returnOrder['ro_number'] = 'RO#';
$text_returnOrder['return_date'] = 'Return Date';
$text_returnOrder['vendor_name'] = 'Vendor Name';
$text_returnOrder['address'] = 'Address';
$text_returnOrder['reason'] = 'Reason';
$text_returnOrder['created_by'] = 'Created By';
$text_returnOrder['adjusted'] = 'Adjusted';
$text_returnOrder['action'] = 'Action';

//action
$text_returnOrder['make_payment'] = 'Make Payment';

//items
$text_returnOrder['item_id'] = 'Item Id';
$text_returnOrder['item_name'] = 'Item Name';
$text_returnOrder['brand_name'] = 'Brand Name';
$text_returnOrder['discount_rate'] = 'Discount Rate';
