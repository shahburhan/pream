<?php

//po view start
//title
$text_purchaseOrder['purchase_order'] = 'Purchase Order';
$text_purchaseOrder['payment_order'] = 'Payment Orders';
//header
$text_purchaseOrder['purchase_order_details'] = 'Purchase Order Details';
$text_purchaseOrder['amount_returnable'] = 'Net Amount Returnable';
$text_purchaseOrder['total_amount'] = 'Total Amount To Be Paid';
$text_purchaseOrder['po_amount'] = 'PO Amount';
$text_purchaseOrder['search'] = 'Search';
$text_purchaseOrder['details'] = 'Details';
//table headings

$text_purchaseOrder['sr'] = 'Sr.No.';
$text_purchaseOrder['ro_number'] = 'RO#';
$text_purchaseOrder['vendor'] = 'Vendor';
$text_purchaseOrder['bol_date'] = 'BOL Date';
$text_purchaseOrder['po'] = 'PO#';
$text_purchaseOrder['amount_ht'] = 'Amount(HT)';
$text_purchaseOrder['ro'] = 'RO#';
$text_purchaseOrder['amount_ttc'] = 'Amount(TTC)';
$text_purchaseOrder['balance'] = 'Balance';
$text_purchaseOrder['action'] = 'Action';
$text_purchaseOrder['order_id'] = 'PO#';
$text_purchaseOrder['return_number'] = 'Return Number';
$text_purchaseOrder['address'] = 'Address';
//action
$text_purchaseOrder['make_payment'] = 'Make Payment';
$text_purchaseOrder['add_payment'] = 'Add To Payment';
$text_purchaseOrder['remove_payment'] = 'Remove From Payment';
$text_purchaseOrder['adjust'] = 'Adjust';

//details
$text_purchaseOrder['grand_total'] = 'Grand Total';
$text_purchaseOrder['adjust'] = 'Adjust';
$text_purchaseOrder['order_date'] = 'Order Date';
$text_purchaseOrder['vendor_name'] = 'Vendor Name';
$text_purchaseOrder['vendor_address'] = 'Vendor Address';
$text_purchaseOrder['delivery_date'] = 'Delivery Date';
$text_purchaseOrder['person_responsible'] = 'Person Reaponsible';
$text_purchaseOrder['delivery_address'] = 'Delivery Address';
$text_purchaseOrder['order_id_p'] = 'RO#';

//items
$text_purchaseOrder['item_id'] = 'Item Id';
$text_purchaseOrder['item_name'] = 'Item Name';
$text_purchaseOrder['brand_name'] = 'Brand Name';
$text_purchaseOrder['buy_rate'] = 'Buy Rate';
$text_purchaseOrder['quantity'] = 'Quantity';
$text_purchaseOrder['item_category'] = 'Item Category';
$text_purchaseOrder['mrp'] = 'MRP';
$text_purchaseOrder['reference'] = 'Reference';
$text_purchaseOrder['total'] = 'Total';

//po view end

//--------------------------------------------//

//vendor view start

//title
$text_purchaseOrder['vendor_details'] = 'Vendor Details';

//table headings
$text_purchaseOrder['vendor_id'] = 'Vendor Id';
$text_purchaseOrder['vendor_name'] = 'Vendor Name';
$text_purchaseOrder['vendor_email'] = 'Vendor Email';
$text_purchaseOrder['vendor_contact'] = 'Vendor Contact';
$text_purchaseOrder['approval_status'] = 'Approval Status';
$text_purchaseOrder['action'] = 'Action';

//view details
$text_purchaseOrder['balance'] = 'Balance';

//return order
$text_purchaseOrder['return_order'] = 'Return Order';
$text_purchaseOrder['ro'] = 'RO#';
$text_purchaseOrder['return_date'] = 'Return Date';
$text_purchaseOrder['reason'] = 'Reason';
$text_purchaseOrder['amount'] = 'Amount';
$text_purchaseOrder['msrp'] = 'MSRP <br> Per Unit';
$text_purchaseOrder['raised_by'] = 'Raised By';
$text_purchaseOrder['status'] = 'Order Status';

//btn
$text_purchaseOrder['edit'] = 'Edit';

//vendor view end//
