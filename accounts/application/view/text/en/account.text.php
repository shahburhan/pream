<?php

//login start
$text_account['title'] = 'Sign In';
$text_account['email'] = 'Username';
$text_account['password'] = 'Password';
$text_account['login'] = 'Login';
$text_account['subtitle'] = 'Accounts';

//login end//

//Error page start//
$text_account['error_404'] = 'Sorry, the page you are looking for cannot be found.';
//Error page end//

//Unauthorized start//
$text_account['unauthorized_message'] = 'You Are Not Authorized To View This Page.';
//Unauthorized end//

//---------------------------------------//

//overview start

//purchase
$text_account['purchase'] = 'Purchase';
$text_account['notifications'] = 'Notifications';
$text_account['messages'] = 'Messages';
$text_account['purchase_order'] = 'Purchase Orders';
$text_account['return_order'] = 'Return Orders';

//sales
$text_account['sales'] = 'Sales';
$text_account['notifications'] = 'Notifications';
$text_account['messages'] = 'Messages';
$text_account['sales_order'] = 'Purchase Orders';
$text_account['return_order'] = 'Return Orders';

//store
$text_account['store'] = 'Store';
$text_account['notifications'] = 'Notifications';
$text_account['messages'] = 'Messages';
$text_account['dispatched'] = 'Dispatched';
$text_account['received'] = 'Received';

//accounts
$text_account['accounts'] = 'Accounts';
$text_account['notifications'] = 'Notifications';
$text_account['messages'] = 'Messages';
$text_account['payment_made'] = 'Payment Made';
$text_account['payment_received'] = 'Payment Received';
$text_account['read_more'] = 'Read More';

$text_account['activity_1'] = "A <b class='text-danger'>new vendor</b> has been added";
$text_account['activity_2'] = 'Vendor Approved';
$text_account['activity_3'] = 'new_vendor_disapproved';
$text_account['activity_4'] = "A <b class='text-success'>new item</b> has been added";
$text_account['activity_5'] = "A new <b class='text-warning'>return request</b> has been placed";
$text_account['activity_6'] = "Your return request has been <b class='text-success'>approved</b>";
$text_account['activity_7'] = "Your return request has been <b class='text-danger'>disapproved</b>";
$text_account['activity_8'] = "The item in hand is less than <b class='text-danger'>minimum stock value</b>";
$text_account['activity_9'] = "The item has been <b class='text-danger'>in stock</b> for a long time";
$text_account['activity_10'] = " <b class='text-success'>Purchase order </b> has been received";
$text_account['activity_11'] = "<b class='text-warning'>Audit</b> report has to be submitted";
$text_account['activity_12'] = "Return order has been <b class='text-danger'>cancelled</b>";
$text_account['activity_13'] = "A <b class='text-success'>new </b> purchase order has been placed";
$text_account['activity_14'] = "A <b class='text-danger'>new </b> return order has been placed";
$text_account['activity_15'] = "A new <b class='text-success'>sales agent </b> has been added";
$text_account['activity_16'] = "A new <b class='text-success'>client </b> has been added";
$text_account['activity_17'] = "A new <b class='text-success'>sales order </b> has been placed";
$text_account['activity_18'] = "Sales order <b class='text-danger'>return </b> has been placed";
$text_account['activity_19'] = "A new <b class='text-success'>store keeper</b> has been added";
$text_account['activity_20'] = "A new <b class='text-success'>user </b> has been added";
$text_account['activity_21'] = "<b class='text-warning'>Audit </b> date has been added";
$text_account['activity_22'] = "Purchase order has been <b class='text-success'>delivered</b>";
$text_account['activity_23'] = "<b class='text-success'>BOL</b> has been added to purchase order";
$text_account['activity_24'] = "A new <b class='text-success'>sales agent </b> has been added";
$text_account['activity_25'] = "Purchase Order has been <b class='text-danger'>cancelled</b>";
$text_account['activity_26'] = "<b class='text-warning'>Payment</b> sent to client";
$text_account['activity_27'] = "<b class='text-success'>Payment</b> has been made to vendor.";
$text_account['activity_28'] = "<b class='text-success'>Agent Invoice</b> has been paid";
$text_account['activity_29'] = "Office <b class='text-danger'>expense</b> added in accounts";
$text_account['activity_30'] = "Change in monthly <b class='text-danger'>audit date</b>";
$text_account['activity_31'] = "Store Keeper details <b class='text-danger'>updated</b>";
$text_account['activity_32'] = "Sales order dispatched <b class='text-danger'>audit date</b>";
$text_account['activity_33'] = "Commission rate <b class='text-success'>added</b> for sales agent";
$text_account['activity_34'] = "Sales agent has been <b class='text-danger'>deactived</b>";
$text_account['activity_35'] = "Client edited<b class='text-danger'>audit date</b>";
$text_account['activity_36'] = "Client assigned<b class='text-danger'>audit date</b>";
$text_account['activity_37'] = "sales order cancelled<b class='text-danger'>audit date</b>";
$text_account['activity_38'] = "quotation requested<b class='text-danger'>audit date</b>";
$text_account['activity_39'] = "commission invoice generated<b class='text-danger'>audit date</b>";
$text_account['activity_40'] = "item edited<b class='text-danger'>audit date</b>";
$text_account['activity_41'] = "order item changed<b class='text-danger'>audit date</b>";
