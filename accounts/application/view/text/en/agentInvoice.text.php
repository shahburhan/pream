<?php

//agent invoice view start

//title
$text_agentInvoice['title'] = 'Agent Invoices';
$text_agentInvoice['net_amount'] = 'Net Payable Amount';
$text_agentInvoice['search'] = 'Search';

//table headings
$text_agentInvoice['sr'] = 'Sr. no.';
$text_agentInvoice['agent_name'] = 'Agent Name';
$text_agentInvoice['invoice'] = 'Invoice#';
$text_agentInvoice['invoice_type'] = 'Invoice Type';
$text_agentInvoice['invoice_amount'] = 'Invoice Amount';
$text_agentInvoice['generation_date'] = 'Generation Date';
$text_agentInvoice['action'] = 'Action';

//action
$text_agentInvoice['add_payment'] = 'Add To Payment';
$text_agentInvoice['remove_payment'] = 'Remove From Payment';
$text_agentInvoice['make_payment'] = 'Make Payment';

//details
$text_agentInvoice['invoice'] = 'Invoice';
$text_agentInvoice['grand_total'] = 'Grand Total';
$text_agentInvoice['invoice_date'] = 'Invoice Date:';

//items
$text_agentInvoice['item_id'] = 'Item Id';
$text_agentInvoice['item_name'] = 'Item Name';
$text_agentInvoice['brand_name'] = 'Brand Name';
$text_agentInvoice['msrp'] = 'MSRP';
$text_agentInvoice['commission_per_unit'] = 'Commission Per Unit';
$text_agentInvoice['quantity'] = 'Quantity';
$text_agentInvoice['commission'] = 'Commission';

//expense
$text_agentInvoice['expense_name'] = 'Expense Name';
$text_agentInvoice['expense_type'] = 'Expense Type';
$text_agentInvoice['expense_date'] = 'Expense Date';
$text_agentInvoice['expense_amount'] = 'Expense Amount';

//agent invoice view end

//-----------------------------------------------//

//agent invoice history start

//table headings
$text_agentInvoice['invoice'] = 'Invoice#';
$text_agentInvoice['invoice_type'] = 'Invoice Type';
$text_agentInvoice['invoice_amount'] = 'Invoice Amount';
$text_agentInvoice['generation_date'] = 'Generation Date';

//details
$text_agentInvoice['invoice_date'] = 'Invoice Date';
$text_agentInvoice['due_date'] = 'Invoice Date';
$text_agentInvoice['subtotal'] = 'Subtotal';
$text_agentInvoice['tax'] = 'Tax';
$text_agentInvoice['tds'] = 'TDS';
$text_agentInvoice['total'] = 'Total';
//items
$text_agentInvoice['item_id'] = 'Item Id';
$text_agentInvoice['item_name'] = 'Item Name';
$text_agentInvoice['brand_name'] = 'Brand Name';
$text_agentInvoice['discount_rate'] = 'Discount Rate';

//agent invoice history end
