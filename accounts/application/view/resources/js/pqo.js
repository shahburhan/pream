var total_amount = parseFloat($('.total_amount').html());
var purchase_amount = parseFloat($('.purchase_amount').html());
var return_amount = parseFloat($('.return_amount').html());
    

    $(".remove-payment-btn").click(function () {
        var id = $(this).parent().parent().attr("id");
        var ttc_amount = $("tr#"+id).children("#_amount_ttc").html();
        $.ajax({
            url: '?route=payment/removeFromPayment&order_id=' + id,
            type: 'GET',
            dataType: 'json',
            success: function (returnData) {                
                showSnackbar("Removed from payment");
                
                purchase_amount = purchase_amount - parseFloat(ttc_amount);
                if(purchase_amount == 0) $(".confirm-orders").attr("disabled", "disabled");
                calculateTotal();
                updateCount();
                $(".total_amount").html("&euro; "+total_amount);
                $(".purchase_amount").html("&euro; "+purchase_amount);

                loadItems();
            }
        });

    });

    function updateCount(){
    $.ajax({
            url: '?route=payment/updatePaymentCount',
            type: 'GET',
            dataType: 'json',
            success: function (returnData) {
                
                $("#payment-menu-btn").removeClass("no-item");
                $(".payment-queue").removeClass("hidden");
                if(returnData.count == 0){
                    $("#payment-menu-btn").addClass("no-item");
                    $(".payment-queue").addClass("hidden");
                }
                $("#payment-menu-btn").attr("data-badge", returnData.count);
            },
        });
    }

    function getDetails(e) {
        var id = $(e).parent().parent().attr("id");
        $.ajax({
            url: '?route=purchaseOrder/getOrderDetails&order_id=' + id,
            type: 'GET',
            dataType: 'json',
            success: function (returnData) {
                var orderItems = returnData.order_items;
               deliveries = returnData.delivery_address;
                //var raisedBy = returnData.raised_by;
                var purchaseOrder = returnData.purchase_order;
                $("#ttc_m").html(purchaseOrder.ttc);
                $("#tva_m").html(purchaseOrder.tva);
                $("#htt_m").html(purchaseOrder.htt);
                $("#order_id_m").html("#"+purchaseOrder.order_id);
                $("#order_date_m").html(purchaseOrder.order_date);
                $("#company_name_m").html(purchaseOrder.company_name);
                $("#vendor_address_m").html(purchaseOrder.address);
                if(purchaseOrder.delivery_date != "0000-00-00")$("#delivery_date_m").html(purchaseOrder.expected_date);
                else $("#delivery_date_m").html("-");
               d_a = deliveries[purchaseOrder.delivery_address]['address'];
                $("#delivery_address_m").html(d_a + "<small class='text-success'>( " + deliveries[purchaseOrder.delivery_address]['name']+" )</small>");
                 $("#order_items").html("");
                for(var i = 0; i < orderItems.length; i++){
                    var node = $("#items-template").children().clone();
                    node.children(".__refrence").html(orderItems[i].reference);
                    node.children(".__item_name").html(orderItems[i].item_name);
                    node.children(".__brand_name").html(orderItems[i].brand_name);
                    // node.children(".__item_category").html(orderItems[i].item_category);
                    node.children(".__buy_rate").html(orderItems[i].rate);
                    node.children(".__quantity").html(orderItems[i].quantity);
                    $("#order_items").append(node);
                }
                MDialog.start({}, "detail", this, $("#modal-view"));
            }
        });
    }

    var selectBoxItem;
    var selctizedBoxInstanceItem;

    var orderItemsFilter = [];

    function initItemFilterSelectBox() {
        selectBoxItem = $(".selectize-filter-items").selectize({
            onItemAdd: itemAddListener,
            onItemRemove: itemRemoveListener,
            options: [
                {id: 'time_1', type: 'date', label: 'Last day'},
                {id: 'time_2', type: 'date', label: 'Last one week'},
                {id: 'time_3', type: 'date', label: 'Last Two week'},
                {id: 'time_4', type: 'date', label: 'Last Month'},
            ],
            optgroups: [
                {id: 'vendor', name: 'Vendor'},
                {id: 'item', name: 'Item'},
                {id: 'date', name: 'By Added on'},
                {id: 'ref', name: 'Ref#'}
            ],
            labelField: 'label',
            valueField: 'id',
            optgroupField: 'type',
            optgroupLabelField: 'name',
            optgroupValueField: 'id',
            optgroupOrder: ['vendor', 'item', 'date', 'ref'],
            searchField: ['label'],
            plugins: ['optgroup_columns', 'remove_button'],
            maxItems:4,
            maxOptions : 10

        });
        if (selectBoxItem.length != 0) selctizedBoxInstanceItem = selectBoxItem[0].selectize;
    }
    var lastDateItemId = "";
    function itemAddListener(value, item){
        var item = $(item);
        var id = selctizedBoxInstanceItem.options[value].id;
        var type = selctizedBoxInstanceItem.options[value].type;
        var label = selctizedBoxInstanceItem.options[value].label;
        if(type == "date"){
            item.css("background-color","#1565c0");
            if(lastDateItemId != "")selctizedBoxInstanceItem.removeItem(lastDateItemId, false);
            lastDateItemId = id;
            loadItemsSelectBox();
        }
        if(type == "vendor"){
            loadItemsSelectBox();
            item.css("background-color","#00695c");
        }
        if(type == "item")item.css("background-color","#2e7d32");

        if(type == "ref"){
            item.css("background-color","#0277bd");
            loadItemsSelectBox();
        }

        loadItems();

    }
    function itemRemoveListener(value){
        var type = selctizedBoxInstanceItem.options[value].type;
        loadItemsSelectBox();
        loadItems();
    }

    function loadItemsSelectBox(){
        filterResults();
        if(vendorFilterList.length == 0 && dateFilter.length == 0) {
            clearItemOptions();
            return;
        }
        $(".mdl-progress").show();
        var postData = {
            all: 1,
            vendorList : vendorFilterList,
            dateFilter : dateFilter,
            refFilterList: refFilterList,
            list:'item'
        };
        $.ajax({
            url: '?route=purchaseOrder/getItemsList&all=1',
            type: 'POST',
            data: postData,
            dataType: 'json',
            success: function (returnData) {
                clearItemOptions();
                data = returnData.itemList;
                for(var i = 0; i < data.length ; i++){
                    //console.log(data[i]);
                    selctizedBoxInstanceItem.addOption({id : "item-"+data[i].item_id, type: "item", label: (data[i].item_name + "-" + data[i].reference)});
                }
                selctizedBoxInstanceItem.refreshOptions(false);
            },
            error: function () {
                $(".mdl-progress").hide();
                showSnackbar("Can't load Item list.");
            }
        });

    }


    function loadVendorsSelectBox(){
        $.ajax({
            url: '?route=purchaseOrder/getVendorsList&all=1',
            type: 'GET',
            dataType: 'json',
            success: function (returnData) {
                data = returnData.vendors_list;
                for(var i = 0; i < data.length ; i++){
                    selctizedBoxInstanceItem.addOption({id : data[i].vendor_id, type: "vendor", label: (data[i].company_name)});
                }
                selctizedBoxInstanceItem.refreshOptions(false);
            },
            error: function () {
                $(".mdl-progress").hide();
                showSnackbar("Can't load vendor list.");
            }
        });
    }
    function loadRefSelectBox(){
        $.ajax({
            url: '?route=purchaseOrder/getRefList&all=1',
            type: 'GET',
            dataType: 'json',
            success: function (returnData) {
                data = returnData.ref_list;
                for(var i = 0; i < data.length ; i++){
                    selctizedBoxInstanceItem.addOption({id : data[i].order_id, type: "ref", label: ("#"+data[i].order_id)});
                }
                selctizedBoxInstanceItem.refreshOptions(false);
            },
            error: function () {
                $(".mdl-progress").hide();
                showSnackbar("Can't load ref list.");
            }
        });
    }

    var vendorFilterList = [];
    var itemIdFilterList = [];
    var refFilterList = [];
    var dateFilter = [];



    function filterResults(){
        var input = $(this);
        vendorFilterList = [];
        refFilterList = [];
        itemIdFilterList = [];
        dateFilter = [];
        //console.log(selctizedBoxInstanceItem.getValue());
        for(var i=0; i < selctizedBoxInstanceItem.getValue().length; i++){
            var id = selctizedBoxInstanceItem.options[selctizedBoxInstanceItem.getValue()[i]].id;
            var type = selctizedBoxInstanceItem.options[selctizedBoxInstanceItem.getValue()[i]].type;
            var label = selctizedBoxInstanceItem.options[selctizedBoxInstanceItem.getValue()[i]].label;
            if(type == "vendor") vendorFilterList.push(id);
            if(type == "date") dateFilter.push(id);
            if(type == "ref") {
                var cId = id.substr(id.indexOf("-") + 1);
                refFilterList.push(cId);
            }
            if(type == "item"){
                var cId = id.substr(id.indexOf("-") + 1);
                itemIdFilterList.push(cId);
            }
        }

        if(orderItemsFilter.length > 0){
            for(var i=0 ; i < orderItemsFilter.length; i++){
                itemIdFilterList.push(orderItemsFilter[i]);
            }
            orderItemsFilter = [];
        }
    }

    var tableheader = $("#table-head");

    function tableSort() {
        var th = $(this);
        if (th.hasClass("no-sort")) {
            // console.log("NO");
            tableheader.children(".sortable").removeClass("mdl-data-table__header--sorted-ascending");
            tableheader.children(".sortable").removeClass("mdl-data-table__header--sorted-descending");
            tableheader.children(".sortable").addClass("mdl-data-table__header");
            tableheader.children(".sortable").addClass("no-sort");
            th.removeClass("no-sort");
            th.removeClass("mdl-data-table__header");
            th.addClass("mdl-data-table__header--sorted-ascending");
            loadItems();
            return;
        }

        if (th.hasClass("mdl-data-table__header--sorted-ascending")) {
            th.removeClass("mdl-data-table__header--sorted-ascending");
            th.addClass("mdl-data-table__header--sorted-descending");
            // console.log("ASC");
            loadItems();
            return;
        }

        if (th.hasClass("mdl-data-table__header--sorted-descending")) {
            th.removeClass("mdl-data-table__header--sorted-descending");
            th.addClass("mdl-data-table__header");
            th.addClass("no-sort");
            // console.log("DESC");
            loadItems();
        }

    }


    function clearItemOptions(){

        for(var prop in selctizedBoxInstanceItem.options){
            var id = selctizedBoxInstanceItem.options[prop].id;
            var type = selctizedBoxInstanceItem.options[prop].type;
            if(type == "item") {
                selctizedBoxInstanceItem.removeOption(id);
            }
        }

//        for(var i=0; i < selctizedBoxInstanceItem.options.length; i++){
//            console.log(selctizedBoxInstanceItem.options[i]);
//            var id = selctizedBoxInstanceItem.options[i].id;
//            var type = selctizedBoxInstanceItem.options[i].type;
//            if(type == "item") {
//                selctizedBoxInstanceItem.removeOption(id);
//            }
//
//        }
        selctizedBoxInstanceItem.refreshOptions(false);
    }

    function loadItems() {
        $(".mdl-progress").show();
        //filterResults();
        var postData = {
            all: 1,
            vendorList : vendorFilterList,
            dateFilter : dateFilter,
            itemIdList : itemIdFilterList,
            refList: refFilterList
        };
        // console.log(dateFilter);
        if (tableheader.children(".sortable.mdl-data-table__header--sorted-ascending").length > 0) {
            postData.sort = "ASC";
            postData.sortField = $(tableheader.children(".sortable.mdl-data-table__header--sorted-ascending")[0]).attr("id");
        }
        if (tableheader.children(".sortable.mdl-data-table__header--sorted-descending").length > 0) {
            postData.sort = "DESC";
            postData.sortField = $(tableheader.children(".sortable.mdl-data-table__header--sorted-descending")[0]).attr("id");
        }
         $.ajax({
            url: '?route=payment/getOrdersInQueue&all=1',
            type: 'POST',
            data: postData,
            dataType: 'json',
            success: function (returnData) {
                data = returnData.order_list;
                $("#order-body").html("");
                for (var i = 0; i < data.length; i++) {
                    var rowNode = $("#order_template >  tbody > tr").clone(true);
                    rowNode.attr("id", data[i].order_id);
                    rowNode.children("#_order_id").html(data[i].order_id);
                    rowNode.children("#_order_date").html(data[i].order_date);
                    rowNode.children("#_vendor_name").html(data[i].company_name);
                    rowNode.children("#_amount_htt").html(data[i].htt);
                    rowNode.children("#_amount_ttc").html(data[i].ttc);
                    rowNode.children("#_delivery_address").html("<b>"+returnData.client[data[i].delivery_address]['name']+"</b> <br>"+returnData.client[data[i].delivery_address]['address']);
                    rowNode.children("#action_child").children(".remove-payment-btn").attr("id", "remove"+data[i].order_id);
                    rowNode.children("#action_child").children(".remove-payment-label").attr("for", "remove"+data[i].order_id);
                    //rowNode.children("._quantity").children("input").val();
                    // if (data[i].item_pdf == 0) rowNode.children("._buttons").children(".pdf").hide();
                    // if (data[i].image == 0) rowNode.children("._buttons").children(".image").hide();
                    $("#order-body").append(rowNode);

                }
                $(".mdl-progress").hide();
                showSnackbar("Data loaded.");
            },
            error: function () {
                $(".mdl-progress").hide();
                showSnackbar("Can't complete your request.");
            }
        });
    }
    function calculateTotal(){
        total_amount = purchase_amount - return_amount;
    }
    
    $(document).ready(function () {
            $("#refresh-data-btn").click(loadItems);
            tableheader.children(".sortable").click(tableSort);
            initItemFilterSelectBox();
            loadVendorsSelectBox();
            loadRefSelectBox();
            loadItems();
            calculateTotal();
            $("#filter-box").change(filterResults);
            $(".total_amount").html("&euro; "+total_amount);
            $(".purchase_amount").html("&euro; "+purchase_amount);

    });
