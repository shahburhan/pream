<?php

class ControllerPayment extends Controller
{
    public function addToPayment()
    {
        if ($_POST) {
            $orders = [];
            $orders = $_POST['orders'];
            if (!empty($orders)) {
                $this->load->model('payment');
                foreach ($orders as $order) {
                    $order = (int) $order;
                    $this->model_payment->addItemToPaymentQueue($order);
                }
                echo json_encode(['msg'=>'success']);
            } else {
                trigger_error('Error Fetching Orders');
            }
        }
    }

    public function updatePaymentCount()
    {
        $this->load->model('payment');
        echo json_encode(['count'=>$this->model_payment->ordersInQueue()]);
    }

    public function make()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }

        $this->load->model('payment');

        if ($_POST) {
            if (isset($_GET['vendor_id']) && $_GET['vendor_id'] > 0) {
                $data['party'] = (int) $_GET['vendor_id'];
                $data['date'] = $_POST['payment_date'];
                $data['payment'] = (int) $_POST['payment_mode'];
                $data['cheque_no'] = (int) $_POST['pay_no'];
                $data['amount'] = $_POST['amount'];

                if ($data['payment'] == 2 || $data['payment'] == 3) {
                    if ($cheque_no = '') {
                        die('No cheque no provided');
                    }
                }

                $this->session->data['status'] = 'Payment Successfully made';

                $this->model_payment->confirmPayment($data, 0);
            } elseif (isset($_GET['client_id']) && $_GET['client_id'] > 0) {
                $data['party'] = (int) $_GET['client_id'];
                $data['date'] = $_POST['payment_date'];
                $data['payment'] = (int) $_POST['payment_mode'];
                $data['cheque_no'] = (int) $_POST['pay_no'];
                $data['amount'] = $_POST['amount'];

                if ($data['payment'] == 2 || $data['payment'] == 3) {
                    if ($cheque_no = '') {
                        die('No cheque number provided');
                    }
                }

                $this->session->data['status'] = 'Payment Successfully made';

                $this->model_payment->confirmPayment($data, 1);
            }
            header('location:?route=purchaseOrder/vendorPayment');
        }
        $data = [];
        $header['page_title'] = 'Payment Confirmation';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Vendor Payment', 'href' => '?route=purchaseOrder/vendorPayment'];
        $breadcrumb[] = ['title'=>'Make', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;

        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchaseOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $data['ttc'] = 0;
        $data['ht'] = 0;
        $data['returnAmount'] = 0;
        $data['returnF'] = 0;
        $data['payment_modes'] = $this->model_payment->getPaymentModes();

        if (isset($_GET['vendor_id'])) {
            $vendor_id = (int) $_GET['vendor_id'];
            $this->load->model('payment');
            $data['ht'] = $this->model_payment->getPaymentAmount(true, $vendor_id);
            $ttc = ($data['ht'] * 20) / 100;
            $data['ttc'] = $data['ht'] + $ttc;
            $data['vendor_details'] = $this->model_payment->getVendorDetails($vendor_id);
            $data['returnAmount'] = $this->model_payment->getReturnAmount($vendor_id, 0);
        }
        $this->load->view('viewPQO', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrdersInQueue()
    {
        if ($_POST) {
            if (isset($_POST['all']) && $_POST['all']) {
                $this->load->model('payment');
                $client = $this->model_payment->getClientAddress();

                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }
                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';

                $order_list = $this->model_payment->getOrdersInQueue(20, true);
                echo json_encode(['order_list'=>$order_list, 'client'=>$data]);
            }
        }
    }

    public function removeFromPayment()
    {
        $id = (isset($_GET['order_id'])) ? (int) $_GET['order_id'] : 0;
        $this->load->model('payment');
        if ($this->model_payment->removeFromPayment($id)) {
            echo json_encode(['msg'=>'success']);
        } else {
            echo 'failed';
        }
    }

    public function confirmPayment()
    {
        if ($_POST) {
            $payment_date = $_POST['payment_date'];
            $this->load->model('payment');
            $this->model_payment->confirmPayment($payment_date);

            $this->load->controller('activity');
            $this->controller_activity->logActivity(27, $id, $this->session->loggedUser(), true);
        } else {
            echo json_encode(['error'=>'Invalid Access method']);
        }
    }

    public function returnAmount()
    {
        if (isset($_GET['vendor_id'])) {
            $vendor_id = (int) $_GET['vendor_id'];
            $this->load->model('payment');
            $returnAmount = $this->model_payment->getReturnAmount($vendor_id, 0);

            return $returnAmount;
        }
    }
}
