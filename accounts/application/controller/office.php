<?php

class ControllerOffice extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }

        $data = [];

        $this->load->view('404', $data);
    }

    public function expenses()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }

        $data = [];
        $header['page_title'] = 'Office Expenses';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Report', 'href' => '?route=report/vendor'];
        $breadcrumb[] = ['title'=>'Expense', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->load->text('office');
        $this->load->model('office');
        $data['expense_list'] = $this->model_office->getExpenses();
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('expenses', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
