<?php

class ControllerPurchaseOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $this->load->view('404', $data);
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Purchase Orders';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Orders', 'href' => '?route=purchaseOrder/view'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchaseOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('purchaseOrder');

        $this->load->model('payment');

        $orders = $this->model_purchaseOrder->getPurchaseOrderList();

        $total_count = $this->model_purchaseOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/view&page={page}';
        $data['pagination'] = $this->pagination->render();
        $amt = $this->model_payment->getPaymentAmount();
        $data['ttc'] = ($amt != '' && $amt != 0) ? $amt : 0;
        $data['orders'] = $orders;
        $this->load->view('viewPo', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id']) && isset($_GET['bol_number'])) {
                $response = [];
                $order_id = (int) $_GET['order_id'];
                $bol_number = (int) $_GET['bol_number'];
                $this->load->model('purchaseOrder');
                $purchase_order = $this->model_purchaseOrder->getOrderDetails($order_id);
                $order_items = $this->model_purchaseOrder->getOrderItemsList($order_id);
                $bol_details = $this->model_purchaseOrder->getBolDetails($bol_number);
                $client = $this->model_purchaseOrder->getClientAddress();

                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';

                $payment_modes = $this->model_purchaseOrder->getPaymentModes();
                $pm_mapped = [];
                foreach ($payment_modes as $payment_mode) {
                    $pm_mapped[$payment_mode['payment_mode_id']] = $payment_mode['title'];
                }

                $response['delivery_address'] = $data;
                $response['purchase_order'] = $purchase_order;
                $response['order_items'] = $order_items;
                $response['payment_mode'] = $pm_mapped;
                $response['bol_details'] = $bol_details;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function vendors()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Vendors';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Orders', 'href' => '?route=purchaseOrder/view'];
        $breadcrumb[] = ['title'=>'Vendors', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchaseOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('purchaseOrder');
        $vendors = $this->model_purchaseOrder->getVendorList();

        $data['vendors'] = $vendors;
        $this->load->view('vendors', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function vendorPayment()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Vendor Payment';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Orders', 'href' => '?route=purchaseOrder/view'];
        $breadcrumb[] = ['title'=>'Vendor Payment', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchaseOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('purchaseOrder');
        $vendors = $this->model_purchaseOrder->getVendorList();

        $data['vendors'] = $vendors;
        $this->load->view('vendorPayment', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function RoVendor()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'RO To Vendor';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewClient'];
        $breadcrumb[] = ['title'=>'RO To Vendor', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchaseOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('purchaseOrder');
        $return_orders = $this->model_purchaseOrder->getReturnPurchaseOrder();
        $order_status = $this->model_purchaseOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $data['return_orders'] = $return_orders;
        $data['order_status'] = $order_status_mapped;

        $this->load->view('RO_to_vendor', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getVendorDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['vendor_id'])) {
                $response = [];
                $vendor_id = $_GET['vendor_id'];
                $this->load->model('purchaseOrder');
                $vendor_details = $this->model_purchaseOrder->getVendorDetails($vendor_id);
                $purchase_order = $this->model_purchaseOrder->getPurchaseOrder($vendor_id);
                $return_order = $this->model_purchaseOrder->getReturnOrderDetails($vendor_id);
                //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);

                $response['vendor_details'] = $vendor_details;
                $response['purchase_order'] = $purchase_order;
                $response['return_order'] = $return_order;
                //$response['raised_by'] = $created_by;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getVendorsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $vendorsList = $this->model_purchaseOrder->getVendorList();
                echo json_encode(['status'             => 'success',
                                        'vendors_list' => $vendorsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getVendorBol()
    {
        if (isset($_POST['vendor']) && $_POST['vendor']) {
            $bol_number = '';
            if (isset($_POST['filterBol'])) {
                $bol_number = $_POST['filterBol'];
            }
            $vendor_id = (int) $_POST['vendor'];
            $this->load->model('purchaseOrder');
            $clause = $this->buildClause();
            $list = $this->model_purchaseOrder->getVendorBolList($vendor_id, $bol_number);
            $return = $this->model_purchaseOrder->getVendorReturnList($vendor_id);
            $returnQueued = $this->model_purchaseOrder->getFromReturnQueue($vendor_id);
            $order_queue = $this->model_purchaseOrder->getOrdersInQueue($vendor_id);
            $rq = [];
            $o_q = [];
            foreach ($returnQueued as $queued) {
                $rq[$queued['item_id']] = $queued['item_id'];
            }
            foreach ($order_queue as $oq) {
                $o_q[$oq['bol_number']] = $oq['bol_number'];
            }
            $total_count = count($list);
            echo json_encode(['list'=>$list, 'total_count'=>$total_count, 'return'=>$return, 'queued'=>$rq, 'b_queued'=>$o_q]);
        } else {
            trigger_error('Invalid Parameters');
        }
    }

    public function getVendorReturn()
    {
        if (isset($_GET['vendor']) && $_GET['vendor']) {
            $vendor_id = (int) $_GET['vendor'];
            $this->load->model('purchaseOrder');
            $list = $this->model_purchaseOrder->getVendorReturnList($vendor_id);
            echo json_encode(['list'=>$list]);
        }
    }

    public function getBolInfo()
    {
        if (isset($_GET)) {
            if (isset($_GET['bol_number']) && $_GET['bol_number'] > 0) {
                $bol_number = (int) $_GET['bol_number'];
                $this->load->model('purchaseOrder');
                $bol_items = $this->model_purchaseOrder->getBolItems($bol_number);
                $order_details = $this->model_purchaseOrder->getOrderDetails($bol_items[0]['order_id']);

                $client = $this->model_purchaseOrder->getClientAddress();

                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';
                echo json_encode(['bol_items'=>$bol_items, 'order_details'=>$order_details, 'delivery'=>$data]);
            }
        }
    }

    public function getReturnOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('purchaseOrder');
                //$sales_order = $this->model_requestsAction->getReturnOrderDetails($order_id);
                $order_items = $this->model_purchaseOrder->getOrderItemsList($order_id);
                $purchase_order = $this->model_purchaseOrder->getPurchaseReturnOrderDetails($order_id);
                $reasons = $this->model_purchaseOrder->getReason();

                foreach ($reasons as $reason) {
                    $data[$reason['return_reason_id']] = $reason['title'];
                }

                //$response["sales_order"] = $sales_order;
                $response['order_items'] = $order_items;
                $response['purchase_order'] = $purchase_order;
                $response['reason'] = $data;
                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getPurchaseOrderList()
    {
        if ($_POST) {
            $clause = trim($this->buildClause());

            $this->load->model('purchaseOrder');
            $client = $this->model_purchaseOrder->getClientAddress();

            foreach ($client as $address) {
                $data[$address['client_id']]['address'] = $address['address'];
                $data[$address['client_id']]['city'] = $address['city'];
                $data[$address['client_id']]['postal'] = $address['postal_code'];
                $data[$address['client_id']]['name'] = $address['company_name'];
            }
            $data[0]['address'] = $this->config['store_address'];
            $data[0]['city'] = '';
            $data[0]['postal'] = '';
            $data[0]['name'] = 'Store';
            $orders = $this->model_purchaseOrder->getPurchaseOrderList(0, 10, $clause);

            echo json_encode(['order_list'=>$orders, 'clause'=>$clause, 'client'=>$data]);
        }
    }

    public function getReturnOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('purchaseOrder');
            $returnOrders = $this->model_purchaseOrder->getReturnPurchaseOrder(0, 10, $clause);
            echo json_encode(['return_list'=>$returnOrders, 'clause'=>$clause]);
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['orderIDFilterList'])) {
            $c = '';
            $fieldName = 'bol_number';
            $filter = $_POST['orderIDFilterList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $vendor = (int) $_GET['vendor_id'];
                $brand = $_GET['brand_name'];
                $categoryList = $this->model_purchaseOrder->getCategoryList($vendor, $brand);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $areaList = $this->model_purchaseOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $com = false;
            $can = false;
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                if (isset($_GET['ot'])) {
                    if ($_GET['ot'] == 1) {
                        $com = true;
                    }
                    if ($_GET['ot'] == 2) {
                        $can = true;
                    }
                }
                $vendor = (isset($_GET['vendor_id'])) ? (int) $_GET['vendor_id'] : 0;
                $brand_name = (isset($_GET['brand_name'])) ? $_GET['brand_name'] : '';
                $category = (isset($_GET['category'])) ? $_GET['category'] : '';
                $this->load->model('purchaseOrder');
                $refList = $this->model_purchaseOrder->getRefList($ref, $vendor, $brand_name, $category, $com, $can);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getBolList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $bolList = $this->model_purchaseOrder->getBolList();
                echo json_encode(['status'         => 'success',
                                        'bol_list' => $bolList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getBolDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $bolList = $this->model_purchaseOrder->getBolList();
                echo json_encode(['status'         => 'success',
                                        'bol_list' => $bolList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function blackList()
    {
        $vendor_id = (int) $_GET['vendor_id'];
        if ($vendor_id) {
            $this->load->model('purchaseOrder');

            return $this->model_purchaseOrder->createBlackList($vendor_id);
        }
    }

    public function addReturnToQueue()
    {
        if ($_POST) {
            $return_id = (int) $_POST['return_id'];
            $vendor_id = (int) $_POST['vendor_id'];
            $return_type = 0;

            if ($return_id && $vendor_id) {
                $this->load->model('purchaseOrder');
                $this->model_purchaseOrder->addReturnToQueue($return_id, $vendor_id, $return_type);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function removeFromReturnQueue()
    {
        if ($_POST) {
            $return_id = (int) $_POST['return_id'];
            $vendor_id = (int) $_POST['vendor_id'];

            if ($return_id && $vendor_id) {
                $this->load->model('purchaseOrder');
                $this->model_purchaseOrder->removeFromReturnQueue($return_id, $vendor_id);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function addBolToPayment()
    {
        if ($_POST) {
            $bol = (int) $_POST['bol_number'];
            $vendor_id = (int) $_POST['vendor_id'];

            if ($bol && $vendor_id) {
                $this->load->model('payment');
                $this->model_payment->addBolToPaymentQueue($bol, $vendor_id);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function removeFromPayment()
    {
        if ($_POST) {
            $bol = (int) $_POST['bol_number'];
            $vendor_id = (int) $_POST['vendor_id'];

            if ($bol && $vendor_id) {
                $this->load->model('payment');
                $this->model_payment->removeFromPayment($bol, $vendor_id);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['vendor_id']) && $_GET['vendor_id']) {
                $vendor_id = (int) $_GET['vendor_id'];
            }
            if (isset($_GET['all']) && $vendor_id) {
                $this->load->model('purchaseOrder');
                $brandList = $this->model_purchaseOrder->getBrandList($vendor_id);
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
