<?php

class ControllerBalanceSheet extends Controller
{
    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Balance Sheet';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Balance Sheet', 'href' => '?route=balanceSheet/view'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('balanceSheet');
        $orders = $this->model_balanceSheet->getOrders();
        $sales_total = $this->model_balanceSheet->getSalesTotal();
        $purchase_total = $this->model_balanceSheet->getPurchaseTotal();

        $total_count = $this->model_balanceSheet->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=balanceSheet/view&page={page}';
        $data['pagination'] = $this->pagination->render();
        $data['orders'] = $orders;
        $data['sales_total'] = $sales_total;
        $data['purchase_total'] = $purchase_total;

        $this->load->view('balanceSheet', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
