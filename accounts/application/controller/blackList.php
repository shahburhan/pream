<?php

class ControllerBlackList extends Controller
{
    public function getBlacklist()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Clients BlackList';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'BlackList', 'href' => '?route=blackList/getBlacklist'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('blackList');

        $clients = $this->model_blackList->getClientList();

        $data['clients'] = $clients;

        $this->load->view('black_list', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function activeOrder()
    {
        $id = (int) $_GET['id'];
        if ($id) {
            $this->load->model('blackList');
            $active_order = $this->model_blackList->activeOrder($id);
            // $this->load->controller("activity");
            // $this->controller_activity->logActivity(37,$cancel_order,$this->session->loggedUser(),true);
            $data['active_order'] = $active_order;
        }
    }
}
