<?php

class ControllerSalesOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];

        $this->load->view('404', $data);
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Sales Order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Order', 'href' => '?route=salesOrder/view'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $sales_orders = $this->model_salesOrder->getSalesOrderList();
        $data['clients'] = $this->model_salesOrder->getClientList();
        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/view&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $this->load->view('salesOrders', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $sales_order = $this->model_salesOrder->getOrderDetails($order_id);
                $order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                $client = $this->model_salesOrder->getClientAddress();
                //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);
                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';

                $payment_modes = $this->model_salesOrder->getPaymentModes();
                $pm_mapped = [];
                foreach ($payment_modes as $payment_mode) {
                    $pm_mapped[$payment_mode['payment_mode_id']] = $payment_mode['title'];
                }

                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;
                $response['delivery_address'] = $data;
                $response['payment_mode'] = $pm_mapped;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function clients()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Clients';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Order', 'href' => '?route=salesOrder/view'];
        $breadcrumb[] = ['title'=>'Clients', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('salesOrder');
        $clients = $this->model_salesOrder->getClientList();

        $data['clients'] = $clients;

        $this->load->view('clients', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getClientDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $response = [];
                $client_id = $_GET['client_id'];
                $this->load->model('salesOrder');
                $client_details = $this->model_salesOrder->getClientDetails($client_id);
                $sales_order = $this->model_salesOrder->getSalesOrder($client_id);
                $return_order = $this->model_salesOrder->getReturnOrderDetails($client_id);
                $client_details['assigned_agent'] = $this->model_salesOrder->getAgentName($client_details['assigned_agent']);
                //$order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);

                $response['client_details'] = $client_details;
                $response['sales_order'] = $sales_order;
                $response['return_order'] = $return_order;
                //$response['raised_by'] = $created_by;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function RoClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'RO To Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewClient'];
        $breadcrumb[] = ['title'=>'RO From Client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $sales_orders = $this->model_salesOrder->getReturnSalesOrder();
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/RoClient&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;

        $this->load->view('RO_from_client', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getReturnOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $sales_order = $this->model_salesOrder->getReturnOrderList($order_id);
                $order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                $reasons = $this->model_salesOrder->getReason();

                foreach ($reasons as $reason) {
                    $data[$reason['return_reason_id']] = $reason['title'];
                }

                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;
                $response['reason'] = $data;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];

                $clause = $this->buildClause();

                $this->load->model('salesOrder');
                $orders = $this->model_salesOrder->getSalesOrder($client_id);

                echo json_encode(['order_list'=>$orders, 'clause'=>$clause]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getReturnOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('salesOrder');
            $returnOrders = $this->model_salesOrder->getReturnSalesOrder(0, 10, $clause);
            echo json_encode(['return_list'=>$returnOrders, 'clause'=>$clause]);
        }
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('salesOrder');
                $item_details = $this->model_salesOrder->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $brand_name = $_GET['brand_name'];
                $this->load->model('salesOrder');
                $categoryList = $this->model_salesOrder->getCategoryList($brand_name);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderItemsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $itemsList = $this->model_salesOrder->getOrderItemsList($order_id);
                echo json_encode(['status' => 'success',
                    'items_list'           => $itemsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $areaList = $this->model_salesOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $clientClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['clientList'])) {
            $c = '';
            $fieldName = 'client';
            $filter = $_POST['clientList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$itemClause;
                } else {
                    $whereClause = $whereClause.' AND '.$itemClause;
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getClientsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $clientsList = $this->model_salesOrder->getClientList();
                echo json_encode(['status' => 'success',
                    'client_list'          => $clientsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getClientsAddress()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('salesOrder');
                $clientsAddress = $this->model_salesOrder->getClientsAddress($client_id);
                echo json_encode(['status' => 'success',
                    'client_address'       => $clientsAddress, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('salesOrder');
                $clause = '';

                $clause = $this->buildClause();
                $item_List = [];
                $item_List = $this->model_salesOrder->getOrderItemsList($clause);
                /*if(isset($_POST['with_client'])){
                    $item_List = $this->model_salesOrder->getAllItemsListWithClient($clause);
                }else{

                }*/

                foreach ($item_List as $key => $item) {
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                $response_data = ['status' => 'success',
                    'itemList'             => $item_List,
                    'clause'               => $clause, ];

                if (isset($_POST['client_id'])) {
                    $client = $this->model_salesOrder->getClientDetails($_POST['client_id']);
                    $type = $this->model_salesOrder->getClientType($client['client_type']);
                    $client['client_type'] = $type['title'];
                    $response_data['client'] = $client;
                }

                echo json_encode($response_data);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function placeOrder()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $data = [];
                $this->load->model('salesOrder');
                $items = $_POST['items'];
                $sell_rate = $_POST['sell_rates'];
                $quantity = $_POST['quantity'];
                $mrp = $_POST['mrp'];
                $htt = 0;
                $ttc = 0;

                for ($i = 0; $i < count($items); $i++) {
                    $p = $sell_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    $htt = $htt + (($m - ($m * $p) / 100) * $q);
                }

                $ttc = $htt + (($htt * TAX) / 100);
                $_POST['ttc'] = $ttc;
                $_POST['htt'] = $htt;

                $data['htt'] = $htt;
                $data['tva'] = TAX;
                $data['ttc'] = $ttc;
                $data['amount_paid'] = 0;
                $data['order_date'] = date('Y-m-d', time());
                $data['payment_deadline'] = date('Y-m-d', strtotime($data['order_date'].' + 45 days'));
                $data['order_status'] = 2;
                $data['order_type'] = 2;
                $data['client_id'] = $_POST['client'];
                $data['expected_date'] = $_POST['expected_date'];
                $data['delivery_address'] = $_POST['delivery_address'];
                $data['city'] = $_POST['city'];
                $data['raised_by'] = $this->session->data['logged_user_id'];
                $order_id = $this->model_salesOrder->newOrder($data);

                for ($i = 0; $i < count($items); $i++) {
                    $orderData = [];
                    $it = $items[$i];
                    $p = $sell_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    $orderData['order_id'] = $order_id;
                    $orderData['item_id'] = $it;
                    $orderData['quantity'] = $q;
                    $orderData['discount'] = 0;
                    $orderData['rate'] = $p;
                    $orderData['mrp'] = $m;
                    $orderData['order_type'] = $data['order_type'];
                    $this->model_salesOrder->addOrderItems($orderData);
                }

                echo json_encode(['status' => 'success',
                    'order_id'             => $order_id, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function cancelOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('salesOrder');

            return $this->model_salesOrder->cancelOrder($order_id);
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $brand = $_GET['brand_name'];
                $category = $_GET['category'];
                $this->load->model('salesOrder');
                $refList = $this->model_salesOrder->getRefList($ref, $brand, $category);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function blackList()
    {
        $client_id = (int) $_GET['client_id'];
        if ($client_id) {
            $this->load->model('salesOrder');

            return $this->model_salesOrder->createBlackList($client_id);
        }
    }

    public function clientPayment()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Client Payment';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Orders', 'href' => '?route=salesOrder/view'];
        $breadcrumb[] = ['title'=>'Client Payment', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('salesOrder');
        $clients = $this->model_salesOrder->getClientList();

        $data['clients'] = $clients;
        $this->load->view('clientPayment', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function activeList()
    {
        $client_id = (int) $_GET['client_id'];
        if ($client_id) {
            $this->load->model('salesOrder');

            return $this->model_salesOrder->createActiveList($client_id);
        }
    }

    public function getClientBol()
    {
        if (isset($_POST['client']) && $_POST['client']) {
            $bol_number = '';
            if (isset($_POST['filterBol'])) {
                $bol_number = $_POST['filterBol'];
            }
            $client_id = (int) $_POST['client'];
            $this->load->model('salesOrder');
            // $clause = $this->buildClause();
            $list = $this->model_salesOrder->getClientBolList($client_id, $bol_number);
            $return = $this->model_salesOrder->getClientReturnList($client_id);
            $returnQueued = $this->model_salesOrder->getFromReturnQueue($client_id);
            $order_queue = $this->model_salesOrder->getOrdersInQueue($client_id);
            $rq = [];
            $o_q = [];
            foreach ($returnQueued as $queued) {
                $rq[$queued['item_id']] = $queued['item_id'];
            }
            foreach ($order_queue as $oq) {
                $o_q[$oq['order_id']] = $oq['order_id'];
            }
            $total_count = count($list);
            echo json_encode(['list'=>$list, 'total_count'=>$total_count, 'return'=>$return, 'queued'=>$rq, 'b_queued'=>$o_q]);
        } else {
            trigger_error('Invalid Parameters');
        }
    }

    public function addReturnToQueue()
    {
        if ($_POST) {
            $return_id = (int) $_POST['return_id'];
            $client_id = (int) $_POST['client_id'];
            $return_type = 1;

            if ($return_id && $client_id) {
                $this->load->model('salesOrder');
                $this->model_salesOrder->addReturnToQueue($return_id, $client_id, $return_type);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function removeFromReturnQueue()
    {
        if ($_POST) {
            $return_id = (int) $_POST['return_id'];
            $client_id = (int) $_POST['client_id'];

            if ($return_id && $client_id) {
                $this->load->model('salesOrder');
                $this->model_salesOrder->removeFromReturnQueue($return_id, $client_id);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function addBolToPayment()
    {
        if ($_POST) {
            $bol = (int) $_POST['bol_number'];
            $client_id = (int) $_POST['client_id'];

            if ($bol && $client_id) {
                $this->load->model('payment');
                $this->model_payment->addBolToPaymentQueue($bol, $client_id, 1);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function removeFromPayment()
    {
        if ($_POST) {
            $bol = (int) $_POST['bol_number'];
            $client_id = (int) $_POST['client_id'];

            if ($bol && $client_id) {
                $this->load->model('payment');
                $this->model_payment->removeFromPayment($bol, $client_id, 1);
            }
            echo json_encode(['status'=>true]);
        } else {
            echo 'Invalid Request';
        }
    }

    public function getBolInfo()
    {
        if (isset($_GET)) {
            if (isset($_GET['bol_number']) && $_GET['bol_number'] > 0) {
                $bol_number = (int) $_GET['bol_number'];
                $this->load->model('salesOrder');
                $bol_items = $this->model_salesOrder->getBolItems($bol_number);
                $order_details = $this->model_salesOrder->getOrderDetails($bol_items[0]['order_id']);

                $client = $this->model_salesOrder->getClientAddress();

                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';
                echo json_encode(['bol_items'=>$bol_items, 'order_details'=>$order_details, 'delivery'=>$data]);
            }
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['char']) && strlen($_GET['char']) > 2) {
                $string = htmlentities($_GET['char']);
            }
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $brandList = $this->model_salesOrder->getBrandList($string);
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
