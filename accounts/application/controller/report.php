<?php

class ControllerReport extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];

        $this->load->view('404', $data);
    }

    public function vendor()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Reports - Vendor';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Report', 'href' => '?route=report/vendor'];
        $breadcrumb[] = ['title'=>'Vendor', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        //$data['script'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min";
        $this->load->controller('header');
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';
        $this->controller_header->load($header);

        $this->load->text('reports');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('reports', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function client()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Reports - Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Report', 'href' => '?route=report/client'];
        $breadcrumb[] = ['title'=>'Client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        // $header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        // $data['script'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min";
        $this->load->controller('header');
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';
        $this->controller_header->load($header);
        $this->load->text('reports');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('reports', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
