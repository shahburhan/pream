<?php
/**
 * Created by PhpStorm.
 * User: Manveer
 * Date: 10-12-2017
 * Time: 01:41 PM.
 */
class ModelMessage extends Model
{
    public function getUsers()
    {
        $result = $this->db->query('SELECT user.user_id, user.username, user_type.title  FROM user LEFT JOIN user_type ON (user.user_type  =  user_type.user_type_id)');

        return $result->rows;
    }

    public function createMessage($data = [], $files = [])
    {
        foreach ($data as $k => $d) {
            $data[$k] = $this->db->escape($d);
        }
        $date = date('Y-m-d H:i:s', time());
        $status = (int) 0;
        $attachments = json_encode($files);
        $this->db->query('INSERT INTO message SET  
                `subject` = '."'".$data['subject']." '"
            .',`body` = '."'".$data['body']."'"
            .',`sender` = '.$data['sender']
            .',`receiver` = '.$data['receiver']
            .',`status` = '.$status
            .',`attachment` = '."'".$attachments."'"
            .',`sent_date` = '."'".$date."'");

        $id = $this->db->getLastId();

        return $id;
    }

    public function makeMessageSeen($message_id)
    {
        $this->db->query('UPDATE message set message.status = 1 WHERE message_id = '.$message_id);
    }

    public function getMessages($user_id)
    {
        $result = $this->db->query('SELECT message.subject, u.username, u.title, message.status, message.message_id, message.sent_date FROM message LEFT JOIN (SELECT user.*, user_type.title FROM user LEFT JOIN user_type ON user.user_type = user_type.user_type_id) u ON u.user_id = message.sender WHERE message.receiver = '.$user_id.' ORDER BY message.sent_date DESC');

        return $result->rows;
    }

    public function getSentMessages($user_id)
    {
        $result = $this->db->query('SELECT message.subject, user.username, message.status, message.message_id, message.sent_date FROM message LEFT JOIN user ON user.user_id = message.receiver WHERE message.sender = '.$user_id.' ORDER BY message.sent_date DESC ');

        return $result->rows;
    }

    public function getMessageDetails($message_id)
    {
        $result = $this->db->query('SELECT * FROM message LEFT JOIN user ON user.user_id = message.sender WHERE message.message_id= '.$message_id);

        return $result->row;
    }
}
