<?php
/**
 * Created by PhpStorm.
 * User: Manveer
 * Date: 10-12-2017
 * Time: 01:41 PM.
 */
class ModelAccount extends Model
{
    public function getNotifications($user_id)
    {
        $result = $this->db->query('SELECT notification.*, activity.activity_object FROM notification LEFT JOIN activity ON  notification.activity_id = activity.activity_id WHERE notification.status=0 AND notification_for = '.$user_id.' ORDER BY notification.created_on DESC LIMIT 3');
        $this->updateStatusForLastThree($user_id);

        return $result->rows;
    }

    public function getNotificationCount($user_id)
    {
        $result = $this->db->query('SELECT COUNT(notification_id) as total FROM notification WHERE status=0 AND notification_for = '.$user_id.' ORDER BY created_on DESC');

        return $result->row['total'];
    }

    public function login($username, $password)
    {
        $allowed_user_types = $this->session->allowed_user_types;
        $user_types_check = '';
        if (isset($allowed_user_types) && count($allowed_user_types) > 0) {
            $user_types_check = 'AND ( user_type = ';
            foreach ($allowed_user_types as $key => $type) {
                $user_types_check = $user_types_check.' '.$type;
                if ($key != (count($allowed_user_types) - 1)) {
                    $user_types_check = $user_types_check.' OR ';
                }
            }
            $user_types_check = $user_types_check.' )';
        }

        $result = $this->db->query("SELECT user_id, user_type FROM user WHERE (email_id = '".$this->db->escape($username)."' OR username = '".$this->db->escape($username)."')  AND password = '".$this->db->escape($password)."' ".$user_types_check);
        if ($result->num_rows == 1) {
            $this->session->login($result->row['user_id'], $result->row['user_type']);
            $this->update_login();

            return true;
        } else {
            return false;
        }
    }

    private function update_login()
    {
        $lastLogin = date('Y-m-d h:i:s', time());
        $last_ip = $_SERVER['REMOTE_ADDR'];
        $this->db->query("UPDATE `user` SET last_login = '".$this->db->escape($lastLogin)."', last_ip = '".$this->db->escape($last_ip)."' WHERE user_id = '".$this->session->data['logged_user_id']."'");
    }

    public function updateStatusForLastThree($user_id)
    {
        $this->db->query('UPDATE notification SET status = 1 WHERE status=0 AND notification_for = '.$user_id.' ORDER BY created_on DESC LIMIT 3');
    }
}
