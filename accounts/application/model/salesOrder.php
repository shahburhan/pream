<?php
 class ModelsalesOrder extends Model
 {
     public function getSalesOrderList($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 && order_status != 9 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 2 && order_status != 9 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date
        FROM `order` LEFT JOIN (SELECT user.name,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function getPaymentModes()
     {
         $result = $this->db->query("SELECT * FROM payment_mode WHERE lang = '".$this->session->language."'");

         return $result->rows;
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function getClientAddress()
     {
         $result = $this->db->query('SELECT  c.company_name, c.client_id, u.address, u.city, u.postal_code FROM client c LEFT JOIN user u ON c.user_id = u.user_id ');

         return $result->rows;
     }

     public function getUserDetails($user_id)
     {
         $result = $this->db->query('SELECT * FROM user LEFT JOIN user_type ON user.user_type = user_type.user_type_id WHERE user_id = '.$user_id);

         return $result->row;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT item.image, item.item_pdf, item.item_name, item.reference, item.item_id  FROM item WHERE item.item_id = '.$item_id);

         return $result->row;
     }

     public function getClientList()
     {
         $result = $this->db->query('SELECT c.*, u.*, c.company_name as name FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id)');

         return $result->rows;
     }

     public function getClientDetails($client_id)
     {
         $result = $this->db->query('SELECT * FROM client LEFT JOIN user ON user.user_id = client.user_id WHERE client.client_id= '.$client_id);

         return $result->row;
     }

     public function getSalesOrder($client_id)
     {
         $result = $this->db->query('SELECT * FROM `order` LEFT JOIN client ON `order`.client = client.client_id WHERE client ='.$client_id);

         return $result->rows;
     }

     public function getReturnOrderDetails($client_id)
     {
         $result = $this->db->query('SELECT `order`.*, c.*
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE client_id = '.$client_id);

         return $result->row;
     }

     public function orderCount($status)
     {
         $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

         return $result->num_rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getReturnSalesOrder($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 4 && order_status = 6 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 4 && order_status
            = 6 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getBolItems($bol_number)
     {
         $sql = "SELECT oi.*, quantity_received, date_of_receipt FROM order_bol RIGHT JOIN (SELECT item.*, order_item.rate, order_item.order_id FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oi ON (order_bol.item_id = oi.item_id && oi.order_id = order_bol.order_id) WHERE order_bol.bol_id = $bol_number";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getReturnOrderList($order_id)
     {
         $result = $this->db->query("SELECT `order`.*,  DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.*
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id.' && order_type = 4');

         return $result->row;
     }

     public function getRefList($ref = 0)
     {
         $sql = 'SELECT DISTINCT order_id FROM `order` WHERE order_status = 2';
         if ($ref) {
             $sql = 'SELECT DISTINCT reference FROM `item` ';
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getReason()
     {
         $result = $this->db->query('SELECT * FROM return_reason');

         return $result->rows;
     }

     public function createBlackList($client_id)
     {
         $user_type = 8;
         $user_id = $this->db->escape($client_id);
         $this->db->query("UPDATE user RIGHT JOIN client ON user.user_id = client.user_id SET status = 0 WHERE client.client_id = $user_id");
         $this->db->query("INSERT INTO black_list SET
            user_type = '".$this->db->escape($user_type)."',
            user_id = '".$this->db->escape($user_id)."'
             ");
         $id = $this->db->getLastId();

         return $id;
     }

     public function createActiveList($client_id)
     {
         $user_type = 8;
         $user_id = $this->db->escape($client_id);
         $this->db->query("UPDATE user RIGHT JOIN client ON user.user_id = client.user_id SET status = 1 WHERE client.client_id = $user_id");
         $this->db->query("INSERT INTO black_list SET
            user_type = '".$this->db->escape($user_type)."',
            user_id = '".$this->db->escape($user_id)."'
             ");
         $id = $this->db->getLastId();

         return $id;
     }

     public function getClientsBlacklist()
     {
         $result = $this->db->query('SELECT c.*, u.* FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id) WHERE status = 0');

         return $result->rows;
     }

     public function getClientBolList($client_id, $bol_number)
     {
         $sql = "SELECT bol_number, bol_id,  DATE_FORMAT(order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(date_of_receipt, '%d-%m-%Y') as date_of_receipt , order_id, SUM(ht_total) as ht FROM (SELECT order_date,bol_number, bol_id, quantity_received, oi.order_id, date_of_receipt, SUM(rate*quantity_received) as ht_total FROM order_bol LEFT JOIN (SELECT order_date, rate, item_id, client, order_item.order_id FROM order_item RIGHT JOIN `order` ON order_item.order_id = `order`.order_id) oi ON order_bol.order_id = oi.order_id && order_bol.item_id = oi.item_id WHERE oi.client = $client_id && paid_for = 0 GROUP BY oi.order_id, oi.item_id, bol_number) obi GROUP BY obi.bol_number ORDER BY date_of_receipt DESC";
         if (trim($bol_number) != '') {
             $sql = "SELECT bol_number, bol_id, DATE_FORMAT(order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(date_of_receipt, '%d-%m-%Y') as date_of_receipt , order_id, SUM(ht_total) as ht FROM (SELECT  bol_id, order_date,bol_number, quantity_received, oi.order_id, date_of_receipt, SUM(rate*quantity_received) as ht_total FROM order_bol LEFT JOIN (SELECT order_date, rate, item_id, client, order_item.order_id FROM order_item RIGHT JOIN `order` ON order_item.order_id = `order`.order_id) oi ON order_bol.order_id = oi.order_id && order_bol.item_id = oi.item_id WHERE oi.client = $client_id && paid_for = 0 && bol_id LIKE '%".$bol_number."%' GROUP BY oi.order_id, oi.item_id, bol_number) obi GROUP BY obi.bol_number ORDER BY date_of_receipt DESC";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getClientReturnList($client_id)
     {
         $sql = "SELECT * FROM `order` WHERE order_type = 4 && payment_status = 0 && client = $client_id";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getFromReturnQueue($vendor_id = 0)
     {
         $sql = "SELECT item_id FROM return_queue WHERE concerned_party = $vendor_id && status = 0  && return_type = 1";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function addReturnToQueue($return_id = 0, $vendor_id = 0, $return_type = 1)
     {
         $sql = "INSERT INTO return_queue SET return_type = $return_type, concerned_party = $vendor_id, item_id = $return_id, raised_by = ".$this->session->loggedUser();
         $this->db->query($sql);

         return true;
     }

     public function removeFromReturnQueue($return_id = 0, $vendor_id = 0)
     {
         $sql = "DELETE FROM return_queue WHERE concerned_party = $vendor_id && item_id = $return_id && return_type = 1";
         $this->db->query($sql);

         return true;
     }

     public function getOrdersInQueue($id, $type = 0)
     {
         $sql = 'SELECT order_id FROM payment_queue pqo  WHERE pqo.status = 0 && payment_type = 1';
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getAgentName($agent_id)
     {
         $sql = "SELECT name, agent.user_id FROM user RIGHT JOIN agent ON agent.user_id = user.user_id WHERE employee_id = $agent_id";
         $result = $this->db->query($sql);

         return $result->row;
     }

     public function getCategoryList($brand)
     {
         $result = $this->db->query("SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.brand_name = '".$this->db->escape($brand)."' ORDER BY title");

         return $result->rows;
     }

     public function getBrandList($char = '')
     {
         $sql = "SELECT DISTINCT brand_name FROM `item` WHERE brand_name LIKE '%".$this->db->escape($char)."%'";
         $result = $this->db->query($sql);

         return $result->rows;
     }
 }
