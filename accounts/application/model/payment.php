<?php

class ModelPayment extends Model
{
    public function addItemToPaymentQueue($order_id)
    {
        $this->db->query("INSERT INTO payment_queue SET  order_id = '".$this->db->escape($order_id)."'");
        $this->db->query("UPDATE `order` SET payment_status = 1 WHERE  order_id = '".$this->db->escape($order_id)."'");
    }

    public function addBolToPaymentQueue($order_id, $cp, $type = 0)
    {
        $sql = "INSERT INTO payment_queue SET  bol_number = '".$this->db->escape($order_id)."', concerned_party = $cp, payment_type = 0";
        if ($type) {
            $sql = "INSERT INTO payment_queue SET  order_id = '".$this->db->escape($order_id)."', concerned_party = $cp , payment_type = 1";
        }
        $this->db->query($sql);
    }

    public function getPaymentAmount($bol = true, $party = 0)
    {
        if ($bol) {
            $sql = "SELECT SUM(quantity_received*rate) as ht FROM (SELECT quantity_received, paid_for, rate, item_id, obi.bol_number, obi.order_id FROM `payment_queue` LEFT JOIN (SELECT order_item.*, order_bol.bol_number, order_bol.paid_for, order_bol.quantity_received FROM order_bol LEFT JOIN order_item ON order_bol.order_id = order_item.order_id ) obi ON obi.bol_number = payment_queue.bol_number WHERE concerned_party = $party && paid_for = 0) dat";
        } else {
            $sql = "SELECT SUM(quantity_received*rate) as ht FROM (SELECT quantity_received, rate, item_id, obi.bol_number, obi.order_id FROM `payment_queue` LEFT JOIN (SELECT order_item.*, order_bol.bol_number, order_bol.quantity_received FROM order_bol LEFT JOIN order_item ON order_bol.order_id = order_item.order_id ) obi ON obi.bol_number = payment_queue.bol_number WHERE concerned_party = $party) dat";
        }
        $result = $this->db->query($sql);

        return $result->row['ht'];
    }

    public function ordersInQueue()
    {
        $sql = 'SELECT count(payment_queue_id) as total_orders FROM `payment_queue`  WHERE status = 0';
        $result = $this->db->query($sql);

        return $result->row['total_orders'];
    }

    public function getOrdersInQueue($limit = 20, $showList = false)
    {
        if ($showList) {
            $sql = "SELECT pqo.*, o.order_id, o.ttc, o.htt, o.company_name, o.delivery_address, DATE_FORMAT(o.order_date, '%d-%m-%Y') as order_date FROM payment_queue pqo LEFT JOIN (SELECT company_name, od.* FROM `order` od LEFT JOIN vendor v ON od.vendor = v.vendor_id ) o ON pqo.order_id = o.order_id WHERE pqo.status = 0 ORDER BY pqo.payment_queue_id LIMIT 0, $limit";
        } else {
            $sql = "SELECT payment_queue_id, bol_number FROM payment_queue pqo  WHERE pqo.status = 0 ORDER BY payment_queue_id LIMIT 0, $limit";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function removeFromPayment($id, $cp, $type = 0)
    {
        if ($id) {
            $sql = "DELETE FROM payment_queue WHERE `bol_number` = $id && concerned_party = $cp";
            if ($type == 1) {
                $sql = "DELETE FROM payment_queue WHERE `order_id` = $id && concerned_party = $cp";
            }
            $this->db->query($sql);
            if ($this->db->countAffected()) {
                $this->db->query("UPDATE `order` SET payment_status = 0 WHERE order_id = $id");

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getVendorDetails($vendor_id)
    {
        $result = $this->db->query('SELECT vendor.*, user.name, user.address, user.email_id,user.phone FROM vendor LEFT JOIN user ON  vendor.user_id = user.user_id  WHERE vendor.vendor_id ='.$vendor_id);

        return $result->row;
    }

    public function confirmPayment($data = [], $type = 0)
    {
        $now = date('Y-m-d', time());
        $sql = "INSERT INTO payouts SET data='".$this->db->escape(json_encode($data))."', type=$type, party='".$data['party']."', amount = '".$this->db->escape($data['amount'])."'";
        $this->db->query($sql);
        $sql = 'UPDATE return_queue SET status = 1 WHERE concerned_party = '.$data['party']." && return_type = $type";
        $sql2 = "UPDATE payment_queue SET status = 1, paid = '$now' WHERE concerned_party = ".$data['party']." && payment_type = $type";
        $sql3 = "UPDATE `order` RIGHT JOIN return_queue ON return_queue.item_id = `order`.order_id  SET payment_status = 1  WHERE return_type = $type && concerned_party = ".$data['party'];
        if ($type) {
            $sql4 = "UPDATE order_bol RIGHT JOIN payment_queue ON payment_queue.order_id = order_bol.bol_id SET paid_for = 1 WHERE concerned_party = $party && payment_type = 1";
        } else {
            $sql4 = 'UPDATE order_bol RIGHT JOIN payment_queue ON payment_queue.bol_number = order_bol.bol_number SET paid_for = 1 WHERE concerned_party = '.$data['party'].' && payment_type = 0';
        }

        $this->db->query($sql);
        $this->db->query($sql2);
        $this->db->query($sql3);
        $this->db->query($sql4);
    }

    public function getPaymentModes()
    {
        $result = $this->db->query("SELECT * FROM payment_mode WHERE lang = '".$this->session->language."'");

        return $result->rows;
    }

    public function getReturnAmount($vendor_id, $return_type = 0)
    {
        $sql = "SELECT SUM(ttc) as returnTotal FROM `order` LEFT JOIN `return_queue` ON `order`.order_id = return_queue.item_id WHERE order_type = 3 && return_type = 0 && return_queue.status = 0 && vendor = $vendor_id && payment_status = 0 && concerned_party = $vendor_id";
        if ($return_type == 1) {
            $sql = "SELECT SUM(ttc) as returnTotal FROM `order` LEFT JOIN `return_queue` ON `order`.order_id = return_queue.item_id WHERE order_type = 4 && return_type = 1 && return_queue.status = 0 && client = $vendor_id && payment_status = 0 && concerned_party = $vendor_id";
        }
        $result = $this->db->query($sql);

        return round($result->row['returnTotal'], 3);
    }

    public function getClientAddress()
    {
        $result = $this->db->query('SELECT  c.company_name, c.client_id, u.address, u.city, u.postal_code FROM client c LEFT JOIN user u ON c.user_id = u.user_id ');

        return $result->rows;
    }
}
