<?php

class ModelBalanceSheet extends Model
{
    public function getOrders($start = 0, $limit = 10)
    {
        $result = $this->db->query("SELECT ot.*,  ot.title as order_type,  o.*, DATE_FORMAT(o.order_date, '%d-%m-%Y') as order_date FROM order_type ot RIGHT JOIN `order` o   ON (ot.order_type_id = o.order_type) where (order_type = 1 || order_type = 2) && payment_status != 1 LIMIT $start, $limit ");

        return $result->rows;
    }

    public function getSalesTotal()
    {
        $result = $this->db->query('SELECT order_id, SUM(ttc) FROM `order` WHERE order_type = 2');

        return $result->rows;
    }

    public function getPurchaseTotal()
    {
        $result = $this->db->query('SELECT order_id, SUM(ttc) FROM `order` WHERE order_type = 1');

        return $result->rows;
    }

    public function orderCount($status)
    {
        $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

        return $result->num_rows;
    }
}
