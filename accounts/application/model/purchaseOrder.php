<?php

class ModelPurchaseOrder extends Model
{
    public function getPurchaseOrderList($start = 0, $limit = 10, $clause = '')
    {
        $status = 1;
        $sql = "SELECT DISTINCT(bol_number),   DATE_FORMAT(ob.date_of_receipt, '%d-%m-%Y') as date_of_receipt, paid_for, ov.* FROM order_bol ob LEFT JOIN ( SELECT `order`.*,  DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date_f,  v.company_name, v.vendor_id FROM `order` LEFT JOIN vendor v ON `order`.vendor = v.vendor_id ) ov ON ov.order_id = ob.order_id WHERE order_type = 1 && bol_number > 0  && payment_status = 0  LIMIT $start, $limit ";
        if ($clause != '') {
            $sql = "SELECT DISTINCT(bol_number), DATE_FORMAT(ob.date_of_receipt, '%d-%m-%Y') as date_of_receipt, paid_for, ov.* FROM order_bol ob LEFT JOIN ( SELECT `order`.*,  DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date_f,  v.company_name, v.vendor_id FROM `order` LEFT JOIN vendor v ON `order`.vendor = v.vendor_id ) ov ON ov.order_id = ob.order_id ".$clause." && order_type = 1 && bol_number > 0  && payment_status = 0 LIMIT $start, $limit ";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getOrderItemRate($order_id, $item_id)
    {
        $sql = "SELECT rate FROM order_item WHERE order_id = $order_id && item_id = $item_id LIMIT 1";
        $result = $this->db->query($sql);

        return $result->row['rate'];
    }

    public function getBolItems($bol_number)
    {
        $sql = "SELECT oi.*, quantity_received, date_of_receipt FROM order_bol RIGHT JOIN (SELECT item.*, order_item.rate, order_item.order_id FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oi ON (order_bol.item_id = oi.item_id && oi.order_id = order_bol.order_id) WHERE order_bol.bol_number = $bol_number";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getOrderDetails($order_id)
    {
        $result = $this->db->query('SELECT `order`.*, v.* FROM `order` LEFT JOIN (SELECT user.name, user.address, user.city, user.email_id, user.phone, user.postal_code, vendor.vendor_id, vendor.company_name FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_id = '.$order_id);

        return $result->row;
    }

    public function getReason()
    {
        $result = $this->db->query('SELECT * FROM return_reason');

        return $result->rows;
    }

    public function getVendorList()
    {
        $result = $this->db->query('SELECT v.*, u.* FROM vendor v LEFT JOIN user u  ON (v.user_id = u.user_id)');

        return $result->rows;
    }

    public function getCategoryList($vendor = 0, $brand_name = '')
    {
        $sql = "SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.vendor=$vendor ORDER BY title";
        if ($brand_name != '') {
            $sql = "SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.vendor=$vendor && item.brand_name = '".$this->db->escape($brand_name)."' ORDER BY title";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getVendorDetails($vendor_id)
    {
        $result = $this->db->query('SELECT vendor.*, user.name, user.address, user.email_id,user.phone FROM vendor LEFT JOIN user ON  vendor.user_id = user.user_id  WHERE vendor.vendor_id ='.$vendor_id);

        return $result->row;
    }

    public function getOrderItemsList($order_id)
    {
        $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

        return $result->rows;
    }

    public function getPurchaseOrder($vendor_id)
    {
        $result = $this->db->query('SELECT * FROM `order` WHERE vendor ='.$vendor_id);

        return $result->rows;
    }

    public function getReturnOrderDetails($vendor_id)
    {
        $result = $this->db->query('SELECT `order`.*, v.*
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_status = 11 && vendor_id = '.$vendor_id);

        return $result->rows;
    }

    public function getOrderStatus()
    {
        $result = $this->db->query('SELECT * FROM order_status');

        return $result->rows;
    }

    public function orderCount($status)
    {
        $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

        return $result->num_rows;
    }

    //  public function getRefList($ref=0){
    //     $sql = "SELECT DISTINCT order_id FROM `order` WHERE order_status = 3";
    //     if($ref) $sql = "SELECT DISTINCT reference FROM `item` ";
    //     $result = $this->db->query($sql);
    //     return $result->rows;
    // }
    public function getBolList()
    {
        $sql = 'SELECT DISTINCT bol_number FROM `order_bol` WHERE bol_number > 0';
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getVendorBolList($vendor_id, $bol_number)
    {
        $sql = "SELECT bol_number,  DATE_FORMAT(order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(date_of_receipt, '%d-%m-%Y') as date_of_receipt , order_id, SUM(ht_total) as ht FROM (SELECT order_date,bol_number, quantity_received, oi.order_id, date_of_receipt, SUM(rate*quantity_received) as ht_total FROM order_bol LEFT JOIN (SELECT order_date, rate, item_id, vendor, order_item.order_id FROM order_item RIGHT JOIN `order` ON order_item.order_id = `order`.order_id) oi ON order_bol.order_id = oi.order_id && order_bol.item_id = oi.item_id WHERE oi.vendor = $vendor_id && paid_for = 0 GROUP BY oi.order_id, oi.item_id, bol_number) obi GROUP BY obi.bol_number ORDER BY date_of_receipt DESC";
        if (trim($bol_number) != '') {
            $sql = "SELECT bol_number,  DATE_FORMAT(order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(date_of_receipt, '%d-%m-%Y') as date_of_receipt , order_id, SUM(ht_total) as ht FROM (SELECT order_date,bol_number, quantity_received, oi.order_id, date_of_receipt, SUM(rate*quantity_received) as ht_total FROM order_bol LEFT JOIN (SELECT order_date, rate, item_id, vendor, order_item.order_id FROM order_item RIGHT JOIN `order` ON order_item.order_id = `order`.order_id) oi ON order_bol.order_id = oi.order_id && order_bol.item_id = oi.item_id WHERE oi.vendor = $vendor_id && paid_for = 0 && bol_number LIKE '%".$bol_number."%' GROUP BY oi.order_id, oi.item_id, bol_number) obi GROUP BY obi.bol_number ORDER BY date_of_receipt DESC";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getVendorReturnList($vendor_id)
    {
        $sql = "SELECT * FROM `order` WHERE order_type = 3 && payment_status = 0 && vendor = $vendor_id";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getVendorBolCount($vendor_id, $clause = '')
    {
        $sql = "SELECT COUNT(bol_number) as total FROM (SELECT order_date,bol_number, quantity_received, oi.order_id, date_of_receipt, SUM(rate*quantity_received) as ht_total FROM order_bol LEFT JOIN (SELECT order_date, rate, item_id, vendor, order_item.order_id FROM order_item RIGHT JOIN `order` ON order_item.order_id = `order`.order_id) oi ON order_bol.order_id = oi.order_id && order_bol.item_id = oi.item_id WHERE oi.vendor = $vendor_id && paid_for = 0 GROUP BY oi.order_id, oi.item_id, bol_number) obi GROUP BY obi.bol_number ORDER BY date_of_receipt DESC";
        $result = $this->db->query($sql);

        return $result->row['total'];
    }

    public function getBolDetails($bol_number)
    {
        $sql = "SELECT order_bol.*, item.item_name, item.brand_name, item.reference FROM `order_bol` LEFT JOIN item ON order_bol.item_id = item.item_id WHERE bol_number = $bol_number";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getReturnPurchaseOrder($start = 0, $limit = 10, $clause = '')
    {
        $status = 1;
        $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_type = 3 && order_status = 11 LIMIT $start,$limit";
        if ($clause != '') {
            $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id ".$clause." && order_type = 3 && order_status = 11 LIMIT $start,$limit";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getPaymentModes()
    {
        $result = $this->db->query("SELECT * FROM payment_mode WHERE lang = '".$this->session->language."'");

        return $result->rows;
    }

    public function getPurchaseReturnOrderDetails($order_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.*
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_id = ".$order_id);

        return $result->row;
    }

    public function getClientAddress()
    {
        $result = $this->db->query('SELECT  c.company_name, c.client_id, u.address, u.city, u.postal_code FROM client c LEFT JOIN user u ON c.user_id = u.user_id ');

        return $result->rows;
    }

    public function createBlackList($vendor_id)
    {
        $user_type = 9;
        $user_id = $this->db->escape($vendor_id);
        $this->db->query("UPDATE user RIGHT JOIN vendor ON user.user_id = vendor.user_id SET status = 0 WHERE vendor.vendor_id = $vendor_id");
        $this->db->query("INSERT INTO black_list SET
            user_type = '".$this->db->escape($user_type)."',
            user_id = '".$this->db->escape($user_id)."'
             ");
        $id = $this->db->getLastId();

        return $id;
    }

    public function getFromReturnQueue($vendor_id = 0)
    {
        $sql = "SELECT item_id FROM return_queue WHERE concerned_party = $vendor_id && status = 0 && return_type = 0";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function addReturnToQueue($return_id = 0, $vendor_id = 0, $return_type = 0)
    {
        $sql = "INSERT INTO return_queue SET return_type = $return_type, concerned_party = $vendor_id, item_id = $return_id, raised_by = ".$this->session->loggedUser();
        $this->db->query($sql);

        return true;
    }

    public function removeFromReturnQueue($return_id = 0, $vendor_id = 0)
    {
        $sql = "DELETE FROM return_queue WHERE concerned_party = $vendor_id && item_id = $return_id && return_type = 0";
        $this->db->query($sql);

        return true;
    }

    public function getOrdersInQueue($id, $type = 0)
    {
        $sql = 'SELECT bol_number FROM payment_queue pqo  WHERE pqo.status = 0 && payment_type = 0';
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getRefList($ref = 0, $vendor = 0, $brand_name = '', $category = 0, $com = false, $can = false)
    {
        $status = 1;
        if ($com) {
            $status = 3;
        }
        if ($can) {
            $status = 8;
        }
        if (!$ref) {
            $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && vendor = $vendor &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
            if ($category) {
                $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name, item.item_category FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && vendor = $vendor  && item_category = $category &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
            }
        } else {
            $sql = "SELECT DISTINCT reference FROM `item`  WHERE brand_name = '".$this->db->escape($brand_name)."' && vendor = $vendor";
            if ($category) {
                $sql = "SELECT DISTINCT reference FROM `item` WHERE brand_name = '".$this->db->escape($brand_name)."' && item_category = $category && vendor = $vendor";
            }
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getBrandList($vendor = 0)
    {
        $sql = "SELECT DISTINCT brand_name FROM `item` WHERE vendor = $vendor";
        $result = $this->db->query($sql);

        return $result->rows;
    }
}
