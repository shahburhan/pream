<?php
/**
 * Created by PhpStorm.
 * User: Manveer
 * Date: 18-11-2017
 * Time: 01:58 PM.
 */
class ControllerActivity extends Controller
{
    public function index()
    {
        $data = [];
        $data['page_title'] = 'Purchase Order';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }

    // function test(){
    //     $this->logActivity(8,2,2,true);
    // }

    public function logActivity($activity_type, $object_id, $creator_id, $notify)
    {
        $this->load->model('activity');
        $data = [
            'creator_id'    => $creator_id,
            'activity_type' => $activity_type,
            'object_id'     => $object_id,
        ];
        $id = $this->model_activity->logActivity($data);
        if ($notify) {
            $this->notify($activity_type, $object_id, $id);
        }
    }

    private function notify($activity_type, $object_id, $activity_id)
    {
        $this->load->model('activity');
        $temp = $this->model_activity->getActivityUserTypes(['activity_type'  => $activity_type]);
        $user_types = json_decode($temp['notify_to'], true);   //Getting list of user type to be notified according to activity type

        $user_id_list = [];
        if (count($user_types) > 0) {
            foreach ($user_types as $type) {
                switch ($type) {
                    default:
                        $user_id_list = array_merge($user_id_list, $this->model_activity->getUserIdList($type));
                        break;
                }
            }
            //print_r($user_id_list);
            foreach ($user_id_list as $id) {
                $this->addNotification($activity_type, $object_id, $activity_id, $id['user_id']);
            }
        }
    }

    public function addNotification($activity_type, $object_id, $activity_id, $user_id)
    {
        switch ($activity_type) {
            case 1:

                break;
            default:
                $this->model_activity->createNotification([
                    'activity_id'   => $activity_id,
                    'user_id'       => $user_id,
                    'activity_type' => $activity_type,
                ]);
                break;
        }
    }
}
