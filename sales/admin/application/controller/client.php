<?php

class ControllerClient extends Controller
{
    public $limit = 5;

    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Client';
        $this->load->controller('header');

        $this->controller_header->load($header);
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('client', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function createClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Create Client';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Client', 'href' => '?route=client/createClient'];
        $breadcrumb[] = ['title' => 'Create', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('client');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('client');
        $data['client_types'] = $this->model_client->getClientType();
        if ($_POST) {
            $rules = [
                'company_name'    => 'r',
                'authorized_name' => 'r',
                'company_name'    => 'r',
                'client_type'     => 'r',
                'email'           => 'r',
                'password'        => 'r',
                'address'         => 'r',
                'credit_limit'    => 'r',
                'k_bis'           => 'r',
                'rib'             => 'r',
                'papier_entete'   => 'r',
                'phone'           => 'r',
            ];
            $this->form->process_post($rules);

            if (!$this->model_client->validate_email($this->form->data['email'])) {
                $this->form->error['error_email'] = 'Email already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (!$this->model_client->validate_phone($this->form->data['phone'])) {
                $this->form->error['error_phone'] = 'Phone number already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (empty($this->form->error)) {
                $id = $this->model_client->createUser($this->form->data);
                $client_id = $this->model_client->createClient($id, $this->form->data);
                $this->load->controller('activity');
                $this->controller_activity->logActivity(16, $client_id, $this->session->loggedUser(), true);
                header('Location:?route=client/viewClient');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }

        $this->load->view('createClient', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function viewClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Client';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Client', 'href' => '?route=client/view'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('client');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $this->limit;

        $this->load->model('client');
        $clients = $this->model_client->getClientList($start, $this->limit);

        $total_count = $this->model_client->getCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $this->limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=client/viewClient&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['clients'] = $clients;
        $this->load->view('viewClient', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function addNewOffer()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Add New Special Offer';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Stock', 'href' => '?route=stock/viewStock'];
        $breadcrumb[] = ['title' => 'Add New Special Offer', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->load->text('viewStock');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('add_new_offer', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getClientDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('client');
                $client_details = $this->model_client->getClientDetails($client_id);
                $client_details['assigned_agent'] = $this->model_client->getAgentName($client_details['assigned_agent']);
                $order_details = $this->model_client->getOrderDetails($client_id);
                $order_status = $this->model_client->getOrderStatus();
                $order_status_mapped = [];
                foreach ($order_status as $key => $status) {
                    $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
                }
                echo json_encode(['status' => 'success',
                    'client_details'       => $client_details,
                    'order_status'         => $order_status_mapped,
                    'order_details'        => $order_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
        // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }

    public function editClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!isset($_GET['client_id'])) {
            header('location:?route=client/index');
            exit;
        }
        $ajax_call = (isset($_GET['ac'])) ? (int) $_GET['ac'] : 0;
        $client_id = (int) $_GET['client_id'];

        $this->load->model('client');
        $client_details = $this->model_client->getClientDetails($client_id);

        $data = [];

        // if(!$_POST) {
        //     $data = array_merge($data, $client_details[0]);
        //     $data['email'] = $data['email_id'];
        // }

        $header['page_title'] = 'Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Clients', 'href' => '?route=client/view'];
        $breadcrumb[] = ['title'=>'Edit Client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->text('client');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $client_details['email'] = $client_details['email_id'];
        $data = array_merge($data, $client_details);

        if ($_POST) {
            $rules = [
                'email'                    => 'e|r',
                'address'                  => 'r',
                'intercommunataire_number' => 'r',
                'credit_limit'             => 'r',
                'company_name'             => 'r',
                'phone'                    => 'r',

            ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $this->model_client->updateUser($client_details['user_id'], $this->form->data);
                $edit_client = $this->model_client->updateClient($client_id, $this->form->data);
                $this->load->controller('activity');
                $this->controller_activity->logActivity(35, $edit_client, $this->session->loggedUser(), true);
                $data['edit_client'] = $edit_client;
                $this->session->data['status'] = 'Changes Updated';
                header('Location:?route=client/viewClient');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                echo json_encode(array_merge($this->form->error, ['status'=>false]));
            }
        } else {
            $this->load->controller('header');
            $this->controller_header->load($header);
            $this->load->view('editClient', $data);
            $this->load->controller('footer');
            $this->controller_footer->load($data);
        }
    }

    public function assign()
    {
        $data = [];
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if ($_POST) {
            $rules = [
                'client_name' => 'r|b',
                'agent_name'  => 'r|b',
            ];
            $this->form->process_post($rules);
            if (empty($this->form->error)) {
                $this->load->model('client');
                $assign_client = $this->model_client->assign_agent($this->form->data['client_name'], $this->form->data['agent_name']);
                $this->load->controller('activity');

                $this->controller_activity->logActivity(36, $assign_client, $this->session->loggedUser(), true);
                $data['assign_client'] = $assign_client;
                // $this->model_client->assign_agent($this->form->data['client_name'], $this->form->data['agent_name']);

                $this->session->data['status'] = 'Agent successfully assigned';
            } else {
                $data = array_merge($this->form->error, $data);
                $data = array_merge($this->form->data, $data);
            }
        }

        $this->load->model('client');
        $clients = $this->model_client->getClientList(0, 100);

        $this->load->model('salesAgent');
        $agents = $this->model_salesAgent->getAgentList(0, 100);

        $data['clients'] = $clients;
        $data['agents'] = $agents;

        $header['page_title'] = 'Assign Client to agent';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Client', 'href' => '?route=client/viewClient'];
        $breadcrumb[] = ['title' => 'Assign', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->load->text('client');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('assign', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }
}
