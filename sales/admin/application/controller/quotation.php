<?php

class Controllerquotation extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Sales Order';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('quotation', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function createQuotation()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'New Quotation';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Client', 'href' => '?route=quotation/createQuotation'];
        $breadcrumb[] = ['title' => 'New', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('quotation');
        $data['clients'] = $this->model_quotation->getClientsList();
        $data['payment_mode'] = $this->model_quotation->getPaymentModes();
        $item_List = $this->model_quotation->getAllItemsList('');

        foreach ($item_List as $key => $item) {
            $image = json_decode($item_List[$key]['image']);
            if (count($image) > 0) {
                $image_url = RESOURCE_URL.'/'.$image[0];
                $item_List[$key]['image'] = $image_url;
            } else {
                $item_List[$key]['image'] = 0;
            }

            $pdf = json_decode($item_List[$key]['item_pdf']);
            if (count($pdf) > 0) {
                $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                $item_List[$key]['item_pdf'] = $pdf_url;
            } else {
                $item_List[$key]['item_pdf'] = 0;
            }
        }

        if ($_POST) {
            $rules = [
                'Client'  => 'r',
                'address' => 'r',
            ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $id = $this->model_quotation->createQuotation($this->form->data);

                $this->load->controller('activity');
                $this->controller_activity->logActivity(17, $id, $this->session->loggedUser(), true);
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }
        $this->load->view('createQuotation', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    private function lastGivenRate($client_id, $item_id)
    {
        $this->load->model('stock');
        $client_id = (int) $client_id;
        $item_id = (int) $item_id;

        return $this->model_stock->getLastGivenRate($item_id, $client_id);
    }

    public function viewquotation()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Quotations';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Sales Order', 'href' => '?route=quotation/viewquotation'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('quotation');
        $sales_orders = $this->model_quotation->getPendingOrderList($start, $limit);
        // $this->load->controller("activity");
        // $this->controller_activity->logActivity(37,$sales_orders,$this->session->loggedUser(),true);
        $order_status = $this->model_quotation->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_quotation->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=quotation/viewquotation&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;
        $data['type'] = 'p';
        $this->load->view('viewQuotation', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function completeOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Complete Orders';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Sales Order', 'href' => '?route=quotation/completeOrder'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('quotation');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('quotation');
        $complete_orders = $this->model_quotation->getCompleteOrderList($start, $limit);
        $order_status = $this->model_quotation->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }
        $total_count = $this->model_quotation->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=quotation/completeOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['complete_orders'] = $complete_orders;
        $data['order_status'] = $order_status_mapped;
        $data['type'] = 'c';
        $this->load->view('completeOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function cancelledOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Cancelled Orders';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Sales Order', 'href' => '?route=quotation/cancelledOrder'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('quotation');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('quotation');
        $cancelled_orders = $this->model_quotation->getCancelledOrderList($start, $limit);
        $order_status = $this->model_quotation->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }
        $total_count = $this->model_quotation->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=quotation/cancelledOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['cancelled_orders'] = $cancelled_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('cancelledOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('quotation');
                $sales_order = $this->model_quotation->getOrderDetails($order_id);
                $order_items = $this->model_quotation->getOrderItemsList($order_id);

                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('quotation');
            $quotations = $this->model_quotation->getPendingOrderList(0, 10, $clause);
            // $this->load->controller("activity");
            // $this->controller_activity->logActivity(37,$quotations,$this->session->loggedUser(),true);
            $order_status = $this->model_quotation->getOrderStatus();
            $order_status_mapped = [];
            foreach ($order_status as $key => $status) {
                $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
            }
            echo json_encode(['sales_list'=>$quotations, 'order_status'=>$order_status_mapped, 'clause'=>$clause]);
        }
    }

    public function getCompleteOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('quotation');
            $completeOrders = $this->model_quotation->getCompleteOrderList(0, 10, $clause, true, false);
            echo json_encode(['complete_list'=>$completeOrders, 'clause'=>$clause]);
        }
    }

    public function getCancelledOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('quotation');
            $cancelledOrders = $this->model_quotation->getCancelledOrderList(0, 10, $clause, false, true);
            echo json_encode(['cancelled_list'=>$cancelledOrders, 'clause'=>$clause]);
        }
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('quotation');
                $item_details = $this->model_quotation->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $brand_name = $_GET['brand_name'];
                $this->load->model('quotation');
                $categoryList = $this->model_quotation->getCategoryList($brand_name);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('quotation');
                $areaList = $this->model_quotation->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $clientClause = '';
        $itemClause = '';
        $brandClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['brand'])) {
            $c = '';
            $fieldName = 'brand_name';
            $filter = $_POST['brand'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['clientList'])) {
            $c = '';
            $fieldName = 'client';
            $filter = $_POST['clientList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = (isset($_POST['list']) && $_POST['list'] == 'item') ? 'reference' : 'order_id';
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '."item_id = '".$_POST['itemIdList'][$i]."'";
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$itemClause;
                } else {
                    $whereClause = $whereClause.' AND '.$itemClause;
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getClientsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('quotation');
                $clientsList = $this->model_quotation->getClientsList();
                echo json_encode(['status' => 'success',
                    'client_list'          => $clientsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getClientsAddress()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('quotation');
                $clientsAddress = $this->model_quotation->getClientsAddress($client_id);
                echo json_encode(['status' => 'success',
                    'client_address'       => $clientsAddress, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('quotation');
                $clause = '';

                $client_id = (isset($_POST['clientId'])) ? $_POST['clientId'] : 0;
                $clause = $this->buildClause();
                $item_List = [];
                $limit = (isset($_POST['limit'])) ? (int) $_POST['limit'] : 15;
                $page = (isset($_POST['page'])) ? (int) $_POST['page'] : 1;
                $start = ($page - 1) * $limit;
                $total_count = $this->model_quotation->getAllItemsTotal($clause);
                $this->load->helper('pagination');
                $this->pagination->limit = $limit;
                $this->pagination->total = $total_count;
                $this->pagination->page = $page;
                $this->pagination->url = HTTP_SERVER.'?route=quotation/createQuotation&page={page}';
                $pagination = $this->pagination->render();

                $item_List = $this->model_quotation->getAllItemsList($clause, $limit, $start);
                /*if(isset($_POST['with_client'])){
                    $item_List = $this->model_quotation->getAllItemsListWithClient($clause);
                }else{

                }*/

                foreach ($item_List as $key => $item) {
                    $item_List[$key]['given_rate'] = $this->lastGivenRate($client_id, $item_List[$key]['item_id']);
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                $response_data = ['status' => 'success',
                    'itemList'             => $item_List,
                    'clause'               => $clause, ];
                $response_data['pagination'] = $pagination;
                $response_data['total_count'] = $total_count;

                if (isset($_POST['client_id'])) {
                    $client = $this->model_quotation->getClientDetails($_POST['client_id']);
                    $type = $this->model_quotation->getClientType($client['client_type']);
                    // $client['client_type'] = $type['title'];
                    $response_data['client'] = $client;
                }

                echo json_encode($response_data);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function create()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $data = [];
                $this->load->model('quotation');
                $items = $_POST['items'];
                $sell_rate = $_POST['sell_rates'];
                $quantity = $_POST['quantity'];
                $mrp = $_POST['mrp'];
                $htt = 0;
                $ttc = 0;

                for ($i = 0; $i < count($items); $i++) {
                    $p = $sell_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    // $htt = $htt + (($m - ($m * $p) / 100) * $q);
                    $htt = $htt + ($p * $q);
                }

                $ttc = $htt + (($htt * TAX) / 100);
                $_POST['ttc'] = $ttc;
                $_POST['htt'] = $htt;

                $data['htt'] = $htt;
                $data['tva'] = TAX;
                $data['ttc'] = $ttc;
                $data['amount_paid'] = 0;
                $data['order_date'] = date('Y-m-d', time());
                $data['payment_deadline'] = date('Y-m-d', strtotime($data['order_date'].' + 45 days'));
                $data['order_status'] = 14;
                $data['order_type'] = 5;
                $data['client_id'] = (int) $_POST['client'];
                $data['title'] = $_POST['title'];
                $data['delivery_address'] = (int) $_POST['client'];
                $data['city'] = $_POST['city'];
                $data['raised_by'] = $this->session->data['logged_user_id'];
                $order_id = $this->model_quotation->createQuotation($data);
                $this->load->controller('activity');
                for ($i = 0; $i < count($items); $i++) {
                    $orderData = [];
                    $it = (int) $items[$i];
                    $p = $sell_rate[$i]; //Type Removed
                    $q = (int) $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed

                    $orderData['order_id'] = $order_id;
                    $orderData['item_id'] = $it;
                    $orderData['quantity'] = $q;
                    $orderData['discount'] = 0;
                    $orderData['rate'] = $p;
                    $orderData['mrp'] = $m;
                    $orderData['order_type'] = $data['order_type'];

                    $this->model_quotation->addOrderItems($orderData);
                }

                // $this->controller_activity->logActivity(17,$order_id,$this->session->loggedUser(),true);

                echo json_encode(['status' => 'success',
                    'order_id'             => $order_id, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function cancelOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('quotation');
            $cancel_order = $this->model_quotation->cancelOrder($order_id);
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $brand = $_GET['brand_name'];
                $category = $_GET['category'];
                $this->load->model('quotation');
                $refList = $this->model_quotation->getRefList($ref, $brand, $category);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function changeItem()
    {
        $order_id = (int) $_POST['order_id'];
        $item_reference = (int) $_POST['item_reference'];
        $new_reference = (int) $_POST['new_reference'];

        $this->load->model('quotation');

        if ($item_reference != $new_reference) {
            $new_id = $this->model_quotation->validateReference($new_reference);
            if ($new_id) {
                if ($this->model_quotation->changeItem($order_id, $item_reference, $new_id)) {
                    echo json_encode(['status'=>true]);
                } else {
                    echo json_encode(['status'=>false, 'msg'=> 'This Item has already been dispatched']);
                }
            } else {
                echo json_encode(['status'=>false]);
            }
        } else {
            echo json_encode(['status'=>false]);
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['char']) && strlen($_GET['char']) > 2) {
                $string = htmlentities($_GET['char']);
            }
            if (isset($_GET['all'])) {
                $this->load->model('quotation');
                $brandList = $this->model_quotation->getBrandList($string);
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function itemListQ()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['char']) && strlen($_GET['char']) > 2) {
                $string = htmlentities($_GET['char']);
            }
            if (isset($_GET['all'])) {
                $this->load->model('quotation');
                $itemList = $this->model_quotation->itemListQ($string);
                echo json_encode(['status'          => 'success',
                                        'item_list' => $itemList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function markApproved()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['q']) && isset($_GET['i'])) {
                $q = (int) $_GET['q'];
                $i = (int) $_GET['i'];
                $this->load->model('quotation');
                $itemList = $this->model_quotation->markApproved($q, $i);
                echo json_encode(['status' => 'success']);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function offerChange()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['q']) && isset($_GET['i'])) {
                $q = (int) $_GET['q'];
                $i = (int) $_GET['i'];
                $v = (float) $_POST['val'];
                $this->load->model('quotation');
                $itemList = $this->model_quotation->offerChange($q, $i, $v);
                echo json_encode(['status' => 'success']);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function quanChange()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['q']) && isset($_GET['i'])) {
                $q = (int) $_GET['q'];
                $i = (int) $_GET['i'];
                $v = (float) $_POST['val'];
                $this->load->model('quotation');
                $itemList = $this->model_quotation->quanChange($q, $i, $v);
                echo json_encode(['status' => 'success']);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
