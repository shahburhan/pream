<?php

class ControllerAgentInvoice extends Controller
{
    public function index()
    {
        $data = [];
        $header['page_title'] = 'Commission';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('commission', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function generateInvoice()
    {
        $data = [];
        $header['page_title'] = 'Generate Invoice';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Commission', 'href' => '?commission/generateInvoice'];
        $breadcrumb[] = ['title'=>'Generate Invoice', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('commission');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('salesOrder');
        $data['orders'] = $this->model_salesOrder->getAgentOrderList();
        $data['total_sales'] = (int) $this->model_salesOrder->getAgentSales();
        $data['commission_rate'] = (float) $this->model_salesOrder->getCommissionRate($data['total_sales']);
        $data['commission_amount'] = ($data['total_sales'] * $data['commission_rate']) / 100;
        $order_status = $this->model_salesOrder->getOrderStatus();
        foreach ($order_status as $os) {
            // code...
            $data['order_status'][$os['order_status_id']] = $os['title'];
        }
        $this->load->view('generateInvoice', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function commissionHistory()
    {
        $data = [];
        $header['page_title'] = 'History';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Commission', 'href' => '?agentInvoice/commissionHistory'];
        $breadcrumb[] = ['title'=>'History', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('agentInvoice');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('salesOrder');
        $data['commission_history'] = $this->model_salesOrder->getCommissionHistory();

        $this->load->view('commissionHistory', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function invoiceCreate()
    {
        $this->load->model('salesOrder');
        $data['orders'] = $this->model_salesOrder->generateInvoice();
        $this->session->data['status'] = 'Invoice Successfully Created';
        echo 'Created';
    }

    public function getInvoiceDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['invoice_id'])) {
                $response = [];
                $invoice_id = $_GET['invoice_id'];
                $this->load->model('salesOrder');
                $invoice = $this->model_salesOrder->getInvoiceDetails($invoice_id);

                $invoice_order = $this->model_salesOrder->getInvoiceOrderList($invoice['orders']);

                $response['invoice'] = $invoice;
                $response['invoice_order'] = $invoice_order;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
