<?php

$text_client['title'] = 'Create Client';
//buttons
$text_client['company'] = 'Company';
$text_client['individual'] = 'Individual';
//form label
$text_client['title_create'] = 'Create Client';
$text_client['create_client_i'] = 'Create Client - Individual';
$text_client['name'] = 'Name';
$text_client['last_name'] = 'Last Name';
$text_client['client_type'] = 'Client Type';
$text_client['password'] = 'Password';
$text_client['type'] = 'Type';
$text_client['email'] = 'Email';
$text_client['phone'] = 'Phone';
$text_client['address_c'] = 'Company Address';
$text_client['address'] = 'Address';
$text_client['register_address'] = 'Company Registration Address';
$text_client['set_credit_limit'] = 'Set Credit Limit';
$text_client['update_credit_limit'] = 'Update Credit Limit';
$text_client['special_offer'] = 'Special Offer';
$text_client['on_purchase_of'] = 'On Purchase Of';
$text_client['username_c'] = 'Company Name';
$text_client['username'] = 'Username';
$text_client['intercommunataire'] = 'Intercommunataire (Number)';
$text_client['document_upload'] = 'BFA Document Upload';
$text_client['authorized_person'] = 'Authorized Person';
$text_client['person_name'] = 'Person Name';
$text_client['details'] = 'Details';
$text_client['reset'] = 'Reset';
//$text_client['username_i'] = "Individual Name";
$text_client['field_required'] = 'This Field Is Required';

//btn
$text_client['submit'] = 'submit';
$text_client['add'] = 'Add';
$text_client['cancel'] = 'Cancel';
$text_client['edit'] = 'Edit';
//view client
$text_client['title'] = 'Clients';
$text_client['update'] = 'Update';
$text_client['title_edit'] = 'Edit Client';
//change request
//table headings
$text_client['search'] = 'Search';
$text_client['name_c'] = 'Name';
$text_client['client_id'] = 'Client Id';
$text_client['credit_limit'] = 'Credit Limit';
$text_client['email_c'] = 'Email';
$text_client['phone_c'] = 'Phone';
$text_client['address_c'] = 'Address';
$text_client['action_c'] = 'Action';
$text_client['change_request'] = 'Credit Limit Change Request';
$text_client['proposed_by'] = 'Proposed By';
$text_client['agent_contact'] = 'Agent Contact';

//client details
$text_client['client_details'] = 'Client Details';
$text_client['client_name'] = 'Client Name';
$text_client['agent_name'] = 'Agent';
$text_client['assign_agent'] = 'Assigned Agent ';

//item details
$text_client['order_id'] = 'Order Id';
$text_client['order_date'] = 'Order Date';
$text_client['order_status'] = 'Order Status';
$text_client['ttc'] = 'TTC';
$text_client['item_category'] = 'Item Category';
$text_client['quantity'] = 'Quantity';
$text_client['action'] = 'Action';

//action
$text_client['edit_c'] = 'Edit';
$text_client['approve'] = 'Approve';
$text_client['disapprove'] = 'Disapprove';
