<?php

//form label
$text_salesReport['title'] = 'Add Item';

$text_salesReport['order_id'] = 'PO#';
$text_salesReport['order_date'] = 'Order Date';
$text_salesReport['vendor_name'] = 'Vendor Name';
$text_salesReport['delivery_address'] = 'Delivery Address';
$text_salesReport['raised_by'] = 'Raised By';
$text_salesReport['action'] = 'action';
