<?php

$text_agentInvoice['title'] = 'Invoices';
//table headings
$text_agentInvoice['search'] = 'Search';
$text_agentInvoice['invoice_id'] = 'Invoice Id';
$text_agentInvoice['agent_name'] = 'Agent Name';
$text_agentInvoice['commission'] = 'Commision Amount';
$text_agentInvoice['invoice_action'] = 'Action';
$text_agentInvoice['total_amount'] = 'Total Amount';
$text_agentInvoice['commission_rate'] = 'Commision Rate';

//action
$text_agentInvoice['view_details'] = 'View Details';

//details
$text_agentInvoice['details_heading'] = 'Agent Invoice';
$text_agentInvoice['invoice'] = 'Invoice';
$text_agentInvoice['grand_total'] = 'Grand Total';
$text_agentInvoice['invoice_date'] = 'Invoice Date';
$text_agentInvoice['due_date'] = 'Due Date';

//table headings
$text_agentInvoice['invoice_id'] = 'Invoice#';
$text_agentInvoice['invoice_type'] = 'Invoice Type';
$text_agentInvoice['invoice_amount'] = 'Invoice Amount';
$text_agentInvoice['gen_date'] = 'Generation Date';
$text_agentInvoice['action'] = 'Action';
$text_agentInvoice['subtotal'] = 'Subtotal';
$text_agentInvoice['tax'] = 'Tax';
$text_agentInvoice['tds'] = 'TDS';
$text_agentInvoice['total'] = 'Total';

$text_agentInvoice['invoice_title'] = 'Invoice';

//table headings
$text_agentInvoice['order_id'] = 'Order ID';
$text_agentInvoice['order_amount'] = 'Order Amount';
$text_agentInvoice['order_status'] = 'Order Status';
$text_agentInvoice['order_date'] = 'Order Date';
$text_agentInvoice['client'] = 'Client';
$text_agentInvoice['commission_rate'] = 'Commission Rate';
$text_agentInvoice['subtotal'] = 'Subtotal';
$text_agentInvoice['tax'] = 'Tax';
$text_agentInvoice['tds'] = 'TDS';
$text_agentInvoice['total'] = 'Total';

//btn
$text_agentInvoice['generate'] = 'Generate';
$text_agentInvoice['add_expense'] = 'Add Expense';

//commission history
$text_agentInvoice['title'] = 'Commission History';
//table headings
$text_agentInvoice['gen_invoice_id'] = 'Invoice Id';
$text_agentInvoice['gen_tax'] = 'Tax';
$text_agentInvoice['search'] = 'Search';
$text_agentInvoice['TSM'] = 'Total Sales';
$text_agentInvoice['total_commission'] = 'Total Commission';
$text_agentInvoice['gen_date'] = 'Generation Date';
$text_agentInvoice['gen_status'] = 'Status';
$text_agentInvoice['gen_action'] = 'Action';

//agents
$text_agentInvoice['agent_invoice'] = 'Agent Invoice';
$text_agentInvoice['invoice'] = 'Invoice#';
$text_agentInvoice['grand_total'] = 'Grand Total';
$text_agentInvoice['invoice_date'] = 'Invoice Date';
$text_agentInvoice['due_date'] = 'Due Date';
$text_agentInvoice['invoice_type'] = 'Invoice Type';
$text_agentInvoice['invoice_amount'] = 'Invoice Amount';
$text_agentInvoice['generation_date'] = 'Generation Date';
$text_agentInvoice['gen_action'] = 'Action';
$text_agentInvoice['make_payment'] = 'Make Payment';

//action
$text_agentInvoice['details'] = 'View Details';
