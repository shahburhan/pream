<?php

$text_common['overview'] = 'Overview';
$text_common['stock'] = 'Stock';
$text_common['sales_agent'] = 'Sales Agent';
$text_common['sales_target'] = 'Sales Target';
$text_common['sales_order'] = 'Sales';
$text_common['client'] = 'Client';
$text_common['reports'] = 'Reports';
$text_common['notification'] = 'Notification';
$text_common['agent_invoice'] = 'Agent Invoice';

//sales agent
$text_common['create_sales_agent'] = 'Create';
$text_common['view_sales_agent'] = 'View';
$text_common['set_limit'] = 'Set Limit';
$text_common['set_commission'] = 'Set Commission rates';
$text_common['return'] = 'Return';
//client
$text_common['create_client'] = 'Create';
$text_common['view_client'] = 'View';
$text_common['client_assign'] = 'Client to Agent';
$text_common['add_new_offer'] = 'Add new Special Offer';

//stock
$text_common['view_stock'] = 'View';

//sales order
$text_common['new_order'] = 'New Order';
$text_common['view_order'] = 'Pending Orders';
$text_common['complete_order'] = 'Complete Orders';
$text_common['cancelled_order'] = 'Cancelled Orders';
//sales target
$text_common['set_target'] = 'Set Target For Agents';
$text_common['set_inventives'] = 'Set Inventives';

//reports
$text_common['statistics_reports'] = 'Statistics';
$text_common['annual_reports'] = 'Annual/Quaterly/Monthly';

//agent invoice
$text_common['commission_invoice'] = 'Commission Invoices';

//ro
$text_common['ro_pending'] = 'RO Pending';
$text_common['ro_history'] = 'RO History';
$text_common['ro_create'] = 'Create RO';
