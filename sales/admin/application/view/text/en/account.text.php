<?php

//overview
$text_account['sales_agent'] = 'Sales Agent';
$text_account['an_hour_ago'] = 'An Hour Ago';
$text_account['waylon_dalton'] = 'Waylon Dalton';
$text_account['subject'] = 'Subject';
$text_account['notifications'] = 'Notifications';
$text_account['minimum_stock'] = 'Minimum Stock Alert';
$text_account['stock_update'] = 'Stock Update';
$text_account['order_dispatch'] = 'Order Dispatch';
$text_account['new_item_alert'] = 'New Item Alert';
$text_account['read_more'] = 'Read More';

//login
$text_account['title'] = 'Sign In';
$text_account['email'] = 'Username';
$text_account['password'] = 'Password';
$text_account['subtitle'] = 'Sales Admin';
$text_account['error_invalid'] = 'Username Or Password  Invalid';
$text_account['error_empty'] = 'Username Cannot Be Empty.';
$text_account['error_password'] = 'Password Cannot Be Empty.';
$text_account['forgot_password'] = 'Forgot your password';

//login button
$text_account['login'] = 'Login';

//Error page start//
$text_account['error_404'] = 'Sorry, the page you are looking for cannot be found.';
//Error page end//

//Unauthorized start//
$text_account['unauthorized_access'] = 'Unauthorized Access';
$text_account['unauthorized_message'] = 'You Are Not Authorized To View This Page.';
//Unauthorized end//
