<?php

$text_return['title'] = 'Select Items For Return';
$text_return['title_pending'] = 'Pending Return Orders';
$text_return['title_history'] = 'Return Orders';

//filters
$text_return['search'] = 'Search';

//table headings
$text_return['so'] = 'SO#';
$text_return['order_date'] = 'Order Date';
$text_return['vendor_address'] = 'Vendor Address';
$text_return['vendor_name'] = 'Vendor Name';
$text_return['expected_date'] = 'Expected Date';
$text_return['payment_type'] = 'Payment Type';
$text_return['client_name'] = 'Client Name';
$text_return['delivery_address'] = 'Delivery Address';
$text_return['sales_agent'] = 'Sales Agent';
$text_return['action'] = 'Action';

//return details
$text_return['ro'] = 'Return Order';
$text_return['ro_no'] = 'RO#';
$text_return['ro_pending'] = 'RO Pending';
$text_return['details'] = 'Details';
//table headings
$text_return['return_date'] = 'Return Date';
$text_return['client_address'] = 'Client Address';
$text_return['sales_agent'] = 'Sales Agent';
$text_return['heading'] = 'Create Return Order';
//items
$text_return['reference'] = 'Ref#';
$text_return['item_id'] = 'Item Id';
$text_return['item_name'] = 'Item Name';
$text_return['brand_name'] = 'Brand Name';
$text_return['unit_price'] = 'Unit Price';
$text_return['quantity'] = 'Quantity';
$text_return['total'] = 'Total';
$text_return['discount_rate'] = 'Discount Rate';
$text_return['reason'] = 'Reason';
$text_return['submit'] = 'Submit';
$text_return['sell_rate'] = 'Sell Rate';
$text_return['ro_number'] = 'RO#';
//btn
$text_return['add_bill'] = 'Mark Received';
$text_return['approve'] = 'Approve';
$text_return['disapprove'] = 'Disapprove';
$text_return['description'] = 'Description';
$text_return['grand_total'] = 'Grand Total';
