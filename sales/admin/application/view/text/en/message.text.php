<?php

//message compose
$text_message['title'] = 'Compose New Message';
$text_message['send_to'] = 'Send To';
$text_message['sender_empty'] = 'Send Cannot Be Empty.';
$text_message['subject'] = 'Subject';
$text_message['text_lines'] = 'Text Lines...';
$text_message['send'] = 'Send';

//message inbox

$text_message['inbox'] = 'Inbox';
$text_message['search'] = 'Search';

//message outbox
$text_message['outbox'] = 'Inbox';
