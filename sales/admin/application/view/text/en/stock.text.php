<?php

$text_stock['title'] = 'Stock';
$text_stock['category_stock'] = 'Category';
$text_stock['item_name'] = 'Item Name';
$text_stock['vendor_name'] = 'Vendor Name';
$text_stock['reference'] = 'Reference';
$text_stock['brand_name'] = 'Brand Name';
$text_stock['remarks'] = 'Remarks';
$text_stock['min_stock'] = 'Min <br> Stock';
$text_stock['current_stock'] = 'Current Stock';
$text_stock['packing'] = 'Packing';
$text_stock['area'] = 'Item Area';
$text_stock['add_pdf'] = 'Add PDF';
$text_stock['add_image'] = 'Add Image';
$text_stock['status'] = 'Item Status';
$text_stock['action'] = 'Action';
$text_stock['search'] = 'Search';
$text_stock['sr_no'] = 'Sr.No.';
$text_stock['expiry'] = 'Expiry';
$text_stock['action'] = 'Action';

//item details
$text_stock['item_image'] = 'Item Image';
$text_stock['expiry'] = 'Expiry';
$text_stock['action'] = 'Action';
$text_stock['alert'] = 'Alerts';
$text_stock['item_id'] = 'Item Id';
$text_stock['field_name'] = 'Field Name';
$text_stock['requested_by'] = 'Requested By';
$text_stock['previous_value'] = 'Previous Value';
$text_stock['new_value'] = 'New Value Suggested';
$text_stock['action'] = 'Action';
$text_stock['approve'] = 'Approve';
$text_stock['disapprove'] = 'Disapprove';
