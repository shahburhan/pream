<?php
 class ModelClient extends Model
 {
     public function validate_email($email)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE email_id = '".$this->db->escape($email)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function validate_phone($phone)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE phone = '".$this->db->escape($phone)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function validate_username($username)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE username = '".$this->db->escape($username)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function createUser($data = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $lastLogin = date('Y-m-d h:i:s', time());
         $last_ip = $_SERVER['REMOTE_ADDR'];
         $user_type = (int) 9;
         $status = (int) 0;
         // $password = rand(1000,9999);

         $this->db->query("INSERT INTO user SET 
			email_id = '".$this->db->escape($data['email'])."',
			password = '".$this->db->escape($data['password'])."', 
			last_ip = '".$this->db->escape($last_ip)."', 
			created = '".$this->db->escape($date)."',
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			user_type = '".$this->db->escape($user_type)."',
			status = '".$this->db->escape($status)."',
			last_login = '".$this->db->escape($date)."' ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function updateUser($id, $data = [])
     {
         $this->db->query("UPDATE user SET 
			email_id = '".$this->db->escape($data['email'])."',
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."'
			WHERE user_id = $id");

         return true;
     }

     public function getClientType()
     {
         $result = $this->db->query('SELECT * FROM client_type');

         return $result->rows;
     }

     public function createClient($user_id, $data = [])
     {
         $credit_limit_used = (int) 0;
         $balance = (int) 0;
         $assigned_agent = (int) 0;
         $this->db->query("INSERT INTO client SET
			user_id = '".$this->db->escape($user_id)."',
			credit_limit = '".$this->db->escape($data['credit_limit'])."',
			client_type = '".$this->db->escape($data['client_type'])."',
			authorized_name = '".$this->db->escape($data['authorized_name'])."',
			credit_limit_used = '".$this->db->escape($credit_limit_used)."',
			balance = '".$this->db->escape($balance)."',
			bfa_document = '".$this->db->escape($data['bfa_document'])."',
			k_bis = '".$this->db->escape($data['k_bis'])."',
			rib = '".$this->db->escape($data['rib'])."',
			papier_entete = '".$this->db->escape($data['papier_entete'])."',
			pieces_didentite = '".$this->db->escape($data['pieces_didentite'])."',
			liasse_fiscale = '".$this->db->escape($data['liasse_fiscale'])."',
			company_name = '".$this->db->escape($data['company_name'])."',
			intercommunataire_number = '".$this->db->escape($data['intercommunataire_number'])."',
			assigned_agent = '".$this->db->escape($assigned_agent)."'
			");
         $client_id = $this->db->getLastId();

         //$this->session->login($user_id);

         //$this->createVendor($user_id);

         return $client_id;
     }

     public function updateClient($client_id, $data = [])
     {
         $this->db->query("UPDATE client SET 
			client_type = '".$this->db->escape($data['client_type'])."',
			intercommunataire_number = '".$this->db->escape($data['intercommunataire_number'])."',
			credit_limit = '".$this->db->escape($data['credit_limit'])."',
			company_name = '".$this->db->escape($data['company_name'])."'
			 WHERE client_id = $client_id");

         return $client_id;
     }

     public function getClientList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT c.*, u.* FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id) LIMIT $start, $limit");

         return $result->rows;
     }

     public function getCount()
     {
         $result = $this->db->query('SELECT * FROM client');

         return $result->num_rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status ');

         return $result->rows;
     }

     public function getClientDetails($client_id)
     {
         $result = $this->db->query('SELECT * FROM client LEFT JOIN user ON user.user_id = client.user_id WHERE client.client_id= '.$client_id);

         return $result->row;
     }

     public function getOrderDetails($client_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` WHERE `order`.client= ".$client_id);

         return $result->rows;
     }

     public function getItemListByClient($client_id)
     {
         $result = $this->db->query('SELECT * FROM item WHERE client= '.$client_id);

         return $result->rows;
     }

     public function getItemCategory()
     {
         $result = $this->db->query('SELECT * FROM item_category');

         return $result->rows;
     }

     public function assign_agent($client_id, $agent_id)
     {
         $client_id = (int) $client_id;
         $agent_id = (int) $agent_id;
         $sql = "UPDATE client SET assigned_agent = $agent_id WHERE client_id = $client_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getAgentName($agent_id)
     {
         $sql = "SELECT name, agent.user_id FROM user RIGHT JOIN agent ON agent.user_id = user.user_id WHERE employee_id = $agent_id";
         $result = $this->db->query($sql);

         return $result->row;
     }
 }
