<?php
 class ModelsalesOrder extends Model
 {
     public function newOrder($data = [])
     {
         $this->db->query("INSERT INTO `order` SET
			order_date = '".$this->db->escape($data['order_date'])."',
            payment_deadline = '".$this->db->escape($data['payment_deadline'])."',
            payment_mode = '".$this->db->escape($data['payment_mode'])."',
			person_responsible = '".$this->db->escape($data['person_responsible'])."',
			`client` = '".$this->db->escape($data['client_id'])."',
			order_type = '".$this->db->escape($data['order_type'])."',
			amount_paid = '".$this->db->escape($data['amount_paid'])."',
			raised_by = '".$this->db->escape($data['raised_by'])."',
			order_status = '".$this->db->escape($data['order_status'])."',
			htt = '".$this->db->escape($data['htt'])."',
			tva = '".$this->db->escape($data['tva'])."',
			ttc = '".$this->db->escape($data['ttc'])."',
			expected_date = '".$this->db->escape($data['expected_date'])."',
			city = '".$this->db->escape($data['city'])."',
			delivery_address = '".$this->db->escape($data['delivery_address'])."',
			delivery_other = '".$this->db->escape($data['other_address'])."'
			 ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function getPaymentModes()
     {
         $result = $this->db->query("SELECT * FROM payment_mode WHERE lang = '".$this->session->language."'");

         return $result->rows;
     }

     public function addOrderItems($data = [])
     {
         $this->db->query("INSERT INTO `order_item` SET
			order_id = '".$this->db->escape($data['order_id'])."',
			item_id = '".$this->db->escape($data['item_id'])."',
			quantity = '".$this->db->escape($data['quantity'])."',
			discount = '".$this->db->escape($data['discount'])."',
			rate = '".$this->db->escape($data['rate'])."',
			order_type = '".$this->db->escape($data['order_type'])."',
			mrp = '".$this->db->escape($data['mrp'])."'
			 ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function getClientsList()
     {
         $result = $this->db->query('SELECT c.*, u.*, c.company_name as name FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id)');

         return $result->rows;
     }

     public function getClientsAddress($client_id)
     {
         $result = $this->db->query('SELECT user.address,user.city,user.postal_code FROM client LEFT JOIN user ON client.user_id = user.user_id WHERE client.client_id = '.$client_id);

         return $result->row;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT item.image, item.item_pdf, item.item_name, item.reference, item.item_id  FROM item WHERE item.item_id = '.$item_id);

         return $result->row;
     }

     public function getCategoryList($brand)
     {
         $result = $this->db->query("SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.brand_name = '".$this->db->escape($brand)."' ORDER BY title");

         return $result->rows;
     }

     public function getAreaList()
     {
         $result = $this->db->query('SELECT DISTINCT item.area FROM item');

         return $result->rows;
     }

     public function getAllItemsList($clause = '', $limit = 15, $start = 0)
     {
         $result = $this->db->query('SELECT * FROM item '.$clause." LIMIT $start, $limit");

         return $result->rows;
     }

     public function getAllItemsTotal($clause)
     {
         $result = $this->db->query('SELECT COUNT(item_id) as total FROM item  '.$clause);

         return $result->row['total'];
     }

     public function getAllItemsListWithClient($clause)
     {
         $result = $this->db->query('SELECT * FROM item LEFT JOIN  (SELECT user.address , user.city, user.email_id, client.client_id, client.company_name, client.client_type FROM user LEFT JOIN client ON client.user_id = user.user_id) u ON u.client_id = item.client '.$clause);

         return $result->rows;
     }

     public function getClientType($type)
     {
         $result = $this->db->query('SELECT title FROM client_type WHERE client_type_id = '.$type);

         return $result->row;
     }

     public function getClientDetails($client_id)
     {
         $result = $this->db->query('SELECT * FROM client LEFT JOIN user ON user.user_id = client.user_id WHERE client.client_id= '.$client_id);

         return $result->row;
     }

     public function getOrderList($start = 0, $limit = 10, $where = ' ', $completed = false, $cancelled = false)
     {
         $status = 2;
         if ($completed) {
             $status = 5;
         }
         if ($cancelled) {
             $status = 9;
         }
         if (empty(trim($where))) {
             if ($status != 2) {
                 $sql = "SELECT oii.*, DATE_FORMAT(oii.order_date, '%d-%m-%Y') as order_date, client.company_name FROM client RIGHT JOIN     (SELECT oi.*, item_category, brand_name FROM        (SELECT `order`.*, order_item.item_id FROM `order` LEFT JOIN order_item  ON `order`.order_id = order_item.order_id ) oi LEFT JOIN item ON item.item_id = oi.item_id ) oii ON client.client_id = oii.client WHERE order_status = $status LIMIT $start, $limit";
             } else {
                 $sql = "SELECT oii.*, DATE_FORMAT(oii.order_date, '%d-%m-%Y') as order_date, client.company_name FROM client RIGHT JOIN     (SELECT oi.*, item_category, brand_name FROM        (SELECT `order`.*, order_item.item_id FROM `order` LEFT JOIN order_item  ON `order`.order_id = order_item.order_id ) oi LEFT JOIN item ON item.item_id = oi.item_id ) oii ON client.client_id = oii.client WHERE order_status = $status || order_status = 13 LIMIT $start, $limit";
             }
         } else {
             $sql = "SELECT oii.*, DATE_FORMAT(oii.order_date, '%d-%m-%Y') as order_date, client.company_name FROM client RIGHT JOIN     (SELECT oi.*, item_category, brand_name FROM        (SELECT `order`.*, order_item.item_id FROM `order` LEFT JOIN order_item  ON `order`.order_id = order_item.order_id ) oi LEFT JOIN item ON item.item_id = oi.item_id ) oii ON client.client_id = oii.client ".$where." && order_status = $status LIMIT $start, $limit";
             if ($status == 2) {
                 $sql = "SELECT oii.*, DATE_FORMAT(oii.order_date, '%d-%m-%Y') as order_date, client.company_name FROM client RIGHT JOIN     (SELECT oi.*, item_category, brand_name FROM        (SELECT `order`.*, order_item.item_id FROM `order` LEFT JOIN order_item  ON `order`.order_id = order_item.order_id ) oi LEFT JOIN item ON item.item_id = oi.item_id ) oii ON client.client_id = oii.client ".$where." && (order_status = $status || order_status = 13) LIMIT $start, $limit";
             }
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getCancelledOrderList($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 && order_status = 9 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 2 && order_status = 9 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date , DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date
        FROM `order` LEFT JOIN (SELECT user.name, user.address, user.city, user.email_id, user.phone, user.postal_code, client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->rows;
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function getUserDetails($user_id)
     {
         $result = $this->db->query('SELECT * FROM user LEFT JOIN user_type ON user.user_type = user_type.user_type_id WHERE user_id = '.$user_id);

         return $result->row;
     }

     public function orderCount($status = 2)
     {
         $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

         return $result->num_rows;
     }

     public function cancelOrder($order_id)
     {
         $order_id = (int) $order_id;
         // order_status - 9 (sales order cancelled)
         $sql = "UPDATE `order` SET order_status = 9 WHERE order_id = $order_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getRefList($ref = 0, $client = 0, $brand_name = '', $category = 0, $com = false, $can = false)
     {
         $status = 2;
         if ($com) {
             $status = 5;
         }
         if ($can) {
             $status = 9;
         }
         if (!$ref) {
             $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && client = $client &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
             if ($category) {
                 $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name, item.item_category FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && client = $client  && item_category = $category &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
             }
         } else {
             $sql = "SELECT DISTINCT reference FROM `item`  WHERE brand_name = '".$this->db->escape($client)."'";
             if ($category) {
                 $sql = "SELECT DISTINCT reference FROM `item` WHERE brand_name = '".$this->db->escape($client)."' && item_category = $category";
             }
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function validateReference($ref = 0)
     {
         $sql = "SELECT DISTINCT item_id FROM `item` WHERE reference = $ref";
         $result = $this->db->query($sql);

         return (isset($result->row['item_id'])) ? $result->row['item_id'] : false;
     }

     public function changeItem($order_id, $reference, $new_id)
     {
         $sql = "SELECT * FROM order_bol ob LEFT JOIN item i ON ob.item_id = ob.item_id WHERE i.reference = $reference && ob.order_id = $order_id";
         $result = $this->db->query($sql);

         if ($result->num_rows) {
             return false;
         }

         $sql = "UPDATE order_item oi LEFT JOIN item i on oi.item_id = i.item_id SET oi.item_id = $new_id WHERE oi.order_id = $order_id && i.reference = $reference";
         $this->db->query($sql);

         $sql = "UPDATE order_item oi LEFT JOIN item i on oi.item_id = i.item_id SET oi.rate = i.minimum_sell_rate WHERE oi.order_id = $order_id && i.item_id = $new_id";

         $this->db->query($sql);

         return $this->reCalculateTTC($order_id);
     }

     private function reCalculateTTC($order_id)
     {
         $sql = "SELECT SUM(rate * quantity) as ht FROM order_item WHERE order_id = $order_id";
         $result = $this->db->query($sql);
         $ht = $result->row['ht'];
         $sql = "SELECT tva FROM `order` WHERE order_id = $order_id LIMIT 1";
         $result = $this->db->query($sql);
         $tax = $result->row['tva'];
         $ttc = $ht + ($ht * $tax) / 100;
         $sql = "UPDATE `order` SET htt = '$ht', ttc = '$ttc' WHERE order_id = $order_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

//     public function getOrderList($start=0, $limit=10,$where=" "){
//         $status = 2;
//         if($where == " "){
//             $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, client.company_name FROM client RIGHT JOIN
//             `order` ON (`client`.client_id = `order`.client) WHERE `order`.order_status = $status LIMIT $start, $limit";

//         $result = $this->db->query($sql);
//         return $result->rows;
//     }
     // }

     public function getCommissionHistory()
     {
         $result = $this->db->query("SELECT invoice.*,user.*, DATE_FORMAT(invoice.invoice_date, '%d-%m-%Y') as invoice_date FROM invoice LEFT JOIN user ON invoice.agent_id = user.user_id");

         return $result->rows;
     }

     public function getInvoiceDetails($invoice_id)
     {
         $result = $this->db->query("SELECT  invoice.*,user.*, DATE_FORMAT(invoice.invoice_date, '%d-%m-%Y') as invoice_date FROM invoice LEFT JOIN user ON invoice.agent_id = user.user_id WHERE invoice_id = ".$invoice_id);

         return $result->row;
     }

     public function getInvoiceOrderList($orders)
     {
         $orders = json_decode($orders, true);
         $order_q = implode(' || order_id = ', $orders);
         $sql = "SELECT `order`.*,client.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN client ON `order`.client = client.client_id WHERE order_id = $order_q";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function checkStock($item_id)
     {
         $sql = "SELECT current_stock, min_stock FROM item WHERE item_id = $item_id LIMIT 1";
         $result = $this->db->query($sql);

         return $result->row;
     }

     public function getBuyRate($item_id)
     {
         $sql = "SELECT purchase_rate FROM item WHERE item_id = $item_id LIMIT 1";
         $result = $this->db->query($sql);

         return $result->row['purchase_rate'];
     }

     public function getVendor($item_id)
     {
         $sql = "SELECT vendor FROM item WHERE item_id = $item_id LIMIT 1";
         $result = $this->db->query($sql);

         return $result->row['vendor'];
     }

     public function placePORequest($item_id, $quantity, $reference_order, $client = 0)
     {
         $vendor = $this->getVendor($item_id);
         $sql = "INSERT INTO `order` SET order_type = 1, order_status = 12, vendor=$vendor, raised_by = '".$this->session->loggedUser()."', delivery_address = $client, so_ref = $reference_order";

         $this->db->query($sql);

         $ref_id = $this->db->getLastId();

         $orderD['order_id'] = $ref_id;
         $orderD['item_id'] = $item_id;
         $orderD['quantity'] = $quantity;
         $orderD['discount'] = 0;
         $orderD['rate'] = $this->getBuyRate($item_id);
         $orderD['mrp'] = 0;
         $orderD['order_type'] = 1;
         $this->addOrderItems($orderD);
     }

     public function getBrandList($char = '')
     {
         $sql = "SELECT DISTINCT brand_name FROM `item` WHERE brand_name LIKE '%".$this->db->escape($char)."%'";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function itemListQ($char = '')
     {
         $sql = "SELECT * FROM `item` WHERE item_name LIKE '%".$this->db->escape($char)."%'";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function addToQueue($item, $quantity, $rate)
     {
         $sql = "INSERT INTO po_queue SET item_id = $item, quantity=$quantity, rate='".$this->db->escape($rate)."', responsible='".$this->session->loggedUser()."'";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getFromQueue($clause = '')
     {
         $sql = "SELECT i.*, pq.* FROM po_queue pq LEFT JOIN item i ON i.item_id = pq.item_id WHERE responsible='".$this->session->loggedUser()."'";
         if (trim($clause != '')) {
             $sql = 'SELECT i.*, pq.* FROM po_queue pq LEFT JOIN item i ON i.item_id = pq.item_id '.$clause." && responsible='".$this->session->loggedUser()."'";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function removeFromQueue($item_id, $all = false)
     {
         $sql = "DELETE FROM po_queue WHERE item_id = $item_id && responsible='".$this->session->loggedUser()."'";
         if ($all) {
             $sql = "DELETE FROM po_queue WHERE responsible='".$this->session->loggedUser()."'";
         }

         return $this->db->query($sql);
     }
 }
