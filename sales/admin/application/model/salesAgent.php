<?php
 class ModelSalesAgent extends Model
 {
     public function createUser($data = [], $files = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $lastLogin = date('Y-m-d h:i:s', time());
         $last_ip = $_SERVER['REMOTE_ADDR'];
         $status = (int) 0;
         $user_type = (int) 5;
         // $password = rand(1000,9999);
         //$city = rand(1000,9999);
         //$postal_code = rand(1000,9999);
         $files_j = json_encode($files);
         $this->db->query("INSERT INTO user SET 
 			user_type = '".$this->db->escape($user_type)."',
			username = '".$this->db->escape($data['username'])."', 
			name = '".$this->db->escape($data['name'])."', 
			email_id = '".$this->db->escape($data['email'])."', 
			password = '".$this->db->escape($data['password'])."', 
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			city = '".$this->db->escape($data['city'])."',
			postal_code = '".$this->db->escape($data['postal_code'])."',
			created = '".$this->db->escape($date)."',
			status = '".$this->db->escape($status)."',
			image = '".$this->db->escape($files_j)."',
			identification = '".$this->db->escape($files_j)."'
			");
         $id = $this->db->getLastId();
         $this->insertIntoAgent($id, $this->createSalesAgent($id), $data);
         //$this->session->login($user_id);
         return $id;
     }

     public function updateUser($id, $data = [])
     {
         $this->db->query("UPDATE user SET 
			email_id = '".$this->db->escape($data['email'])."',
			phone = '".$this->db->escape($data['phone'])."',
			password = '".$this->db->escape($data['password'])."',
			city = '".$this->db->escape($data['city'])."',
			postal_code = '".$this->db->escape($data['postal_code'])."',
			address = '".$this->db->escape($data['address'])."'
			WHERE user_id = $id");

         return true;
     }

     public function validate_username($username)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE username = '".$this->db->escape($username)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function getAllocateClient()
     {
         $result = $this->db->query('SELECT * FROM `client` LEFT JOIN user ON client.user_id = user.user_id');

         return $result->rows;
     }

     public function createSalesAgent($user_id)
     {
         $contract_type = (int) 0;
         $execution_date = (int) 0;
         $expiry_date = (int) 0;
         $approval_status = (int) 4;
         $this->db->query("INSERT INTO employee SET
			user_id = '".$this->db->escape($user_id)."',
			contract_type = '".$this->db->escape($contract_type)."',
			execution_date = '".$this->db->escape($execution_date)."',
			expiry_date= '".$this->db->escape($expiry_date)."',
			approval_status = '".$this->db->escape($approval_status)."'
			");
         $employee_id = $this->db->getLastId();
         //$this->session->login($user_id);

         //$this->createVendor($user_id);

         return $employee_id;
     }

     public function updateSalesAgent($employee_id, $data = [])
     {
         $this->db->query("UPDATE agent SET 
			
			
			commission_rate = '".$this->db->escape($data['commission_rate'])."'
			 WHERE employee_id = $employee_id");

         return $employee_id;
     }

     public function insertIntoAgent($user_id, $employee_id, $data)
     {
         $last_invoice = (int) 0;
         $this->db->query("INSERT INTO agent SET
			user_id = '".$this->db->escape($user_id)."',
			employee_id = '".$this->db->escape($employee_id)."',
			commission_rate = '".$this->db->escape($data['commission_rate'])."',
			last_invoice = '".$this->db->escape($last_invoice)."'
			");
         $agent_id = $this->db->getLastId();

         //$this->session->login($user_id);

         //$this->createVendor($user_id);

         return $agent_id;
     }

     public function getAgentList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT a.*, u.* FROM agent a LEFT JOIN user u  ON (a.user_id = u.user_id) WHERE a.status = 1 LIMIT $start, $limit");

         return $result->rows;
     }

     public function getCount()
     {
         $result = $this->db->query('SELECT * FROM agent WHERE status = 1');

         return $result->num_rows;
     }

     public function getAgentDetails($employee_id)
     {
         $result = $this->db->query('SELECT * FROM agent LEFT JOIN user ON user.user_id = agent.user_id WHERE agent.employee_id= '.$employee_id);

         return $result->row;
     }

     public function getOrderDetails($employee_id)
     {
         $result = $this->db->query("SELECT `order`.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, user.name, user.address FROM `order` LEFT JOIN user ON (`order`.raised_by = user.user_id)  WHERE raised_by = ".$employee_id);

         return $result->rows;
     }

     public function deleteAgent($employee_id)
     {
         $employee_id = (int) $employee_id;
         // order_status - 8 (purchase order cancelled)
         $sql = "UPDATE agent SET status = 0 WHERE employee_id = $employee_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function isSlabInValid($slab_in, $agent_id)
     {
         $slab_in = (int) $slab_in;
         $agent_id = (int) $agent_id;
         $sql = "SELECT commission_id FROM commission_rate WHERE  slab_in <= $slab_in && slab_out > $slab_in && agent_id = $agent_id";
         $result = $this->db->query($sql);

         return $result->num_rows;
     }

     public function isSlabOutValid($slab_out, $agent_id)
     {
         $slab_out = (int) $slab_out;
         $agent_id = (int) $agent_id;
         $sql = "SELECT commission_id FROM `commission_rate` WHERE  slab_out >= $slab_out && slab_in < $slab_out && agent_id = $agent_id";
         $result = $this->db->query($sql);

         return $result->num_rows;
     }

     public function setCommission($data = [])
     {
         $sql = "INSERT INTO commission_rate SET slab_in = '".$this->db->escape($data['slab_in'])."', slab_out = '".$this->db->escape($data['slab_out'])."', agent_id = '".$this->db->escape($data['agent_name'])."', commission_rate = '".$this->db->escape($data['commission_rate'])."'";
         $this->db->query($sql);
     }

     public function getCommissionRates($employee_id)
     {
         $sql = "SELECT commission_rate.* FROM agent LEFT JOIN commission_rate ON agent.employee_id = commission_rate.agent_id WHERE agent.employee_id= $employee_id";
         $result = $this->db->query($sql);

         return $result->rows;
     }
 }
