<?php
 class ModelReturn extends Model
 {
     public function getClientName()
     {
         $result = $this->db->query('SELECT client.client_id , `user`.`username`, client.company_name FROM `client`  LEFT JOIN `user` ON (`client`.user_id = `user`.user_id) ');

         return $result->rows;
     }

     public function getReason()
     {
         $result = $this->db->query('SELECT * FROM return_reason');

         return $result->rows;
     }

     public function getOrders()
     {
         $result = $this->db->query('SELECT * FROM `order`');

         return $result->rows;
     }

     public function getOrderDetails($client_id)
     {
         $result = $this->db->query('SELECT * FROM `order` WHERE client ='.$client_id);

         return $result->rows;
     }

     public function getItemListByClient($client_id)
     {
         $result = $this->db->query("select i.* from item i right join  (SELECT o.*, oi.item_id FROM `order` o left join order_item oi on oi.order_id = o.order_id WHERE o.client = '".$client_id."') ooi on ooi.item_id = i.item_id GROUP BY ooi.item_id ");

         return $result->rows;
     }

     public function createReturnOrder($data = [])
     {
         $this->db->query("INSERT INTO `order` SET
			order_date = '".$this->db->escape($data['return_date'])."',
			client = '".$this->db->escape($data['client_name'])."',
			return_reason = '".$this->db->escape($data['reason'])."',
            comment = '".$this->db->escape($data['description'])."',
            person_responsible = '".$this->db->escape($data['person_responsible'])."',
			order_type = 4
			");
         $order_id = $this->db->getLastId();
         $htt = 0;
         foreach ($data['selected_item'] as $item):
                $quan = (int) $data['quantity'][$item];
         $htt += $data['return_rate'][$item] * $quan;
         if ($quan < 1) {
             $quan = 1;
         }
         $this->db->query("INSERT INTO order_item SET
                item_id = '".$this->db->escape($item)."',
                order_id = '".$this->db->escape($order_id)."', quantity = $quan, rate = ".$data['return_rate'][$item].'
                ');
         endforeach;
         $tax_amount = ($htt * TAX) / 100;
         $ttc = $htt + $tax_amount;
         $this->db->query("UPDATE `order` SET ttc = '$ttc', htt = '$htt' WHERE order_id = $order_id");

         return $order_id;
     }

     public function getItemsList()
     {
         $result = $this->db->query('SELECT * FROM item');

         return $result->rows;
     }

     public function getReturnSalesOrder($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date  FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 4 && order_status = 6 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date  FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 4 && order_status 
            = 6 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getReturnOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date , DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function getReturnSalesOrderHistory($start = 0, $limit = 10, $clause = '')
     {
         $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date  FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 4  LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 4  LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function approveOrder($order_id)
     {
         $order_id = (int) $order_id;
         // order_status - 9 (sales order cancelled)
         $sql = "UPDATE `order` SET order_status = 6 WHERE order_id = $order_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function disapproveOrder($order_id)
     {
         $order_id = (int) $order_id;
         // order_status - 9 (sales order cancelled)
         $sql = "DELETE FROM `order` WHERE order_id = $order_id";
         $this->db->query($sql);
         $this->db->query("DELETE FROM `order_item` WHERE order_id = $order_id");

         return $this->db->countAffected();
     }

     public function orderCount($status)
     {
         $result = $this->db->query('SELECT order_id FROM `order` WHERE   order_status = '.$status);

         return $result->num_rows;
     }
 }
