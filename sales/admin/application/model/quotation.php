<?php
 class Modelquotation extends Model
 {
     public function createQuotation($data = [])
     {
         $this->db->query("INSERT INTO `quotation` SET
            created_at = '".$this->db->escape($data['order_date'])."',
            `client` = '".$this->db->escape($data['client_id'])."',
            agent_id = '".$this->db->escape($data['raised_by'])."',
            title = '".$this->db->escape($data['title'])."'
             ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function getPaymentModes()
     {
         $result = $this->db->query("SELECT * FROM payment_mode WHERE lang = '".$this->session->language."'");

         return $result->rows;
     }

     public function addOrderItems($data = [])
     {
         $this->db->query("INSERT INTO `quotation_item` SET
            quotation_id = '".$this->db->escape($data['order_id'])."',
            item_id = '".$this->db->escape($data['item_id'])."',
            quantity = '".$this->db->escape($data['quantity'])."',
            offered_rate = '".$this->db->escape($data['rate'])."'
             ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function getClientsList()
     {
         $result = $this->db->query('SELECT c.*, u.*, c.company_name as name FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id)');

         return $result->rows;
     }

     public function getClientsAddress($client_id)
     {
         $result = $this->db->query('SELECT user.address,user.city,user.postal_code FROM client LEFT JOIN user ON client.user_id = user.user_id WHERE client.client_id = '.$client_id);

         return $result->row;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT item.image, item.item_pdf, item.item_name, item.reference, item.item_id  FROM item WHERE item.item_id = '.$item_id);

         return $result->row;
     }

     public function getCategoryList($brand)
     {
         $result = $this->db->query("SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.brand_name = '".$this->db->escape($brand)."' ORDER BY title");

         return $result->rows;
     }

     public function getAreaList()
     {
         $result = $this->db->query('SELECT DISTINCT item.area FROM item');

         return $result->rows;
     }

     public function getAllItemsList($clause = '', $limit = 15, $start = 0)
     {
         $result = $this->db->query('SELECT * FROM item '.$clause." LIMIT $start, $limit");

         return $result->rows;
     }

     public function getAllItemsTotal($clause)
     {
         $result = $this->db->query('SELECT COUNT(item_id) as total FROM item  '.$clause);

         return $result->row['total'];
     }

     public function getAllItemsListWithClient($clause)
     {
         $result = $this->db->query('SELECT * FROM item LEFT JOIN  (SELECT user.address , user.city, user.email_id, client.client_id, client.company_name, client.client_type FROM user LEFT JOIN client ON client.user_id = user.user_id) u ON u.client_id = item.client '.$clause);

         return $result->rows;
     }

     public function getClientType($type)
     {
         $result = $this->db->query('SELECT title FROM client_type WHERE client_type_id = '.$type);

         return $result->row;
     }

     public function getClientDetails($client_id)
     {
         $result = $this->db->query('SELECT * FROM client LEFT JOIN user ON user.user_id = client.user_id WHERE client.client_id= '.$client_id);

         return $result->row;
     }

     public function getPendingOrderList($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT quotation.*, client.company_name,  DATE_FORMAT(`quotation`.created_at, '%d-%m-%Y') as created_at FROM quotation LEFT JOIN client ON client.client_id = quotation.client LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 5 && order_status = 14 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getCompleteOrderList($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 && (order_status = 4 || order_status = 5) LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 2 && (order_status = 4 || order_status = 5) LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getCancelledOrderList($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 && order_status = 9 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*,DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 2 && order_status = 9 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT client.company_name, quotation.*, DATE_FORMAT(`quotation`.created_at, '%d-%m-%Y') as created_at FROM quotation LEFT JOIN client ON client.client_id = quotation.client  WHERE quotation_id = ".$order_id);

         return $result->rows;
     }

     public function getClientFromQuotation($quotation_id)
     {
         $sql = "SELECT client FROM quotation WHERE quotation_id = $quotation_id LIMIT 1";
         $res = $this->db->query($sql);

         return $res->row['client'];
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT quotation_item.*, item.item_name, item.reference, item.brand_name FROM quotation_item LEFT JOIN item ON  quotation_item.item_id = item.item_id WHERE quotation_id = '.$order_id);

         return $result->rows;
     }

     public function getUserDetails($user_id)
     {
         $result = $this->db->query('SELECT * FROM user LEFT JOIN user_type ON user.user_type = user_type.user_type_id WHERE user_id = '.$user_id);

         return $result->row;
     }

     public function orderCount($status = 0)
     {
         $result = $this->db->query("SELECT quotation_id FROM `quotation` WHERE status = $status");

         return $result->num_rows;
     }

     public function cancelOrder($order_id)
     {
         $order_id = (int) $order_id;
         $sql = "DELETE FROM `quotation` WHERE quotation_id = $order_id";
         $this->db->query($sql);
         $sql = "DELETE FROM `quotation_item` WHERE quotation_id = $order_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getRefList($ref = 0, $brand = '', $category = '')
     {
         $sql = 'SELECT DISTINCT order_id FROM `order` WHERE order_status = 2';
         if ($ref) {
             $sql = "SELECT DISTINCT reference FROM `item` WHERE brand_name='".$this->db->escape($brand)."'";
             if ($category != '') {
                 $sql .= " && item_category = '".$this->db->escape($category)."'";
             }
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function validateReference($ref = 0)
     {
         $sql = "SELECT DISTINCT item_id FROM `item` WHERE reference = $ref";
         $result = $this->db->query($sql);

         return (isset($result->row['item_id'])) ? $result->row['item_id'] : false;
     }

     public function changeItem($order_id, $reference, $new_id)
     {
         $sql = "SELECT * FROM order_bol ob LEFT JOIN item i ON ob.item_id = ob.item_id WHERE i.reference = $reference && ob.order_id = $order_id";
         $result = $this->db->query($sql);

         if ($result->num_rows) {
             return false;
         }

         $sql = "UPDATE order_item oi LEFT JOIN item i on oi.item_id = i.item_id SET oi.item_id = $new_id WHERE oi.order_id = $order_id && i.reference = $reference";
         $this->db->query($sql);

         $sql = "UPDATE order_item oi LEFT JOIN item i on oi.item_id = i.item_id SET oi.rate = i.minimum_sell_rate WHERE oi.order_id = $order_id && i.item_id = $new_id";

         $this->db->query($sql);

         return $this->reCalculateTTC($order_id);
     }

     private function reCalculateTTC($order_id)
     {
         $sql = "SELECT SUM(rate * quantity) as ht FROM order_item WHERE order_id = $order_id";
         $result = $this->db->query($sql);
         $ht = $result->row['ht'];
         $sql = "SELECT tva FROM `order` WHERE order_id = $order_id LIMIT 1";
         $result = $this->db->query($sql);
         $tax = $result->row['tva'];
         $ttc = $ht + ($ht * $tax) / 100;
         $sql = "UPDATE `order` SET htt = '$ht', ttc = '$ttc' WHERE order_id = $order_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

//     public function getOrderList($start=0, $limit=10,$where=" "){
//         $status = 2;
//         if($where == " "){
//             $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, client.company_name FROM client RIGHT JOIN
//             `order` ON (`client`.client_id = `order`.client) WHERE `order`.order_status = $status LIMIT $start, $limit";

//         $result = $this->db->query($sql);
//         return $result->rows;
//     }
     // }

     public function getCommissionHistory()
     {
         $result = $this->db->query("SELECT invoice.*,user.*, DATE_FORMAT(invoice.invoice_date, '%d-%m-%Y') as invoice_date FROM invoice LEFT JOIN user ON invoice.agent_id = user.user_id");

         return $result->rows;
     }

     public function getInvoiceDetails($invoice_id)
     {
         $result = $this->db->query("SELECT  invoice.*,user.*, DATE_FORMAT(invoice.invoice_date, '%d-%m-%Y') as invoice_date FROM invoice LEFT JOIN user ON invoice.agent_id = user.user_id WHERE invoice_id = ".$invoice_id);

         return $result->row;
     }

     public function getInvoiceOrderList($orders)
     {
         $orders = json_decode($orders, true);
         $order_q = implode(' || order_id = ', $orders);
         $sql = "SELECT `order`.*,client.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN client ON `order`.client = client.client_id WHERE order_id = $order_q";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function checkStock($item_id)
     {
         $sql = "SELECT current_stock, min_stock FROM item WHERE item_id = $item_id LIMIT 1";
         $result = $this->db->query($sql);

         return $result->row;
     }

     public function getBuyRate($item_id)
     {
         $sql = "SELECT purchase_rate FROM item WHERE item_id = $item_id LIMIT 1";
         $result = $this->db->query($sql);

         return $result->row['purchase_rate'];
     }

     public function placePORequest($item_id, $quantity, $reference_order, $client = 0)
     {
         $sql = "INSERT INTO `order` SET order_type = 1, order_status = 12, client=$client, raised_by = '".$this->session->loggedUser()."', delivery_address = $client, so_ref = $reference_order";

         $this->db->query($sql);

         $ref_id = $this->db->getLastId();

         $orderD['order_id'] = $ref_id;
         $orderD['item_id'] = $item_id;
         $orderD['quantity'] = $quantity;
         $orderD['discount'] = 0;
         $orderD['rate'] = $this->getBuyRate($item_id);
         $orderD['mrp'] = 0;
         $orderD['order_type'] = 1;
         $this->addOrderItems($orderD);
     }

     public function getBrandList($char = '')
     {
         $sql = "SELECT DISTINCT brand_name FROM `item` WHERE brand_name LIKE '%".$this->db->escape($char)."%'";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function itemListQ($char = '')
     {
         $sql = "SELECT * FROM `item` WHERE item_name LIKE '%".$this->db->escape($char)."%'";
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function markApproved($q, $i)
     {
         $sql = "UPDATE quotation_item SET status = 1 WHERE quotation_id = $q && item_id = $i";
         $this->db->query($sql);
     }

     public function offerChange($q, $i, $v)
     {
         $sql = "UPDATE quotation_item SET offered_rate = '".$v."' WHERE quotation_id = $q && item_id = $i";
         $this->db->query($sql);
     }

     public function quanChange($q, $i, $v)
     {
         $sql = "UPDATE quotation_item SET quantity = '".$v."' WHERE quotation_id = $q && item_id = $i";
         $this->db->query($sql);
     }
 }
