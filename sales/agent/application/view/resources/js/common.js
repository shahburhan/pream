
    function initButtonGroups(){
        var buttonGroup = $($(".btn-grp"));
        var buttonActiveBgcolor = "#2196F3";
        var buttonActivTextcolor = "#fff";
        var buttoninActiveBgcolor = "#C6CDD0";
        var buttoninActivTextcolor = "#000";
        for(var i = 0; i < buttonGroup.length;  i++){
            if( $(buttonGroup[i]).children(".selected").length > 0){
                $($($(buttonGroup[i]).children(".selected")).children()[0]).css("background-color",buttonActiveBgcolor);
                $($($(buttonGroup[i]).children(".selected")).children()[0]).css("color",buttonActivTextcolor);
            }else{
                $($($(buttonGroup[i]).children()[0]).children()[0]).css("background-color",buttonActiveBgcolor);
                $($($(buttonGroup[i]).children()[0]).children()[0]).css("color",buttonActivTextcolor);
                $($(buttonGroup[i]).children()[0]).addClass("selected");

            }
            $($(buttonGroup[i]).children()).click(function(){
                var buttonGroupT = $(this).parent();
                for(var i = 0; i < buttonGroupT.children().length; i++){
                    $($(buttonGroupT.children()[i]).children()[0]).css("background-color",buttoninActiveBgcolor);
                    $($(buttonGroupT.children()[i]).children()[0]).css("color",buttoninActivTextcolor);
                    $(buttonGroupT.children()[i]).removeClass("selected");
                }
                $($(this).children()[0]).css("background-color",buttonActiveBgcolor)
                $($(this).children()[0]).css("color",buttonActivTextcolor);
                $(this).addClass("selected");
            });
        }
    }




    function Dialog(xDialog){
        var _this = this;
        this.dialog = xDialog;
        this.dialog.css("top","-"+ this.dialog.css("height"));
        this.header = $($(".m-dialog-header")[0]);
        this.body = $(".m-dialog-body");
        this.footer = $($(".m-dialog-footer")[0]);
        this.title = $(".m-dialog-title");
        this.actionButton = null;
        this.orgView = null;
        this.view = null;
        this.refreshBtn = $("#m-dialog-refresh-btn");
        this.submitBtn = $("#m-dialog-submit-btn");
        this.resetBtn = $("#m-dialog-reset-btn");

        this.start = function(obj,type,actBtn,mView){
            switch (type){
                case "form":
                    _this.refreshBtn.hide();
                    _this.submitBtn.show();
                    _this.resetBtn.show();
                    break;
                case "detail":
                    _this.refreshBtn.show();
                    _this.submitBtn.hide();
                    _this.resetBtn.hide();
                    break;
                case "table":
                    _this.refreshBtn.show();
                    _this.submitBtn.hide();
                    _this.resetBtn.hide();
                    break;
            }
            _this.actionButton = actBtn;
            _this.orgView = mView;
            _this.loadView(_this.orgView);
            _this.show();

        };

        $("#m-dialog-close-btn").click(function () {
            _this.hide();

        });

        this.isOpen = function(){
            return _this.dialog.hasClass("opened");
        };

        window.addEventListener("resize",function () {
            if(_this.isOpen())_this.show();
        });


        this.loadView = function(mView){
            _this.view = $(mView).children(".view-body");
            _this.body.html(_this.view);
            _this.title.html(($(mView).children(".view-title").html()));

        };

        this.removeView = function(){
            _this.view = $(_this.orgView).append(_this.view);
            _this.view = null;
            _this.body.html("");
            _this.title.html("");
        };


        this.show = function(){
            if($(".mdl-layout").hasClass("is-small-screen")){
                _this.dialog.css("top",0);
                _this.dialog.css("height","100%");
            }else{
                _this.dialog.css("top",$("header").css("height"));
            }
            _this.dialog.css("height",$(".mdl-layout__content").css("height"));
            _this.body.css("height","" + parseInt($(".mdl-layout__content").css("height").substring(0,$(".mdl-layout__content").css("height").length)) - 60 + "px");
            _this.dialog.removeClass("closed");
            _this.dialog.addClass("opened");

        };

        this.hide = function(){

            _this.dialog.css("top","-"+ _this.dialog.css("height"));
            _this.dialog.removeClass("opened");
            _this.dialog.addClass("closed");
            _this.removeView();
            _this.submitBtn.unbind();
            _this.resetBtn.unbind();
        };
    };


    function Table(tableRoot){
        _this = this;
        this.table = tableRoot;
        this.tBody = this.table.children("tbody");
        this.parentRows = this.table.children("tbody").children(".parent-row");
        this.childRows = this.table.children("tbody").children(".child-row");
        this.parentRows.click(expandChild);
        this.parentRows.children().children("button").unbind(expandChild);
        function expandChild(){
            var child = getChild($(this));
            if(child.hasClass("child-row-collapse")) {
                _this.childRows.addClass("child-row-collapse");
                _this.childRows.children().addClass("child-row-collapse");
                child.removeClass("child-row-collapse");
                child.children().removeClass("child-row-collapse");
            }else{
                _this.childRows.addClass("child-row-collapse");
                _this.childRows.children().addClass("child-row-collapse");
            }
        }
        function getChild (parentR){
            return _this.tBody.children(".child-row[for='"+ parentR.attr("id") +"']");
        }
        function getParent(child){
            return $("#"+child.attr("for"));
        }
    }

    var selectBox;
    var selctizedBoxInstance;
    function initFilterSelectBox(){
        selectBox = $(".selectize-filter").selectize({
            options: [
                {id: 'avenger', make: 'dodge', model: 'Avenger'},
                {id: 'caliber', make: 'dodge', model: 'Caliber'},
                {id: 'caravan-grand-passenger', make: 'dodge', model: 'Caravan Grand Passenger'},
                {id: 'challenger', make: 'dodge', model: 'Challenger'},
                {id: 'ram-1500', make: 'dodge', model: 'Ram 1500'},
                {id: 'viper', make: 'dodge', model: 'Viper'},
                {id: 'a3', make: 'audi', model: 'A3'},
                {id: 'a6', make: 'audi', model: 'A6'},
                {id: 'r8', make: 'audi', model: 'R8'},
                {id: 'rs-4', make: 'audi', model: 'RS 4'},
                {id: 's4', make: 'audi', model: 'S4'},
                {id: 's8', make: 'audi', model: 'S8'},
                {id: 'tt', make: 'audi', model: 'TT'},
                {id: 'avalanche', make: 'chevrolet', model: 'Avalanche'},
                {id: 'aveo', make: 'chevrolet', model: 'Aveo'},
                {id: 'cobalt', make: 'chevrolet', model: 'Cobalt'},
                {id: 'silverado', make: 'chevrolet', model: 'Silverado'},
                {id: 'suburban', make: 'chevrolet', model: 'Suburban'},
                {id: 'tahoe', make: 'chevrolet', model: 'Tahoe'},
                {id: 'trail-blazer', make: 'chevrolet', model: 'TrailBlazer'},

            ],
            optgroups: [
                {id: 'dodge', name: 'Client'},
                {id: 'audi', name: 'Vendor'},
                {id: 'chevrolet', name: 'Salesman'}
            ],
            labelField: 'model',
            valueField: 'id',
            optgroupField: 'make',
            optgroupLabelField: 'name',
            optgroupValueField: 'id',
            optgroupOrder: ['chevrolet', 'dodge', 'audi'],
            searchField: ['model'],
            plugins: ['optgroup_columns']
        });
        if(selectBox.length !=0 )selctizedBoxInstance =  selectBox[0].selectize;
    }
    //##Usage
    //Name for Selected option -- selctizedBoxInstance.options[selctizedBoxInstance.getValue()].make
    //Value for Selected option -- selctizedBoxInstance.getValue()

    function showSnackbar(message) {
        var data = {
            message: message,
            timeout: 2000,
            actionText: 'Undo'
        };
        $("#snackbar-container")[0].MaterialSnackbar.showSnackbar(data);
    }

    var expandableTable;
    var MDialog;
    $(document).ready(function () {
        $(".mdl-progress").hide();   //Progress Bar hide on load complete
       // expandableTable = new Table($("table.expandable"));

        initButtonGroups();
        MDialog = new Dialog($("#m-dialog"));
        initFilterSelectBox();
        $(document).keyup(function (event) {
            if(event.keyCode == 27 && $(".drawer").hasClass("shown")){
                closeDrawer();
                if(MDialog.isOpen())MDialog.hide();
            }
        });
    });

    $('.selectize').selectize({
        sortField: 'text',
        theme: 'links'
    });