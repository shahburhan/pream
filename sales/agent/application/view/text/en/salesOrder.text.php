<?php

$text_salesOrder['title'] = 'New Order';
$text_salesOrder['title_orders'] = 'Sales Orders';
//form label
$text_salesOrder['order_heading'] = 'New Order';
$text_salesOrder['order_number'] = 'Order Number';
$text_salesOrder['order_date'] = 'Order Date';
$text_salesOrder['client_name'] = 'Name of Client';
$text_salesOrder['address'] = 'Address';
$text_salesOrder['city'] = 'City';
$text_salesOrder['postal_code'] = 'Postal Code';
$text_salesOrder['delivery_address'] = 'Address of Delivery';
$text_salesOrder['client_type'] = 'Client Type';
$text_salesOrder['person_responsible'] = 'Person Responsible';
$text_salesOrder['expected_date'] = 'Expected Date';
$text_salesOrder['select_items'] = 'Select Items';
$text_salesOrder['items_selected'] = 'Items Selected';
$text_salesOrder['htt'] = 'HTT';
$text_salesOrder['tva'] = 'TVA';
$text_salesOrder['ttc'] = 'TTC';
$text_salesOrder['details'] = 'Details';
$text_salesOrder['grand_total'] = 'Grand Total';
$text_salesOrder['total_amount'] = 'Total Amount';
//btn
$text_salesOrder['add_items'] = 'Add Items';
$text_salesOrder['reset'] = 'Reset';
$text_salesOrder['search'] = 'Search';
$text_salesOrder['submit_neworder'] = 'Submit';
$text_salesOrder['cancel'] = 'Cancel';
//table
$text_salesOrder['order_detail'] = 'Create Sales Order';
$text_salesOrder['sr_no'] = 'Sr.No.';
$text_salesOrder['reference'] = 'Reference';
$text_salesOrder['item_id'] = 'Item Id';
$text_salesOrder['item_name'] = 'Item Name';
$text_salesOrder['brand_name'] = 'Brand Name';
$text_salesOrder['stock'] = 'Stock';
$text_salesOrder['vendor_name'] = 'Vendor Name';
$text_salesOrder['packing'] = 'Packing';
$text_salesOrder['item_area'] = 'Item <br> Area';
$text_salesOrder['sell_rate'] = 'Sell Rate';
$text_salesOrder['mrp'] = 'MRP';
$text_salesOrder['reference'] = 'Reference';
$text_salesOrder['quantity'] = 'Quantity';
$text_salesOrder['confirm'] = 'Confirm Order';
$text_salesOrder['send_mail'] = 'Send Mail';
$text_salesOrder['status'] = 'Status';
$text_salesOrder['order_action'] = 'Action';
$text_salesOrder['order_id'] = 'SO#';
//view details
$text_salesOrder['sales_order'] = 'Sales Order';
$text_salesOrder['so'] = 'SO#';
$text_salesOrder['confirm'] = 'Confirm Order';
$text_salesOrder['send_mail'] = 'Send Mail';
$text_salesOrder['status'] = 'Status';
$text_salesOrder['client_address'] = 'Client Address';
$text_salesOrder['expected_date'] = 'Expected Date';
$text_salesOrder['person_responsible'] = 'Person Responsible';
$text_salesOrder['raised_by'] = 'Raised By';
$text_salesOrder['unit_price'] = 'Unit Price';
$text_salesOrder['total'] = 'Total';
$text_salesOrder['delivery_date'] = 'Delivery Date';
$text_salesOrder['action'] = 'Action';
