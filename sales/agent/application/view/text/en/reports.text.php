<?php

//new expense
$text_reports['weekly'] = 'Weekly';
$text_reports['monthly'] = 'Monthly';
$text_reports['quaterly'] = 'Quaterly';
$text_reports['yearly'] = 'Yearly';
$text_reports['completed'] = 'Completed Orders';
$text_reports['pending'] = 'Pending Orders';
$text_reports['cancelled'] = 'Cancelled Orders';
$text_reports['total'] = 'Total Orders';
$text_reports['order_id'] = 'Order Id';
$text_reports['order_date'] = 'Order Date';
$text_reports['client_name'] = 'Client Name';
$text_reports['raised_by'] = 'Raised By';
