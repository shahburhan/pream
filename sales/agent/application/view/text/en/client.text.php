<?php

$text_client['title'] = 'Create Client';
$text_client['title_view'] = 'View Clients';
//form label
$text_client['name'] = 'Name';
$text_client['last_name'] = 'Last Name';
$text_client['client_type'] = 'Client Type';
$text_client['password'] = 'Password';
$text_client['type'] = 'Type';
$text_client['email'] = 'Email';
$text_client['phone'] = 'Phone';
$text_client['address_c'] = 'Company Address';
$text_client['address'] = 'Address';
$text_client['register_address'] = 'Company Registration Address';
$text_client['set_credit_limit'] = 'Set Credit Limit';
$text_client['update_credit_limit'] = 'Update Credit Limit';
$text_client['special_offer'] = 'Special Offer';
$text_client['on_purchase_of'] = 'On Purchase Of';
$text_client['username_c'] = 'Company Name';
$text_client['username'] = 'Username';
$text_client['company'] = 'Company';
$text_client['individual'] = 'Individual';
$text_client['create_c'] = 'Create Client - Company';
$text_client['intercommunataire'] = 'Intercommunataire (Number)';
$text_client['bfa_document'] = 'BFA Document Upload';
$text_client['authorized'] = 'Authorized Person';
$text_client['person_name'] = 'Person Name';
$text_client['create_i'] = 'Create Client - Individual';
$text_client['upload'] = 'Document Upload';
$text_client['title_edit'] = 'Edit Client';
$text_client['details'] = 'Details';

//$text_client['username_i'] = "Individual Name";
$text_client['field_required'] = 'This Field Is Required';

//btn
$text_client['submit'] = 'submit';
$text_client['add'] = 'Add';
$text_client['edit'] = 'Edit';
//view client
$text_client['search'] = 'Search';
$text_client['propose_limit'] = 'Propose Credit Limit';

//client details
$text_client['client_details'] = 'Client Details';
$text_client['so'] = 'SO#';
$text_client['client_id'] = 'Client Id';
$text_client['client_name'] = 'Client Name';
$text_client['client_address'] = 'Client Address';
$text_client['client_email'] = 'Client Email';
$text_client['delivery_date'] = 'Delivery Date';
$text_client['person_responsible'] = 'Person Responsible';
$text_client['delivery_address'] = 'Delivery Address';
$text_client['document_upload'] = 'Document Upload';
$text_client['authorized'] = 'Authorized Person';
$text_client['create_client_c'] = 'Create Client - Company';
$text_client['create_client_i'] = 'Create Client - Individual';
$text_client['update'] = 'Update';
//so#
$text_client['order_date'] = 'Order Date';
$text_client['order_id'] = 'Order Id';
$text_client['order_status'] = 'Order Status';
$text_client['ttc'] = 'TTC';
$text_client['payment_type'] = 'Payment Type';
$text_client['sales_agent'] = 'Sales Agent';
$text_client['purchaser'] = 'Purchaser';

//view clients
$text_client['name_c'] = 'Name';
$text_client['client_id'] = 'Client Id';
$text_client['credit_limit'] = 'Credit Limit';
$text_client['email_c'] = 'Email';
$text_client['phone_c'] = 'Phone';
$text_client['address_c'] = 'Address';
$text_client['action_c'] = 'Action';

//item details
$text_client['reference'] = 'Reference';
$text_client['item_name'] = 'Item Name';
$text_client['brand_name'] = 'Brand Name';
$text_client['item_category'] = 'Item Category';
$text_client['action'] = 'Action';
