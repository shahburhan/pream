<?php

$text_common['overview'] = 'Overview';
$text_common['client'] = 'Client';
$text_common['stock'] = 'Stock';
$text_common['sales_order'] = 'Sales Order';
$text_common['sales_target'] = 'Sales Target';
$text_common['reports'] = 'Reports';
$text_common['allowance'] = 'Allowance';
$text_common['commission'] = 'Commission';

//client
$text_common['create_client'] = 'create';
$text_common['view_client'] = 'View';

//stock
$text_common['view_stock'] = 'View';
$text_common['show_stock'] = 'Stock';

//sales order
$text_common['new_order'] = 'New Order';
$text_common['view_order'] = 'View';
$text_common['cancelled_order'] = ' Cancelled Order';
$text_common['request_order'] = 'Request Quote';

//sales target
$text_common['my_target'] = 'My Target';
$text_common['current_target'] = 'Current Sale Stats';

//reports
$text_common['sales_reports'] = 'Sales statistics';
$text_common['annual_reports'] = 'Annual/Quaterly/Monthly';

//allowance
$text_common['new_expense'] = 'Expense';
$text_common['view_expense'] = 'View Expense';

//commission
$text_common['generate_invoice'] = 'Generate Invoice';
$text_common['history'] = 'History';
