<?php

$text_requestOrder['title'] = 'Request Quote';

//form label
$text_requestOrder['req_heading'] = 'Request Quote For Bulk';
$text_requestOrder['item_req'] = 'Item Name';
$text_requestOrder['vendor_req'] = 'Vendor Name';
$text_requestOrder['brand_req'] = 'Brand Name';
$text_requestOrder['sell_req'] = 'Sell Rate';
$text_requestOrder['profit'] = 'Profit';
$text_requestOrder['quantity'] = 'Quantity';

//btn
$text_requestOrder['submit_req'] = 'Submit';
