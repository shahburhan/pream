<?php

$text_commission['invoice_title'] = 'Invoice';

//table headings
$text_commission['order_id'] = 'Order ID';
$text_commission['order_amount'] = 'Order Amount';
$text_commission['order_status'] = 'Order Status';
$text_commission['order_date'] = 'Order Date';
$text_commission['client'] = 'Client';
$text_commission['commission_rate'] = 'Commission Rate';
$text_commission['subtotal'] = 'Subtotal';
$text_commission['tax'] = 'Tax';
$text_commission['tds'] = 'TDS';
$text_commission['total'] = 'Total';

//btn
$text_commission['generate'] = 'Generate';
$text_commission['add_expense'] = 'Add Expense';

//commission history
$text_commission['title'] = 'Commission History';
//table headings
$text_commission['gen_invoice_id'] = 'Invoice Id';
$text_commission['gen_tax'] = 'Tax';
$text_commission['search'] = 'Search';
$text_commission['TSM'] = 'Total Sales';
$text_commission['total_commission'] = 'Total Commission';
$text_commission['gen_date'] = 'Generation Date';
$text_commission['gen_status'] = 'Status';
$text_commission['gen_action'] = 'Action';

//agents
$text_commission['agent_invoice'] = 'Agent Invoice';
$text_commission['invoice'] = 'Invoice#';
$text_commission['grand_total'] = 'Grand Total';
$text_commission['invoice_date'] = 'Invoice Date';
$text_commission['due_date'] = 'Due Date';
$text_commission['invoice_type'] = 'Invoice Type';
$text_commission['invoice_amount'] = 'Invoice Amount';
$text_commission['generation_date'] = 'Generation Date';
$text_commission['gen_action'] = 'Action';
$text_commission['make_payment'] = 'Make Payment';

//action
$text_commission['details'] = 'View Details';
