<?php

class ControllerReports extends Controller
{
    public function index()
    {
        $data = [];
        $data['page_title'] = 'Purchase Order';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }

    public function sales()
    {
        $data = [];
        $header['page_title'] = 'Report - Sales';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Report', 'href' => '?route=reports/sales'];
        $breadcrumb[] = ['title'=>'Sales', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');

        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';

        $this->controller_header->load($header);
        $this->load->text('reports');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('reports', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function item()
    {
        $data = [];
        $data['page_title'] = 'Item Report';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }

    public function vendor()
    {
        $data = [];
        $data['page_title'] = 'Vendor Report';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }
}
