<?php

class ControllerAllowance extends Controller
{
    public function index()
    {
        $data = [];
        $data['page_title'] = 'Allowance';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('allowance', $data);
    }

    public function newExpense()
    {
        $data = [];
        $data['page_title'] = 'Expense';
        $this->load->text('expense');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('newExpense', $data);
    }

    public function viewExpense()
    {
        $data = [];
        $data['page_title'] = 'View Expense';
        $this->load->text('expense');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('viewExpense', $data);
    }
}
