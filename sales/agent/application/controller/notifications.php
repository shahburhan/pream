<?php

class ControllerNotifications extends Controller
{
    public function index()
    {
        $data = [];
        $header['page_title'] = 'Notifications';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        $this->load->model('notification');
        $this->load->model('activity');

        $notifications = $this->model_notification->getNotifications($this->session->data['logged_user_id']);
        for ($i = 0; $i < count($notifications); $i++) {
            $notifications[$i]['activity_type'] = $this->model_activity->getActivityType($notifications[$i]['activity_type']);
        }
        $data['notifications'] = $notifications;

        $this->load->view('notifications_full', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function stockAlert()
    {
        $data = [];
        $header['page_title'] = 'Minimum Stock Alert';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('stock_details', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function residueStock()
    {
        $data = [];
        $header['page_title'] = 'Residue Stock Alert';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('residue_stock', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function newSalesAlert()
    {
        $data = [];
        $header['page_title'] = 'New Sales Alert';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('sales_details', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function newItemsAlert()
    {
        $data = [];
        $header['page_title'] = 'New Items Added';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('item_details', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function requestAlert()
    {
        $data = [];
        $header['page_title'] = 'Request Orders';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('request_details', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function poAlert()
    {
        $data = [];
        $header['page_title'] = 'PO Received';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('purchase_details', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function stockUpdate()
    {
        $data = [];
        $header['page_title'] = 'Stock Update';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('stock_update', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function orderDispatch()
    {
        $data = [];
        $header['page_title'] = 'Order Dispatch';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        //$this->load->text("requestOrder");

        //foreach ($this->text as $key => $value) {
        //assign text variables
        //$data['text_'.$key] = $value;
        // }

        $this->load->view('order_dispatch', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function auditAlert()
    {
        $data = [];
        $header['page_title'] = 'Audit';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $data['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        $this->load->text('audit');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('audit', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
