<?php

class ControllerMessage extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Reports';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function inbox()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Inbox';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Message', 'href' => '?route=message/compose'];
        $breadcrumb[] = ['title' => 'Inbox', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $header['script'][] = 'application/view/resources/js/moment/min/moment.min';
        if ($this->session->language == 'en') {
            $header['script'][] = 'application/view/resources/js/moment/locale/en-gb';
        }
        if ($this->session->language == 'fr') {
            $header['script'][] = 'application/view/resources/js/moment/locale/fr';
        }

        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('message');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('message');
        $messages = $this->model_message->getMessages($this->session->data['logged_user_id']);
        $data['messages'] = $messages;
        $this->load->view('message_inbox', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function outbox()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Outbox';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Message', 'href' => '?route=message/compose'];
        $breadcrumb[] = ['title' => 'Outbox', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $header['script'][] = 'application/view/resources/js/moment/min/moment.min';
        if ($this->session->language == 'en') {
            $header['script'][] = 'application/view/resources/js/moment/locale/en-gb';
        }
        if ($this->session->language == 'fr') {
            $header['script'][] = 'application/view/resources/js/moment/locale/fr';
        }

        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('message');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('message');
        $messages = $this->model_message->getSentMessages($this->session->data['logged_user_id']);
        $data['messages'] = $messages;
        $this->load->view('message_outbox', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function compose()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Compose';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Message', 'href' => '?route=message/compose'];
        $breadcrumb[] = ['title' => 'Compose', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('message');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('message');
        $users_list = $this->model_message->getUsers();
        foreach ($users_list as $key => $value) {
            $users_list[$key]['title'] = ucwords(str_replace('_', ' ', $value['title']));
        }
        $data['users_list'] = $users_list;

        if ($_POST) {
            $rules = [
                'receivers' => 'r',
            ];

            $file_rules = ['attachment' => 'size_limit:2|ext:pdf,doc,docx,ppt,pptx,xls,xlsx,jpg,png,bmp'];
            $this->form->process_post($rules);
            $this->file->process_files($file_rules);
            if (empty($this->form->errors) && empty($this->file->errors)) {
                $this->file->move_files();
                //print_r($this->file->files);
                foreach ($this->form->data['receivers'] as $receiver) {
                    if (isset($this->form->data['subject'])) {
                        $subject = $this->form->data['subject'];
                    } else {
                        $subject = 'Empty';
                    }
                    if (isset($this->form->data['body'])) {
                        $body = $this->form->data['body'];
                    } else {
                        $body = 'Empty';
                    }
                    $sender = $this->session->data['logged_user_id'];
                    $d = ['subject' => $subject,
                        'body'      => $body,
                        'sender'    => $sender,
                        'receiver'  => $receiver, ];
                    $files = $this->file->files['attachment'];
                    $this->model_message->createMessage($d, $files);
                }
            } else {
                print_r($this->file->errors);
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->errors);
            }
        }

        $this->load->view('message_compose', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function makeMessageSeen()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['message_id'])) {
                $message_id = $_GET['message_id'];
                $this->load->model('message');
                $this->model_message->makeMessageSeen($message_id);
                echo json_encode(['status' => 'success']);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        } else {
            echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
        }
    }

    public function getMessageDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['message_id'])) {
                $message_id = $_GET['message_id'];
                $this->load->model('message');
                $message_details = $this->model_message->getMessageDetails($message_id);
                $attachments = [];
                if (count(json_decode($message_details['attachment'])) > 0) {
                    foreach (json_decode($message_details['attachment']) as $att) {
                        $attachments[] = RESOURCE_URL.'/'.$att;
                    }
                    $message_details['attachment'] = $attachments;
                } else {
                    $message_details['attachment'] = 0;
                }
                echo json_encode(['status' => 'success',
                    'message_details'      => $message_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        } else {
            echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
        }
    }

    public function upload()
    {
        header('Content-Type: application/json');
        echo json_encode(['success' => true]);
    }
}
