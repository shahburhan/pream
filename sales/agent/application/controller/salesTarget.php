<?php

class ControllerClient extends Controller
{
    public function index()
    {
        $data = [];
        $data['page_title'] = 'Sales Target';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('salesTarget', $data);
    }
}
