<?php

class ControllerSalesOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Sales Order';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('salesOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function newOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'New Order';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Client', 'href' => '?route=salesOrder/newOrder'];
        $breadcrumb[] = ['title' => 'New', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('salesOrder');
        $data['clients'] = $this->model_salesOrder->getClientsList();
        $data['payment_mode'] = $this->model_salesOrder->getPaymentModes();
        $item_List = $this->model_salesOrder->getAllItemsList('');

        foreach ($item_List as $key => $item) {
            $image = json_decode($item_List[$key]['image']);
            if (count($image) > 0) {
                $image_url = RESOURCE_URL.'/'.$image[0];
                $item_List[$key]['image'] = $image_url;
            } else {
                $item_List[$key]['image'] = 0;
            }

            $pdf = json_decode($item_List[$key]['item_pdf']);
            if (count($pdf) > 0) {
                $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                $item_List[$key]['item_pdf'] = $pdf_url;
            } else {
                $item_List[$key]['item_pdf'] = 0;
            }
        }

        if ($_POST) {
            $rules = [
                'Client'  => 'r',
                'address' => 'r',
            ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $id = $this->model_salesOrder->newOrder($this->form->data);

                $this->load->controller('activity');
                $this->controller_activity->logActivity(17, $id, $this->session->loggedUser(), true);
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }
        $this->load->view('salesOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function viewSalesOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Sales Order';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Sales Order', 'href' => '?route=salesOrder/viewSalesOrder'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $sales_orders = $this->model_salesOrder->getOrderList($start, $limit);
        // $this->load->controller("activity");
        // $this->controller_activity->logActivity(37,$sales_orders,$this->session->loggedUser(),true);
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/viewSalesOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;
        $data['type'] = 'p';
        $this->load->view('viewSalesOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function completeOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Complete Orders';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Sales Order', 'href' => '?route=salesOrder/completeOrder'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $complete_orders = $this->model_salesOrder->getOrderList($start, $limit, ' ', true);
        // $order_status = $this->model_salesOrder->getOrderStatus();
        // $order_status_mapped = array();
        // foreach ($order_status as $key => $status) {
        //     $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace("_", " ", $status['title']));
        // }
        $total_count = $this->model_salesOrder->orderCount(5);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/completeOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['complete_orders'] = $complete_orders;
        $data['order_status'] = $order_status_mapped;
        $data['type'] = 'c';
        $this->load->view('completeOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function cancelledOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Cancelled Orders';
        $breadcrumb[] = ['title' => 'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title' => 'Sales Order', 'href' => '?route=salesOrder/cancelledOrder'];
        $breadcrumb[] = ['title' => 'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $cancelled_orders = $this->model_salesOrder->getCancelledOrderList($start, $limit);
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }
        $total_count = $this->model_salesOrder->orderCount(9);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/cancelledOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['cancelled_orders'] = $cancelled_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('cancelledOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $sales_order = $this->model_salesOrder->getOrderDetails($order_id);
                $order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                $payment_modes = $this->model_salesOrder->getPaymentModes();
                $pm_mapped = [];
                foreach ($payment_modes as $payment_mode) {
                    $pm_mapped[$payment_mode['payment_mode_id']] = $payment_mode['title'];
                }

                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;
                $response['payment_mode'] = $pm_mapped;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('salesOrder');
            if (isset($_POST['c']) && $_POST['c'] == 1) {
                $orders = $this->model_salesOrder->getOrderList(0, 10, $clause, true);
            } elseif (isset($_POST['can']) && $_POST['can'] == 1) {
                $orders = $this->model_salesOrder->getOrderList(0, 10, $clause, false, true);
            } else {
                $orders = $this->model_salesOrder->getOrderList(0, 10, $clause, false, false);
            }
            // $this->load->controller("activity");
            // $this->controller_activity->logActivity(37,$salesOrders,$this->session->loggedUser(),true);

            echo json_encode(['sales_list'=>$orders,  'clause'=>$clause]);
        }
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('salesOrder');
                $item_details = $this->model_salesOrder->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $brand_name = $_GET['brand_name'];
                $this->load->model('salesOrder');
                $categoryList = $this->model_salesOrder->getCategoryList($brand_name);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $areaList = $this->model_salesOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $clientClause = '';
        $itemClause = '';
        $brandClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['brand'])) {
            $c = '';
            $fieldName = 'brand_name';
            $filter = $_POST['brand'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['clientList'])) {
            $c = '';
            $fieldName = 'client';
            $filter = $_POST['clientList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = (isset($_POST['list']) && $_POST['list'] == 'item') ? 'reference' : 'order_id';
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '."item_id = '".$_POST['itemIdList'][$i]."'";
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$itemClause;
                } else {
                    $whereClause = $whereClause.' AND '.$itemClause;
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getClientsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $clientsList = $this->model_salesOrder->getClientsList();
                echo json_encode(['status' => 'success',
                    'client_list'          => $clientsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getClientsAddress()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('salesOrder');
                $clientsAddress = $this->model_salesOrder->getClientsAddress($client_id);
                echo json_encode(['status' => 'success',
                    'client_address'       => $clientsAddress, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('salesOrder');
                $clause = '';
                $client_id = (isset($_POST['clientId'])) ? $_POST['clientId'] : 0;
                $clause = $this->buildClause();
                $item_List = [];
                $limit = (isset($_POST['limit'])) ? (int) $_POST['limit'] : 15;
                $page = (isset($_POST['page'])) ? (int) $_POST['page'] : 1;
                $start = ($page - 1) * $limit;
                $total_count = $this->model_salesOrder->getAllItemsTotal($clause);
                $this->load->helper('pagination');
                $this->pagination->limit = $limit;
                $this->pagination->total = $total_count;
                $this->pagination->page = $page;
                $this->pagination->url = HTTP_SERVER.'?route=salesOrder/newOrder&page={page}';
                $pagination = $this->pagination->render();

                $item_List = $this->model_salesOrder->getAllItemsList($clause, $limit, $start);
                /*if(isset($_POST['with_client'])){
                    $item_List = $this->model_salesOrder->getAllItemsListWithClient($clause);
                }else{

                }*/

                foreach ($item_List as $key => $item) {
                    $item_List[$key]['given_rate'] = $this->lastGivenRate($client_id, $item_List[$key]['item_id']);
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                $response_data = ['status' => 'success',
                    'itemList'             => $item_List,
                    'clause'               => $clause, ];
                $response_data['pagination'] = $pagination;
                $response_data['total_count'] = $total_count;

                if (isset($_POST['client_id'])) {
                    $client = $this->model_salesOrder->getClientDetails($_POST['client_id']);
                    $type = $this->model_salesOrder->getClientType($client['client_type']);
                    // $client['client_type'] = $type['title'];
                    $response_data['client'] = $client;
                }

                echo json_encode($response_data);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    private function lastGivenRate($client_id, $item_id)
    {
        $this->load->model('stock');
        $client_id = (int) $client_id;
        $item_id = (int) $item_id;

        return $this->model_stock->getLastGivenRate($item_id, $client_id);
    }

    public function placeOrder()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $data = [];
                $this->load->model('salesOrder');
                $items = $_POST['items'];
                $sell_rate = $_POST['sell_rates'];
                $quantity = $_POST['quantity'];
                $mrp = $_POST['mrp'];
                $htt = 0;
                $ttc = 0;

                for ($i = 0; $i < count($items); $i++) {
                    $p = $sell_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    // $htt = $htt + (($m - ($m * $p) / 100) * $q);
                    $htt = $htt + ($p * $q);
                }

                $ttc = $htt + (($htt * TAX) / 100);
                $_POST['ttc'] = $ttc;
                $_POST['htt'] = $htt;

                $data['htt'] = $htt;
                $data['tva'] = TAX;
                $data['ttc'] = $ttc;
                $data['amount_paid'] = 0;
                $data['order_date'] = date('Y-m-d', time());
                $data['payment_deadline'] = date('Y-m-d', strtotime($data['order_date'].' + 45 days'));
                $data['order_status'] = 2;
                $data['order_type'] = 2;
                $data['client_id'] = (int) $_POST['client'];
                $data['expected_date'] = $_POST['expected_date'];
                $data['person_responsible'] = $_POST['person_responsible'];
                $data['payment_mode'] = (int) $_POST['payment_mode'];
                $data['delivery_address'] = (int) $_POST['client'];
                $data['other_address'] = $_POST['other_address'];
                $data['city'] = $_POST['city'];
                $data['raised_by'] = $this->session->data['logged_user_id'];
                $this->load->controller('activity');
                for ($i = 0; $i < count($items); $i++) {
                    $orderData = [];
                    $it = (int) $items[$i];
                    $p = $sell_rate[$i]; //Type Removed
                    $q = (int) $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed

                    $this->model_salesOrder->removeFromQueue(0, true);

                    //check for stock

                    $stock_details = $this->model_salesOrder->checkStock($it);
                    if ($stock_details['min_stock'] == '') {
                        $stock_details['min_stock'] = 0;
                    }
                    if ($stock_details['current_stock'] < $q) {
                        $data['order_status'] = 13;
                        $required_stock = $q - $stock_details['current_stock'];
                        $maintain_stock = $required_stock + $stock_details['min_stock'];

                        $order_id = $this->model_salesOrder->newOrder($data);

                        $this->model_salesOrder->placePORequest($it, $maintain_stock, $order_id, $_POST['client']);
                    } elseif ($stock_details['current_stock'] > $q && $stock_details['current_stock'] - $q < $stock_details['min_stock']) {
                        $maintain_stock = $stock_details['min_stock'] - ($stock_details['current_stock'] - $q);

                        $order_id = $this->model_salesOrder->newOrder($data);

                        $this->model_salesOrder->placePORequest($it, $maintain_stock, $order_id);
                    } else {
                        $order_id = $this->model_salesOrder->newOrder($data);
                    }

                    $orderData['order_id'] = $order_id;
                    $orderData['item_id'] = $it;
                    $orderData['quantity'] = $q;
                    $orderData['discount'] = 0;
                    $orderData['rate'] = $p;
                    $orderData['mrp'] = $m;
                    $orderData['order_type'] = $data['order_type'];

                    $this->model_salesOrder->addOrderItems($orderData);
                }

                $this->controller_activity->logActivity(17, $order_id, $this->session->loggedUser(), true);

                echo json_encode(['status' => 'success',
                    'order_id'             => $order_id, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function cancelOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('salesOrder');
            $cancel_order = $this->model_salesOrder->cancelOrder($order_id);
            $this->load->controller('activity');
            $this->controller_activity->logActivity(37, $cancel_order, $this->session->loggedUser(), true);
            $data['cancel_order'] = $cancel_order;
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $brand = $_GET['brand_name'];
                $category = $_GET['category'];
                $this->load->model('salesOrder');
                $refList = $this->model_salesOrder->getRefList($ref, $brand, $category);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function changeItem()
    {
        $order_id = (int) $_POST['order_id'];
        $item_reference = (int) $_POST['item_reference'];
        $new_reference = (int) $_POST['new_reference'];

        $this->load->model('salesOrder');

        if ($item_reference != $new_reference) {
            $new_id = $this->model_salesOrder->validateReference($new_reference);
            if ($new_id) {
                if ($this->model_salesOrder->changeItem($order_id, $item_reference, $new_id)) {
                    echo json_encode(['status'=>true]);
                } else {
                    echo json_encode(['status'=>false, 'msg'=> 'This Item has already been dispatched']);
                }
            } else {
                echo json_encode(['status'=>false]);
            }
        } else {
            echo json_encode(['status'=>false]);
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['char']) && strlen($_GET['char']) > 2) {
                $string = htmlentities($_GET['char']);
            }
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $brandList = $this->model_salesOrder->getBrandList($string);
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function itemListQ()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['char']) && strlen($_GET['char']) > 2) {
                $string = htmlentities($_GET['char']);
            }
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $itemList = $this->model_salesOrder->itemListQ($string);
                echo json_encode(['status'          => 'success',
                                        'item_list' => $itemList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function addToQueue()
    {
        if ($_POST) {
            $item_id = (int) $_POST['item_id'];
            $quantity = (int) $_POST['quantity'];
            $rate = $_POST['rate'];

            $this->load->model('salesOrder');

            if ($quantity && $item_id) {
                $this->model_salesOrder->addToQueue($item_id, $quantity, $rate);
                echo json_encode(['status'=>true]);
            } else {
                echo json_encode(['status'=>false,  'msg'=> 'Invalid Data.']);
            }
        }
    }

    public function getFromQueue()
    {
        $this->load->model('salesOrder');
        if ($_POST) {
            $clause = '';
            $clause = $this->buildClause();
            $queue['queue'] = $this->model_salesOrder->getFromQueue($clause);
            $queue['count'] = count($queue['queue']);
            echo json_encode($queue);
        }
    }

    public function removeFromQueue()
    {
        $item_id = (int) $_GET['id'];

        $this->load->model('salesOrder');

        $this->model_salesOrder->removeFromQueue($item_id);
    }
}
