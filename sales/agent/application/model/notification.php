<?php

class ModelNotification extends Model
{
    public function getNotifications($user_id)
    {
        $result = $this->db->query('SELECT notification.*, activity.activity_object FROM notification LEFT JOIN activity ON  notification.activity_id = activity.activity_id WHERE notification_for = '.$user_id.' ORDER BY notification.created_on DESC');

        return $result->rows;
    }
}
