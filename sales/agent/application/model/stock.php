<?php
 class ModelStock extends Model
 {
     public function getStockList()
     {
         $result = $this->db->query('SELECT * From item');

         return $result->rows;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT * FROM item  WHERE item_id= '.$item_id);

         return $result->row;
     }

     public function getSalesOrderDetails($order_id)
     {
         $result = $this->db->query('SELECT `order`.*, c.*
        FROM `order` LEFT JOIN (SELECT user.name,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = '.$order_id);

         return $result->row;
     }

     public function getLastGivenRate($item_id, $client_id)
     {
         $sql = "SELECT order_item.rate FROM order_item LEFT JOIN `order` ON order_item.order_id = `order`.order_id WHERE `order`.client = $client_id && item_id = $item_id ORDER BY item_list_id DESC LIMIT 1";
         $result = $this->db->query($sql);

         return (isset($result->row['rate']) && ($result->row['rate'] != '' || $result->row['rate'] != null)) ? $result->row['rate'] : 0;
     }
 }
