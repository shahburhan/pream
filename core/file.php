<?php

class file
{
    /**
     *  USAGE.
     *
     * ###############--------RULES FORMAT-----------######################    Called explicitly process_files(rules)
     * array(
     * "PARAM_NAME" => size_limit:SIZE_IN_MB|ext:EXTENSIONS_ALLOWED_SEPARATED BY COMMAS|r
     * )
     * PARAM_NAME -- Name given to the HTML input type.
     * size_limit -- In Megabytes and can be in float.
     * ext        -- allowed extensions separated by commas and in lowercase.
     * r          -- If added then the file cannot be empty.
     *
     * **Can be in any order.
     * eg:--
     *
     *
     *
     * array(
     * "attachment" => "size_limit:1|ext:bmp,jpeg,img|r"
     * "attachment2" => "size_limit:1.5"
     * "attachment3" => "ext:bmp,jpeg,img"
     * );
     *
     * After processing the error will be returned in $errors in format error_NAME similar like form core file.
     *
     *
     * Note:--  Multiple file input type is also processed.
     * All files selected in multiple file type are processed with same rules.
     *
     * ##############---------Move Files----------#########################
     *
     * HAVE TO BE CALLED EXPLICITLY OTHERWISE FILES WILL NOT BE MOVED TO RESOURCES DIRECTORY.
     *
     * move_files() will move all files to resources dir and will return new names of all the files in the $files attribute
     * which can be accessed using parameter name given to the input
     *
     * output -- array( "single_input_type_param_name" => array("filename"),
     *                  "multiple_input_type_param_name" => array("filename1", "filename2", "filename3");
     */
    public $files = [];
    public $errors = [];

    public function process_files($rules = [])
    {
        //if(count($rules) == 0) return;
        //print_r($_FILES);
        foreach ($rules as $param_name => $rule) {
            $values = $_FILES[$param_name];
            if (is_array($values['name'])) {    //For multiple file uploads input type
                for ($i = 0; $i < count($values['name']); $i++) {
                    $this->intermediateProcessing($param_name, $rule, [
                        'error' => $values['error'][$i],
                        'name'  => $values['name'][$i],
                        'size'  => $values['size'][$i],
                        'type'  => $values['type'][$i],
                    ]);
                }
            } else {    //For single file upload input type
                $this->intermediateProcessing($param_name, $rule, $_FILES[$param_name]);
            }
        }
    }

    public function move_files()
    {
        foreach ($_FILES as $param_name => $values) {
            $values = $_FILES[$param_name];
            if (is_array($values['name'])) {    //For multiple file uploads input type
                for ($i = 0; $i < count($values['name']); $i++) {
                    $this->moveFile($param_name, [
                        'error'    => $values['error'][$i],
                        'name'     => $values['name'][$i],
                        'size'     => $values['size'][$i],
                        'type'     => $values['type'][$i],
                        'tmp_name' => $values['tmp_name'][$i],
                    ]);
                }
            } else {    //For single file upload input type
                $this->moveFile($param_name, $_FILES[$param_name]);
            }
        }
    }

    private function intermediateProcessing($param_name, $rule, $values = [])
    {
        if ($values['error'] == 4) {
            if (in_array('r', explode('|', $rule))) {
                $this->errors['error_'.$param_name] = 'This cannot be empty';
            }

            return;
        }

        if ($values['error'] == 0) {
            $this->validate_file($param_name, $rule, [
                'name'      => $values['name'],
                'size'      => $values['size'],
                'extension' => strtolower(pathinfo(basename($values['name']), PATHINFO_EXTENSION)),
                'type'      => $values['type'], ]);
        } elseif ($values['error'] == 1) {
            if (!isset($this->errors['error_'.$param_name])) {
                $this->errors['error_'.$param_name] = $values['name'].'- File size limit exceeded';
            } else {
                $this->errors['error_'.$param_name] = $this->errors['error_'.$param_name].', '.$values['name'].'- File size limit exceeded';
            }
        } elseif ($values['error'] == 3) {
            if (!isset($this->errors['error_'.$param_name])) {
                $this->errors['error_'.$param_name] = $values['name'].'- The file was uploaded partially.';
            } else {
                $this->errors['error_'.$param_name] = $this->errors['error_'.$param_name].', '.$values['name'].'- The file was uploaded partially.';
            }
        }
    }

    private function validate_file($param_name, $rules, $file)
    {
        $isErrorFree = true;
        $rules = trim($rules);
        if ($rules == '') {
            return $isErrorFree;
        }
//        echo "<br>".$param_name."<br>";
//        print_r($file);
//        echo "<br>".$rules."<br>";
        $cons = explode('|', $rules);
        foreach ($cons as $c) {
            $temp = explode(':', $c);
            if ($temp[0] == 'ext') {
                if (!(in_array($file['extension'], explode(',', $temp[1])))) {
                    $isErrorFree = false;
                    if (!isset($this->errors['error_'.$param_name])) {
                        $this->errors['error_'.$param_name] = $file['name'].' - File type not allowed';
                    } else {
                        $this->errors['error_'.$param_name] = $this->errors['error_'.$param_name].', '.$file['name'].' - File type not allowed';
                    }
                }
            }
            if ($temp[0] == 'size_limit') {
                $size = (float) $temp[1] * 1024 * 1024;
                if ($size < (float) $file['size']) {
                    if (!isset($this->errors['error_'.$param_name])) {
                        $this->errors['error_'.$param_name] = $file['name'].' - File size limit exceeded of '.$temp[1].'mb.';
                    } else {
                        $this->errors['error_'.$param_name] = $this->errors['error_'.$param_name].', '.$file['name'].' - File size limit exceeded of '.$temp[1].'mb.';
                    }
                }
            }
        }
    }

    private function moveFile($param_name, $value)
    {
        if (!isset($this->files[$param_name])) {
            $this->files[$param_name] = [];
        }
        if ($value['error'] == 4) {
            return;
        }
        $target_dir = DIR_RESOURCES;
        $new_name = time().'-'.rand(1000, 9999).'.'.pathinfo(basename($value['name']), PATHINFO_EXTENSION);
        $target_file = $target_dir.$new_name;
        move_uploaded_file($value['tmp_name'], $target_file);
        $this->files[$param_name][] = $new_name;
    }
}
