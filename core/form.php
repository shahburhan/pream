<?php

class form
{
    public $data = [];
    public $error = [];

    public function process_post($rules = [])
    {
        $this->data = $_POST;
        $this->validate($rules);
        if (empty($this->error)) {
            return true;
        } else {
            return false;
        }
    }

    public function process_get($rules = [])
    {
        $this->data = $_GET;
        $this->validate($rules);
        if (empty($this->error)) {
            return true;
        } else {
            return false;
        }
    }

    protected function validate($rules = [])
    {
        $cheat = [
            'r' => 'required',
            'l' => 'length',
            'n' => 'number',
            'e' => 'email',
            'd' => 'date',
            'b'	=> 'boolean',
            ];
        foreach ($rules as $k => $rule) {
            $checks = explode('|', rtrim($rule, '|'));
            foreach ($checks as $check) {
                if (strpos($check, ':')) {
                    $vals = explode(':', $check);
                    if (strlen($this->data[$k]) < $vals[1]) {
                        $this->error['error_'.$k] = "Please enter a minimum of $vals[1] characters.";
                    }
                }
                switch ($check) {
                        case 'r':
                            if ($this->data[$k] == '') {
                                $this->error['error_'.$k] = 'This field is required';
                            }
                            break;
                        case 'n':
                            if (!is_numeric($this->data[$k])) {
                                $this->error['error_'.$k] = 'Please enter a valid '.ucfirst(str_replace('_', ' ', $k)).'';
                            }
                            break;
                        case 'e':
                            if (!filter_var($this->data[$k], FILTER_VALIDATE_EMAIL)) {
                                $this->error['error_'.$k] = 'Please enter a valid '.ucfirst(str_replace('_', ' ', $k)).'';
                            }
                            break;
                        case 'b':
                            if ($this->data[$k] == 0 || $this->data[$k] == false) {
                                $this->error['error_'.$k] = 'Please select valid option';
                            }
                            break;
                }
            }
        }
    }
}
