<?php

class loader
{
    protected $registry;

    public function __construct($registry)
    {
        $this->registry = $registry;
    }

    public function mDebug($data)
    {
        $output = $data;
        if (is_array($output)) {
            $output = implode(',', $output);
        }

        echo "<script>console.log( 'PHP: ".$output."' );</script>";
    }

    public function model($model)
    {
        $file = DIR_MODEL.$model.'.php';
        if (is_file($file)) {
            $modelName = 'model_'.$model;
            if (!$this->registry->has($modelName)) {
                include_once $file;
                $model = 'Model'.ucfirst($model);
                $modelObj = new $model($this->registry);

                $this->registry->set($modelName, $modelObj);
            }
        }
    }

    public function controller($controller)
    {
        $file = DIR_CONTROLLER.$controller.'.php';
        if (is_file($file)) {
            $controllerName = 'controller_'.$controller;
            if (!$this->registry->has($controllerName)) {
                include_once $file;
                $controller = 'Controller'.ucfirst($controller);
                $controllerObj = new $controller($this->registry);

                $this->registry->set($controllerName, $controllerObj);
            }
        }
    }

    public function view($view, $args = [])
    {
        extract($args, EXTR_PREFIX_SAME, 'gm');
        $file = DIR_VIEW.$view.'.view.html';
        if (is_file($file)) {
            include_once $file;
        } else {
            trigger_error('Failed to load view.');
        }
    }

    public function helper($helper)
    {
        $file = DIR_HELP.$helper.'.php';
        if (is_file($file)) {
            include_once $file;
            $helper = ucfirst($helper);
            $helperObj = new $helper();
            $this->registry->set(strtolower($helper), $helperObj);
        } else {
            trigger_error('Failed to load helper.');
        }
    }

    public function text($text)
    {
        $file = DIR_VIEW.'text/'.$this->registry->get('session')->language.'/'.$text.'.text.php';
        if (is_file($file)) {
            include_once $file;
            $text_code = 'text_'.$text;
            $this->registry->set('text', $$text_code);
        } else {
            trigger_error('Failed to load text.');
        }
    }
}
