<?php

class session extends SessionHandler
{
    public $session_id = '';
    public $data = [];
    public $language = '';
    public $allowed_user_types = [];

    public function __construct()
    {
        session_set_save_handler($this);
        session_start();
        if (isset($_COOKIE['lang']) && ($_COOKIE['lang'] == 'en' || $_COOKIE['lang'] == 'fr')) {
            $this->language = $_COOKIE['lang'];
        } else {
            setcookie('lang', 'en', time() + 2592000);
            $this->language = 'en';
        }
    }

    public function start()
    {
        $this->session_id = $this->create_sid();
        $this->data = &$_SESSION;
    }

    public function login($id = 0, $user_type = 3)
    {
        if (is_numeric($id) && $id > 0) {
            $this->data['logged_user_id'] = $id;
            $this->data['logged_user_type'] = $user_type;

            return true;
        } else {
            return false;
        }
    }

    public function changeLanguage($language)
    {
        if ($this->language !== $language) {
            $this->language = $language;
            setcookie('lang', $language, time() + 2592000);
        }

        return true;
    }

    public function isRightUser()
    {
        $is_right_user_type = false;
        if (isset($this->data['logged_user_id']) && count($this->allowed_user_types) > 0) {
            foreach ($this->allowed_user_types as $types) {
//                echo $this->data['logged_user_type'] . "  " . $types;
                if ($types == $this->data['logged_user_type']) {
                    $is_right_user_type = true;
                }
            }
        } else {
            $is_right_user_type = true;
        }
//         echo "is ". !$is_right_user_type;
        return $is_right_user_type;
    }

    public function isLoggedIn()
    {
        return  (isset($this->data['logged_user_id']) && $this->data['logged_user_id'] > 0) ? true : false;
    }

    public function loggedUser()
    {
        return ($this->isLoggedIn()) ? $this->data['logged_user_id'] : null;
    }

    public function destroySession()
    {
        unset($this->data['logged_user_id']);
        unset($this->data['logged_user_type']);
        $this->destroy($this->session_id);
        header('Location:?route=account');
    }
}
