<?php

class initialize
{
    private $controller = 'account';
    private $method = 'overview';
    private $vars = [];

    public function __construct($registry)
    {
        define('NEW_REQUEST_ORDER_ALERT', 5);
        define('MINIMUM_STOCK_ALERT', 8);
        define('RESIDUE_STOCK_ALERT', 9);
        define('PO_RECEIPT_ALERT', 10);
        define('AUDIT_ALERT_PURCHASER', 11);
        define('REQUEST_ORDER_CANCEL', 12);

        if (isset($_GET['route'])) {
            $route = explode('/', stripslashes(rtrim($_GET['route'], '/')));
            if (count($route) < 3) {
                if (isset($route[0]) && !empty(trim($route[0]))) {
                    $this->controller = $route[0];
                }
                if (isset($route[1]) && !empty(trim($route[1]))) {
                    $this->method = $route[1];
                }
            }
        }
        if (file_exists(DIR_CONTROLLER.$this->controller.'.php')) {
            if (substr($this->method, 0, 2) == '__') {
                trigger_error('Error: Calls to magic methods are not allowed!');
            }

            include_once DIR_CONTROLLER.$this->controller.'.php';
            $class = 'Controller'.ucfirst($this->controller);
            $classObj = new $class($registry);
            if (method_exists($classObj, $this->method)) {
                return call_user_func_array([$classObj, $this->method], $this->vars);
            } else {
                $load = $registry->get('load');
                $load->view('404', ['page_title'=>'Page not found']);
            }
        } else {
            $load = $registry->get('load');
            $load->view('404', ['page_title'=>'Page not found']);
        }
    }
}
