<?php

//Import the PHPMailer class into the global namespace
use PHPMailer\PHPMailer\PHPMailer;

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Asia/Calcutta');

require DIR_HELP.'mail/src/PHPMailer.php';
require DIR_HELP.'mail/src/Exception.php';
require DIR_HELP.'mail/src/SMTP.php';
require DIR_HELP.'mail/mailer.php';

/**
 * Mailer class extends PHP Mailer
 * Construct fixes certain settings to make the mailer work.
 */
class mail extends PHPMailer
{
    use Mailer;

    public function __construct()
    {
        parent::__construct(false);

        $this->CharSet = 'UTF-8';
        $this->SMTPDebug = 0;                                 // Enable verbose debug output
        $this->isSMTP();                                      // Set mailer to use SMTP
        $this->Host = $this->smtp_host;  // Specify main and backup SMTP servers
        $this->SMTPAuth = true;                               // Enable SMTP authentication
        $this->Username = $this->smtp_username;                 // SMTP username
        $this->Password = $this->smtp_password;                           // SMTP password
        $this->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->Port = $this->smtp_port;

        $this->setFrom($this->smtp_username, $this->sender_title);
        /*
            Use
            * addAddress(string email, string name) - Inheritted Method - Sets a recipient
            * isHTML(true);                           Inheritted Method - Sets email format to HTML
            * Subject                                 Inheritted Attr - Sets mail Subject
            * Body                                    Inheritted Attr - Sets mail content
            * send();                                 Inheritted Method to shoot mail

        */
    }

    public static function build()
    {
        return new self();
    }
}
