<?php

class ControllerAccount extends Controller
{
    public function index()
    {
        $data = [];
        $this->load->text('account');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('404', $data);
    }

    public function overview()
    {
        $data = [];
        $data['page_title'] = 'List';
        if ($this->session->isLoggedIn()) {
            $data['user_type'] = $this->session->data['logged_user_type'];
        }
        $this->load->view('dashboard', $data);
    }

    public function login()
    {
        if ($this->session->isLoggedIn()) {
            header('Location:?route=account/overview');
            exit;
        }

        $data = [];
        $data['page_title'] = 'Login';
        $this->load->text('account');
        foreach ($this->text as $key => $value) {
            $data['text_'.$key] = $value;
        }

        if ($_POST) {
            $rules = [
                'username' => 'r',
                'password' => 'r',
            ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $this->load->model('account');
                $is_valid = $this->model_account->login($this->form->data['username'], $this->form->data['password']);
                if (!$is_valid) {
                    $data['invalid_user'] = '1';
                } else {
                    $data['invalid_user'] = '0';
                    header('Location:?route=account/overview');
                    exit;
                }
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }
        $this->load->view('login', $data);
    }

    public function logout()
    {
        $this->session->destroySession();
    }

    public function unauthorized()
    {
        $data = [];
        $data['page_title'] = 'Login';
        $this->load->text('account');
        foreach ($this->text as $key => $value) {
            $data['text_'.$key] = $value;
        }
        $this->load->view('unauthorized', $data);
    }
}
