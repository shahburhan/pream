-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 06:39 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pream`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_type`
--

CREATE TABLE `activity_type` (
  `activity_type_id` int(11) NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `prefix` varchar(64) DEFAULT NULL,
  `notify_to` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_type`
--

INSERT INTO `activity_type` (`activity_type_id`, `title`, `prefix`, `notify_to`) VALUES
(1, 'new_vendor_added', 'vendor', '[1]'),
(2, 'new_vedor_approved', 'vendor', '[1,3]'),
(3, 'new_vendor_disapproved', 'vendor', '[1,3]'),
(4, 'new_item_added', 'item', '[1,4,5,6,7]'),
(5, 'new_request_order', 'order', '[1,3]'),
(6, 'new_request_order_approved', 'order', '[1,6,7]'),
(7, 'new_request_order_disapproved', 'order', '[1,6,7]'),
(8, 'minimum_stock_alert', 'item', '[1,3,6,7]'),
(9, 'residue_stock_alert', 'item', '[1,3,6,7]'),
(10, 'po_receipt_alert', 'order', '[1,3]'),
(11, 'audit_alert_purchaser', 'audit', '[3]'),
(12, 'request_order_cancel', 'order', '[1,3]'),
(13, 'new_purchase_order', 'order', '[1,3]'),
(14, 'new_return_order', 'order', '[1,3]'),
(15, 'new_sales_agent', 'agent', '[1,4,5]'),
(16, 'new_client_added', 'client', '[1,4]'),
(17, 'new_sales_order', 'order', '[1,5]'),
(18, 'new_return_sales_order', 'order', '[1,5]'),
(19, 'new_storekeeper_added', 'storekeeper', '[1,7,8]'),
(20, 'new_user_added', 'user', '[1]'),
(21, 'new_audit', 'audit', '[1]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_type`
--
ALTER TABLE `activity_type`
  ADD PRIMARY KEY (`activity_type_id`),
  ADD UNIQUE KEY `activity_type_activity_type_id_uindex` (`activity_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_type`
--
ALTER TABLE `activity_type`
  MODIFY `activity_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
