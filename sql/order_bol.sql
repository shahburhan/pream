-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2018 at 05:36 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pream`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_bol`
--

CREATE TABLE `order_bol` (
  `bol_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `bol_number` int(11) NOT NULL,
  `quantity_received` int(11) NOT NULL,
  `date_of_receipt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_bol`
--

INSERT INTO `order_bol` (`bol_id`, `item_id`, `order_id`, `bol_number`, `quantity_received`, `date_of_receipt`) VALUES
(1, 1, 13, 0, 3, '2018-02-04'),
(2, 1, 13, 0, 2, '2018-02-06'),
(3, 2, 2, 0, 2, '2018-02-14'),
(5, 1, 8, 4578, 6, '2018-02-15'),
(6, 1, 10, 4785, 5, '2018-02-16'),
(8, 1, 12, 7474, 1, '2018-02-17'),
(10, 1, 13, 21324, 5, '2018-02-18'),
(11, 4, 13, 4845, 5, '2018-02-19'),
(12, 4, 13, 4845, 5, '2018-02-20'),
(16, 1, 13, 74545, 1, '2018-02-21'),
(18, 1, 13, 123123, 1, '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_bol`
--
ALTER TABLE `order_bol`
  ADD PRIMARY KEY (`bol_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_bol`
--
ALTER TABLE `order_bol`
  MODIFY `bol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
