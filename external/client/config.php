<?php

// ini_set("display_error", 0);
// error_reporting(E_ALL);
date_default_timezone_set('Asia/Calcutta');

define('HTTP_SERVER', 'http://projects.dev/getmax-alpha/external/client');
define('HTTPS_SERVER', 'https://projects.dev/getmax-alpha/external/client');

define('DIR_CORE', 'C://xampp/htdocs/projects/pream/core/');
define('DIR_CONTROLLER', 'C://xampp/htdocs/projects/pream/external/client/application/controller/');
define('DIR_MODEL', 'C://xampp/htdocs/projects/pream/sales/external/client/model/');
define('DIR_VIEW', 'C://xampp/htdocs/projects/pream/sales/external/client/view/');
define('DIR_HELP', 'C://xampp/htdocs/projects/pream/helper/');
define('DIR_RESOURCES', 'C://xampp/htdocs/projects/pream/resources/');

//function to include all core files
function core($class)
{
    $file = DIR_CORE.strtolower($class).'.php';

    if (is_file($file)) {
        include_once $file;

        return true;
    } else {
        return false;
    }
}

//autoload core objects and handlers
spl_autoload_register('core');

//function to clean all external sources
function clean($var)
{
    if (is_array($var)) {
        foreach ($var as $key => $value) {
            // code...
            $var[clean($key)] = clean($value);
        }
    } else {
        $var = htmlentities(stripslashes($var));
    }

    return $var;
}

$_POST = clean($_POST);
$_GET = clean($_GET);
$_REQUEST = clean($_REQUEST);
//create registry instance
$registry = new Registry();

//create session instance
$session = new Session();
$session->start();
//create form instance
$form = new Form();
$file = new File();
//create db instance
$db = new DB('localhost', 'root', '', 'gm_realtors');

//set db handler in registry
$registry->set('db', $db);

//set form handler in registry
$registry->set('form', $form);

//set session handler in registry
$registry->set('session', $session);

//create loader instance
$loader = new Loader($registry);

//set loader in registry
$registry->set('load', $loader);

$registry->set('file', $file);

//intialize application and route
$init = new Initialize($registry);
