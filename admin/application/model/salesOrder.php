<?php
 class ModelSalesOrder extends Model
 {
     public function getSalesOrderList($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 && order_status != 9 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 2 && order_status != 9 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getCancelledOrderList($start = 0, $limit = 10)
     {
         $status = 2;
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  c.* FROM `order` LEFT JOIN (SELECT user.name, user.address, user.city, user.email_id, user.phone, user.postal_code, client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 && order_status = 9 LIMIT $start,$limit");

         return $result->rows;
     }

     public function getOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* 
        FROM `order` LEFT JOIN (SELECT user.name, user.address, user.city, user.email_id, user.phone, user.postal_code, client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function getReturnOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT * FROM `order` WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function getClientList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT c.*, u.* FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id) LIMIT $start,$limit");

         return $result->rows;
     }

     public function getClientDetails($client_id)
     {
         $result = $this->db->query('SELECT client.*, user.name, user.address, user.email_id,user.phone FROM client LEFT JOIN user ON  client.user_id = user.user_id  WHERE client.client_id ='.$client_id);

         return $result->row;
     }

     public function getSalesOrder($client_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  c.* FROM `order` LEFT JOIN (SELECT user.name,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE c.client_id =".$client_id.'&& order_type=2');

         return $result->rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getReturnReason()
     {
         $result = $this->db->query('SELECT * FROM return_reason');

         return $result->rows;
     }

     public function getReturnSalesOrder($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 4 && order_status = 6 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 4 && order_status 
            = 6 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getReturnOrderList($order_id)
     {
         $result = $this->db->query("SELECT `order`.*,  DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* 
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id.' && order_type = 4');

         return $result->row;
     }

     public function getReturnOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  c.* 
        FROM `order` LEFT JOIN client c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function orderCount($status)
     {
         $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

         return $result->num_rows;
     }

     public function getReturnOrder($client_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  c.* FROM `order` LEFT JOIN (SELECT user.name,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 4 && order_status = 6 && c.client_id =".$client_id);

         return $result->rows;
     }

     public function getRefList($ref = 0)
     {
         $sql = 'SELECT DISTINCT order_id FROM `order` WHERE order_status = 2';
         if ($ref) {
             $sql = 'SELECT DISTINCT reference FROM `item` ';
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }
 }
