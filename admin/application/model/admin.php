<?php
 class ModelAdmin extends Model
 {
     public function createUser($data = [], $files = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $lastLogin = date('Y-m-d h:i:s', time());
         $last_ip = $_SERVER['REMOTE_ADDR'];
         $status = (int) 0;
         // $password = rand(1000,9999);
         $files_j = json_encode($files);
         $this->db->query("INSERT INTO user SET 
			user_type = '".$this->db->escape($data['user_type'])."', 
			username = '".$this->db->escape($data['username'])."', 
			name = '".$this->db->escape($data['name'])."', 
			email_id = '".$this->db->escape($data['email'])."', 
			password = '".$this->db->escape($data['password'])."', 
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			created = '".$this->db->escape($date)."',
			status = '".$this->db->escape($status)."',
			image = '".$this->db->escape($data['image'])."',
			identification = '".$this->db->escape($data['identification'])."'
			 ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function validate_username($username)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE username = '".$this->db->escape($username)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function updateUser($id, $data = [], $files = [])
     {
         $this->db->query("UPDATE user SET 
			email_id = '".$this->db->escape($data['email'])."',
			username = '".$this->db->escape($data['username'])."',
			name = '".$this->db->escape($data['name'])."',
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			image = '".$this->db->escape($data['image'])."',
			identification = '".$this->db->escape($data['identification'])."'
			WHERE user_id = $id");

         return true;
     }

     public function getUserType()
     {
         $result = $this->db->query("SELECT * FROM user_type WHERE prefix = 'senior'");

         return $result->rows;
     }

     public function createAdmin($user_id)
     {
         $contract_type = (int) 0;
         $execution_date = (int) 0;
         $expiry_date = (int) 0;
         $this->db->query("INSERT INTO employee SET
			user_id = '".$this->db->escape($user_id)."',
			contract_type = '".$this->db->escape($contract_type)."',
			execution_date = '".$this->db->escape($execution_date)."',
			expiry_date = '".$this->db->escape($expiry_date)."'
			");
         $employee_id = $this->db->getLastId();

         //$this->session->login($user_id);

         //$this->createVendor($user_id);

         return $employee_id;
     }

     public function getAdminList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT ut.*, u.user_id as employee_id, ut.title as user_type, u.* FROM user_type ut RIGHT JOIN user u  ON (ut.user_type_id = u.user_type) LIMIT $start,$limit");

         return $result->rows;
     }

     public function deleteAdmin($employee_id)
     {
         $employee_id = (int) $employee_id;
         // order_status - 0 (delete storekeeper)
         $sql = "DELETE FROM user WHERE user_id = $employee_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getAdminDetails($employee_id)
     {
         $result = $this->db->query('SELECT * FROM user WHERE user_id= '.$employee_id);

         return $result->row;
     }

     public function userCount()
     {
         $result = $this->db->query('SELECT user_id FROM user ');

         return $result->num_rows;
     }
 }
