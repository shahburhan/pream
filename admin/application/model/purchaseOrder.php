<?php

class ModelPurchaseOrder extends Model
{
    public function getPurchaseOrderList($start = 0, $limit = 10, $clause = '')
    {
        $status = 1;
        $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_type = 1 && order_status != 8 LIMIT $start,$limit";
        if ($clause != '') {
            $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id ".$clause." && order_type = 1 && order_status != 8 LIMIT $start,$limit";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getCancelledOrderList($start = 0, $limit = 10)
    {
        $status = 1;
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_type = 1 && order_status = 8 LIMIT $start,$limit");

        return $result->rows;
    }

    public function getPurchaseOrderDetails($order_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* 
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_id = ".$order_id);

        return $result->row;
    }

    public function getReturnPurchaseOrder($start = 0, $limit = 10, $clause = '')
    {
        $status = 1;
        $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_type = 3 && order_status = 6 LIMIT $start,$limit";
        if ($clause != '') {
            $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id ".$clause." && order_type = 3 && order_status = 6 LIMIT $start,$limit";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getPurchaseReturnOrderDetails($order_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* 
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_id = ".$order_id);

        return $result->row;
    }

    public function getVendorsList($start = 0, $limit = 10)
    {
        $result = $this->db->query("SELECT v.*, u.* FROM vendor v LEFT JOIN user u  ON (v.user_id = u.user_id) LIMIT $start,$limit");

        return $result->rows;
    }

    public function getVendorDetails($vendor_id)
    {
        $result = $this->db->query('SELECT vendor.*, user.name, user.address, user.email_id,user.phone FROM vendor LEFT JOIN user ON  vendor.user_id = user.user_id  WHERE vendor.vendor_id ='.$vendor_id);

        return $result->row;
    }

    public function getVendorReturnOrder($vendor_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_status = 6 && v.vendor_id =".$vendor_id);

        return $result->rows;
    }

    public function getPurchaseOrder($vendor_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date,  v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE v.vendor_id =".$vendor_id);

        return $result->rows;
    }

    public function getAllItemsListWithVendor($clause)
    {
        $result = $this->db->query('SELECT * FROM item LEFT JOIN  (SELECT user.address , user.city, user.email_id, vendor.vendor_id, vendor.company_name, vendor.vendor_type FROM user LEFT JOIN vendor ON vendor.user_id = user.user_id) u ON u.vendor_id = item.vendor '.$clause);

        return $result->rows;
    }

    public function getOrderStatus()
    {
        $result = $this->db->query('SELECT * FROM order_status');

        return $result->rows;
    }

    public function orderCount($status)
    {
        $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

        return $result->num_rows;
    }

    public function getOrderItemsList($order_id)
    {
        $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

        return $result->rows;
    }

    public function getCategoryList()
    {
        $result = $this->db->query('SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id ORDER BY title');

        return $result->rows;
    }

    public function getAreaList()
    {
        $result = $this->db->query('SELECT DISTINCT item.area FROM item');

        return $result->rows;
    }

    public function getRefList($ref = 0)
    {
        $sql = 'SELECT DISTINCT order_id FROM `order` WHERE order_status = 1';
        if ($ref) {
            $sql = 'SELECT DISTINCT reference FROM `item` ';
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }
}
