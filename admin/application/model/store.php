<?php
 class ModelStore extends Model
 {
     public function getStockList()
     {
         $result = $this->db->query('SELECT * From item');

         return $result->rows;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT *, c.title as item_category FROM item i LEFT JOIN item_category c ON (c.item_category_id = i.item_category)  WHERE item_id= '.$item_id);

         return $result->row;
     }

     public function getSalesOrderDetails($order_id)
     {
         $result = $this->db->query('SELECT `order`.*, c.* 
        FROM `order` LEFT JOIN (SELECT user.name,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = '.$order_id);

         return $result->row;
     }
 }
