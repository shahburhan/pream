<?php

ob_start();
 class ModelAudit extends Model
 {
     public function createAudit($data = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $this->db->query("INSERT INTO audit SET 
			department = '".$this->db->escape($data['user_type'])."', 
			audit_date = '".$this->db->escape($date)."', 
			appoint_person = '".$this->db->escape($data['appoint_person'])."'
			
			");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function updateAudit($id, $data = [])
     {
         $this->db->query("UPDATE audit SET 
			audit_date = '".$this->db->escape($data['audit_date'])."'
			
			WHERE audit_id = $id");

         return true;
     }

     public function getUserType()
     {
         $result = $this->db->query("SELECT * FROM user_type WHERE prefix = 'senior'");

         return $result->rows;
     }

     public function deleteAudit($audit_id)
     {
         $audit_id = (int) $audit_id;
         // order_status - 0 (delete storekeeper)
         $sql = "DELETE FROM audit WHERE audit_id = $audit_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function getAuditList()
     {
         $result = $this->db->query("SELECT *, DATE_FORMAT(`audit`.audit_date, '%d-%m-%Y') as audit_date FROM audit");

         return $result->rows;
     }

     public function getAuditDetails($audit_id)
     {
         $result = $this->db->query('SELECT * FROM audit WHERE audit_id ='.$audit_id);

         return $result->rows;
     }
 }
