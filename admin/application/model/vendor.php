<?php
 class ModelVendor extends Model
 {
     public function validate_email($email, $id = 0)
     {
         $q = "SELECT user_id FROM user WHERE email_id = '".$this->db->escape($email)."'";
         if ($id) {
             $q = "SELECT user_id FROM user WHERE email_id = '".$this->db->escape($email)."' && user_id != $id";
         }
         $result = $this->db->query($q);

         return ($result->num_rows == 1) ? false : true;
     }

     public function validate_phone($phone, $id = 0)
     {
         $q = "SELECT user_id FROM user WHERE phone = '".$this->db->escape($phone)."'";
         if ($id) {
             $q = "SELECT user_id FROM user WHERE phone = '".$this->db->escape($phone)."' && user_id != $id";
         }
         $result = $this->db->query($q);

         return ($result->num_rows == 1) ? false : true;
     }

     public function validate_username($username)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE username = '".$this->db->escape($username)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function createUser($data = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $lastLogin = date('Y-m-d h:i:s', time());
         $last_ip = $_SERVER['REMOTE_ADDR'];
         $user_type = (int) 9;
         $status = (int) 1;
         //$city = rand(1000,9999);
         ///$postal_code = rand(1000,9999);
         $password = rand(1000, 9999);

         $this->db->query("INSERT INTO user SET 
            email_id = '".$this->db->escape($data['email'])."',
            password = '".$this->db->escape($password)."', 
            last_ip = '".$this->db->escape($last_ip)."', 
            created = '".$this->db->escape($date)."',
            phone = '".$this->db->escape($data['phone'])."',
            address = '".$this->db->escape($data['address'])."',
            user_type = '".$this->db->escape($user_type)."',
            status = '".$this->db->escape($status)."',
            city = '".$this->db->escape($data['city'])."',
            postal_code = '".$this->db->escape($data['postal_code'])."',
            last_login = '".$this->db->escape($date)."' ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);

         return $id;
     }

     public function updateUser($id, $data = [])
     {
         $this->db->query("UPDATE user SET 
            email_id = '".$this->db->escape($data['email'])."',
            phone = '".$this->db->escape($data['phone'])."',
            address = '".$this->db->escape($data['address'])."',
            city = '".$this->db->escape($data['city'])."',
            postal_code = '".$this->db->escape($data['postal_code'])."' WHERE user_id = $id");

         return true;
     }

     public function createVendor($user_id, $company_name, $data = [])
     {
         $vendor_type = (int) 0;
         $approval_status = (int) 1;
         $this->db->query("INSERT INTO vendor SET
            user_id = '".$this->db->escape($user_id)."',
            vendor_type = '".$this->db->escape($vendor_type)."',
            company_name = '".$this->db->escape($company_name)."',
            approval_status = '".$this->db->escape($approval_status)."',
            franco = '".$this->db->escape($data['franco'])."',
            mobile = '".$this->db->escape($data['mobile'])."',
            fax_number = '".$this->db->escape($data['fax_number'])."',
            category = '".$this->db->escape($data['category'])."',
            person_responsible = '".$this->db->escape($data['person_responsible'])."'
            ");
         $vendor_id = $this->db->getLastId();

         //$this->session->login($user_id);

         //$this->createVendor($user_id);

         return $vendor_id;
     }

     public function updateVendor($vendor_id, $company_name, $data = [])
     {
         $this->db->query("UPDATE vendor SET 
            company_name = '".$this->db->escape($company_name)."',
            franco = '".$this->db->escape($data['franco'])."',
            mobile = '".$this->db->escape($data['mobile'])."',
            fax_number = '".$this->db->escape($data['fax_number'])."',
            person_responsible = '".$this->db->escape($data['person_responsible'])."'
             WHERE vendor_id = $vendor_id");

         return $vendor_id;
     }

     public function getVendorList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT v.*, u.* FROM vendor v LEFT JOIN user u  ON (v.user_id = u.user_id) WHERE status = 1 LIMIT $start, $limit");

         return $result->rows;
     }

     public function getCount()
     {
         $result = $this->db->query('SELECT * FROM vendor');

         return $result->num_rows;
     }

     public function getVendorDetails($vendor_id)
     {
         $result = $this->db->query('SELECT * FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id WHERE vendor.vendor_id= '.$vendor_id);

         return $result->row;
     }

     public function getItemListByVendor($vendor_id)
     {
         $result = $this->db->query('SELECT *, item_category.title as category_title FROM item LEFT JOIN item_category ON item_category.item_category_id = item.item_category  WHERE vendor= '.$vendor_id);

         return $result->rows;
     }

     public function getPurchaseOrderByVendor($vendor_id)
     {
         $result = $this->db->query("SELECT *, order_status.title as status_title, DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date FROM `order` LEFT JOIN order_status ON (`order`.order_status = order_status.order_status_id)  WHERE vendor= ".$vendor_id);

         return $result->rows;
     }
 }
