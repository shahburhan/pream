<?php

class ControllerSetting extends Controller
{
    public function overview()
    {
        $this->load->model('setting');
        $config = $this->model_setting->getConfig();
        $config_mapped = [];
        $data = [];

        foreach ($config as $conf) {
            $config_mapped[$conf['setting_key']] = $conf['setting_value'];
        }

        if ($_POST) {
            $date_change = false;
            $rules = [];
            foreach ($config_mapped as $k => $v) {
                if ($config_mapped['audit_date'] !== $_POST['audit_date']) {
                    $date_change = true;
                }

                $rules[$k] = 'r';
                $config_mapped[$k] = $_POST[$k];
            }
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $this->load->model('setting');
                foreach ($config_mapped as $key => $value) {
                    $this->model_setting->update($key, $value);
                }
                if ($date_change) {
                    $this->load->controller('activity');
                    $this->controller_activity->logActivity(30, $config_mapped['audit_date'], $this->session->loggedUser(), true);
                }
            } else {
                $data = array_merge($data, $this->form->error);
            }
        }

        $data['settings'] = $config_mapped;

        $this->load->controller('header');
        $header['page_title'] = 'System Config';
        $data['setting_title'] = 'System Config';
        $this->controller_header->load($header);

        $this->load->view('setting', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
