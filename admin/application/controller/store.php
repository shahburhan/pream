<?php

class ControllerStore extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Store';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function viewStock()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Stock';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Store', 'href' => '?route=stock/viewStock'];
        $breadcrumb[] = ['title'=>'View Stock', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('store');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('store');
        $items = $this->model_store->getStockList();

        $data['items'] = $items;

        $this->load->view('view_stock', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function viewItems()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Items';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Store', 'href' => '?route=stock/viewItems'];
        $breadcrumb[] = ['title'=>'View Items', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->controller_header->load($header);
        $this->load->text('viewItems');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('view_items', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('store');
                $item_details = $this->model_store->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }
                //if(count(json_decode($message_details['attachment'])) > 0)$message_details['attachment'] = RESOURCE_URL."/".json_decode($message_details['attachment'])[0];
                //else $message_details['attachment'] = 0;
                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
           // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('store');
                $sales_order = $this->model_store->getSalesOrderDetails($order_id);

                $response['sales_order'] = $sales_order;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
