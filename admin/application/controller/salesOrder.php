<?php

class ControllerSalesOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Reports';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function viewSalesOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Sales Order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewSalesOrder'];
        $breadcrumb[] = ['title'=>'Sales Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $sales_orders = $this->model_salesOrder->getSalesOrderList($start, $limit);
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/viewSalesOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('view_sales', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function cancelledOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Cancelled Order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/cancelledOrder'];
        $breadcrumb[] = ['title'=>'Cancelled Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $cancelled_orders = $this->model_salesOrder->getCancelledOrderList($start, $limit);
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/cancelledOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['cancelled_orders'] = $cancelled_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('cancelledOrder', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $sales_order = $this->model_salesOrder->getOrderDetails($order_id);
                $order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                //$purchase_order = $this->model_salesOrder->getPurchaseOrderDetails($order_id);

                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;
                //$response['purchase_order'] = $purchase_order;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function viewClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewClient'];
        $breadcrumb[] = ['title'=>'Client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $clients = $this->model_salesOrder->getClientList();

        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/viewClient&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['clients'] = $clients;
        $this->load->view('view_client', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function RoClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'RO To Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewClient'];
        $breadcrumb[] = ['title'=>'RO From Client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 5;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesOrder');
        $sales_orders = $this->model_salesOrder->getReturnSalesOrder();
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_salesOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/RoClient&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;

        $this->load->view('RO_from_client', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getReturnOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $sales_order = $this->model_salesOrder->getReturnOrderList($order_id);
                $order_items = $this->model_salesOrder->getOrderItemsList($order_id);

                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getReturnDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $return_order = $this->model_salesOrder->getReturnOrderDetails($order_id);
                $order_items = $this->model_salesOrder->getReturnOrderItemsList($order_id);
                $return_reason = $this->model_salesOrder->getReturnReason();
                $return_reason_mapped = [];
                foreach ($return_reason as $reason) {
                    $return_reason_mapped[$reason['return_reason_id']] = ucfirst(str_replace('_', ' ', $reason['title']));
                }

                $response['return_order'] = $return_order;
                $response['order_items'] = $order_items;
                $response['return_reason'] = $return_reason_mapped;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getClientDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $response = [];
                $client_id = $_GET['client_id'];
                $this->load->model('salesOrder');
                $client_details = $this->model_salesOrder->getClientDetails($client_id);
                $sales_order = $this->model_salesOrder->getSalesOrder($client_id);
                $return_order = $this->model_salesOrder->getReturnOrder($client_id);
                //$order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);

                $response['client_details'] = $client_details;
                $response['sales_order'] = $sales_order;
                $response['return_order'] = $return_order;
                //$response['raised_by'] = $created_by;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('salesOrder');
            $orders = $this->model_salesOrder->getSalesOrderList(0, 10, $clause);
            $status = $this->model_salesOrder->getOrderStatus();
            foreach ($status as $key):
                $order_status[$key['order_status_id']] = $key['title'];
            endforeach;

            echo json_encode(['order_list'=>$orders, 'order_status'=> $order_status, 'clause'=>$clause]);
        }
    }

    public function getReturnOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('salesOrder');
            $returnOrders = $this->model_salesOrder->getReturnSalesOrder(0, 10, $clause);
            $status = $this->model_salesOrder->getOrderStatus();
            foreach ($status as $key):
                $order_status[$key['order_status_id']] = $key['title'];
            endforeach;
            echo json_encode(['return_list'=>$returnOrders, 'order_status'=> $order_status, 'clause'=>$clause]);
        }
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('salesOrder');
                $item_details = $this->model_salesOrder->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $categoryList = $this->model_salesOrder->getCategoryList();
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderItemsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $itemsList = $this->model_salesOrder->getOrderItemsList($order_id);
                echo json_encode(['status' => 'success',
                    'items_list'           => $itemsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $areaList = $this->model_salesOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $clientClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['clientList'])) {
            $c = '';
            $fieldName = 'client';
            $filter = $_POST['clientList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$itemClause;
                } else {
                    $whereClause = $whereClause.' AND '.$itemClause;
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getClientsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $clientsList = $this->model_salesOrder->getClientList();
                echo json_encode(['status' => 'success',
                    'client_list'          => $clientsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getClientsAddress()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('salesOrder');
                $clientsAddress = $this->model_salesOrder->getClientsAddress($client_id);
                echo json_encode(['status' => 'success',
                    'client_address'       => $clientsAddress, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('salesOrder');
                $clause = '';

                $clause = $this->buildClause();
                $item_List = [];
                $item_List = $this->model_salesOrder->getOrderItemsList($clause);
                /*if(isset($_POST['with_client'])){
                    $item_List = $this->model_salesOrder->getAllItemsListWithClient($clause);
                }else{

                }*/

                foreach ($item_List as $key => $item) {
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                $response_data = ['status' => 'success',
                    'itemList'             => $item_List,
                    'clause'               => $clause, ];

                if (isset($_POST['client_id'])) {
                    $client = $this->model_salesOrder->getClientDetails($_POST['client_id']);
                    $type = $this->model_salesOrder->getClientType($client['client_type']);
                    $client['client_type'] = $type['title'];
                    $response_data['client'] = $client;
                }

                echo json_encode($response_data);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function placeOrder()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $data = [];
                $this->load->model('salesOrder');
                $items = $_POST['items'];
                $sell_rate = $_POST['sell_rates'];
                $quantity = $_POST['quantity'];
                $mrp = $_POST['mrp'];
                $htt = 0;
                $ttc = 0;

                for ($i = 0; $i < count($items); $i++) {
                    $p = $sell_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    $htt = $htt + (($m - ($m * $p) / 100) * $q);
                }

                $ttc = $htt + (($htt * TAX) / 100);
                $_POST['ttc'] = $ttc;
                $_POST['htt'] = $htt;

                $data['htt'] = $htt;
                $data['tva'] = TAX;
                $data['ttc'] = $ttc;
                $data['amount_paid'] = 0;
                $data['order_date'] = date('Y-m-d', time());
                $data['payment_deadline'] = date('Y-m-d', strtotime($data['order_date'].' + 45 days'));
                $data['order_status'] = 2;
                $data['order_type'] = 2;
                $data['client_id'] = $_POST['client'];
                $data['expected_date'] = $_POST['expected_date'];
                $data['delivery_address'] = $_POST['delivery_address'];
                $data['city'] = $_POST['city'];
                $data['raised_by'] = $this->session->data['logged_user_id'];
                $order_id = $this->model_salesOrder->newOrder($data);

                for ($i = 0; $i < count($items); $i++) {
                    $orderData = [];
                    $it = $items[$i];
                    $p = $sell_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    $orderData['order_id'] = $order_id;
                    $orderData['item_id'] = $it;
                    $orderData['quantity'] = $q;
                    $orderData['discount'] = 0;
                    $orderData['rate'] = $p;
                    $orderData['mrp'] = $m;
                    $orderData['order_type'] = $data['order_type'];
                    $this->model_salesOrder->addOrderItems($orderData);
                }

                echo json_encode(['status' => 'success',
                    'order_id'             => $order_id, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function cancelOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('salesOrder');

            return $this->model_salesOrder->cancelOrder($order_id);
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $refList = $this->model_salesOrder->getRefList($ref);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
