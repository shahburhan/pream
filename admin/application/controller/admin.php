<?php

ob_start();
class ControllerAdmin extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Admin';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function createAdmin()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Create Admin';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Admin', 'href' => '?route=reports/createAdmin'];
        $breadcrumb[] = ['title'=>'Create', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');

        $this->controller_header->load($header);
        $this->load->text('admin');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('admin');
        $data['user_types'] = $this->model_admin->getUserType();

        if ($_POST) {
            $rules = [
                'user_type' => 'r',
                'username'  => 'r',
                'name'      => 'r',
                'email'     => 'r',
                'password'  => 'r',
                'phone'     => 'r',
                'address'   => 'r',
                ];

            $file_rules = ['image'              => 'size_limit:2|ext:bmp,jpeg,jpg,png',
                                'identification'=> 'size_limit:2|ext:bmp,jpeg,jpg,pdf', ];
            $this->form->process_post($rules);
            $this->file->process_files($file_rules);
            $this->load->model('admin');

            if (!$this->model_admin->validate_username($this->form->data['username'])) {
                $this->form->error['error_username'] = 'username already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (empty($this->form->error) && empty($this->file->errors)) {
                $this->file->move_files();
                $this->form->data['image'] = json_encode($this->file->files['image']);
                $this->form->data['identification'] = json_encode($this->file->files['identification']);

                $id = $this->model_admin->createUser($this->form->data, $this->file->files);
                $employee_id = $this->model_admin->createAdmin($id);
                $this->load->controller('activity');
                $user_id = 9;
                $this->controller_activity->logActivity(20, $employee_id, 9, true);

                header('Location:?route=admin/viewAdmin');
                exit();
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->errors);
            }
        }

        $this->load->view('create_admin', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function viewAdmin()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Admin';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Admin', 'href' => '?route=reports/viewAdmin'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('admin');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('admin');
        $admins = $this->model_admin->getAdminList($start, $limit);

        foreach ($admins as $key => $admin) {
            $image = json_decode($admins[$key]['image']);
            if (count($image) > 0) {
                $image_url = RESOURCE_URL.'/'.$image[0];
                $admins[$key]['image'] = $image_url;
            } else {
                $admins[$key]['image'] = 0;
            }

            $pdf = json_decode($admins[$key]['identification']);
            if (count($pdf) > 0) {
                $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                $admins[$key]['identification'] = $pdf_url;
            } else {
                $admins[$key]['identification'] = 0;
            }
        }

        $total_count = $this->model_admin->userCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=admin/viewAdmin&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['admins'] = $admins;
        $this->load->view('view_admin', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function deleteAdmin()
    {
        $employee_id = (int) $_GET['employee_id'];
        if ($employee_id) {
            $this->load->model('admin');

            return $this->model_admin->deleteAdmin($employee_id);
        }
    }

    public function editUser()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!isset($_GET['employee_id'])) {
            header('location:?route=admin/index');
            exit;
        }
        $ajax_call = (isset($_GET['ac'])) ? (int) $_GET['ac'] : 0;
        $employee_id = (int) $_GET['employee_id'];

        $this->load->model('admin');
        $admin_d = $this->model_admin->getAdminDetails($employee_id);
        $data = [];

        if ($_POST) {
            $rules = [
                'name'     => 'r',
                'email'    => 'e|r',
                'address'  => 'r',
                'username' => 'r',
                'phone'    => 'r',
            ];
            $this->form->process_post($rules);

            $file_rules = ['image'              => 'size_limit:2|ext:bmp,jpeg,jpg,png',
                                'identification'=> 'size_limit:2|ext:bmp,jpeg,jpg,pdf', ];

            $this->file->process_files($file_rules);

            if (empty($this->file->errors)) {
                $this->file->move_files();
                $this->form->data['image'] = json_encode($this->file->files['image']);
                $this->form->data['identification'] = json_encode($this->file->files['identification']);
            }
            if (empty($this->form->data['image'])) {
                $this->form->data['image'] = $admin_d['image'];
            }
            if (empty($this->form->data['identification'])) {
                $this->form->data['identification'] = $admin_d['identification'];
            }
            $this->load->model('admin');

            // if(!$this->model_vendor->validate_email($this->form->data['email'], $vendor_details['user_id'])){
            //     $this->form->error['error_email'] = "Email already registered";
            //     $data = array_merge($data, $this->form->data);
            // }

            // if(!$this->model_vendor->validate_phone($this->form->data['phone'], $vendor_details['user_id'])){
            //     $this->form->error['error_phone'] = "Phone number already registered";
            //     $data = array_merge($data, $this->form->data);
            // }

            if (empty($this->form->error)) {
                $this->model_admin->updateUser($admin_d['user_id'], $this->form->data);
                $this->session->data['status'] = 'Changes Updated';
                // $this->model_vendor->updateVendor($vendor_id, $this->form->data['name'],$this->form->data);
                // $this->load->controller("activity");

                // $this->controller_activity->logActivity(1,$vendor_id,2,true);
                if ($ajax_call) {
                    echo json_encode(['status'=>true]);

                    return;
                } else {
                    header('Location:?route=admin/viewAdmin');
                    exit;
                }
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                if ($ajax_call) {
                    echo json_encode(['status'=>false]);

                    return;
                }
            }
        }

        $header['page_title'] = 'User';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Users', 'href' => '?route=admin/viewAdmin'];
        $breadcrumb[] = ['title'=>'Edit Admin', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('admin');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        // $admin_details['username'] = $admin_details['username'];
        // unset($admin_details['username']);
        // $admin_details['email'] = $admin_details['email_id'];
        // unset($admin_details['email_id']);
        $data = array_merge($data, $admin_d);

        $this->load->view('editAdmin', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getAdminDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['user_id'])) {
                $employee_id = $_GET['user_id'];
                $this->load->model('admin');
                $admin_details = $this->model_admin->getAdminDetails($employee_id);
                $image = json_decode($admin_details['image']);

                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $admin_details['image'] = $image_url;
                } else {
                    $admin_details['image'] = 0;
                }

                $pdf = json_decode($admin_details['identification']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $admin_details['identification'] = $pdf_url;
                } else {
                    $admin_details['identification'] = 0;
                }
                //if(count(json_decode($message_details['attachment'])) > 0)$message_details['attachment'] = RESOURCE_URL."/".json_decode($message_details['attachment'])[0];
                //else $message_details['attachment'] = 0;
                echo json_encode(['status' => 'success',
                    'admin_details'        => $admin_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
           // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }
}
