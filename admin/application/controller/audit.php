<?php

ob_start();
class ControllerAudit extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Audit';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function createAudit()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Create Audit';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Audit', 'href' => '?route=audit/createAudit'];
        $breadcrumb[] = ['title'=>'Create Audit', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('audit');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('audit');
        $data['user_types'] = $this->model_audit->getUserType();
        $ut = [];
        foreach ($data['user_types'] as $dt) {
            if (strpos($dt['title'], '_')) {
                $dt_temp = explode('_', $dt['title']);
                $dt['title'] = ucfirst($dt_temp[0]);
            }
            $dt['title'] = ucfirst($dt['title']);
            $ut[] = $dt;
        }

        $data['user_types'] = $ut;

        if ($_POST) {
            $rules = [
                'user_type'  => 'r',
                'audit_date' => 'r',
                ];

            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $id = $this->model_audit->createAudit($this->form->data);
                $this->load->controller('activity');
                $this->controller_activity->logActivity(21, $id, $this->session->loggedUser(), true);

                header('Location:?route=audit/viewAudit');
                exit();
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }

        $this->load->view('create_audit', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function viewAudit()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Audit';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Reports', 'href' => '?route=audit/createAudit'];
        $breadcrumb[] = ['title'=>'Create Audit', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('audit');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('audit');
        $audits = $this->model_audit->getAuditList();
        $user_type = $this->model_audit->getUserType();
        $user_type_mapped = [];
        foreach ($user_type as $key => $type) {
            $user_type_mapped[$type['user_type_id']] = ucfirst(str_replace('_', ' ', $type['title']));
        }

        $data['audits'] = $audits;
        $data['user_type'] = $user_type_mapped;

        $this->load->view('view_audit', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function deleteAudit()
    {
        $audit_id = (int) $_GET['audit_id'];
        if ($audit_id) {
            $this->load->model('audit');

            return $this->model_audit->deleteAudit($audit_id);
        }
    }

    public function editAudit()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!isset($_GET['audit_id'])) {
            header('location:?route=audit/index');
            exit;
        }
        $ajax_call = (isset($_GET['ac'])) ? (int) $_GET['ac'] : 0;
        $audit_id = (int) $_GET['audit_id'];

        $this->load->model('audit');
        $audit = $this->model_audit->getauditDetails($audit_id);
        $data = [];

        if ($_POST) {
            $rules = [
                'audit_date'=> 'r',

            ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $edit_audit = $this->model_audit->updateAudit($audit[0]['audit_id'], $this->form->data);
                if ($ajax_call) {
                    echo json_encode(['status'=>true]);

                    return;
                } else {
                    header('Location:?route=audit/viewAudit');
                    exit;
                }
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                if ($ajax_call) {
                    echo json_encode(['status'=>false]);

                    return;
                }
            }
        }

        //  if(!$_POST) {
        //     $data = array_merge($data, $audit[0]);
        //     // $data['email'] = $data['email_id'];
        // }

        $header['page_title'] = 'Audit';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'audit', 'href' => '?route=audit/view'];
        $breadcrumb[] = ['title'=>'Edit Audit', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('audit');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $data = array_merge($data, $audit);

        $this->load->view('editAudit', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
