<?php

class ControllerError extends Controller
{
    public function index()
    {
        $header['page_title'] = 'Page not found';
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }
}
