<?php

ob_start();
class ControllerSalesAgent extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Sales Agent';
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('salesAgent', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function createSalesAgent()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Create Sales Agent';

        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Agent', 'href' => '?route=salesAgent/createSalesAgent'];
        $breadcrumb[] = ['title'=>'Create', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('salesAgent');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('salesAgent');
        $data['clients'] = $this->model_salesAgent->getAllocateClient();

        if ($_POST) {
            $rules = [
                'username'    => 'r',
                'name'        => 'r',
                'email'       => 'r',
                'phone'       => 'r',
                'password'    => 'r',
                'address'     => 'r',
                'city'        => 'r',
                'client'      => 'r',
                'postal_code' => 'r',
                ];
            $file_rules = ['image'               => 'size_limit:2|ext:jpg,bmp,png',
                                'identification' => 'size_limit:2|ext:jpg,bmp,png,pdf', ];
            $this->form->process_post($rules);
            $this->file->process_files($file_rules);
            $this->load->model('salesAgent');
            if (!$this->model_salesAgent->validate_username($this->form->data['username'])) {
                $this->form->error['error_username'] = 'username already registered';
                $data = array_merge($data, $this->form->data);
            }
            //$this->file->files['image']['new_name'];
            //$this->file->files['identification']['new_name'];
            if (empty($this->form->error) && empty($this->file->errors)) {
                $id = $this->model_salesAgent->createUser($this->form->data, $this->file->files);
                $this->load->controller('activity');
                $user_id = 5;
                $this->controller_activity->logActivity(15, $id, 5, true);

                header('Location:?route=salesAgent/viewSalesAgent');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->errors);
            }
        }

        $this->load->view('createSalesAgent', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return $clause;
    }

    public function viewSalesAgent()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Create Sales Agent';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Agent', 'href' => '?route=salesAgent/viewSalesAgent'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('salesAgent');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('salesAgent');
        $agents_list = $this->model_salesAgent->getAgentList($start, $limit);
        $total_count = $this->model_salesAgent->getCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesAgent/viewSalesAgent&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['agents_list'] = $agents_list;
        $this->load->view('viewSalesAgent', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getAgentDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['employee_id'])) {
                $employee_id = (int) $_GET['employee_id'];
                $this->load->model('salesAgent');
                $agent_details = $this->model_salesAgent->getAgentDetails($employee_id);
                $order_details = $this->model_salesAgent->getOrderDetails($employee_id);
                $status = $this->model_salesAgent->getOrderStatus();
                $commission_rate = $this->model_salesAgent->getCommissionRates($employee_id);
                foreach ($status as $key):
                $order_status[$key['order_status_id']] = $key['title'];
                endforeach;
                echo json_encode(['status' => 'success',
                    'agent_details'        => $agent_details,
                    'order_status'         => $order_status,
                    'order_details'        => $order_details,
                    'commission'           => $commission_rate, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
           // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }

    public function getCommissionRates()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['employee_id'])) {
                $employee_id = (int) $_GET['employee_id'];
                $this->load->model('salesAgent');

                $commission_rate = $this->model_salesAgent->getCommissionRates($employee_id);

                echo json_encode(['commission' => $commission_rate]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function deleteAgent()
    {
        $employee_id = (int) $_GET['employee_id'];
        if ($employee_id) {
            $this->load->model('salesAgent');
            $delete_agent = $this->model_salesAgent->deleteAgent($employee_id);
            $this->load->controller('activity');
            $user_id = 5;
            $this->controller_activity->logActivity(34, $delete_agent, 5, true);

            $data['delete_agent'] = $delete_agent;
        }
    }

    public function commission()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];

        $this->load->model('salesAgent');

        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Agent', 'href' => '?route=salesAgent/viewSalesAgent'];
        $breadcrumb[] = ['title'=>'Set Commission', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;

        if ($_POST) {
            $rules = [
                'commission_rate' => 'r|b',
                'slab_out'        => 'r|b',
                'agent_name'      => 'r|b',
            ];

            $this->form->process_post($rules);

            if ($this->model_salesAgent->isSlabInValid($this->form->data['slab_in'], $this->form->data['agent_name'])) {
                $this->form->error['error_slab'] = 'Invalid Range : Intial value already in range';
            }
            if ($this->model_salesAgent->isSlabOutValid($this->form->data['slab_out'], $this->form->data['agent_name'])) {
                $this->form->error['error_slab'] = 'Invalid Range : Closing value already in range';
            }

            if (empty($this->form->error)) {
                $set_commission = $this->model_salesAgent->setCommission($this->form->data);

                $this->session->data['status'] = 'Commission successfully set';
                $this->load->controller('activity');
                $this->controller_activity->logActivity(33, $this->form->data['agent_name'], 5, true);
                $data['set_commission'] = $set_commission;
            } else {
                if (isset($this->form->error['error_commission_rate'])) {
                    $this->form->error['error_commission_rate'] = 'Please enter a valid commission rate';
                }
                $data = array_merge($this->form->error, $data);
                $data = array_merge($this->form->data, $data);
            }
        }

        $header['page_title'] = 'Set Commission Rates';
        $this->load->controller('header');
        $header['style'][] = RESOURCE_URL.'/style/jquery-ui.min';
        $header['style'][] = RESOURCE_URL.'/style/jquery-ui.structure.min';
        $this->controller_header->load($header);

        $this->load->model('salesAgent');
        $agents_list = $this->model_salesAgent->getAgentList(0, 100);
        $data['agents'] = $agents_list;
        $this->load->text('salesAgent');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $header['script'][] = RESOURCE_URL.'/js/jquery-ui.min';
        $this->load->view('commission', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function editSalesAgent()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!isset($_GET['employee_id'])) {
            header('location:?route=salesAgent/index');
            exit;
        }
        $ajax_call = (isset($_GET['ac'])) ? (int) $_GET['ac'] : 0;
        $employee_id = (int) $_GET['employee_id'];

        $this->load->model('salesAgent');
        $agent_details = $this->model_salesAgent->getAgentDetails($employee_id);

        $data = [];

        // if(!$_POST) {
        //     $data = array_merge($data, $client_details[0]);
        //     $data['email'] = $data['email_id'];
        // }

        $header['page_title'] = 'Sales Agent';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Clients', 'href' => '?route=salesAgent/view'];
        $breadcrumb[] = ['title'=>'Edit Sales Agent', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->text('salesAgent');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $agent_details['email'] = $agent_details['email_id'];
        $data = array_merge($data, $agent_details);

        if ($_POST) {
            $rules = [
                'email'           => 'e|r',
                'name'            => 'r',
                'username'        => 'r',
                'address'         => 'r',
                'commission_rate' => 'r',
                'password'        => 'r',
                'city'            => 'r',
                'postal_code'     => 'r',
                'phone'           => 'r',
            ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $this->model_salesAgent->updateUser($agent_details['user_id'], $this->form->data);
                $edit_agent = $this->model_salesAgent->updateSalesAgent($employee_id, $this->form->data);
                // $this->load->controller("activity");
                // $this->controller_activity->logActivity(35,$edit_client,$this->session->loggedUser(),true);
                $data['edit_agent'] = $edit_agent;
                echo json_encode(['status'=>true]);
                header('Location:?route=salesAgent/viewSalesAgent');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                echo json_encode(array_merge($this->form->error, ['status'=>false]));
            }
        } else {
            $this->load->controller('header');
            $this->controller_header->load($header);
            $this->load->view('editSalesAgent', $data);
            $this->load->controller('footer');
            $this->controller_footer->load($data);
        }
    }
}
