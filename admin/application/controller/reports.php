<?php

class ControllerReports extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Reports';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function purchaseReports()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Purchase Reports';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Reports', 'href' => '?route=reports/viewReports'];
        $breadcrumb[] = ['title'=>'View Purchase Reports', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        //$header['script'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min";
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';
        $this->controller_header->load($header);

        $this->load->text('reports');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('view_reports', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function salesReports()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Sale Reports';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Reports', 'href' => '?route=reports/viewReports'];
        $breadcrumb[] = ['title'=>'View Sale Reports', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        //$header['script'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min";
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';
        $this->controller_header->load($header);

        $this->load->text('reports');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('sales_reports', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function accountReports()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Accounts Reports';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Reports', 'href' => '?route=reports/viewReports'];
        $breadcrumb[] = ['title'=>'View Sale Reports', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        //$header['script'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min";
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';
        $this->controller_header->load($header);

        $this->load->text('reports');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('sales_reports', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }
}
