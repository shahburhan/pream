<?php

class ControllerPurchaseOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Reports';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function viewPurchaseOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Purchase Order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewPurchaseOrder'];
        $breadcrumb[] = ['title'=>'Purchase Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('purchaseOrder');
        $purchase_orders = $this->model_purchaseOrder->getPurchaseOrderList($start, $limit);
        $order_status = $this->model_purchaseOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_purchaseOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/viewPurchaseOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['purchase_orders'] = $purchase_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('view_purchase', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function cancelledOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Cancelled Order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/cancelledOrder'];
        $breadcrumb[] = ['title'=>'Cancelled Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('purchaseOrder');
        $cancelled_orders = $this->model_purchaseOrder->getCancelledOrderList($start, $limit);
        $order_status = $this->model_purchaseOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_purchaseOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/cancelledOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['cancelled_orders'] = $cancelled_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('cancelledPurchaseOrder', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function viewVendor()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Vendor';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewVendor'];
        $breadcrumb[] = ['title'=>'Vendor', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('purchaseOrder');
        $vendors = $this->model_purchaseOrder->getVendorsList();

        $total_count = $this->model_purchaseOrder->orderCount(1);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/viewVendor&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['vendors'] = $vendors;
        $this->load->view('view_vendor', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function RoVendor()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'RO To Vendor';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/viewClient'];
        $breadcrumb[] = ['title'=>'RO To Vendor', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('requestAction');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('purchaseOrder');
        $return_orders = $this->model_purchaseOrder->getReturnPurchaseOrder();
        $order_status = $this->model_purchaseOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $data['return_orders'] = $return_orders;
        $data['order_status'] = $order_status_mapped;

        $this->load->view('RO_to_vendor', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getVendorDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['vendor_id'])) {
                $response = [];
                $vendor_id = $_GET['vendor_id'];
                $this->load->model('purchaseOrder');
                $vendor_details = $this->model_purchaseOrder->getVendorDetails($vendor_id);
                $purchase_order = $this->model_purchaseOrder->getPurchaseOrder($vendor_id);
                $return_order = $this->model_purchaseOrder->getVendorReturnOrder($vendor_id);
                //$order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);

                $response['vendor_details'] = $vendor_details;
                $response['purchase_order'] = $purchase_order;
                $response['return_order'] = $return_order;
                //$response['raised_by'] = $created_by;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('purchaseOrder');
                //$sales_order = $this->model_requestsAction->getOrderDetails($order_id);
                $order_items = $this->model_purchaseOrder->getOrderItemsList($order_id);
                $purchase_order = $this->model_purchaseOrder->getPurchaseOrderDetails($order_id);

                //$response["sales_order"] = $sales_order;
                $response['order_items'] = $order_items;
                $response['purchase_order'] = $purchase_order;

                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getVendorsList()
    {
        $this->load->model('purchaseOrder');
        $vendors = $this->model_purchaseOrder->getVendorsList();

        echo json_encode(['vendors_list'=>$vendors]);
    }

    public function getReturnOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('purchaseOrder');
                //$sales_order = $this->model_requestsAction->getReturnOrderDetails($order_id);
                $order_items = $this->model_purchaseOrder->getOrderItemsList($order_id);
                $purchase_order = $this->model_purchaseOrder->getPurchaseReturnOrderDetails($order_id);

                //$response["sales_order"] = $sales_order;
                $response['order_items'] = $order_items;
                $response['purchase_order'] = $purchase_order;
                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getPurchaseOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('purchaseOrder');
            $orders = $this->model_purchaseOrder->getPurchaseOrderList(0, 10, $clause);
            $status = $this->model_purchaseOrder->getOrderStatus();
            foreach ($status as $key):
                $order_status[$key['order_status_id']] = $key['title'];
            endforeach;
            echo json_encode(['order_list'=>$orders, 'order_status'=> $order_status, 'clause'=>$clause]);
        }
    }

    public function getReturnOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('purchaseOrder');
            $returnOrders = $this->model_purchaseOrder->getReturnPurchaseOrder(0, 10, $clause);
            $status = $this->model_purchaseOrder->getOrderStatus();
            foreach ($status as $key):
                $order_status[$key['order_status_id']] = $key['title'];
            endforeach;
            echo json_encode(['return_list'=>$returnOrders, 'order_status'=> $order_status, 'clause'=>$clause]);
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = '`order`.order_id';
            if (isset($_POST['list']) && $_POST['list'] == 'item') {
                $fieldName = 'reference';
            }
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $categoryList = $this->model_purchaseOrder->getCategoryList();
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $areaList = $this->model_purchaseOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $refList = $this->model_purchaseOrder->getRefList($ref);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
