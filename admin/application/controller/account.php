<?php

class ControllerAccount extends Controller
{
    public function index()
    {
        $data = [];
        $this->load->view('404', $data);
    }

    public function overview()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Overview';
        $this->load->controller('header');

        $this->load->model('notification');

        $notifications = $this->model_notification->getNotifications($this->session->data['logged_user_id']);

        $this->controller_header->load($header);
        $this->load->text('account');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $data['minimum_stock_alerts'] = [];
        $data['residue_stock_alerts'] = [];
        $data['new_ro_alerts'] = [];
        $data['ro_cancel_alerts'] = [];
        $data['po_receipt_alerts'] = [];
        $data['audit_alerts'] = [];
        $data['others'] = [];
        foreach ($notifications as $notification) {
            switch ($notification['activity_type']) {
                case NEW_REQUEST_ORDER_ALERT:
                    $data['new_ro_alerts'][] = $notification;
                    break;
                case MINIMUM_STOCK_ALERT:
                    $data['minimum_stock_alerts'][] = $notification;
                    break;
                case RESIDUE_STOCK_ALERT:
                    $data['residue_stock_alerts'][] = $notification;
                    break;
                case PO_RECEIPT_ALERT:
                    $data['po_receipt_alerts'][] = $notification;
                    break;
                case AUDIT_ALERT_PURCHASER:
                    $data['audit_alerts'][] = $notification;
                    break;
                case REQUEST_ORDER_CANCEL:
                    $data['new_ro_alerts'][] = $notification;
                    break;
                default:
                    $data['others'][] = $notification;
            }
        }

        $this->load->view('dashboard', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function login()
    {
        if ($this->session->isLoggedIn()) {
            header('Location:?route=account/overview');
            exit;
        }

        $data = [];
        $data['page_title'] = 'Login';
        $this->load->text('account');
        foreach ($this->text as $key => $value) {
            $data['text_'.$key] = $value;
        }

        if ($_POST) {
            $rules = [
                 'username' => 'r',
                 'password' => 'r',
             ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $this->load->model('account');
                $is_valid = $this->model_account->login($this->form->data['username'], $this->form->data['password']);
                if (!$is_valid) {
                    $data['invalid_user'] = '1';
                } else {
                    $data['invalid_user'] = '0';
                    header('Location:?route=account/overview');
                    exit;
                }
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }
        $this->load->view('login', $data);
    }

    public function logout()
    {
        $this->session->destroySession();
    }

    public function unauthorized()
    {
        $data = [];
        $data['page_title'] = 'Login';
        $this->load->text('account');
        foreach ($this->text as $key => $value) {
            $data['text_'.$key] = $value;
        }
        $this->load->view('unauthorized', $data);
    }

    public function displayNotifications()
    {
        if (!$this->session->isLoggedIn()) {
            echo 'You need to log in';
            exit;
        }
        $this->load->model('account');
        $this->load->model('activity');
        $notifications = $this->model_account->getNotifications($this->session->data['logged_user_id']);
        for ($i = 0; $i < count($notifications); $i++) {
            $notifications[$i]['activity_type'] = $this->model_activity->getActivityType($notifications[$i]['activity_type']);
        }
        $this->load->view('notifications', ['notifications'=>$notifications]);
    }
}
