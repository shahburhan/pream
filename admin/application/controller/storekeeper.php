<?php

ob_start();
class ControllerStorekeeper extends Controller
{
    public $limit = 10;

    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        }
        $header['page_title'] = 'Storekeeper';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Storekeeper - List';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Storekeeper', 'href' => '?route=storekeeper/view'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('viewStorekeeper');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $this->limit;

        $this->load->model('storekeeper');
        $storekeepers = $this->model_storekeeper->getStorekeeperList($start, $this->limit);
        $approval_status = $this->model_storekeeper->getApprovalStatus();
        $approval_status_mapped = [];
        foreach ($approval_status as $key => $status) {
            $approval_status_mapped[$status['approval_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        foreach ($storekeepers as $key => $storekeeper) {
            $image = json_decode($storekeepers[$key]['image']);
            if (count($image) > 0) {
                $image_url = RESOURCE_URL.'/'.$image[0];
                $storekeepers[$key]['image'] = $image_url;
            } else {
                $storekeepers[$key]['image'] = 0;
            }

            $identification = json_decode($storekeepers[$key]['identification']);
            if (count($identification) > 0) {
                $identification_url = RESOURCE_URL.'/'.$identification[0];
                $storekeepers[$key]['identification'] = $identification_url;
            } else {
                $storekeepers[$key]['identification'] = 0;
            }
        }

        $total_count = $this->model_storekeeper->getCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $this->limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=storekeeper/view&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['storekeepers'] = $storekeepers;
        $data['approval_status'] = $approval_status_mapped;

        $this->load->view('storekeeperView', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function create()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Storekeeper - Create';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Storekeeper', 'href' => '?route=storekeeper/create'];
        $breadcrumb[] = ['title'=>'Create', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('createStorekeeper');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('storekeeper');

        if ($_POST) {
            $rules = [
                'username' => 'r',
                'name'     => 'r',
                'email'    => 'r',
                'password' => 'r',
                'phone'    => 'r',
                'address'  => 'r',
                ];
            $file_rules = ['image'              => 'size_limit:2|ext:bmp,jpeg,jpg,png',
                                'identification'=> 'size_limit:2|ext:bmp,jpeg,jpg,pdf', ];
            $this->form->process_post($rules);
            $this->file->process_files($file_rules);
            $this->load->model('storekeeper');

            if (!$this->model_storekeeper->validate_username($this->form->data['username'])) {
                $this->form->error['error_username'] = 'username already registered';
                $data = array_merge($data, $this->form->data);
            }

            //$this->file->files['image']['new_name'];
            //$this->file->files['identification']['new_name'];
            if (empty($this->form->error) && empty($this->file->errors)) {
                $this->file->move_files();
                $this->form->data['image'] = json_encode($this->file->files['image']);
                $this->form->data['identification'] = json_encode($this->file->files['identification']);
                $id = $this->model_storekeeper->createUser($this->form->data, $this->file->files);
                $employee_id = $this->model_storekeeper->createStorekeeper($id);
                $this->load->controller('activity');
                $user_id = 7;
                $this->controller_activity->logActivity(19, $employee_id, 7, true);

                header('Location:?route=storekeeper/view');
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->errors);
            }
        }

        $this->load->view('storekeeperCreate', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function deleteStorekeeper()
    {
        $employee_id = (int) $_GET['employee_id'];
        if ($employee_id) {
            $this->load->model('storekeeper');

            return $this->model_storekeeper->deleteStorekeeper($employee_id);
        }
    }

    public function editStorekeeper()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!isset($_GET['employee_id'])) {
            header('location:?route=storekeeper/index');
            exit;
        }
        $ajax_call = (isset($_GET['ac'])) ? (int) $_GET['ac'] : 0;
        $employee_id = (int) $_GET['employee_id'];

        $this->load->model('storekeeper');
        $storekeeper = $this->model_storekeeper->getStorekeeperDetails($employee_id);
        $data = [];

        if ($_POST) {
            $rules = [
                'username'=> 'r',
                'name'    => 'r',
                'email'   => 'e|r',
                'address' => 'r',
                'phone'   => 'r',
            ];
            $this->form->process_post($rules);

            $file_rules = ['image'              => 'size_limit:2|ext:bmp,jpeg,jpg,png',
                                'identification'=> 'size_limit:2|ext:bmp,jpeg,jpg,pdf', ];

            $this->file->process_files($file_rules);

            // if(!$this->model_storekeeper->validate_email($this->form->data['email'], $data['user_id'])){
            //     $this->form->error['error_email'] = "Email already registered";
            //     $data = array_merge($data, $this->form->data);
            // }

            // if(!$this->model_storekeeper->validate_phone($this->form->data['phone'], $data['user_id'])){
            //     $this->form->error['error_phone'] = "Phone number already registered";
            //     $data = array_merge($data, $this->form->data);
            // }

            if (empty($this->form->error) && empty($this->file->errors)) {
                $this->file->move_files();
                $this->form->data['image'] = json_encode($this->file->files['image']);
                $this->form->data['identification'] = json_encode($this->file->files['identification']);

                $editStorekeeper = $this->model_storekeeper->updateUser($storekeeper[0]['user_id'], $this->form->data, $this->file->data);
                $this->load->controller('activity');

                $this->controller_activity->logActivity(31, $editStorekeeper, $this->session->loggedUser(), true);
                $data['editStorekeeper'] = $editStorekeeper;
                // $this->model_storekeeper->updateStorekeeper($employee_id, $this->form->data);
                // $this->load->controller("activity");

                // $this->controller_activity->logActivity(1,$vendor_id,2,true);
                // if($ajax_call){
                //     echo json_encode(array("status"=>true));
                //     return;
                // }
                header('Location:?route=storekeeper/view');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->data);
                $data = array_merge($data, $this->file->errors);

                if ($ajax_call) {
                    echo json_encode(['status'=>false]);

                    return;
                }
            }
        }

        if (!$_POST) {
            $data = array_merge($data, $storekeeper[0]);
            $data['email'] = $data['email_id'];
        }

        $header['page_title'] = 'Storekeeper';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'storekeeper', 'href' => '?route=storekeeper/view'];
        $breadcrumb[] = ['title'=>'Edit Storekeeper', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('createStorekeeper');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('editStorekeeper', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getStorekeeperDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['employee_id'])) {
                $employee_id = $_GET['employee_id'];
                $this->load->model('storekeeper');
                $store_details = array_shift($this->model_storekeeper->getStorekeeperDetails($employee_id));
                $image = json_decode($store_details['image']);

                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $store_details['image'] = $image_url;
                } else {
                    $store_details['image'] = 0;
                }

                $identification = json_decode($store_details['identification']);
                if (count($identification) > 0) {
                    $identification_url = RESOURCE_URL.'/'.$identification[0];
                    $store_details['identification'] = $identification_url;
                } else {
                    $store_details['identification'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'store_details'        => $store_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
