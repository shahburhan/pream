<?php

class ControllerRequestsAction extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Reports';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    // function viewSalesOrder(){
    //        if(!$this->session->isLoggedIn()  ) {
    //            header("Location:?route=account/login");
    //            exit;
    //        }else if(!$this->session->isRightUser()){
    //            header("Location:?route=account/unauthorized");
    //            exit;
    //        }
    //  	$data = array();
    // 	$header['page_title'] = "View Sales Order";
    //        $breadcrumb[] = array("title"=>"Home", "href" => "?route=account/overview");
    //        $breadcrumb[] = array("title"=>"Action", "href" => "?route=reports/viewSalesOrder");
    //        $breadcrumb[] = array("title"=>"Sales Order", "href" => "");
    //        $header['breadcrumb'] = $breadcrumb;
    // 	$this->load->controller("header");
    // 	$this->controller_header->load($header);
    // 	$this->load->text("requestAction");

    //        foreach ($this->text as $key => $value) {
    //            //assign text variables
    //            $data['text_'.$key] = $value;
    //        }

    //        $limit = 10;
    //        $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
    //        $start = ($page-1)*$limit;

    //        $this->load->model("requestsAction");
    //        $sales_orders = $this->model_requestsAction->getSalesOrderList($start,$limit);
    //        $order_status = $this->model_requestsAction->getOrderStatus();
    //        $order_status_mapped = array();
    //        foreach ($order_status as $key => $status) {
    //            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace("_", " ", $status['title']));
    //        }

    //        $total_count = $this->model_requestsAction->orderCount(2);
    //        $this->load->helper("pagination");
    //        $this->pagination->limit = $limit;
    //        $this->pagination->total = $total_count;
    //        $this->pagination->page = $page;
    //        $this->pagination->url = HTTP_SERVER."?route=requestsAction/viewSalesOrder&page={page}";
    //        $data['pagination'] = $this->pagination->render();

    //        $data['sales_orders'] = $sales_orders;
    //        $data['order_status'] = $order_status_mapped;
    // 	$this->load->view("view_sales",$data);

    // 	$this->load->controller("footer");
    // 	$this->controller_footer->load($data);
    // }

    //    public function getOrderDetails(){
    //        header('Content-Type: application/json');
    //        if ($_GET) {
    //            if (isset($_GET['order_id'])) {
    //                $response = array();
    //                $order_id = $_GET['order_id'];
    //                $this->load->model("requestsAction");
    //                $sales_order = $this->model_requestsAction->getOrderDetails($order_id);
    //                $order_items = $this->model_requestsAction->getOrderItemsList($order_id);
    //                $purchase_order = $this->model_requestsAction->getPurchaseOrderDetails($order_id);

    //                $response["sales_order"] = $sales_order;
    //                $response['order_items'] = $order_items;
    //                $response['purchase_order'] = $purchase_order;

    //                echo json_encode($response);
    //                } else {
    //                echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //            }
    //        }

    //    }
    // function viewPurchaseOrder(){
    //        if(!$this->session->isLoggedIn()  ) {
    //            header("Location:?route=account/login");
    //            exit;
    //        }else if(!$this->session->isRightUser()){
    //            header("Location:?route=account/unauthorized");
    //            exit;
    //        }
    //  	$data = array();
    // 	$header['page_title'] = "View Purchase Order";
    //        $breadcrumb[] = array("title"=>"Home", "href" => "?route=account/overview");
    //        $breadcrumb[] = array("title"=>"Action", "href" => "?route=reports/viewPurchaseOrder");
    //        $breadcrumb[] = array("title"=>"Purchase Order", "href" => "");
    //        $header['breadcrumb'] = $breadcrumb;
    // 	$this->load->controller("header");
    // 	$this->controller_header->load($header);
    // 	$this->load->text("requestAction");

    //        foreach ($this->text as $key => $value) {
    //            //assign text variables
    //            $data['text_'.$key] = $value;
    //        }

    //        $limit = 10;
    //        $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
    //        $start = ($page-1)*$limit;

    //        $this->load->model("requestsAction");
    //        $purchase_orders = $this->model_requestsAction->getPurchaseOrderList($start,$limit);
    //        $order_status = $this->model_requestsAction->getOrderStatus();
    //        $order_status_mapped = array();
    //        foreach ($order_status as $key => $status) {
    //            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace("_", " ", $status['title']));
    //        }

    //        $total_count = $this->model_requestsAction->orderCount(2);
    //        $this->load->helper("pagination");
    //        $this->pagination->limit = $limit;
    //        $this->pagination->total = $total_count;
    //        $this->pagination->page = $page;
    //        $this->pagination->url = HTTP_SERVER."?route=requestsAction/viewPurchaseOrder&page={page}";
    //        $data['pagination'] = $this->pagination->render();

    //        $data['purchase_orders'] = $purchase_orders;
    //        $data['order_status'] = $order_status_mapped;
    // 	$this->load->view("view_purchase",$data);

    // 	$this->load->controller("footer");
    // 	$this->controller_footer->load($data);
    // }
    // function viewVendor(){
    //     if(!$this->session->isLoggedIn()  ) {
    //         header("Location:?route=account/login");
    //         exit;
    //     }else if(!$this->session->isRightUser()){
    //         header("Location:?route=account/unauthorized");
    //         exit;
    //     }
    //     $data = array();
    //     $header['page_title'] = "View Vendor";
    //     $breadcrumb[] = array("title"=>"Home", "href" => "?route=account/overview");
    //     $breadcrumb[] = array("title"=>"Action", "href" => "?route=reports/viewVendor");
    //     $breadcrumb[] = array("title"=>"Vendor", "href" => "");
    //     $header['breadcrumb'] = $breadcrumb;
    //     $this->load->controller("header");
    //     $this->controller_header->load($header);
    //     $this->load->text("requestAction");

    //     foreach ($this->text as $key => $value) {
    //         //assign text variables
    //         $data['text_'.$key] = $value;
    //     }

    //     $limit = 10;
    //     $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
    //     $start = ($page-1)*$limit;

    //     $this->load->model("requestsAction");
    //     $vendors = $this->model_requestsAction->getVendorsList();

    //     $total_count = $this->model_requestsAction->orderCount(1);
    //     $this->load->helper("pagination");
    //     $this->pagination->limit = $limit;
    //     $this->pagination->total = $total_count;
    //     $this->pagination->page = $page;
    //     $this->pagination->url = HTTP_SERVER."?route=requestsAction/viewVendor&page={page}";
    //     $data['pagination'] = $this->pagination->render();

    //     $data["vendors"] = $vendors;
    //     $this->load->view("view_vendor",$data);

    //     $this->load->controller("footer");
    //     $this->controller_footer->load($data);
    // }

    // function viewClient(){
    //        if(!$this->session->isLoggedIn()  ) {
    //            header("Location:?route=account/login");
    //            exit;
    //        }else if(!$this->session->isRightUser()){
    //            header("Location:?route=account/unauthorized");
    //            exit;
    //        }
    //  	$data = array();
    // 	$header['page_title'] = "View Client";
    //        $breadcrumb[] = array("title"=>"Home", "href" => "?route=account/overview");
    //        $breadcrumb[] = array("title"=>"Action", "href" => "?route=reports/viewClient");
    //        $breadcrumb[] = array("title"=>"Client", "href" => "");
    //        $header['breadcrumb'] = $breadcrumb;
    // 	$this->load->controller("header");
    // 	$this->controller_header->load($header);
    // 	$this->load->text("requestAction");

    //        foreach ($this->text as $key => $value) {
    //            //assign text variables
    //            $data['text_'.$key] = $value;
    //        }

    //        $limit = 10;
    //        $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
    //        $start = ($page-1)*$limit;

    //        $this->load->model("requestsAction");
    //        $clients = $this->model_requestsAction->getClientList();

    //        $total_count = $this->model_requestsAction->orderCount(2);
    //        $this->load->helper("pagination");
    //        $this->pagination->limit = $limit;
    //        $this->pagination->total = $total_count;
    //        $this->pagination->page = $page;
    //        $this->pagination->url = HTTP_SERVER."?route=requestsAction/viewClient&page={page}";
    //        $data['pagination'] = $this->pagination->render();

    //        $data["clients"] = $clients;
    // 	$this->load->view("view_client",$data);

    // 	$this->load->controller("footer");
    // 	$this->controller_footer->load($data);
    // }

    // function RoVendor(){
    //        if(!$this->session->isLoggedIn()  ) {
    //            header("Location:?route=account/login");
    //            exit;
    //        }else if(!$this->session->isRightUser()){
    //            header("Location:?route=account/unauthorized");
    //            exit;
    //        }
    //  	$data = array();
    // 	$header['page_title'] = "RO To Vendor";
    //        $breadcrumb[] = array("title"=>"Home", "href" => "?route=account/overview");
    //        $breadcrumb[] = array("title"=>"Action", "href" => "?route=reports/viewClient");
    //        $breadcrumb[] = array("title"=>"RO To Vendor", "href" => "");
    //        $header['breadcrumb'] = $breadcrumb;
    // 	$this->load->controller("header");
    // 	$this->controller_header->load($header);
    // 	$this->load->text("requestAction");

    //        foreach ($this->text as $key => $value) {
    //            //assign text variables
    //            $data['text_'.$key] = $value;
    //        }

    //        $this->load->model("requestsAction");
    //        $purchase_orders = $this->model_requestsAction->getReturnPurchaseOrder();
    //        $order_status = $this->model_requestsAction->getOrderStatus();
    //        $order_status_mapped = array();
    //        foreach ($order_status as $key => $status) {
    //            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace("_", " ", $status['title']));
    //        }

    //        $data['purchase_orders'] = $purchase_orders;
    //        $data['order_status'] = $order_status_mapped;

    // 	$this->load->view("RO_to_vendor",$data);

    // 	$this->load->controller("footer");
    // 	$this->controller_footer->load($data);
    // }

    // function RoClient(){
    //        if(!$this->session->isLoggedIn()  ) {
    //            header("Location:?route=account/login");
    //            exit;
    //        }else if(!$this->session->isRightUser()){
    //            header("Location:?route=account/unauthorized");
    //            exit;
    //        }
    //  	$data = array();
    // 	$header['page_title'] = "RO To Client";
    //        $breadcrumb[] = array("title"=>"Home", "href" => "?route=account/overview");
    //        $breadcrumb[] = array("title"=>"Action", "href" => "?route=reports/viewClient");
    //        $breadcrumb[] = array("title"=>"RO From Client", "href" => "");
    //        $header['breadcrumb'] = $breadcrumb;
    // 	$this->load->controller("header");
    // 	$this->controller_header->load($header);
    // 	$this->load->text("requestAction");

    //        foreach ($this->text as $key => $value) {
    //            //assign text variables
    //            $data['text_'.$key] = $value;
    //        }

    //        $this->load->model("requestsAction");
    //        $sales_orders = $this->model_requestsAction->getReturnSalesOrder();
    //        $order_status = $this->model_requestsAction->getOrderStatus();
    //        $order_status_mapped = array();
    //        foreach ($order_status as $key => $status) {
    //            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace("_", " ", $status['title']));
    //        }

    //        $data['sales_orders'] = $sales_orders;
    //        $data['order_status'] = $order_status_mapped;

    // 	$this->load->view("RO_from_client",$data);

    // 	$this->load->controller("footer");
    // 	$this->controller_footer->load($data);
    // }

    //    public function getReturnOrderDetails(){
    //        header('Content-Type: application/json');
    //        if ($_GET) {
    //            if (isset($_GET['order_id'])) {
    //                $response = array();
    //                $order_id = $_GET['order_id'];
    //                $this->load->model("requestsAction");
    //                $sales_order = $this->model_requestsAction->getReturnOrderDetails($order_id);
    //                $order_items = $this->model_requestsAction->getOrderItemsList($order_id);
    //                $purchase_order = $this->model_requestsAction->getPurchaseReturnOrderDetails($order_id);

    //                $response["sales_order"] = $sales_order;
    //                $response['order_items'] = $order_items;
    //                $response["purchase_order"] = $purchase_order;
    //                echo json_encode($response);
    //            } else {
    //                echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //            }
    //        }

    //    }

    //  public function getClientDetails(){
    //     header('Content-Type: application/json');
    //     if ($_GET) {
    //         if (isset($_GET['client_id'])) {
    //             $response = array();
    //             $client_id = $_GET['client_id'];
    //             $this->load->model("requestsAction");
    //             $client_details = $this->model_requestsAction->getClientDetails($client_id);
    //             $sales_order = $this->model_requestsAction->getSalesOrder($client_id);
    //             $return_order = $this->model_requestsAction->getReturnOrder($client_id);
    //             //$order_items = $this->model_salesOrder->getOrderItemsList($order_id);
    //             //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);

    //             $response["client_details"] = $client_details;
    //             $response['sales_order'] = $sales_order;
    //             $response['return_order'] = $return_order;
    //             //$response['raised_by'] = $created_by;

    //             echo json_encode($response);
    //             } else {
    //             echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //         }
    //     }

    // }

    // public function getVendorDetails(){
    //     header('Content-Type: application/json');
    //     if ($_GET) {
    //         if (isset($_GET['vendor_id'])) {
    //             $response = array();
    //             $vendor_id = $_GET['vendor_id'];
    //             $this->load->model("requestsAction");
    //             $vendor_details = $this->model_requestsAction->getVendorDetails($vendor_id);
    //              $purchase_order = $this->model_requestsAction->getPurchaseOrder($vendor_id);
    //              $return_order = $this->model_requestsAction->getVendorReturnOrder($vendor_id);
    //             //$order_items = $this->model_salesOrder->getOrderItemsList($order_id);
    //             //$created_by =$this->model_salesOrder->getUserDetails($sales_order['raised_by']);

    //             $response["vendor_details"] = $vendor_details;
    //             $response['purchase_order'] = $purchase_order;
    //             $response['return_order'] = $return_order;
    //             //$response['raised_by'] = $created_by;

    //             echo json_encode($response);
    //             } else {
    //             echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //         }
    //     }

    // }

    public function getOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('requestsAction');
            $orders = $this->model_requestsAction->getPurchaseOrderList(0, 10, $clause);

            echo json_encode(['order_list'=>$orders, 'clause'=>$clause]);
        }
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('requestsAction');
                $item_details = $this->model_requestsAction->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('requestsAction');
                $categoryList = $this->model_requestsAction->getCategoryList();
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('requestsAction');
                $areaList = $this->model_requestsAction->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = 'order_id';
            if (isset($_POST['list']) && $_POST['list'] == 'item') {
                $fieldName = 'reference';
            }
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$itemClause.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$itemClause.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return $clause;
    }

    public function getVendorsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('requestsAction');
                $vendorsList = $this->model_requestsAction->getVendorsList($start = 0, $limit = 10);
                echo json_encode(['status'             => 'success',
                                        'vendors_list' => $vendorsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('requestsAction');
                $clause = '';
                $clause = $this->buildClause();
                $item_List = [];
                if (isset($_POST['with_vendor'])) {
                    $item_List = $this->model_requestsAction->getAllItemsListWithVendor($clause);
                } else {
                    $item_List = $this->model_requestsAction->getAllItemsList($clause);
                }

                foreach ($item_List as $key => $item) {
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                echo json_encode(['status' => 'success',
                    'itemList'             => $item_List,
                    'clause'               => $clause, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    // function placeOrder(){
    //     header('Content-Type: application/json');
    //     if($_POST){
    //         $rules = array(
    //             "expected_date" => "r"
    //             );

    //              $this->form->process_post($rules);
    //         if(isset($_POST['all']) && empty($this->form->error)){

    //             $data = array();
    //             $this->load->model("requestsAction");
    //             $items = $_POST['items'];
    //             $purchase_rate = $_POST['purchase_rates'];
    //             $quantity = $_POST['quantity'];
    //             $mrp = $_POST['mrp'];
    //             $htt = 0;
    //             $ttc = 0;

    //             for($i = 0; $i <count($items); $i++ ){
    //                $p = $purchase_rate[$i]; //Type Removed
    //                $q = $quantity[$i]; //Type Removed
    //                $m = $mrp[$i]; //Type Removed
    //                $htt += $p*$q;
    //             }

    //             $ttc = $htt + (($htt * TAX)/100);
    //             $_POST['ttc'] = $ttc;
    //             $_POST['htt'] = $htt ;

    //             $data['htt'] = $htt;
    //             $data['tva'] = TAX;
    //             $data['ttc'] = $ttc;
    //             $data['amount_paid'] = 0;
    //             $data['order_date'] = date("Y-m-d", time());
    //             $data['payment_deadline'] = date('Y-m-d', strtotime($data['order_date'] . " + 45 days"));
    //             $data['order_status'] = 1;
    //             $data['order_type'] = 1;
    //             $data['vendor_id'] = $_POST['vendor'];
    //             // $data["person_responsible"] = $_POST['person_responsible'];
    //             $data["expected_date"] = $_POST['expected_date'];
    //             $data["delivery_address"] = $_POST['delivery_address'];
    //             $data["city"] = $_POST['city'];
    //             $data['raised_by'] = $this->session->data['logged_user_id'];
    //             $order_id = $this->model_requestsAction->newOrder($data);

    //             for($i = 0; $i <count($items); $i++ ){
    //                 $orderData = array();
    //                 $it = $items[$i];
    //                 $p =  $purchase_rate[$i]; //Type Removed
    //                 $q = $quantity[$i]; //Type Removed
    //                 $m = $mrp[$i]; //Type Removed
    //                 $orderData['order_id'] = $order_id;
    //                 $orderData['item_id'] = $it;
    //                 $orderData['quantity'] = $q;
    //                 $orderData['discount'] = 0;
    //                 $orderData['rate'] = $p;
    //                 $orderData['mrp'] = $m;
    //                 $orderData['order_type'] = $data['order_type'];
    //                 $this->model_requestsAction->addOrderItems($orderData);
    //             }

    //             echo json_encode(array('status' => 'success',
    //                 'order_id' => $order_id));
    //         }else{
    //             echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //         }
    //     }
    // }

    public function cancelOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('requestsAction');

            return $this->model_requestsAction->cancelOrder($order_id);
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $this->load->model('requestsAction');
                $refList = $this->model_requestsAction->getRefList($ref);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    // public function cancelOrder(){
    //     $order_id = (int)$_GET['order_id'];
    //     if($order_id){
    //         $this->load->model("requestsAction");
    //         return $this->model_requestsAction->cancelOrder($order_id);
    //     }
    // }

    // public function getRefList(){
    //      header('Content-Type: application/json');
    //     if($_GET){
    //         $ref = 0;
    //         if(isset($_GET['list']) && $_GET['list'] == "item") $ref = 1;
    //         if(isset($_GET['all'])){
    //             $this->load->model("requestsAction");
    //             $refList = $this->model_requestsAction->getRefList($ref);
    //             echo json_encode(array('status' => 'success',
    //                                     'ref_list' => $refList));
    //         }else{
    //             echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //         }
    //     }
    // }
}
