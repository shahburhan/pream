<?php

//view sales order view start
$text_requestAction['title'] = 'Sales Orders';
$text_requestAction['search'] = 'Search';

//table headings
$text_requestAction['order_number'] = 'Order Number';
$text_requestAction['order_date'] = 'Order Date';
$text_requestAction['client_name'] = 'Client Name';
$text_requestAction['address'] = 'Address';
$text_requestAction['delivery_address'] = 'Delivery Address';
$text_requestAction['order_quantity'] = 'Order Quantity';
$text_requestAction['raised_by'] = 'Raised By';

//bulk order
$text_requestAction['bulk_order'] = 'Bulk Order';
$text_requestAction['so'] = 'SO#';
$text_requestAction['order_id'] = 'SO#';
$text_requestAction['ttc'] = 'T.T.C.';
$text_requestAction['discount_rate'] = 'Discount Rate';
$text_requestAction['delivery_date'] = 'Delivery Date';
$text_requestAction['person_responsible'] = 'Person Responsible';
//item details
$text_requestAction['item_id'] = 'Item Id';
$text_requestAction['item_name'] = 'Item Name';
$text_requestAction['brand_name'] = 'Brand Name';
$text_requestAction['mrp'] = 'MRP';
$text_requestAction['quantity'] = 'Quantity';
$text_requestAction['total'] = 'Total';
$text_requestAction['sr_no'] = 'Sr.No.';
$text_requestAction['reference'] = 'Reference';
$text_requestAction['sell_rate'] = 'Sell Rate';
$text_requestAction['details'] = 'Details';
//view sales order end//

//-----------------------------------//

 //ro from client start

$text_requestAction['return_order'] = 'Return Order';
$text_requestAction['return_number'] = 'Return Number';
$text_requestAction['return_date'] = 'Return Date';
$text_requestAction['client_name'] = 'Client Name';
$text_requestAction['address'] = 'Address';
$text_requestAction['reason'] = 'Reason';
$text_requestAction['created_by'] = 'Created By';
$text_requestAction['adjusted'] = 'Adjusted';
$text_requestAction['return_reason'] = 'Return Reason';
//ro details
$text_requestAction['grand_total'] = 'Grand Total';
$text_requestAction['return_date'] = 'Return Date';
$text_requestAction['delivery_date'] = 'Delivery Date';
$text_requestAction['purchaser'] = 'Purchaser';
$text_requestAction['sell_rate'] = 'Sell Rate';
$text_requestAction['status'] = 'Status';
//ro from client end//

//--------------------------------------//

//client view start

$text_requestAction['title_client'] = 'Clients';

//table headings
$text_requestAction['client_id'] = 'Client Id';
$text_requestAction['client_id_m'] = 'Client Id#';
$text_requestAction['order_id_p'] = 'PO#';
$text_requestAction['client_name'] = 'Client Name';
$text_requestAction['client_email'] = 'Client Email';
$text_requestAction['client_contact'] = 'Client Contact';
$text_requestAction['client_address'] = 'Client Address';
$text_requestAction['approval_status'] = 'Approval Status';
$text_requestAction['action'] = 'Action';
$text_requestAction['credit_limit'] = 'Credit Limit <br> Change Request';
$text_requestAction['edit'] = 'Edit';
$text_requestAction['client_details'] = 'Client Details';
$text_requestAction['order_status'] = 'Order Status';
//return order
$text_requestAction['return_order'] = 'Retrun Order';
$text_requestAction['return_number'] = 'Return Number';
$text_requestAction['vendor_name'] = 'Vendor Name';
$text_requestAction['address'] = 'Address';
$text_requestAction['reason'] = 'Reason';

//details
$text_requestAction['current_credit_limit'] = 'Current Credit Limit';
$text_requestAction['proposed_by'] = 'Proposed By';
$text_requestAction['forward_date_by'] = 'Forward Date By';
$text_requestAction['agent_contact'] = 'Agent Contact';
$text_requestAction['credit_change_request'] = 'Credit Change Request';

//btn
$text_requestAction['approve'] = 'Approve';
$text_requestAction['disapprove'] = 'Disapprove';

//client view end//

//-------------------------------------------------//

//purchase order view start
$text_requestAction['purchase_order'] = 'Purchase Order';
//header
$text_requestAction['search'] = 'Search';

//table headings

$text_requestAction['sr'] = 'Sr.No.';
$text_requestAction['vendor'] = 'Vendor';
$text_requestAction['bol_date'] = 'BOL Date';
$text_requestAction['po'] = 'PO#';
$text_requestAction['amount_ht'] = 'Amount(HTT)';
$text_requestAction['ro'] = 'RO#';
$text_requestAction['amount_ttc'] = 'Amount(TTC)';
$text_requestAction['paid'] = 'Paid';
$text_requestAction['balance'] = 'Balance';
$text_requestAction['action'] = 'Action';

//action
$text_requestAction['make_payment'] = 'Make Payment';
$text_requestAction['add_payment'] = 'Add To Payment';
$text_requestAction['remove_payment'] = 'Remove From Payment';
$text_requestAction['adjust'] = 'Adjust';

//details
$text_requestAction['grand_total'] = 'Grand Total';
$text_requestAction['adjust'] = 'Adjust';
$text_requestAction['order_date'] = 'Order Date';
$text_requestAction['vendor_name'] = 'Vendor Name';
$text_requestAction['vendor_address'] = 'Vendor Address';
$text_requestAction['delivery_date'] = 'Delivery Date';
$text_requestAction['person_responsible'] = 'Person Reaponsible';
$text_requestAction['delivery_address'] = 'Delivery Address';
$text_requestAction['item_order'] = 'Return Order Items';
//items
$text_requestAction['item_id'] = 'Item Id';
$text_requestAction['item_name'] = 'Item Name';
$text_requestAction['brand_name'] = 'Brand Name';
$text_requestAction['buy_rate'] = 'Buy Rate';
$text_requestAction['quantity'] = 'Quantity';
$text_requestAction['total'] = 'Total';

//po view end//

//-------------------------------------------//

//vendor view start
$text_requestAction['vendor_id'] = 'Vendor Id';
$text_requestAction['vendor_id_m'] = 'Vendor Id#';
$text_requestAction['vendor_name'] = 'Vendor Name';
$text_requestAction['vendor_email'] = 'Vendor Email';
$text_requestAction['vendor_contact'] = 'Vendor Contact';
$text_requestAction['vendor_address'] = 'Vendor Address';
$text_requestAction['vendor_details'] = 'Vendor Details';

//return to vendor end//
