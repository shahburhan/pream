<?php

$text_salesAgent['title'] = 'Create Sales Agent';
$text_salesAgent['title_view'] = 'View Sales Agent';

//form labels
$text_salesAgent['username'] = 'Username';
$text_salesAgent['sales_agent_name'] = 'Sales Agent Name';
//$text_salesAgent['last_name'] = "Last Name";
$text_salesAgent['password'] = 'Password';
$text_salesAgent['email_createsales'] = 'Email';
$text_salesAgent['phone_createsales'] = 'Phone';
$text_salesAgent['address_createsales'] = 'Address';
$text_salesAgent['travel_limit'] = 'Travel Expense Limit';
$text_salesAgent['daily_limit'] = 'Daily Limit';
$text_salesAgent['set_commission_rate'] = 'Set Commission Rate';
$text_salesAgent['add_profile'] = 'Add Image';
$text_salesAgent['add_identification'] = 'Add Identification';
$text_salesAgent['field_required'] = 'This Field Is Required';
$text_salesAgent['allocate_client'] = 'Allocate Client';

//btn
$text_salesAgent['submit_salesAgent'] = 'Submit';

//view sales Agent
$text_salesAgent['search'] = 'Search';
$text_salesAgent['agent_id'] = 'Agent Id';
$text_salesAgent['details'] = 'Details';
//table headings
$text_salesAgent['name_viewsales'] = 'Name';
$text_salesAgent['email_viewsales'] = 'Email';
$text_salesAgent['phone_viewsales'] = 'Phone';
$text_salesAgent['address_viewsales'] = 'Address';
$text_salesAgent['view_travel_limit'] = 'Travel Limit';
$text_salesAgent['view_daily_limit'] = 'Daily Limit';
$text_salesAgent['city'] = 'City';
$text_salesAgent['postal_code'] = 'Postal Code';
$text_salesAgent['view_commission_rate'] = 'Commission Rate';
$text_salesAgent['view_action'] = 'Action';

//details
$text_salesAgent['agent'] = 'Agents';
$text_salesAgent['agent_name'] = 'Agent';
$text_salesAgent['employee_id'] = 'Employee Id';

$text_salesAgent['title_sales'] = 'Sales Order';
$text_salesAgent['order_number'] = 'Order Number';
$text_salesAgent['order_date'] = 'Order Date';
$text_salesAgent['client_name'] = 'Client Name';
$text_salesAgent['address'] = 'Address';
$text_salesAgent['delivery_address'] = 'Delivery Address';
$text_salesAgent['order_action'] = 'Action';
$text_salesAgent['dispatch'] = 'Dispatch';
$text_salesAgent['track'] = 'Track';
$text_salesAgent['order'] = 'Orders';
$text_salesAgent['status'] = 'Status';
$text_salesAgent['add_commission'] = 'Add Commission';
