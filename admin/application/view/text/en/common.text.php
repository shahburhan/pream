<?php

//--Navigation options
$text_common['overview'] = 'Overview';
$text_common['store'] = 'Store';
$text_common['reports'] = 'Reports';
$text_common['requests_action'] = 'Requests Action';
$text_common['admin'] = 'User';
$text_common['audit'] = 'Audit';
$text_common['notifications'] = 'Notifications';

//--store

$text_common['view_order_stock'] = 'View Stock';
$text_common['view_order_items'] = 'View Items';

//--reports
$text_common['view_purchase_report'] = 'View Purchase Report';
$text_common['view_sales_report'] = 'View Sales Report';
$text_common['view_account_report'] = 'View Accounts Report';

//--Requests Action
$text_common['sales_order'] = 'Bulk Order Approved/Disapproved';
$text_common['purchase_order'] = 'Purchase Order';
$text_common['vendor'] = 'Vendor';
$text_common['client'] = 'Client';
$text_common['ro_vendor'] = 'RO To Vendor';
$text_common['ro_client'] = 'RO From Client';

//--admin
$text_common['create_admin'] = 'Create User';
$text_common['view_admin'] = 'View User';

//--audit
$text_common['create_audit'] = 'Create Audit';
$text_common['view_audit'] = 'View Audit';

//--notifications
$text_common['minimum_stock_alert'] = 'Minimum Stock Alert';
$text_common['residue_stock_alert'] = 'Residue Stock Alert';
$text_common['new_request_order'] = 'New Request Order Alert';
$text_common['ro_cancel'] = 'RO Cancel Alert';
$text_common['po_receipt'] = 'PO Receipt Alert';
$text_common['audit_alert'] = 'Audit Alert';
$text_common['payment'] = 'Payment';
$text_common['items'] = 'Items';
//$text_common['sales_order'] = "Sales Alert";
