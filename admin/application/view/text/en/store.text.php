<?php

$text_store['title'] = 'Stock';
$text_store['search'] = 'Search';
//table headings
$text_store['item_id'] = 'Item Id';
$text_store['item_name'] = 'Item Name';
$text_store['vendor_name'] = 'Vendor Name';
$text_store['brand_name'] = 'Brand Name';
$text_store['item_location'] = 'Item Location';
$text_store['item_description'] = 'Description';
$text_store['category'] = 'Category';
$text_store['item_mrp'] = 'MRP';
$text_store['remarks_stock'] = 'Remarks';
$text_store['min_stock'] = 'Stock <br> Minimum <br> Required';
$text_store['item_minimum_stock'] = 'Min Stock';
$text_store['item_max_stock'] = 'Max Stock';
$text_store['item_expired_stock'] = 'Expired Stock';
$text_store['item_damaged_stock'] = 'Damaged Stock';
$text_store['buy_rate'] = 'Buy Rate';
$text_store['sell_rate'] = 'Sell Rate';
$text_store['item_in_transit'] = 'On Hold';
$text_store['stock_inhand'] = 'Stock <br> Inhand';
$text_store['so'] = 'SO';
$text_store['po'] = 'PO';
$text_store['ro_from_client'] = 'RO <br> From <br> Client';
$text_store['ro_to_vendor'] = 'RO <br> To <br> Vendor';
$text_store['total_stock'] = 'Total <br> Stock';
$text_store['expiry'] = 'Expiry';
$text_store['last_po'] = 'Last <br> PO';
$text_store['alert'] = 'Alert';
$text_store['reference'] = 'Reference';
$text_store['history'] = 'History';

//item details
$text_store['item_details'] = 'Item Details';
$text_store['item_id'] = 'Item Id';
$text_store['ttc'] = 'T.T.C.';
$text_store['stock'] = 'Stock';
$text_store['packing'] = 'Packing';
$text_store['remarks'] = 'Remarks';
$text_store['item_area'] = 'Item Area';
$text_store['min_stock'] = 'Min Stock';
$text_store['current_stock'] = 'Current Stock';
$text_store['item_image'] = 'Item Image';

//sales order
$text_store['sales_order'] = 'Sales Order';
$text_store['order_date'] = 'Order Date';
$text_store['order_number'] = 'Order Number';
$text_store['client_name'] = 'Client Name';
$text_store['delivery_address'] = 'Delivery Address';
$text_store['raised_by'] = 'Raised By';
$text_store['order_id'] = 'Id#';
$text_store['grand_total'] = 'Grand Total';
//purchase order
$text_store['purchase_order'] = 'Purchase Order';

//alerts
$text_store['alerts'] = 'Alerts';
$text_store['field_name'] = 'Field Name';
$text_store['requested_by'] = 'Requested By';
$text_store['previous_value'] = 'Previous Value';
$text_store['new_value_suggested'] = 'New Value Suggested';
$text_store['action'] = 'Action';

//btn
$text_store['edit'] = 'Edit';
$text_store['approve'] = 'Approve';
$text_store['disapprove'] = 'Disapprove';
