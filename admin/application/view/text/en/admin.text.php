<?php

//create admin start
$text_admin['title'] = 'Create User';

//form label
$text_admin['admin_type'] = 'Admin Type';
$text_admin['username'] = 'Username';
$text_admin['name'] = 'Name';
$text_admin['admin_email'] = 'Email';
$text_admin['admin_phone'] = 'Phone';
$text_admin['admin_address'] = 'Address';
$text_admin['add_profile'] = 'Add Profile Image';
$text_admin['add_identification'] = 'Add Identification';
$text_admin['password'] = 'Password';
$text_admin['field_required'] = 'This Field Is Required';
//submit btn
$text_admin['submit'] = 'Submit';
$text_admin['update'] = 'Update';
//create admin end//

//-------------------------------------------//

//view admin start
$text_admin['title_view'] = 'Users List';
$text_admin['search'] = 'Search';
//table heading
$text_admin['admin_id'] = 'User Id #';
$text_admin['admin_type'] = 'User Type';
$text_admin['admin_name'] = 'Username';
$text_admin['admin_email'] = 'Email';
$text_admin['admin_phone'] = 'Phone';
$text_admin['admin_address'] = 'Address';
$text_admin['action'] = 'Action';

//action
$text_admin['edit'] = 'Edit';
$text_admin['delete'] = 'Delete';
$text_admin['title_edit'] = 'Edit User';

//view admin end
