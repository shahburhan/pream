<?php

$text_viewItems['title'] = 'Items';

//table headings
$text_viewItems['category'] = 'Category';
$text_viewItems['item_name'] = 'Item Name';
$text_viewItems['vendor_name'] = 'Vendor Name';
$text_viewItems['reference'] = 'Reference';
$text_viewItems['buy_rate'] = 'Buy Rate';
$text_viewItems['sell_rate'] = 'Sell Rate';
$text_viewItems['brand_name'] = 'Brand Name';
$text_viewItems['remarks'] = 'Remarks';
$text_viewItems['stock'] = 'Stock';
$text_viewItems['packing'] = 'Packing';
$text_viewItems['item_area'] = 'Item Area';
$text_viewItems['add_pdf'] = 'View PDF';
$text_viewItems['add_image'] = 'View Image';
$text_viewItems['item_status'] = 'Item Status';
$text_viewItems['item_request_type'] = 'Request Type';
$text_viewItems['item_approval'] = 'Approval';
$text_viewItems['item_raisedby'] = 'Raised By';

//approval
$text_viewItems['item_approve'] = 'Approve';
$text_viewItems['item_disapprove'] = 'Disapprove';

//filters
$text_viewItems['item_filter1'] = 'Filter By Vendors';
$text_viewItems['item_filter2'] = 'Filter By Approval';
$text_viewItems['item_filter3'] = 'Filter By Item Name';
