<?php

//title
$text_viewStorekeeper['title'] = 'View Storekeeper';
//table headings
$text_viewStorekeeper['sk'] = 'SK#';
$text_viewStorekeeper['name'] = 'Name';
$text_viewStorekeeper['phone'] = 'Phone';
$text_viewStorekeeper['email'] = 'Email';
$text_viewStorekeeper['password'] = 'Password';
$text_viewStorekeeper['address'] = 'Address';
$text_viewStorekeeper['approval_status'] = 'Approval Status';
$text_viewStorekeeper['action'] = 'Action';
$text_viewStorekeeper['delete'] = 'Delete';
//btn
$text_viewStorekeeper['edit'] = 'Edit';
$text_viewStorekeeper['remove'] = 'Remove';
