<?php

//title
$text_createStorekeeper['title'] = 'Create Storekeeper';
$text_createStorekeeper['edit_title'] = 'Edit Storekeeper';
//form labels
$text_createStorekeeper['username'] = 'Username';
$text_createStorekeeper['name'] = 'Name';
$text_createStorekeeper['last_name'] = 'Last Name';
$text_createStorekeeper['phone'] = 'Phone';
$text_createStorekeeper['email'] = 'Email';
$text_createStorekeeper['password'] = 'Password';
$text_createStorekeeper['address'] = 'Address';
$text_createStorekeeper['add_profile'] = 'Add Image';
$text_createStorekeeper['add_identification'] = 'Add Identification';

//btn
$text_createStorekeeper['reset'] = 'Reset';
$text_createStorekeeper['submit'] = 'Submit';
$text_createStorekeeper['update'] = 'Update';
