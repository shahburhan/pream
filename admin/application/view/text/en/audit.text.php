<?php

//create audit start

//title
$text_audit['title'] = 'Create Audit';
$text_audit['title_edit'] = 'Edit Audit';

//form labels
$text_audit['select_module'] = 'Select Department';
$text_audit['set_date'] = 'Set Date';
$text_audit['appoint_person'] = 'Appoint Person';
$text_audit['third_party'] = 'Third Party Audit';

//
$text_audit['submit'] = 'Create Audit';
$text_audit['update'] = 'Update';
//create audit end//

//----------------------------------------------//

//audit view start

//title
$text_audit['title_view'] = 'View Audit';

//table headings
$text_audit['audit_id'] = 'Audit Id';
$text_audit['audit_date'] = 'Audit Date';
$text_audit['audit_for'] = 'Audit For';
$text_audit['status'] = 'Status';
$text_audit['completed_on'] = 'Completed By';
$text_audit['action'] = 'Action';
$text_audit['edit'] = 'Edit';

//btn
$text_audit['reminder'] = 'Reminder';

//audit view end //
