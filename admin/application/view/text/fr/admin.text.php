<?php

//create admin start
$text_admin['title'] = 'Cr�er un administrateur';

//form label
$text_admin['admin_type'] = "Type d'administrateur";
$text_admin['username'] = "Nom d'utilisateur";
$text_admin['first_name'] = 'Pr�nom';
$text_admin['last_name'] = 'Nom de famille';
$text_admin['admin_email'] = 'Email';
$text_admin['admin_phone'] = 'T�l�phone';
$text_admin['admin_address'] = 'Adresse';
$text_admin['add_profile'] = 'Ajouter une image de profil';
$text_admin['add_identification'] = 'Ajouter une identification';
$text_admin['password'] = 'Mot de passe';
$text_admin['field_required'] = 'Ce champ est requis';
//submit btn
$text_admin['submit'] = 'Soumettre';

//create admin end//

//-------------------------------------------//

//view admin start
$text_admin['title_view'] = 'Liste des administrateurs';
$text_admin['search'] = 'Chercher';
//table heading
$text_admin['admin_id'] = 'Identifiant Admin';
$text_admin['admin_type'] = "Type d'administrateur";
$text_admin['admin_name'] = 'pr�nom';
$text_admin['admin_email'] = 'Email';
$text_admin['admin_phone'] = 'T�l�phone';
$text_admin['admin_address'] = 'Adresse';
$text_admin['action'] = 'action';

//action
$text_admin['edit'] = 'modifier';
$text_admin['delete'] = 'Effacer';

//view admin end
