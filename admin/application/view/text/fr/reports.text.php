<?php

$text_reports['weekly'] = 'Hebdomadaire';
$text_reports['monthly'] = 'Mensuel';
$text_reports['quaterly'] = 'Trimestriel';
$text_reports['yearly'] = 'Annuel';
$text_reports['completed'] = 'Termin�';
$text_reports['pending'] = 'en attendant';
$text_reports['cancelled'] = 'Annul�';
$text_reports['completed_orders'] = 'Ordres termin�s';
$text_reports['pending_orders'] = 'Les ordres en attente';
$text_reports['cancelled_orders'] = 'Ordres annul�s';
$text_reports['total_orders'] = 'Total des commandes';
$text_reports['order_id'] = 'Num�ro de commande';
$text_reports['order_date'] = 'Date de commande';
$text_reports['vendor_name'] = 'Nom du vendeur';
$text_reports['raised_by'] = '�lev� par';
