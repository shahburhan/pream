<?php

$text_viewArticles['title'] = 'Items';

//table headings
$text_viewItems['category'] = 'Cat�gorie';
$text_viewItems['item_name'] = "Nom de l'article";
$text_viewItems['vendor_name'] = 'Nom du vendeur';
$text_viewItems['reference'] = 'R�f�rence';
$text_viewItems['buy_rate'] = "Taux d'achat";
$text_viewItems['sell_rate'] = 'Taux de vente';
$text_viewItems['brand_name'] = 'Marque';
$text_viewItems['remarks'] = 'Remarques';
$text_viewItems['stock'] = 'Stock';
$text_viewItems['packing'] = 'Emballage';
$text_viewItems['item_area'] = "Zone d'article";
$text_viewItems['add_pdf'] = 'Voir le PDF';
$text_viewItems['add_image'] = "Voir l'image";
$text_viewItems['item_status'] = "Statut de l'article";
$text_viewItems['item_request_type'] = 'type de demande';
$text_viewItems['item_approval'] = 'Approbation';
$text_viewItems['item_raisedby'] = '�lev� par';

//approval
$text_viewItems['item_approve'] = 'Approuver';
$text_viewItems['item_disapprove'] = 'D�sapprouver';

//filters
$text_viewItems['item_filter1'] = 'Filtrer par fournisseurs';
$text_viewItems['item_filter2'] = 'Filtrer par approbation';
$text_viewItems['item_filter3'] = "Filtrer par nom d'article";
