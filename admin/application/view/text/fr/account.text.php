<?php

//overview
$text_account['sales_agent'] = 'Agent de ventes';
$text_account['an_hour_ago'] = 'Il y a une heure';
$text_account['waylon_dalton'] = 'Waylon Dalton';
$text_account['subject'] = 'Assujettir';
$text_account['notifications'] = 'Notifications';
$text_account['minimum_stock'] = 'Alerte Stock Minimum';
$text_account['stock_update'] = 'Mise � jour du stock';
$text_account['order_dispatch'] = "Commande d'exp�dition";
$text_account['new_item_alert'] = "Nouvelle alerte d'article";
$text_account['read_more'] = 'Lire la suite';

//login
$text_account['title'] = 'Se connecter';
$text_account['email'] = "Nom d'utilisateur";
$text_account['password'] = 'Mot de passe';
$text_account['subtitle'] = 'Admin';

//login button
$text_account['login'] = "S'identifier";

//Error page start//
$text_account['error_404'] = 'D�sol�, la page que vous recherchez ne peut pas �tre trouv�e.';
//Error page end//

//Unauthorized start//
$text_account['unauthorized_message'] = "Vous n'�tes pas autoris� � afficher cette page.";
//Unauthorized end//
