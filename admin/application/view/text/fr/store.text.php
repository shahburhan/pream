<?php

$text_store['title'] = 'Stock';
$text_store['search'] = 'Chercher';
//table headings
$text_store['item_id'] = "ID de l'article";
$text_store['item_name'] = "Nom de l'article";
$text_store['vendor_name'] = 'Nom du vendeur';
$text_store['brand_name'] = 'Marque';
$text_store['item_location'] = "Emplacement de l'article";
$text_store['remarks_stock'] = 'Remarques';
$text_store['min_stock'] = 'Stock <br> Minimum requis';
$text_store['stock_inhand'] = 'Stock de Inhand';
$text_store['so'] = 'ALORS';
$text_store['po'] = 'PO';
$text_store['ro_from_client'] = 'RO <br> De <br> Client';
$text_store['ro_to_vendor'] = 'RO � To Vendor';
$text_store['total_stock'] = 'Total des actions';
$text_store['expiry'] = 'Expiration';
$text_store['last_po'] = 'Dernier <br> PO';
$text_store['alert'] = 'Alerte';

//item details
$text_store['item_details'] = "D�tails de l'article";
$text_store['item_id'] = "ID de l'article";
$text_store['ttc'] = 'T.T.C.';
$text_store['stock'] = 'Stock';
$text_store['packing'] = 'Emballage';
$text_store['remarks'] = 'Remarques';
$text_store['item_area'] = "Zone d'article";
$text_store['min_stock'] = 'Stock minimum';
$text_store['current_stock'] = 'Stock actuel';

//sales order
$text_store['sales_order'] = 'Commande';
$text_store['order_date'] = 'Date de commande';
$text_store['order_number'] = 'Num�ro de commande';
$text_store['client_name'] = 'Nom du client';
$text_store['delivery_address'] = 'Adresse de livraison';
$text_store['raised_by'] = '�lev� par';

//purchase order
$text_store['purchase_order'] = 'Bon de commande';

//alerts
$text_store['alerts'] = 'Alertes';
$text_store['field_name'] = 'Nom de domaine';
$text_store['requested_by'] = 'Demand� par';
$text_store['previous_value'] = 'Valeur pr�c�dente';
$text_store['new_value_suggested'] = 'Nouvelle valeur sugg�r�e';
$text_store['action'] = 'action';

//btn
$text_store['edit'] = 'modifier';
$text_store['approve'] = 'Approuver';
$text_store['disapprove'] = 'D�sapprouver';
