<?php

//create audit start

//title
$text_audit['title'] = 'Cr�er un audit';

//form labels
$text_audit['select_module'] = 'S�lectionnez le module';
$text_audit['set_date'] = 'R�gler la date';
$text_audit['appoint_person'] = 'Personne nomm�e';
$text_audit['third_party'] = 'V�rification par une tierce partie';

//
$text_audit['submit'] = 'Cr�er un audit';

//create audit end//

//----------------------------------------------//

//audit view start

//title
$text_audit['title_view'] = "Voir l'audit";

//table headings
$text_audit['audit_id'] = "Identifiant d'audit";
$text_audit['audit_date'] = 'Date de v�rification';
$text_audit['audit_for'] = 'Audit pour';
$text_audit['status'] = 'Statut';
$text_audit['completed_on'] = 'Termin� par';
$text_audit['action'] = 'action';

//btn
$text_audit['reminder'] = 'Rappel';

//audit view end //
