<?php

//--Navigation options
$text_common['overview'] = 'Aper�u';
$text_common['store'] = 'le magasin';
$text_common['reports'] = 'Rapports';
$text_common['requests_action'] = "Demande d'action";
$text_common['admin'] = 'Admin';
$text_common['audit'] = 'V�rification';
$text_common['notifications'] = 'Notifications';

//--store

$text_common['view_order_stock'] = 'Voir Stock';
$text_common['view_order_items'] = 'Afficher les articles';

//--reports
$text_common['view_purchase_report'] = "Afficher le rapport d'achat";
$text_common['view_sales_report'] = 'Afficher le rapport de vente';
$text_common['view_account_report'] = 'Afficher le rapport sur les comptes';

//--Requests Action
$text_common['sales_order'] = 'Commande en gros approuv�e / refus�e';
$text_common['purchase_order'] = 'Bon de commande';
$text_common['vendor'] = 'Vendeur';
$text_common['client'] = 'Client';
$text_common['ro_vendor'] = 'RO To Vendor';
$text_common['ro_client'] = 'RO du client';

//--admin
$text_common['create_admin'] = 'Cr�er un administrateur';
$text_common['view_admin'] = 'Voir Admin';

//--audit
$text_common['create_audit'] = 'Cr�er un audit';
$text_common['view_audit'] = "Voir l'audit";

//--notifications
$text_common['minimum_stock_alert'] = 'Alerte Stock Minimum';
$text_common['residue_stock_alert'] = 'Alerte stock r�sidu';
$text_common['new_request_order'] = 'Nouvelle demande de commande';
$text_common['ro_cancel'] = "RO Annulez l'alerte";
$text_common['po_receipt'] = 'PO R�ception Alerte';
$text_common['audit_alert'] = 'Alerte audit';
$text_common['payment'] = 'Paiement';
$text_common['items'] = 'Articles';
//$text_common['sales_order'] = "Alerte de vente";
