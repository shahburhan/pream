<?php
 class ModelStock extends Model
 {
     public function getStockList()
     {
         $result = $this->db->query('SELECT * From item');

         return $result->rows;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT * FROM item  WHERE item_id= '.$item_id);

         return $result->row;
     }

     public function getAllItemsList($clause)
     {
         $result = $this->db->query('SELECT *,  ic.title as item_category From item i LEFT JOIN item_category ic ON (i.item_category = ic.item_category_id) '.$clause);

         return $result->rows;
     }

     public function getAllItemsListWithVendor($clause)
     {
         $result = $this->db->query('SELECT * FROM item LEFT JOIN  (SELECT user.address , user.city, user.email_id, vendor.vendor_id, vendor.company_name, vendor.vendor_type FROM user LEFT JOIN vendor ON vendor.user_id = user.user_id) u ON u.vendor_id = item.vendor '.$clause);

         return $result->rows;
     }
 }
