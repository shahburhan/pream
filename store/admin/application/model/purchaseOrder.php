<?php

class ModelPurchaseOrder extends Model
{
    public function getOrderList($start = 0, $limit = 10, $where = ' ', $completed = false)
    {
        $status = 1;
        if ($completed) {
            $status = 3;
        }
        if (trim($where) == '') {
            $sql = "SELECT `order`.*,  DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date,'%d %M %Y') as expected_date FROM `order` WHERE `order`.order_status = $status LIMIT $start, $limit";
        } else {
            $sql = "SELECT `order`.*,  DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date,'%d %M %Y') as expected_date FROM `order` ".$where." && `order`.order_status = $status LIMIT $start, $limit";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getOrderDetails($order_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date, vendor.company_name FROM vendor LEFT JOIN 
            `order` ON (`vendor`.vendor_id = `order`.vendor) WHERE order_id = ".$order_id);

        return $result->row;
    }

    public function getVendorsList()
    {
        $result = $this->db->query('SELECT vendor.company_name, vendor.vendor_id  FROM vendor LEFT JOIN user ON vendor.user_id = user.user_id');

        return $result->rows;
    }

    public function getItemOrderDetails($order_id)
    {
        $result = $this->db->query('SELECT item.item_name, item.brand_name, item.reference, item.area, item.image, item.packing, order_item.* FROM order_item LEFT JOIN item ON (order_item.item_id = item.item_id) WHERE order_item.order_id = '.$order_id);

        return $result->rows;
    }

    public function getOrderBol($order_id)
    {
        $result = $this->db->query('SELECT SUM(quantity_received) as quantity_received, item_id, order_id FROM order_bol WHERE order_id = '.$order_id.' GROUP BY item_id, order_id');

        return $result->rows;
    }

    public function orderCount()
    {
        $result = $this->db->query('SELECT order_id FROM `order` WHERE order_status = 1 && order_type = 1');

        return $result->num_rows;
    }

    public function getAreaList()
    {
        $result = $this->db->query('SELECT DISTINCT item.area FROM item');

        return $result->rows;
    }

    public function getRefList($ref = 0, $vendor = 0, $brand_name = '', $category = 0, $com = false, $can = false)
    {
        $status = 1;
        if ($com) {
            $status = 3;
        }
        if ($can) {
            $status = 8;
        }
        if (!$ref) {
            $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && vendor = $vendor &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
            if ($category) {
                $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name, item.item_category FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && vendor = $vendor  && item_category = $category &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
            }
        } else {
            $sql = "SELECT DISTINCT reference FROM `item`  WHERE brand_name = '".$this->db->escape($brand_name)."' && vendor = $vendor";
            if ($category) {
                $sql = "SELECT DISTINCT reference FROM `item` WHERE brand_name = '".$this->db->escape($brand_name)."' && item_category = $category && vendor = $vendor";
            }
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getBrandList($vendor = 0)
    {
        $sql = "SELECT DISTINCT brand_name FROM `item` WHERE vendor = $vendor";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getCategoryList($vendor = 0, $brand_name = '')
    {
        $sql = "SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.vendor=$vendor ORDER BY title";
        if ($brand_name != '') {
            $sql = "SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.vendor=$vendor && item.brand_name = '".$this->db->escape($brand_name)."' ORDER BY title";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function addBol($order_number, $bol_number, $item_id, $quantity, $bol_date, $status = false)
    {
        $sql = "INSERT INTO order_bol SET item_id = $item_id, order_id = $order_number, bol_number = $bol_number, quantity_received = $quantity, date_of_receipt = '$bol_date'";
        $sql2 = "UPDATE item SET current_stock = current_stock + $quantity WHERE item_id = $item_id";
        $result = $this->db->query($sql);
        $id = $this->db->getLastId();
        if ($status) {
            $this->db->query("UPDATE `order` SET order_status = 3 WHERE order_id = $order_number ");
        }
        $update = $this->db->query($sql2);

        return $id;
    }

    public function orderedQuantity($order_id, $item_id)
    {
        $result = $this->db->query('SELECT quantity FROM order_item WHERE item_id = '.$item_id.' && order_id = '.$order_id);

        return $result->row['quantity'];
    }

    public function getClientAddress()
    {
        $result = $this->db->query('SELECT  c.company_name, c.client_id, u.address, u.city, u.postal_code FROM client c LEFT JOIN user u ON c.user_id = u.user_id ');

        return $result->rows;
    }

    public function receivedQuantity($order_id, $item_id)
    {
        $result = $this->db->query('SELECT SUM(quantity_received) as quantity_received FROM order_bol WHERE order_id = '.$order_id.' && item_id = '.$item_id.' GROUP BY item_id, order_id');

        if (!isset($result->row['quantity_received'])) {
            return 0;
        }

        return $result->row['quantity_received'];
    }
}
