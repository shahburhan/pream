<?php
 class ModelSalesOrder extends Model
 {
     public function getSalesOrderList($start = 0, $limit = 10, $clause = '')
     {
         $sql = "SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 2 LIMIT $start, $limit";
         if ($clause != '') {
             $sql = 'SELECT `order`.*, c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id '.$clause." && order_status = 2 LIMIT $start, $limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, c.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date 
		FROM `order` LEFT JOIN (SELECT user.name,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT order_item.*, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function orderCount()
     {
         $result = $this->db->query('SELECT order_id FROM `order` WHERE order_type = 2');

         return $result->num_rows;
     }

     public function getRefList($ref = 0, $client = 0, $brand_name = '', $category = 0, $com = false, $can = false)
     {
         $status = 2;
         if ($com) {
             $status = 5;
         }
         if ($can) {
             $status = 9;
         }
         if (!$ref) {
             $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && client = $client &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
             if ($category) {
                 $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name, item.item_category FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && client = $client  && item_category = $category &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
             }
         } else {
             $sql = "SELECT DISTINCT reference FROM `item`  WHERE brand_name = '".$this->db->escape($client)."'";
             if ($category) {
                 $sql = "SELECT DISTINCT reference FROM `item` WHERE brand_name = '".$this->db->escape($client)."' && item_category = $category";
             }
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getClientsList()
     {
         $result = $this->db->query('SELECT c.*, u.*, c.company_name as name FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id)');

         return $result->rows;
     }

     public function getClientAddress()
     {
         $result = $this->db->query('SELECT  c.company_name, c.client_id, u.address, u.city, u.postal_code FROM client c LEFT JOIN user u ON c.user_id = u.user_id ');

         return $result->rows;
     }

     public function getCategoryList($brand)
     {
         $result = $this->db->query("SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.brand_name = '".$this->db->escape($brand)."' ORDER BY title");

         return $result->rows;
     }

     public function getAreaList()
     {
         $result = $this->db->query('SELECT DISTINCT item.area FROM item');

         return $result->rows;
     }

     public function getAllItemsList($clause)
     {
         $result = $this->db->query('SELECT * FROM item '.$clause);

         return $result->rows;
     }

     public function getAllItemsListWithClient($clause)
     {
         $result = $this->db->query('SELECT * FROM item LEFT JOIN  (SELECT user.address , user.city, user.email_id, client.client_id, client.company_name, client.client_type FROM user LEFT JOIN client ON client.user_id = user.user_id) u ON u.client_id = item.client '.$clause);

         return $result->rows;
     }

     public function getOrderBol($order_id)
     {
         $result = $this->db->query('SELECT SUM(quantity_received) as quantity_received, item_id, order_id FROM order_bol WHERE order_id = '.$order_id.' GROUP BY item_id, order_id');

         return $result->rows;
     }

     public function addBol($order_number, $bol_number, $item_id, $quantity, $bol_date, $status = false)
     {
         $sql = "INSERT INTO order_bol SET item_id = $item_id, order_id = $order_number, bol_number = $bol_number, quantity_received = $quantity, date_of_receipt = '$bol_date'";
         $sql2 = "UPDATE item SET current_stock = current_stock - $quantity WHERE item_id = $item_id";
         $result = $this->db->query($sql);
         $id = $this->db->getLastId();
         if ($status) {
             $this->db->query("UPDATE `order` SET order_status = 4 WHERE order_id = $order_number ");
         }
         $update = $this->db->query($sql2);

         return $id;
     }

     public function getBrandList($char = '')
     {
         $sql = "SELECT DISTINCT brand_name FROM `item` WHERE brand_name LIKE '%".$this->db->escape($char)."%'";
         $result = $this->db->query($sql);

         return $result->rows;
     }
 }
