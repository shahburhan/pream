<?php

class ModelReport extends Model
{
    public function salesNumber($month, $year)
    {
        $current_month = date('Y-m-', time());
        $start = $current_month.'01';
        $end = $current_month.'31';
        $sql = "SELECT COUNT(order_id) as total, order_date FROM `order` WHERE order_type = 2 && order_date BETWEEN '$start' AND '$end' GROUP BY order_date";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function purchaseNumber($month, $year)
    {
        $current_month = date('Y-m-', time());
        $start = $current_month.'01';
        $end = $current_month.'31';
        $sql = "SELECT COUNT(order_id) as total, order_date FROM `order` WHERE order_type = 1 && order_date BETWEEN '$start' AND '$end' GROUP BY order_date";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function totalSalesNumber($month = 0, $year = 0)
    {
        $sql = 'SELECT EXTRACT(year from order_date) as year, EXTRACT(month from order_date) as month,
		       	COUNT(order_id) as total
				FROM `order`
				WHERE order_type = 2
				GROUP BY year, month
				ORDER BY year, month
    	';

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function totalPurchaseNumber($month = 0, $year = 0)
    {
        $sql = 'SELECT EXTRACT(year from order_date) as year, EXTRACT(month from order_date) as month,
		       	COUNT(order_id) as total
				FROM `order`
				WHERE order_type = 1
				GROUP BY year, month
				ORDER BY year, month
    	';
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function totalSalesValue($month = 0, $year = 0)
    {
        $sql = 'SELECT EXTRACT(year from order_date) as year, EXTRACT(month from order_date) as month,
		       	SUM(ttc) as total
				FROM `order`
				WHERE order_type = 2
				GROUP BY year, month
				ORDER BY year, month
    	';
        if ($month != 0 && $year != 0) {
            $sql = "SELECT EXTRACT(year from order_date) as year, EXTRACT(month from order_date) as month,
			       	SUM(ttc) as total
					FROM `order`
					WHERE order_type = 2
					&&	EXTRACT(year from order_date) = '$year'
					&&	EXTRACT(month from order_date) = '$month'
					GROUP BY year, month
					ORDER BY year, month
	    	";
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function totalPurchaseValue($month = 0, $year = 0)
    {
        $sql = 'SELECT EXTRACT(year from order_date) as year, EXTRACT(month from order_date) as month,
		       	SUM(ttc) as total
				FROM `order`
				WHERE order_type = 1
				GROUP BY year, month
				ORDER BY year, month
    	';
        if ($month != 0 && $year != 0) {
            $sql = "SELECT EXTRACT(year from order_date) as year, EXTRACT(month from order_date) as month,
			       	SUM(ttc) as total
					FROM `order`
					WHERE order_type = 1
					&&	EXTRACT(year from order_date) = '$year'
					&&	EXTRACT(month from order_date) = '$month'
					GROUP BY year, month
					ORDER BY year, month
	    	";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }
}
