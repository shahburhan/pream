
<?php
 class ModelStorekeeper extends Model
 {
     public function createUser($data = [], $files = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $lastLogin = date('Y-m-d h:i:s', time());
         $last_ip = $_SERVER['REMOTE_ADDR'];
         $status = (int) 0;
         $user_type = (int) 7;
         // $password = rand(1000, 9999);
         $files_j = json_encode($files);
         $this->db->query("INSERT INTO user SET 
 			user_type = '".$this->db->escape($user_type)."',
			username = '".$this->db->escape($data['username'])."', 
			name = '".$this->db->escape($data['name'])."', 
			email_id = '".$this->db->escape($data['email'])."', 
			password = '".$this->db->escape($data['password'])."', 
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			created = '".$this->db->escape($date)."',
			status = '".$this->db->escape($status)."',
			image = '".$this->db->escape($data['image'])."',
			identification = '".$this->db->escape($data['identification'])."'
			");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function validate_username($username)
     {
         $result = $this->db->query("SELECT user_id FROM user WHERE username = '".$this->db->escape($username)."'");

         return ($result->num_rows == 1) ? false : true;
     }

     public function updateUser($id, $data = [], $files = [])
     {
         $files_j = json_encode($files);
         $this->db->query("UPDATE user SET 
			email_id = '".$this->db->escape($data['email'])."',
			username = '".$this->db->escape($data['username'])."',
			name = '".$this->db->escape($data['name'])."',
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			image = '".$this->db->escape($data['image'])."',
			identification = '".$this->db->escape($data['identification'])."'
			WHERE user_id = $id");

         return true;
     }

     public function createStorekeeper($user_id)
     {
         $contract_type = (int) 0;
         $execution_date = date('Y-m-d h:i:s', time());
         $expiry_date = date('Y-m-d h:i:s', time());
         $approval_status = (int) 4;
         $this->db->query("INSERT INTO employee SET
			user_id = '".$this->db->escape($user_id)."',
			contract_type = '".$this->db->escape($contract_type)."',
			execution_date = '".$this->db->escape($execution_date)."',
			expiry_date = '".$this->db->escape($expiry_date)."',
			approval_status = '".$this->db->escape($approval_status)."'
			");
         $employee_id = $this->db->getLastId();

         //$this->session->login($user_id);

         //$this->createVendor($user_id);

         return $employee_id;
     }

     public function getStorekeeperList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT e.*, u.* FROM employee e LEFT JOIN user u  ON (e.user_id = u.user_id) WHERE u.user_type = 7 LIMIT $start, $limit");

         return $result->rows;
     }

     public function getStorekeeperDetails($employee_id)
     {
         $result = $this->db->query('SELECT e.*, u.* FROM employee e LEFT JOIN user u  ON (e.user_id = u.user_id) WHERE employee_id ='.$employee_id);

         return $result->rows;
     }

     public function getCount()
     {
         $result = $this->db->query('SELECT user_id FROM user WHERE user_type = 7');

         return $result->num_rows;
     }

     public function getApprovalStatus()
     {
         $result = $this->db->query('SELECT * FROM approval_status ');

         return $result->rows;
     }

     public function deleteStorekeeper($employee_id)
     {
         $employee_id = (int) $employee_id;
         // order_status - 0 (delete storekeeper)
         $sql = "DELETE FROM employee WHERE employee_id = $employee_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }
 }
