<?php
 class ModelReturn extends Model
 {
     public function getVendorName()
     {
         $result = $this->db->query('SELECT vendor.vendor_id , `user`.`username`, vendor.company_name FROM `vendor`  LEFT JOIN `user` ON (`vendor`.user_id = `user`.user_id) ');

         return $result->rows;
     }

     public function getReason()
     {
         $result = $this->db->query('SELECT * FROM return_reason');

         return $result->rows;
     }

     public function getOrders()
     {
         $result = $this->db->query("SELECT *, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date FROM `order`");

         return $result->rows;
     }

     public function getOrderDetails($vendor_id)
     {
         $result = $this->db->query('SELECT * FROM `order` WHERE vendor ='.$vendor_id);

         return $result->rows;
     }

     public function getItemListByVendor($vendor_id)
     {
         $result = $this->db->query('SELECT * FROM item  WHERE vendor= '.$vendor_id);

         return $result->rows;
     }

     public function createReturnOrder($data = [])
     {
         $this->db->query("INSERT INTO `order` SET
			order_date = '".$this->db->escape($data['return_date'])."',
			vendor = '".$this->db->escape($data['vendor_name'])."',
			return_reason = '".$this->db->escape($data['reason'])."',
			comment = '".$this->db->escape($data['description'])."',
			order_type = 3,
			order_status = 6
			");
         $order_id = $this->db->getLastId();
         foreach ($data['selected_item'] as $item):
                $details = $this->getItemDetails($item);
         $quan = (int) $data['quantity'][$item];
         if ($quan < 1) {
             $quan = 1;
         }
         $this->db->query("INSERT INTO order_item SET
				item_id = '".$this->db->escape($item)."',
				order_id = '".$this->db->escape($order_id)."', quantity = $quan
				");
         endforeach;

         return $order_id;
     }

     public function getItemsList()
     {
         $result = $this->db->query('SELECT * FROM item');

         return $result->rows;
     }

     public function getClientName()
     {
         $result = $this->db->query('SELECT client.client_id , `user`.`username`, client.company_name FROM `client`  LEFT JOIN `user` ON (`client`.user_id = `user`.user_id) ');

         return $result->rows;
     }

     public function getClientsOrderDetails($client_id)
     {
         $result = $this->db->query("SELECT * FROM `order` o LEFT JOIN (SELECT item.item_name, item.brand_name, item.minimum_sell_rate, item.purchase_rate, order_item.* FROM item LEFT JOIN order_item ON (item.item_id = order_item.item_id)) oi ON(oi.order_id = o.order_id) WHERE o.client = '".$client_id."' && o.order_type = 2");

         return $result->rows;
     }

     public function createClientReturnOrder($data = [])
     {
         $this->db->query("INSERT INTO `order` SET
			order_date = '".$this->db->escape($data['return_date'])."',
			client = '".$this->db->escape($data['client_name'])."',
			return_reason = '".$this->db->escape($data['reason'])."',
			person_responsible = '".$this->db->escape($data['person_responsible'])."',
			comment = '".$this->db->escape($data['description'])."',
			order_type = 4,
			order_status = 6
			");
         $order_id = $this->db->getLastId();
         $htt = 0;
         foreach ($data['selected_item'] as $item):
                $details = $this->getItemDetails($item);
         $quan = (int) $data['quantity'][$item];
         $htt += $details['purchase_rate'] * $quan;
         if ($quan < 1) {
             $quan = 1;
         }
         $this->db->query("INSERT INTO order_item SET
				item_id = '".$this->db->escape($item)."',
				order_id = '".$this->db->escape($order_id)."', quantity = $quan
				");
         endforeach;
         $tax_amount = ($htt * TAX) / 100;
         $ttc = $htt + $tax_amount;
         $this->db->query("UPDATE `order` SET ttc = '$ttc', htt = '$htt' WHERE order_id = $order_id");

         return $order_id;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT item.mrp,  item.purchase_rate FROM item WHERE item.item_id = '.$item_id);

         return $result->row;
     }

     public function getReturnPurchaseOrder($start = 0, $limit = 10, $clause = '')
     {
         $status = 1;
         $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_type = 3 && order_status = 6 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id ".$clause." && order_type = 3 && order_status = 6 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getOrderStatus()
     {
         $result = $this->db->query('SELECT * FROM order_status');

         return $result->rows;
     }

     public function getReturnOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date, v.* 
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function getOrderItemsList($order_id)
     {
         $result = $this->db->query('SELECT order_item.*, item.minimum_sell_rate, item.purchase_rate, item.item_name, item.reference, item.brand_name FROM order_item LEFT JOIN item ON  order_item.item_id = item.item_id  WHERE order_id = '.$order_id);

         return $result->rows;
     }

     public function getReturnSalesOrder($start = 0, $limit = 10, $clause = '')
     {
         $status = 2;
         $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_type = 4 && order_status = 6 LIMIT $start,$limit";
         if ($clause != '') {
             $sql = "SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* FROM `order` LEFT JOIN (SELECT user.name,user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id ".$clause." && order_type = 4 && order_status 
            = 6 LIMIT $start,$limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getClientsReturnOrderDetails($order_id)
     {
         $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, c.* 
        FROM `order` LEFT JOIN (SELECT user.name, user.address,  client.* FROM client LEFT JOIN user ON user.user_id = client.user_id) c ON `order`.client = c.client_id WHERE order_id = ".$order_id);

         return $result->row;
     }

     public function getVendorsList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT v.*, u.* FROM vendor v LEFT JOIN user u  ON (v.user_id = u.user_id) LIMIT $start,$limit");

         return $result->rows;
     }

     public function getClientList($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT c.*, u.* FROM client c LEFT JOIN user u  ON (c.user_id = u.user_id) LIMIT $start,$limit");

         return $result->rows;
     }

     public function orderCount($status = 1, $order_type = 1)
     {
         $result = $this->db->query("SELECT order_id FROM `order` WHERE order_type = $order_type && order_status = $status");

         return $result->num_rows;
     }

     public function getRefList($ref = 0)
     {
         $sql = 'SELECT DISTINCT order_id FROM `order` WHERE order_status = 2';
         if ($ref) {
             $sql = 'SELECT DISTINCT reference FROM `item` ';
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }
 }
