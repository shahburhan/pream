<?php

//form title
$text_createReturnOrderClient['title'] = 'Select SO';
$text_createReturnOrderClient['heading'] = 'Create Return Order - Client';
$text_createReturnOrderClient['title_item'] = 'Select items for return';

//form label
$text_createReturnOrderClient['return_date'] = 'Date Of Return';
$text_createReturnOrderClient['client_name'] = 'Client Name';
$text_createReturnOrderClient['reason'] = 'Reason';

//table title
$text_createReturnOrderClient['title_table'] = 'Items To Return';

//table headings
$text_createReturnOrderClient['item_id'] = 'Item Id';
$text_createReturnOrderClient['reference'] = 'Ref#';
$text_createReturnOrderClient['item_name'] = 'Item Name';
$text_createReturnOrderClient['brand_name'] = 'Brand Name';
$text_createReturnOrderClient['quantity_transfer'] = 'Quantity Transferred';
$text_createReturnOrderClient['price'] = 'Price UT';
$text_createReturnOrderClient['total_price'] = 'Total Price';
$text_createReturnOrderClient['description'] = 'Description';
$text_createReturnOrderClient['grand_total'] = 'Grand Total';

//btn
$text_createReturnOrderClient['submit'] = 'Submit';
$text_createReturnOrderClient['reset'] = 'Reset';
