<?php

//form title
$text_createReturnOrder['title'] = 'Select PO';
$text_createReturnOrder['heading'] = 'Create Return Order';
$text_createReturnOrder['title_items'] = 'Select items for return';

//form label
$text_createReturnOrder['return_date'] = 'Date Of Return';
$text_createReturnOrder['vendor_name'] = 'Vendor Name';
$text_createReturnOrder['reason'] = 'Reason';

//table title
$text_createReturnOrder['title_table'] = 'Items To Return';

$text_createReturnOrder['order_id'] = 'PO#';
$text_createReturnOrder['order_date'] = 'Order Date';
$text_createReturnOrder['vendor_name'] = 'Vendor Name';
$text_createReturnOrder['delivery_address'] = 'Delivery Address';
$text_createReturnOrder['raised_by'] = 'Raised By';
$text_createReturnOrder['description'] = 'Description';
$text_createReturnOrder['select_item'] = 'Select Items For Return';
//table headings
$text_createReturnOrder['reference'] = 'Ref#';
$text_createReturnOrder['item_name'] = 'Item Name';
$text_createReturnOrder['brand_name'] = 'Brand Name';
$text_createReturnOrder['quantity_transfer'] = 'Quantity Transferred';
$text_createReturnOrder['price'] = 'Price UT';
$text_createReturnOrder['total_price'] = 'Total Price';
$text_createReturnOrder['item_id'] = 'Item Id';
$text_createReturnOrder['grand_total'] = 'Grand Total';

//btn
$text_createReturnOrder['submit'] = 'Submit';
$text_createReturnOrder['reset'] = 'Reset';
