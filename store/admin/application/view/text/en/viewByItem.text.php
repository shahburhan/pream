<?php

//table headings
$text_viewByItem['so'] = 'SO#';
$text_viewByItem['client_name'] = 'Client Name';
$text_viewByItem['order_date'] = 'Order Date';
$text_viewByItem['client_address'] = 'Client Address';
$text_viewByItem['delivery_date'] = 'Delivery Date';
$text_viewByItem['payment_type'] = 'Payment Type';
$text_viewByItem['person_responsible'] = 'Person Responsible';
$text_viewByItem['sales_agent'] = 'Sales Agent';
$text_viewByItem['purchaser'] = 'Purchaser';

//items
$text_viewByItem['item_id'] = 'Item Id';
$text_viewByItem['item_name'] = 'Item Name';
$text_viewByItem['brand_name'] = 'Brand Name';
$text_viewByItem['discount_rate'] = 'Discount Rate';

//btn
$text_viewByItem['add_bill'] = 'Add Bill Of Lading';
