<?php

$text_salesOrder['title'] = 'Sales Order';
$text_salesOrder['search'] = 'Search';
//table headings
$text_salesOrder['so'] = 'SO#';
$text_salesOrder['client_name'] = 'Client Name';
$text_salesOrder['order_date'] = 'Order Date';
$text_salesOrder['client_address'] = 'Client Address';
$text_salesOrder['delivery_date'] = 'Delivery Date';
$text_salesOrder['payment_type'] = 'Payment Type';
$text_salesOrder['person_responsible'] = 'Person Responsible';
$text_salesOrder['sales_agent'] = 'Sales Agent';
$text_salesOrder['purchaser'] = 'Purchaser';
$text_salesOrder['status'] = 'Status';
$text_salesOrder['action'] = 'Action';
$text_salesOrder['delivery_address'] = 'Delivery Address';
$text_salesOrder['order_id'] = 'Order Id';

//items
$text_salesOrder['item_id'] = 'Item Id';
$text_salesOrder['item_name'] = 'Item Name';
$text_salesOrder['brand_name'] = 'Brand Name';
$text_salesOrder['discount_rate'] = 'Discount Rate';
$text_salesOrder['sr_no'] = 'Sr.No.';
$text_salesOrder['reference'] = 'Reference';
$text_salesOrder['mrp'] = 'MRP';
$text_salesOrder['sell_rate'] = 'Sell Rate';
$text_salesOrder['quantity'] = 'Quantity';
$text_salesOrder['ttc'] = 'TTC';
$text_salesOrder['details'] = 'Details';
$text_salesOrder['sales_order'] = 'Sales Order Details';
