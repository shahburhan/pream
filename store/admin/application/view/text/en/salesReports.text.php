<?php

//form label
$text_salesReports['title'] = 'Add Item';

$text_salesReports['order_id'] = 'PO#';
$text_salesReports['order_date'] = 'Order Date';
$text_salesReports['vendor_name'] = 'Vendor Name';
$text_salesReports['delivery_address'] = 'Delivery Address';
$text_salesReports['raised_by'] = 'Raised By';
$text_salesReports['action'] = 'action';
