<?php

//title
$text_audit['title'] = 'Audit';
//form labels
$text_audit['current_stock'] = 'Current Stock';
$text_audit['return_stock'] = 'Return Stock';
$text_audit['purchase_order'] = 'Purchase Order';
$text_audit['dispatch_order'] = 'Dispatch Order';

//submit

$text_audit['submit'] = 'Submit';
