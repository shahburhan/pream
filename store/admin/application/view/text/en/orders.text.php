<?php

$text_orders['title'] = 'PO Queue';
$text_orders['search'] = 'Search';

//table headings
$text_orders['po'] = 'PO#';
$text_orders['purchase_order'] = 'Purchase Order Details';
$text_orders['order_date'] = 'Order Date';
$text_orders['order_id'] = 'Order Id';
$text_orders['vendor_address'] = 'Vendor Address';
$text_orders['vendor_name'] = 'Vendor Name';
$text_orders['expected_date'] = 'Expected Date';
$text_orders['payment_type'] = 'Payment Type';
$text_orders['person_responsible'] = 'Person Responsible';
$text_orders['delivery_address'] = 'Delivery Address';
$text_orders['delivery_date'] = 'Delivery Date';
$text_orders['purchaser'] = 'Purchaser';
$text_orders['total_items'] = 'Total Items';
$text_orders['item_received'] = 'Item Received';
$text_orders['latest_bol'] = 'Latest BOL No.';
$text_orders['previous_bol'] = 'Previous BOLs';
$text_orders['new_bill'] = 'New Bill Of Lading';
$text_orders['add'] = 'Add';

//items
$text_orders['sr_no'] = 'Sr.No.';
$text_orders['reference'] = 'Ref#';
$text_orders['item_name'] = 'Item Name';
$text_orders['brand_name'] = 'Brand Name';
$text_orders['discount_rate'] = 'Discount Rate';
$text_orders['quantity_ordered'] = 'Quantity Ordered';
$text_orders['quantity_received'] = 'Quantity Received';
$text_orders['quantity_to_receive'] = 'Quantity To Receive';
$text_orders['item_image'] = 'Item Image';
$text_orders['packing'] = 'Packing';
$text_orders['area'] = 'Area';

//btn
$text_orders['add_bill'] = 'Add Bill of Lading';
$text_orders['mark_received'] = 'Mark Received';
$text_orders['action'] = 'Action';
$text_orders['details'] = 'Details';
$text_orders['submit'] = 'Add BOL';
$text_orders['site'] = 'Site';
