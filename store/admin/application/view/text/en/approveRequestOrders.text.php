<?php

//table headings
$text_approveRequestOrders['po'] = 'PO#';
$text_approveRequestOrders['order_date'] = 'order_date';
$text_approveRequestOrders['vendor'] = 'Vendor';
$text_approveRequestOrders['vendor_address'] = 'Vendor Address';
$text_approveRequestOrders['delivery_date'] = 'Delivery Date';
$text_approveRequestOrders['payment_type'] = 'Payment Type';
$text_approveRequestOrders['person_responsible'] = 'Person Responsible';
$text_approveRequestOrders['delivery_address'] = 'Delivery Address';
$text_approveRequestOrders['purchaser'] = 'Purchaser';

//items
$text_approveRequestOrders['item_id'] = 'Item Id';
$text_approveRequestOrders['item_name'] = 'Item Name';
$text_approveRequestOrders['brand_name'] = 'Brand Name';
$text_approveRequestOrders['discount_rate'] = 'Discount Rate';

//btn
$text_approveRequestOrders['edit'] = 'Discount Rate';
$text_approveRequestOrders['cancel'] = 'Discount Rate';
