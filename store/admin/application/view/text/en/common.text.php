<?php

$text_common['overview'] = 'Overview';
$text_common['po'] = 'Purchase Order';
$text_common['stock'] = 'Stock';
$text_common['return'] = 'Returns';
$text_common['request'] = 'Request Orders';
$text_common['storekeeper'] = 'Storekeeper';
$text_common['reports'] = 'Reports';
$text_common['new_order'] = 'New Order';
$text_common['view_order'] = 'View Order';
$text_common['sales_order'] = 'Sales Order';
$text_common['view_stock'] = 'View Stock';
$text_common['create_return_order'] = 'Create';
$text_common['notification'] = 'notification';
$text_common['storekeeper'] = 'Storekeeper';
$text_common['notifications'] = 'Notifications';

//-- PO Queue Nav Menu

$text_common['po_list'] = 'List';

//-- Stock Nav Menu

$text_common['view_stock'] = 'View Stock';
$text_common['view_item'] = 'View Item';

//-- Return Order Nav Menu

$text_common['ro_create'] = 'Create';
$text_common['ro_create_client'] = 'Create For Client';

//-- Requests Order Nav Menu

$text_common['request_manage'] = 'Manage';
$text_common['request_approve'] = 'Approved';

//-- Reports Nav Menu

$text_common['report_received'] = 'Received';
$text_common['report_dispatched'] = 'Dispatched';

//-- Sales Order Nav Menu

$text_common['sales_all'] = 'All';
$text_common['sales_pending'] = 'Pending';

//-- Shopkeeper Nav menu

$text_common['storekeeper_list'] = 'View';
$text_common['storekeeper_create'] = 'Create New';

//notifications
$text_common['minimum_stock_alert'] = 'Minimum Stock Alert';
$text_common['residue_stock_alert'] = 'Residue Stock Alert';
$text_common['new_sales_order'] = 'New Sales Order Alert';
$text_common['new_item_added'] = 'New Item Added';
$text_common['request_order_status'] = 'Request Order Status';
$text_common['audit_alert'] = 'Audit Alert';
$text_common['po_received'] = 'PO Received';
$text_common['stock_update'] = 'Stock Update';
$text_common['order_dispatch'] = 'Order Dispatch';
