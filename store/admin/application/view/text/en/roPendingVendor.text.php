<?php

$text_roPendingVendor['title'] = 'RO Pending - Vendor';

//filters
$text_roPendingVendor['order_filter1'] = 'Filter By Vendors';
$text_roPendingVendor['order_filter2'] = 'Filter By Order Number';

//table headings
$text_roPendingVendor['ro_no'] = 'RO#';
$text_roPendingVendor['ro_number'] = 'RO#';
$text_roPendingVendor['ro'] = 'Pending Return Order Details';
$text_roPendingVendor['order_date'] = 'Order Date';
$text_roPendingVendor['vendor_address'] = 'Vendor Address';
$text_roPendingVendor['vendor_name'] = 'Vendor Name';
$text_roPendingVendor['returned_by'] = 'Returned By';
$text_roPendingVendor['person_responsible'] = 'Person Responsible';
$text_roPendingVendor['purchaser'] = 'Purchaser';
$text_roPendingVendor['total'] = 'Total';
$text_roPendingVendor['ro_no'] = 'RO#';
$text_roPendingVendor['return_date'] = 'Return Date';
$text_roPendingVendor['expected_date'] = 'Expected Date';
$text_roPendingVendor['action'] = 'Action';
$text_roPendingVendor['description'] = 'Description';
$text_roPendingVendor['grand_total'] = 'Grand Total';
//items
$text_roPendingVendor['item_id'] = 'Item Id';
$text_roPendingVendor['reference'] = 'Reference';
$text_roPendingVendor['item_name'] = 'Item Name';
$text_roPendingVendor['brand_name'] = 'Brand Name';
$text_roPendingVendor['unit_price'] = 'Unit Price';
$text_roPendingVendor['buy_rate'] = 'Buy Rate';
$text_roPendingVendor['quantity'] = 'Quantity';
$text_roPendingVendor['discount_rate'] = 'Discount Rate';

//btn
$text_roPendingVendor['add_bill'] = 'Mark Sent';
$text_roPendingVendor['details'] = 'Details';
