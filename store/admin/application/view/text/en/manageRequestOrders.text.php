<?php

//table headings
$text_manageRequestOrders['po'] = 'PO#';
$text_manageRequestOrders['order_date'] = 'order_date';
$text_manageRequestOrders['vendor_address'] = 'Vendor Address';
$text_manageRequestOrders['delivery_date'] = 'Delivery Date';
$text_manageRequestOrders['payment_type'] = 'Payment Type';
$text_manageRequestOrders['person_responsible'] = 'Person Responsible';
$text_manageRequestOrders['delivery_address'] = 'Delivery Address';
$text_manageRequestOrders['purchaser'] = 'Purchaser';

//items
$text_manageRequestOrders['item_id'] = 'Item Id';
$text_manageRequestOrders['item_name'] = 'Item Name';
$text_manageRequestOrders['brand_name'] = 'Brand Name';
$text_manageRequestOrders['discount_rate'] = 'Discount Rate';

//btn
$text_manageRequestOrders['edit'] = 'Discount Rate';
$text_manageRequestOrders['cancel'] = 'Discount Rate';
