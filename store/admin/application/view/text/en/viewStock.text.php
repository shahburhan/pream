<?php

$text_viewStock['title'] = 'Stock';
$text_viewStock['search'] = 'Search';
//table headings
$text_viewStock['reference_no'] = 'Reference #';
$text_viewStock['item_id'] = 'Item Id';
$text_viewStock['item_name'] = 'Item Name';
$text_viewStock['brand_name'] = 'Brand Name';
$text_viewStock['category'] = 'Category';
$text_viewStock['item_area'] = 'Item Area';
$text_viewStock['stock_inhand'] = 'Stock <br> Inhand';
$text_viewStock['action'] = 'Action';
$text_viewStock['current_stock'] = 'Current Stock';
$text_viewStock['sell_rate'] = 'Sell Rate';
$text_viewStock['profit'] = 'Profit %';
$text_viewStock['discount'] = 'Discount Rate';
$text_viewStock['min_stock'] = 'Min <br> Stock';
$text_viewStock['expiry'] = 'Expiry';
$text_viewStock['action'] = 'Action';

//action
$text_viewStock['create_request_order'] = 'Create Request Order';
