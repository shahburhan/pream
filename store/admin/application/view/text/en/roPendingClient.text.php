<?php

$text_roPendingClient['title'] = 'RO Pending - Client';
$text_roPendingClient['ro'] = 'Pending Return Order Details';
//filters
$text_roPendingClient['order_filter1'] = 'Filter By Vendors';
$text_roPendingClient['order_filter2'] = 'Filter By Order Number';

//table headings
$text_roPendingClient['ro'] = 'RO#';
$text_roPendingClient['ro_number'] = 'RO#';
$text_roPendingClient['ro_no'] = 'RO#';
$text_roPendingClient['order_date'] = 'Order Date';
$text_roPendingClient['total'] = 'Total';
$text_roPendingClient['expected_date'] = 'Expected Date';
$text_roPendingClient['return_date'] = 'Return Date';
$text_roPendingClient['payment_type'] = 'Payment Type';
$text_roPendingClient['client_name'] = 'Client Name';
$text_roPendingClient['client_address'] = 'Client Address';
$text_roPendingClient['delivery_address'] = 'Delivery Address';
$text_roPendingClient['sales_agent'] = 'Sales Agent';
$text_roPendingClient['return_detail'] = 'Return Order Details';
$text_roPendingClient['grand_total'] = 'Grand Total';

//items
$text_roPendingClient['item_id'] = 'Item Id';
$text_roPendingClient['item_name'] = 'Item Name';
$text_roPendingClient['reference'] = 'Reference';
$text_roPendingClient['unit_price'] = 'Unit Price';
$text_roPendingClient['sell_rate'] = 'Sell Rate';
$text_roPendingClient['quantity'] = 'Quantity';
$text_roPendingClient['brand_name'] = 'Brand Name';
$text_roPendingClient['discount_rate'] = 'Discount Rate';

//btn
$text_roPendingClient['add_bill'] = 'Mark Received';
$text_roPendingClient['action'] = 'Action';
$text_roPendingClient['details'] = 'Details';
