<?php

$text_common['overview'] = 'aperçu';
$text_common['po'] = 'bon de commande Queue';
$text_common['stock'] = 'stock';
$text_common['return'] = 'demande du retour';
$text_common['request'] = 'demande des commandes';
$text_common['storekeeper'] = 'magasinier';
$text_common['reports'] = 'rapport';
$text_common['new_order'] = 'New Order';
$text_common['view_order'] = 'View Order';
$text_common['sales_order'] = 'Sales Order';
$text_common['view_stock'] = 'View Stock';
$text_common['create_return_order'] = 'Create';
$text_common['notification'] = 'notification';
