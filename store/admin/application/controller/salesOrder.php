<?php

class ControllerSalesOrder extends Controller
{
    public $limit = 10;

    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Sales';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Sales Order View';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Order', 'href' => '?route=sales/view'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $this->limit;

        $this->load->model('salesOrder');
        $sales_orders = $this->model_salesOrder->getSalesOrderList($start, $this->limit);
        $order_status = $this->model_salesOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_salesOrder->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $this->limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=salesOrder/view&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('salesOrders', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('salesOrder');
                $sales_order = $this->model_salesOrder->getOrderDetails($order_id);
                $bol = $this->model_salesOrder->getOrderBol($order_id);
                $order_items = $this->model_salesOrder->getOrderItemsList($order_id);
                $client = $this->model_salesOrder->getClientAddress();
                $bol_details = [];
                foreach ($bol as $bl) {
                    $bol_details[$bl['order_id']][$bl['item_id']] = $bl['quantity_received'];
                }

                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';

                echo json_encode(['status' => 'success',
                    'sales_order'          => $sales_order,
                    'order_items'          => $order_items,
                    'bol_details'          => $bol_details,
                    'delivery_address'     => $data, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function viewPending()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Sales Order - Pending';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Sales Order', 'href' => '?route=sales/viewPending'];
        $breadcrumb[] = ['title'=>'Pending', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';

        $this->load->text('salesOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('salesOrders', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $brand_name = $_GET['brand_name'];
                $this->load->model('salesOrder');
                $categoryList = $this->model_salesOrder->getCategoryList($brand_name);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $areaList = $this->model_salesOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['clientList'])) {
            $c = '';
            $fieldName = 'client';
            $filter = $_POST['clientList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = 'order_id';
            if (isset($_POST['list']) && $_POST['list'] == 'item') {
                $fieldName = 'reference';
            }
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$itemClause.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$itemClause.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getClientsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $clientsList = $this->model_salesOrder->getClientsList();
                echo json_encode(['status' => 'success',
                    'client_list'          => $clientsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getClientsAddress()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('salesOrder');
                $clientsAddress = $this->model_salesOrder->getClientsAddress($client_id);
                echo json_encode(['status' => 'success',
                    'client_address'       => $clientsAddress, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('salesOrder');
                $clause = '';

                $clause = $this->buildClause();
                $item_List = [];
                $item_List = $this->model_salesOrder->getAllItemsList($clause);
                if (isset($_POST['with_client'])) {
                    $item_List = $this->model_salesOrder->getAllItemsListWithClient($clause);
                } else {
                }

                foreach ($item_List as $key => $item) {
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                $response_data = ['status' => 'success',
                    'itemList'             => $item_List,
                    'clause'               => $clause, ];

                if (isset($_POST['client_id'])) {
                    $client = $this->model_salesOrder->getClientDetails($_POST['client_id']);
                    $type = $this->model_salesOrder->getClientType($client['client_type']);
                    $client['client_type'] = $type['title'];
                    $response_data['client'] = $client;
                }

                echo json_encode($response_data);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $brand = $_GET['brand_name'];
                $category = $_GET['category'];
                $this->load->model('salesOrder');
                $refList = $this->model_salesOrder->getRefList($ref, $brand, $category);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['char']) && strlen($_GET['char']) > 2) {
                $string = htmlentities($_GET['char']);
            }
            if (isset($_GET['all'])) {
                $this->load->model('salesOrder');
                $brandList = $this->model_salesOrder->getBrandList();
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getSalesOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();
            $this->load->model('salesOrder');
            $orders = $this->model_salesOrder->getSalesOrderList(0, 10, $clause);
            $status = $this->model_salesOrder->getOrderStatus();
            foreach ($status as $key):
                $order_status[$key['order_status_id']] = $key['title'];
            endforeach;
            echo json_encode(['order_list' => $orders, 'order_status'=> $order_status]);
        }
    }

    public function dispatch()
    {
        $order_id = (isset($_POST['order_number'])) ? (int) $_POST['order_number'] : 0;
        if (!$order_id) {
            echo 'Error';
            exit;
        }
        $bol_date = date('Y-m-d', time());

        if (isset($_POST['quantities'])) {
            $complete = [];
            $count = 0;
            $status = false;
            foreach ($_POST['quantities'] as $k => $v) {
                $count++;
                $this->load->model('purchaseOrder');
                $this->load->model('salesOrder');
                $v = (int) $v;
                $k = (int) $k;
                $o_q = $this->model_purchaseOrder->orderedQuantity($order_id, $k);
                $r_q = $this->model_purchaseOrder->receivedQuantity($order_id, $k);
                if ($o_q >= $r_q) {
                    if ($v < ($o_q - $r_q)) {
                        $nq = $v;
                        $complete[] = false;
                    } elseif ($v == ($o_q - $r_q)) {
                        $nq = $v;
                        $complete[] = true;
                    } else {
                        $nq = $o_q - $r_q;
                    }

                    if ($count == count($_POST['quantities'])) {
                        foreach ($complete as $key => $value) {
                            if ($value == true) {
                                $status = true;
                            } else {
                                $status = false;
                                break;
                            }
                        }
                    }
                    $dispatch = $this->model_salesOrder->addBol($order_id, 0, $k, $nq, $bol_date, $status);
                    $this->load->controller('activity');
                    $this->controller_activity->logActivity(32, $dispatch, $this->session->loggedUser(), true);
                    $data['dispatch'] = $dispatch;
                }
            }
            echo '{"status" : "success"}';
        }
    }
}
