<?php

class ControllerSchedules extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Audit';
        $this->load->controller('header');
        $this->controller_header->load($header);

        //$this->load->view("items",$header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Schedules';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Schedules', 'href' => '?route=audit/createAudit'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $header['style'][] = 'application/view/resources/js/fullcalendar/dist/fullcalendar.min';
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->controller_header->load($header);
        $this->load->text('calender');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('calender', $data);

        $this->load->controller('footer');
        $footer['script'][] = 'application/view/resources/js/moment/min/moment.min';
        $footer['script'][] = 'application/view/resources/js/fullcalendar/dist/fullcalendar.min';
        $this->controller_footer->load($footer);
    }
}
