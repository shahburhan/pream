<?php

class ControllerPurchaseOrder extends Controller
{
    public $limit = 10;

    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Purchase Order Queue';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->view('orders', $header);
        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function getVendorsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $vendorsList = $this->model_purchaseOrder->getVendorsList();
                echo json_encode(['status'             => 'success',
                                        'vendors_list' => $vendorsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function vendorMapped()
    {
        $this->load->model('purchaseOrder');
        $v_map = [];
        $vendorsList = $this->model_purchaseOrder->getVendorsList();

        foreach ($vendorsList as $vendor) {
            // code...
            $v_map[$vendor['vendor_id']] = $vendor['company_name'];
        }

        return $v_map;
    }

    public function poList()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Purchase Order Queue';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Order', 'href' => '?route=purchase/poList'];
        $breadcrumb[] = ['title'=>'Queue', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('orders');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $this->limit;

        $this->load->model('purchaseOrder');
        $orders = $this->model_purchaseOrder->getOrderList($start, $this->limit);
        $total_count = $this->model_purchaseOrder->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $this->limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/poList&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['orders'] = $orders;
        $this->load->view('orders', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $order_id = $_GET['order_id'];
                $this->load->model('purchaseOrder');
                $order_details = $this->model_purchaseOrder->getOrderDetails($order_id);
                $items = $this->model_purchaseOrder->getItemOrderDetails($order_id);
                $bol = $this->model_purchaseOrder->getOrderBol($order_id);
                $client = $this->model_purchaseOrder->getClientAddress();
                $bol_details = [];
                foreach ($bol as $bl) {
                    $bol_details[$bl['order_id']][$bl['item_id']] = $bl['quantity_received'];
                }
                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';
                //if(count(json_decode($message_details['attachment'])) > 0)$message_details['attachment'] = RESOURCE_URL."/".json_decode($message_details['attachment'])[0];
                //else $message_details['attachment'] = 0;
                echo json_encode(['status' => 'success',
                    'order_details'        => $order_details,
                    'item_details'         => $items,
                    'bol_details'          => $bol_details,
                    'delivery_address'     => $data, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
           // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }

    public function getOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('purchaseOrder');
            $this->load->model('salesOrder');
            $orders = $this->model_purchaseOrder->getOrderList(0, 10, $clause);
            $v_map = $this->vendorMapped();
            $client = $this->model_salesOrder->getClientAddress();

            foreach ($client as $address) {
                $data[$address['client_id']]['address'] = $address['address'];
                $data[$address['client_id']]['city'] = $address['city'];
                $data[$address['client_id']]['postal'] = $address['postal_code'];
                $data[$address['client_id']]['name'] = $address['company_name'];
            }

            $data[0]['address'] = $this->config['store_address'];
            $data[0]['city'] = '';
            $data[0]['postal'] = '';
            $data[0]['name'] = 'Store';
            echo json_encode(['order_list'=>$orders, 'vendor'=>$v_map, 'site'=>$data]);
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $brandClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = '`order`.order_id';
            if (isset($_POST['list']) && $_POST['list'] == 'item') {
                $fieldName = 'reference';
            }
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return $clause;
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $vendor = (int) $_GET['vendor_id'];
                $brand = $_GET['brand_name'];
                $categoryList = $this->model_purchaseOrder->getCategoryList($vendor, $brand);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $areaList = $this->model_purchaseOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $com = false;
            $can = false;
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                if (isset($_GET['ot'])) {
                    if ($_GET['ot'] == 1) {
                        $com = true;
                    }
                    if ($_GET['ot'] == 2) {
                        $can = true;
                    }
                }
                $vendor = (isset($_GET['vendor_id'])) ? (int) $_GET['vendor_id'] : 0;
                $brand_name = (isset($_GET['brand_name'])) ? $_GET['brand_name'] : '';
                $category = (isset($_GET['category'])) ? $_GET['category'] : '';
                $this->load->model('purchaseOrder');
                $refList = $this->model_purchaseOrder->getRefList($ref, $vendor, $brand_name, $category, $com, $can);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['vendor_id']) && $_GET['vendor_id']) {
                $vendor_id = (int) $_GET['vendor_id'];
            }
            if (isset($_GET['all']) && $vendor_id) {
                $this->load->model('purchaseOrder');
                $brandList = $this->model_purchaseOrder->getBrandList($vendor_id);
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function addBol()
    {
        $order_id = (isset($_POST['order_number'])) ? (int) $_POST['order_number'] : 0;
        $bol_number = (isset($_POST['bol_number'])) ? (int) $_POST['bol_number'] : 0;
        if (!$bol_number || !$order_id) {
            echo 'Error';
            exit;
        }
        $bol_date = $_POST['bol_date'];
        if (isset($_POST['quantities'])) {
            $status = false;
            $complete = [];
            $count = 0;
            foreach ($_POST['quantities'] as $k => $v) {
                $count++;
                $this->load->model('purchaseOrder');
                $v = (int) $v;
                $k = (int) $k;
                $o_q = $this->model_purchaseOrder->orderedQuantity($order_id, $k);
                $r_q = $this->model_purchaseOrder->receivedQuantity($order_id, $k);
                if ($o_q >= $r_q) {
                    if ($v < ($o_q - $r_q)) {
                        $nq = $v;
                        $complete[] = false;
                    } elseif ($v == ($o_q - $r_q)) {
                        $nq = $v;
                        $complete[] = true;
                    } else {
                        $nq = $o_q - $r_q;
                    }

                    if ($count == count($_POST['quantities'])) {
                        foreach ($complete as $key => $value) {
                            if ($value == true) {
                                $status = true;
                            } else {
                                $status = false;
                                break;
                            }
                        }
                    }

                    if ($v != 0) {
                        $this->model_purchaseOrder->addBol($order_id, $bol_number, $k, $nq, $bol_date, $status);
                        $this->load->controller('activity');

                        if ($status) {
                            $this->controller_activity->logActivity(22, $order_id, 6, true);
                        } else {
                            $this->controller_activity->logActivity(23, $order_id, 6, true);
                        }
                    }
                }
            }
            echo '{"status" : "success"}';
        }
    }
}
