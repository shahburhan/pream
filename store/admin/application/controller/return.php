<?php

class ControllerReturn extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Returns';
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function create()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Return Request - Store';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=return/create'];
        $breadcrumb[] = ['title'=>'Create', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('createReturnOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('return');

        $orders = $this->model_return->getOrders();
        $data['vendors'] = $this->model_return->getVendorName();
        $data['reasons'] = $this->model_return->getReason();
        $data['orders'] = $orders;

        if ($_POST) {
            $rules = [
                'return_date' => 'r|d',
                'vendor_name' => 'r|b',
                'reason'      => 'r|b',
                ];
            $this->form->process_post($rules);
            if (!isset($_POST['selected_item'])) {
                $this->form->error['error_selected_item'] = 'Please select items for return';
            }

            if (empty($this->form->error)) {
                $id = $this->model_return->createReturnOrder($this->form->data);

                $this->load->controller('activity');
                $user_id = 7;
                $this->controller_activity->logActivity(14, $id, 7, true);

                header('Location: ?route=return/roPendingVendor');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }

        $this->load->view('createReturnRequest', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['vendor_id'])) {
                $vendor_id = $_GET['vendor_id'];
                $this->load->model('return');

                $order_details = $this->model_return->getOrderDetails($vendor_id);
                $items = $this->model_return->getItemListByVendor($vendor_id);

                echo json_encode(['status' => 'success',
                    'order_details'        => $order_details,
                    'item_details'         => $items, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function createForClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Return Request - Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=return/createForClient'];
        $breadcrumb[] = ['title'=>'Create for client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('createReturnOrderClient');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('return');

        $orders = $this->model_return->getOrders();
        $data['clients'] = $this->model_return->getClientName();
        $data['reasons'] = $this->model_return->getReason();
        $data['orders'] = $orders;

        if ($_POST) {
            $rules = [
                'return_date'        => 'r|d',
                'client_name'        => 'r|b',
                'person_responsible' => 'r',
                'reason'             => 'r|b',
                ];
            $this->form->process_post($rules);
            if (!isset($_POST['selected_item'])) {
                $this->form->error['error_selected_item'] = 'Please select items for return';
            }

            if (empty($this->form->error)) {
                $id = $this->model_return->createClientReturnOrder($this->form->data);
                $this->load->controller('activity');
                $user_id = 7;
                $this->controller_activity->logActivity(18, $id, 7, true);
                header('Location: ?route=return/roPendingClient');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }

        $this->load->view('createReturnRequestClient', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getClientsOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['client_id'])) {
                $client_id = $_GET['client_id'];
                $this->load->model('return');

                $order_details = $this->model_return->getClientsOrderDetails($client_id);

                echo json_encode(['status' => 'success',
                    'item_details'         => $order_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function roPendingClient()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'RO Pending -  Client';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=return/createForClient'];
        $breadcrumb[] = ['title'=>'Create for client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('roPendingClient');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('return');
        $sales_orders = $this->model_return->getReturnSalesOrder();
        $order_status = $this->model_return->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_return->orderCount(6, 4);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=return/roPendingClient&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['sales_orders'] = $sales_orders;
        $data['order_status'] = $order_status_mapped;

        $this->load->view('ROClientView', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function roPendingVendor()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'RO Pending - Vendor';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=return/createForClient'];
        $breadcrumb[] = ['title'=>'Create for client', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('roPendingVendor');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('return');
        $purchase_orders = $this->model_return->getReturnPurchaseOrder();
        $order_status = $this->model_return->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_return->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=return/roPendingVendor&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['purchase_orders'] = $purchase_orders;
        $data['order_status'] = $order_status_mapped;

        $this->load->view('ROVendorView', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getReturnOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $response = [];
                $order_id = $_GET['order_id'];
                $this->load->model('return');
                $purchase_order = $this->model_return->getReturnOrderDetails($order_id);
                $sales_order = $this->model_return->getClientsReturnOrderDetails($order_id);
                $order_items = $this->model_return->getOrderItemsList($order_id);
                $reasons = $this->model_return->getReason();

                foreach ($reasons as $reason) {
                    $data[$reason['return_reason_id']] = $reason['title'];
                }

                $response['purchase_order'] = $purchase_order;
                $response['sales_order'] = $sales_order;
                $response['order_items'] = $order_items;
                $response['reason'] = $data;
                echo json_encode($response);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getReturnOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('return');
            $returnOrders = $this->model_return->getReturnSalesOrder(0, 10, $clause);
            echo json_encode(['return_list'=>$returnOrders, 'clause'=>$clause]);
        }
    }

    public function getPurchaseReturnOrderList()
    {
        if ($_POST) {
            $clause = $this->buildClause();

            $this->load->model('return');
            $returnPurchaseOrders = $this->model_return->getReturnPurchaseOrder(0, 10, $clause);
            echo json_encode(['returns_list'=>$returnPurchaseOrders, 'clause'=>$clause]);
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $clientClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['clientList'])) {
            $c = '';
            $fieldName = 'client';
            $filter = $_POST['clientList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$c;
                } else {
                    $whereClause = $whereClause.' AND '.$c;
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE '.$itemClause;
                } else {
                    $whereClause = $whereClause.' AND '.$itemClause;
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return trim($clause);
    }

    public function getClientsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('return');
                $clientsList = $this->model_return->getClientList();
                echo json_encode(['status' => 'success',
                    'client_list'          => $clientsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getVendorsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('return');
                $vendorsList = $this->model_return->getVendorsList();
                echo json_encode(['status' => 'success',
                    'vendor_list'          => $vendorsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    // function getItemsList()
    // {
    //     header('Content-Type: application/json');
    //     if ($_POST) {
    //         if (isset($_POST['all'])) {
    //             $this->load->model("returnOrder");
    //             $clause = "";

    //             $clause = $this->buildClause();
    //             $item_List = [];
    //             $item_List = $this->model_returnOrder->getOrderItemsList($clause);
    //             /*if(isset($_POST['with_client'])){
    //                 $item_List = $this->model_salesOrder->getAllItemsListWithClient($clause);
    //             }else{

    //             }*/

    //             foreach ($item_List as $key => $item) {
    //                 $image = json_decode($item_List[$key]["image"]);
    //                 if (count($image) > 0) {
    //                     $image_url = RESOURCE_URL . "/" . $image[0];
    //                     $item_List[$key]["image"] = $image_url;
    //                 } else {
    //                     $item_List[$key]["image"] = 0;
    //                 }

    //                 $pdf = json_decode($item_List[$key]["item_pdf"]);
    //                 if (count($pdf) > 0) {
    //                     $pdf_url = RESOURCE_URL . "/" . $pdf[0];
    //                     $item_List[$key]["item_pdf"] = $pdf_url;
    //                 } else {
    //                     $item_List[$key]["item_pdf"] = 0;
    //                 }

    //             }

    //             $response_data = array('status' => 'success',
    //                 'itemList' => $item_List,
    //                 'clause' => $clause);

    //             if (isset($_POST['client_id'])) {
    //                 $client = $this->model_returnOrder->getClientDetails($_POST['client_id']);
    //                 $type = $this->model_returnOrder->getClientType($client['client_type']);
    //                 $client['client_type'] = $type['title'];
    //                 $response_data['client'] = $client;
    //             }

    //             echo json_encode($response_data);

    //         } else {
    //             echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
    //         }
    //     }
    // }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                $this->load->model('return');
                $refList = $this->model_return->getRefList($ref);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }
}
