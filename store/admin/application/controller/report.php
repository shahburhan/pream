<?php

class ControllerReport extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Reports';
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->view('orders', $header);

        $this->load->controller('footer');
        $this->controller_footer->load();
    }

    public function received()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Report - Items Received';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Report', 'href' => '?route=reports/sales'];
        $breadcrumb[] = ['title'=>'Store Admin', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');

        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';

        $this->controller_header->load($header);
        $this->load->text('salesReports');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('report-received', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function dispatched()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Report - Items Dispatched';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Report', 'href' => '?route=reports/sales'];
        $breadcrumb[] = ['title'=>'Store Admin', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');

        //$header['style'][] = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min";
        $header['script'][] = 'https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.bundle.min';

        $this->controller_header->load($header);
        $this->load->text('salesReports');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('report-dispatched', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function salesNumber()
    {
        $org_sale_num = [];
        $this->load->model('report');
        $month = date('m', time());
        $year = date('Y', time());
        $sales_number = $this->model_report->salesNumber($month, $year);
        foreach ($sales_number as $sn) {
            // code...
            $org_sale_num[$sn['order_date']] = $sn['total'];
        }

        return $org_sale_num;
    }

    public function purchaseNumber()
    {
        $org_purchase_num = [];
        $this->load->model('report');
        $month = date('m', time());
        $year = date('Y', time());
        $purchase_number = $this->model_report->purchaseNumber($month, $year);
        foreach ($purchase_number as $pn) {
            // code...
            $org_purchase_num[$pn['order_date']] = $pn['total'];
        }

        return $org_purchase_num;
    }
}
