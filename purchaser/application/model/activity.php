<?php

class ModelActivity extends Model
{
    public function logActivity($data = [])
    {
        foreach ($data as $k => $d) {
            $data[$k] = $this->db->escape($d);
        }

        $created_on = date('Y-m-d h:i:s', time());

        $result = $this->db->query('INSERT INTO activity SET  
                `creator_id` = '.$data['creator_id']
            .',`activity_type` = '.$data['activity_type']
            .',`created_on` = '."'".$created_on."'"
            .',`activity_object` = '.$data['object_id']);

        $id = $this->db->getLastId();

        return $id;
    }

    public function createNotification($data = [])
    {
        foreach ($data as $k => $d) {
            $data[$k] = $this->db->escape($d);
        }

        $created_on = date('Y-m-d h:i:s', time());

        $result = $this->db->query('INSERT INTO notification SET  
                `activity_id` = '.$data['activity_id']
            .',`notification_for` = '.$data['user_id']
            .',`activity_type` = '.$data['activity_type']
            .',`status` = '. 0
            .',`created_on` = '."'".$created_on."'");
    }

    public function getActivityUserTypes($data = [])
    {
        foreach ($data as $k => $d) {
            $data[$k] = $this->db->escape($d);
        }
        $result = $this->db->query('SELECT activity_type.notify_to FROM activity_type WHERE activity_type.activity_type_id = '.$data['activity_type']);

        return $result->row;
    }

    public function getActivityType($activity_type_id)
    {
        $result = $this->db->query("SELECT title FROM activity_type WHERE activity_type_id = $activity_type_id ");

        return $result->row['title'];
    }

    public function getUserIdList($usertype)
    {
        $result = $this->db->query('SELECT user.user_id FROM `user` WHERE user_type = '.$usertype);

        return $result->rows;
    }
}
