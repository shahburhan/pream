<?php
 class ModelReturnOrder extends Model
 {
     public function getVendorName()
     {
         $result = $this->db->query('SELECT vendor.vendor_id , `user`.`username`, vendor.company_name FROM `vendor`  LEFT JOIN `user` ON (`vendor`.user_id = `user`.user_id) ');

         return $result->rows;
     }

     public function getReason()
     {
         $result = $this->db->query('SELECT * FROM return_reason');

         return $result->rows;
     }

     public function getOrders($start = 0, $limit = 10, $where = ' ')
     {
         if ($where == ' ') {
             $sql = "SELECT *, DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date FROM `order` LEFT JOIN ( SELECT vendor.*, user.address as delivery_address FROM vendor LEFT JOIN user ON (vendor.user_id = user.user_id) ) uv ON (`order`.vendor = uv.vendor_id) WHERE order_type = 3 && order_status = 11 LIMIT $start, $limit";
         } else {
             $sql = "SELECT *, DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date FROM `order` LEFT JOIN ( SELECT vendor.*, user.address as delivery_address FROM vendor LEFT JOIN user ON (vendor.user_id = user.user_id) ) uv ON (`order`.vendor = uv.vendor_id) ".$where." && order_type = 3 && order_status = 11 LIMIT $start, $limit";
         }
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function getRequestOrders($start = 0, $limit = 10)
     {
         $result = $this->db->query("SELECT *, DATE_FORMAT(`order`.order_date,'%d-%m-%Y') as order_date FROM `order` LEFT JOIN ( SELECT vendor.*, user.address as delivery_address FROM vendor LEFT JOIN user ON (vendor.user_id = user.user_id) ) uv ON (`order`.vendor = uv.vendor_id) WHERE order_type = 3 && order_status = 6 LIMIT $start, $limit");

         return $result->rows;
     }

     public function getOrderDetails($vendor_id)
     {
         $result = $this->db->query('SELECT * FROM `order` WHERE vendor ='.$vendor_id);

         return $result->rows;
     }

     public function getItemListByVendor($vendor_id)
     {
         $result = $this->db->query('SELECT * FROM item  WHERE vendor= '.$vendor_id);

         return $result->rows;
     }

     public function createReturnOrder($data = [])
     {
         $this->db->query("INSERT INTO `order` SET
			order_date = '".$this->db->escape($data['return_date'])."',
			vendor = '".$this->db->escape($data['vendor_name'])."',
			return_reason = '".$this->db->escape($data['reason'])."',
			comment = '".$this->db->escape($data['description'])."',
			person_responsible = '".$this->db->escape($data['person_responsible'])."',
			order_type = 3,
			order_status = 11
			");
         $order_id = $this->db->getLastId();
         $htt = 0;
         foreach ($data['selected_item'] as $item):
                $quan = (int) $data['quantity'][$item];
         $htt += $data['return_rate'][$item] * $quan;
         if ($quan < 1) {
             $quan = 1;
         }
         $this->db->query("INSERT INTO order_item SET
				item_id = '".$this->db->escape($item)."',
				order_id = '".$this->db->escape($order_id)."', quantity = $quan, rate = ".$data['return_rate'][$item].'
				');
         endforeach;
         $tax_amount = ($htt * TAX) / 100;
         $ttc = $htt + $tax_amount;
         $this->db->query("UPDATE `order` SET ttc = '$ttc', htt = '$htt' WHERE order_id = $order_id");

         return $order_id;
     }

     public function getItemsList()
     {
         $result = $this->db->query('SELECT * FROM item');

         return $result->rows;
     }

     public function orderCount($status = 6)
     {
         $result = $this->db->query("SELECT order_id FROM `order` WHERE order_type = 3 && order_status = $status");

         return $result->num_rows;
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT item.mrp,  item.purchase_rate FROM item WHERE item.item_id = '.$item_id);

         return $result->row;
     }

     public function approveOrder($order_id)
     {
         $order_id = (int) $order_id;
         // order_status - 8 (purchase order cancelled)
         $sql = "UPDATE `order` SET order_status = 11 WHERE order_id = $order_id";
         $this->db->query($sql);

         return $this->db->countAffected();
     }

     public function disapproveOrder($order_id)
     {
         $order_id = (int) $order_id;
         // order_status - 8 (purchase order cancelled)
         $sql = "DELETE FROM `order` WHERE order_id = $order_id";
         $this->db->query($sql);
         $this->db->query("DELETE FROM `order_item` WHERE order_id = $order_id");

         return $this->db->countAffected();
     }
 }
