<?php

class ModelNotification extends Model
{
    public function getNotifications($user_id, $start = 0, $limit = 10)
    {
        $result = $this->db->query("SELECT notification.*, DATE_FORMAT(`notification`.created_on, '%d-%b-%Y') as created_on, activity.activity_object FROM notification LEFT JOIN activity ON  notification.activity_id = activity.activity_id WHERE notification_for = ".$user_id." ORDER BY notification.created_on DESC LIMIT $start, $limit");

        return $result->rows;
    }

    public function getMinimumStockAlert()
    {
        $result = $this->db->query('SELECT item.* FROM item WHERE item.current_stock < item.min_stock  ');

        return $result->rows;
    }
}
