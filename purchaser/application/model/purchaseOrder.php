<?php

class ModelPurchaseOrder extends Model
{
    public function newOrder($data = [])
    {
        $this->db->query("INSERT INTO `order` SET
			order_date = '".$this->db->escape($data['order_date'])."',
			payment_deadline = '".$this->db->escape($data['payment_deadline'])."',
			vendor = '".$this->db->escape($data['vendor_id'])."',
			order_type = '".$this->db->escape($data['order_type'])."',
			amount_paid = '".$this->db->escape($data['amount_paid'])."',
			raised_by = '".$this->db->escape($data['raised_by'])."',
			order_status = '".$this->db->escape($data['order_status'])."',
			htt = '".$this->db->escape($data['htt'])."',
			tva = '".$this->db->escape($data['tva'])."',
			ttc = '".$this->db->escape($data['ttc'])."',
            expected_date = '".$this->db->escape($data['expected_date'])."',
            person_responsible = '".$this->db->escape($data['person_responsible'])."',
            payment_mode = '".$this->db->escape($data['payment_mode'])."',
			city = '".$this->db->escape($data['city'])."',
            delivery_address = '".$this->db->escape($data['delivery_address'])."',
			delivery_other = '".$this->db->escape($data['delivery_other'])."'
			 ");
        $id = $this->db->getLastId();

        //$this->session->login($user_id);
        return $id;
    }

    public function getPaymentModes()
    {
        $result = $this->db->query("SELECT * FROM payment_mode WHERE lang = '".$this->session->language."'");

        return $result->rows;
    }

    public function getClientAddress()
    {
        $result = $this->db->query('SELECT c.company_name, c.client_id, u.address, u.city, u.postal_code FROM client c LEFT JOIN user u ON c.user_id = u.user_id');

        return $result->rows;
    }

    public function addOrderItems($data = [])
    {
        $this->db->query("INSERT INTO `order_item` SET
			order_id = '".$this->db->escape($data['order_id'])."',
			item_id = '".$this->db->escape($data['item_id'])."',
			quantity = '".$this->db->escape($data['quantity'])."',
			discount = '".$this->db->escape($data['discount'])."',
			rate = '".$this->db->escape($data['rate'])."',
			order_type = '".$this->db->escape($data['order_type'])."',
			mrp = '".$this->db->escape($data['mrp'])."'
			 ");
        $id = $this->db->getLastId();

        //$this->session->login($user_id);
        return $id;
    }

    public function getItemDetails($item_id)
    {
        $result = $this->db->query('SELECT item.image, item.mrp,item.purchase_rate as rate, item.item_pdf, item.item_name, item.reference, item.item_id  FROM item WHERE item.item_id = '.$item_id);

        return $result->row;
    }

    public function getVendorsList()
    {
        $result = $this->db->query('SELECT vendor.company_name, vendor.vendor_id  FROM vendor LEFT JOIN user ON vendor.user_id = user.user_id');

        return $result->rows;
    }

    public function getCategoryList($vendor = 0, $brand_name = '')
    {
        $sql = "SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.vendor=$vendor ORDER BY title";
        if ($brand_name != '') {
            $sql = "SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id WHERE item.vendor=$vendor && item.brand_name = '".$this->db->escape($brand_name)."' ORDER BY title";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getAreaList()
    {
        $result = $this->db->query('SELECT DISTINCT item.area FROM item');

        return $result->rows;
    }

    public function getAllItemsList($clause, $limit = 15, $start = 0)
    {
        $result = $this->db->query('SELECT *,  ic.title as item_category From item i LEFT JOIN item_category ic ON (i.item_category = ic.item_category_id) '.$clause." LIMIT $start, $limit");

        return $result->rows;
    }

    public function getAllItemsListWithVendor($clause, $limit, $start)
    {
        $result = $this->db->query('SELECT * FROM item LEFT JOIN  (SELECT user.address , user.city, user.email_id, vendor.vendor_id, vendor.company_name, vendor.vendor_type FROM user LEFT JOIN vendor ON vendor.user_id = user.user_id) u ON u.vendor_id = item.vendor '.$clause." LIMIT $start, $limit");

        return $result->rows;
    }

    public function getVendorName()
    {
        $result = $this->db->query('SELECT vendor.vendor_id , `user`.`username` FROM `vendor`  LEFT JOIN `user` ON (`vendor`.user_id = `user`.user_id) WHERE vendor.approval_status = 2');

        return $result->rows;
    }

    public function getOrderList($start = 0, $limit = 10, $where = ' ', $completed = false, $cancelled = false)
    {
        $status = 1;
        if ($completed) {
            $status = 3;
        }
        if ($cancelled) {
            $status = 8;
        }
        if (empty(trim($where))) {
            $sql = "SELECT oii.*, DATE_FORMAT(oii.order_date, '%d-%m-%Y') as order_date, vendor.company_name FROM vendor RIGHT JOIN     (SELECT oi.*, item_category, brand_name FROM        (SELECT `order`.*, order_item.item_id FROM `order` LEFT JOIN order_item  ON `order`.order_id = order_item.order_id ) oi LEFT JOIN item ON item.item_id = oi.item_id ) oii ON vendor.vendor_id = oii.vendor WHERE order_status = $status LIMIT $start, $limit";
        } else {
            $sql = "SELECT oii.*, DATE_FORMAT(oii.order_date, '%d-%m-%Y') as order_date, vendor.company_name FROM vendor RIGHT JOIN     (SELECT oi.*, item_category, brand_name FROM        (SELECT `order`.*, order_item.item_id FROM `order` LEFT JOIN order_item  ON `order`.order_id = order_item.order_id ) oi LEFT JOIN item ON item.item_id = oi.item_id ) oii ON vendor.vendor_id = oii.vendor ".$where." && order_status = $status LIMIT $start, $limit";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getOrderDetails($order_id)
    {
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, DATE_FORMAT(`order`.expected_date, '%d-%m-%Y') as expected_date, vendor.company_name FROM vendor LEFT JOIN
        	`order` ON (`vendor`.vendor_id = `order`.vendor) WHERE order_id = ".$order_id);

        return $result->row;
    }

    public function getItemOrderDetails($order_id)
    {
        $result = $this->db->query('SELECT order_item.*,item.item_name, item.mrp, item.brand_name,item.packing,item.reference  FROM item RIGHT JOIN order_item ON (`item`.item_id = `order_item`.item_id) WHERE order_id = '.$order_id);

        return $result->rows;
    }

    public function orderCount($status = 1)
    {
        $result = $this->db->query("SELECT order_id FROM `order` WHERE order_status = $status");

        return $result->num_rows;
    }

    public function cancelOrder($order_id)
    {
        $order_id = (int) $order_id;
        // order_status - 8 (purchase order cancelled)
        $sql = "UPDATE `order` SET order_status = 8 WHERE order_id = $order_id";
        $this->db->query($sql);

        return $this->db->countAffected();
    }

    public function getRefList($ref = 0, $vendor = 0, $brand_name = '', $category = 0, $com = false, $can = false)
    {
        $status = 1;
        if ($com) {
            $status = 3;
        }
        if ($can) {
            $status = 8;
        }
        if (!$ref) {
            $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && vendor = $vendor &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
            if ($category) {
                $sql = "SELECT DISTINCT order_id as reference FROM `order` LEFT JOIN (SELECT order_item.order_id as o_id, item.brand_name, item.item_category FROM order_item LEFT JOIN item ON order_item.item_id = item.item_id) oii ON `order`.order_id = oii.o_id WHERE order_status = $status && vendor = $vendor  && item_category = $category &&  oii.brand_name = '".$this->db->escape($brand_name)."'";
            }
        } else {
            $sql = "SELECT DISTINCT reference FROM `item`  WHERE brand_name = '".$this->db->escape($brand_name)."' && vendor = $vendor";
            if ($category) {
                $sql = "SELECT DISTINCT reference FROM `item` WHERE brand_name = '".$this->db->escape($brand_name)."' && item_category = $category && vendor = $vendor";
            }
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getBrandList($vendor = 0)
    {
        $sql = "SELECT DISTINCT brand_name FROM `item` WHERE vendor = $vendor";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getCancelledOrderList($start = 0, $limit = 10)
    {
        $status = 1;
        $result = $this->db->query("SELECT `order`.*, DATE_FORMAT(`order`.order_date, '%d-%m-%Y') as order_date, v.* FROM `order` LEFT JOIN (SELECT user.name,  vendor.* FROM vendor LEFT JOIN user ON user.user_id = vendor.user_id) v ON `order`.vendor = v.vendor_id WHERE order_type = 1 && `order`.order_status = 8 LIMIT $start, $limit");

        return $result->rows;
    }

    public function getOrderStatus()
    {
        $result = $this->db->query('SELECT * FROM order_status');

        return $result->rows;
    }

    public function getDiscount($order_id)
    {
        $result = $this->db->query("SELECT * FROM order_discount WHERE order_id = $order_id");

        return $result->rows;
    }

    public function addDiscount($data = [])
    {
        $sql = "
            INSERT INTO `order_discount` SET
            order_id = '".$this->db->escape($data['order_id'])."',
            discount_type = '".$this->db->escape($data['discount_type'])."',
            discount_on = '".$this->db->escape($data['discount_on'])."',
            item_id = '".$this->db->escape($data['item_id'])."',
            discount_amount = '".$this->db->escape($data['discount_amount'])."',
            attachment = '".$this->db->escape($data['attachment'])."'
        ";
        $this->db->query($sql);
    }

    public function checkForMaxStock($item_id)
    {
        $sql = "SELECT max_stock, current_stock FROM item WHERE item_id = '".(int) $item_id."'";
        $result = $this->db->query($sql);

        return $result->row;
    }

    public function checkForApplicableReturn($vendor_id)
    {
        $sql = "SELECT SUM(oi.quantity) as total FROM `order` o LEFT JOIN order_item oi ON o.order_id = oi.order_id WHERE o.vendor = '".(int) $vendor_id."'";
        $result = $this->db->query($sql);

        return $result->row['total'];
    }

    public function fetchBolDetails($order_id)
    {
        $order_id = (int) $order_id;

        $sql = "SELECT i.item_id, DATE_FORMAT(date_of_receipt, '%d-%m-%Y') as date_of_receipt, bol_number, SUM(quantity_received) as quantity, i.reference FROM order_bol o LEFT JOIN item i ON o.item_id = i.item_id WHERE order_id = $order_id GROUP BY item_id";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function validateQuantity($order, $item)
    {
        $sql = "SELECT DISTINCT quantity FROM order_item WHERE order_id = $order && item_id = $item";
        $result = $this->db->query($sql);

        return (isset($result->row['quantity'])) ? $result->row['quantity'] : false;
    }

    public function changeQuantity($order_id, $item_id, $new_quantity)
    {
        $sql = "UPDATE order_item SET quantity = $new_quantity WHERE order_id = $order_id && item_id = $item_id";

        $this->db->query($sql);

        return $this->reCalculateTTC($order_id);
    }

    private function reCalculateTTC($order_id)
    {
        $sql = "SELECT SUM(rate * quantity) as ht FROM order_item WHERE order_id = $order_id";
        $result = $this->db->query($sql);
        $ht = $result->row['ht'];
        $sql = "SELECT tva FROM `order` WHERE order_id = $order_id LIMIT 1";
        $result = $this->db->query($sql);
        $tax = $result->row['tva'];
        $ttc = $ht + ($ht * $tax) / 100;
        $sql = "UPDATE `order` SET htt = '$ht', ttc = '$ttc' WHERE order_id = $order_id";
        $this->db->query($sql);

        return $this->db->countAffected();
    }

    public function addToQueue($item, $quantity, $rate)
    {
        $sql = "INSERT INTO po_queue SET item_id = $item, quantity=$quantity, rate='".$this->db->escape($rate)."', responsible='".$this->session->loggedUser()."'";
        $this->db->query($sql);

        return $this->db->countAffected();
    }

    public function getFromQueue($clause = '')
    {
        $sql = "SELECT i.*, pq.* FROM po_queue pq LEFT JOIN item i ON i.item_id = pq.item_id WHERE responsible='".$this->session->loggedUser()."'";
        if (trim($clause != '')) {
            $sql = 'SELECT i.*, pq.* FROM po_queue pq LEFT JOIN item i ON i.item_id = pq.item_id '.$clause." && responsible='".$this->session->loggedUser()."'";
        }
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function removeFromQueue($item_id, $all = false)
    {
        $sql = "DELETE FROM po_queue WHERE item_id = $item_id && responsible='".$this->session->loggedUser()."'";
        if ($all) {
            $sql = "DELETE FROM po_queue WHERE responsible='".$this->session->loggedUser()."'";
        }

        return $this->db->query($sql);
    }

    public function getRequests()
    {
        $sql = 'SELECT order_id FROM `order` WHERE order_status = 12';
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function getRequestDetails($order_id)
    {
        $sql = "SELECT order_item.* FROM `order` LEFT JOIN order_item ON `order`.order_id = order_item.order_id WHERE `order`.order_id = $order_id LIMIT 1";
        $result = $this->db->query($sql);

        return $result->row;
    }

    public function validateRO($id)
    {
        $result = $this->db->query("SELECT COUNT(order_id) as total FROM `order` WHERE order_id = $id && order_status = 12");

        return $result->row['total'];
    }

    public function getRODetails($id)
    {
        $result = $this->db->query("SELECT * FROM `order` o LEFT JOIN vendor  ON o.vendor = vendor.vendor_id WHERE order_id = $id && order_status = 12 LIMIT 1");

        return $result->row;
    }

    public function getROItems($id)
    {
        $result = $this->db->query("SELECT * FROM order_item oi LEFT JOIN item i ON oi.item_id = i.item_id WHERE order_id = $id ");

        return $result->rows;
    }

    public function raisedBy($id)
    {
        $result = $this->db->query("SELECT name FROM user WHERE user_id = $id ");

        return $result->row['name'];
    }

    public function RO_HT($id)
    {
        $result = $this->db->query("SELECT SUM(quantity*rate) as total FROM order_item WHERE order_id = $id GROUP BY order_id");

        return $result->row['total'];
    }

    public function RO2PO($data)
    {
        $this->db->query("UPDATE `order` SET payment_mode = '".$this->db->escape($data['payment_mode'])."', order_status = 1, htt = '".$this->db->escape($data['ht'])."', ttc = '".$this->db->escape($data['ttc'])."', tva = '".$this->db->escape($data['tva'])."', delivery_address = '".$this->db->escape($data['delivery_address'])."', delivery_other = '".$this->db->escape($data['delivery_other'])."', order_date = '".$this->db->escape($data['order_date'])."' WHERE order_id = '".$this->db->escape($data['order_id'])."'");

        $this->db->query("UPDATE `order_item` SET rate = '".$this->db->escape($data['purchase_rate'])."' WHERE order_id = '".$this->db->escape($data['order_id'])."'");
    }
}
