<?php
 class ModelStock extends Model
 {
     public function addItem($data = [], $files = [])
     {
         $data['item_status'] = 1;
         $pa = ($data['purchase_rate'] * $this->config['item_margin']) / 100;
         $data['minimum_sell_rate'] = $pa + $data['purchase_rate'];
         if (!isset($data['mrp'])) {
             $data['mrp'] = 0;
         }
         if (!isset($data['current_stock'])) {
             $data['current_stock'] = 0;
         }
         if (!isset($data['expired_stock'])) {
             $data['expired_stock'] = 0;
         }
         if (!isset($data['damaged_stock'])) {
             $data['damaged_stock'] = 0;
         }
         if (!isset($data['discount_contractor'])) {
             $data['discount_contractor'] = 0;
         }
         if (!isset($data['discount_retailer'])) {
             $data['discount_retailer'] = 0;
         }
         if (!isset($data['discount_wholesaler'])) {
             $data['discount_wholesaler'] = 0;
         }
         if (!isset($data['expiry_date'])) {
             $data['expiry_date'] = date('Y-m-d h:i:s', time());
         }
         $files_j = json_encode($files);
         $this->db->query("INSERT INTO item SET 
			item_name = '".$this->db->escape($data['item_name'])."', 
			item_category = '".$this->db->escape($data['category'])."',
			packing = '".$this->db->escape($data['packing'])."',
			vendor = ".$this->db->escape($data['vendor_name']).",
			mrp = '".$this->db->escape($data['mrp'])."',
			brand_name = '".$this->db->escape($data['brand_name'])."', 
			min_stock = '".$this->db->escape($data['min_stock'])."', 
			item_description = '".$this->db->escape($data['description'])."',
			item_status = '".$this->db->escape($data['item_status'])."',
			minimum_sell_rate = '".$this->db->escape($data['minimum_sell_rate'])."',
			purchase_rate = '".$this->db->escape($data['purchase_rate'])."',
			max_stock = '".$this->db->escape($data['max_stock'])."',
			area = '".$this->db->escape($data['item_area'])."',
			reference = '".$this->db->escape($data['reference'])."',
			current_stock = '".$this->db->escape($data['current_stock'])."',			
			expired_stock = '".$this->db->escape($data['expired_stock'])."',	
			damaged_stock = '".$this->db->escape($data['damaged_stock'])."',
			image = '".$this->db->escape($data['add_image'])."',
			item_pdf = '".$this->db->escape($data['pdf'])."',
			discount_contractor = '".$this->db->escape($data['discount_contractor'])."',
			discount_retailer = '".$this->db->escape($data['discount_retailer'])."',
			discount_wholesaler = '".$this->db->escape($data['discount_wholesaler'])."',
			vendor_special_discount = '".$this->db->escape($data['special_offer'])."',
			vendor_special_discount_on = '".$this->db->escape($data['purchaser'])."' ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);
         return $id;
     }

     public function editItem($item_id, $data = [], $files = [])
     {
         $data['item_status'] = 1;
         if (!isset($data['mrp'])) {
             $data['mrp'] = 0;
         }
         if (!isset($data['current_stock'])) {
             $data['current_stock'] = 0;
         }
         if (!isset($data['expired_stock'])) {
             $data['expired_stock'] = 0;
         }
         if (!isset($data['damaged_stock'])) {
             $data['damaged_stock'] = 0;
         }
         if (!isset($data['discount_contractor'])) {
             $data['discount_contractor'] = 0;
         }
         if (!isset($data['discount_retailer'])) {
             $data['discount_retailer'] = 0;
         }
         if (!isset($data['discount_wholesaler'])) {
             $data['discount_wholesaler'] = 0;
         }
         if (!isset($data['expiry_date'])) {
             $data['expiry_date'] = date('Y-m-d h:i:s', time());
         }
         $files_j = json_encode($files);

         $str = '';
         if ($data['add_image'] != '[]') {
             $str .= "image = '".$this->db->escape($data['add_image'])."',";
         } else {
             $str .= '';
         }
         if ($data['pdf'] != '[]') {
             $str .= "item_pdf = '".$this->db->escape($data['pdf'])."',";
         }

         $this->db->query("UPDATE item SET 
			item_name = '".$this->db->escape($data['item_name'])."', 
			item_category = '".$this->db->escape($data['category'])."',
			packing = ".$this->db->escape($data['packing']).',
			vendor = '.$this->db->escape($data['vendor_name']).",
			brand_name = '".$this->db->escape($data['brand_name'])."', 
			min_stock = '".$this->db->escape($data['min_stock'])."', 
			item_description = '".$this->db->escape($data['description'])."',
			item_status = '".$this->db->escape($data['item_status'])."',
			max_stock = '".$this->db->escape($data['max_stock'])."',
			area = '".$this->db->escape($data['item_area'])."',
			reference = '".$this->db->escape($data['reference'])."',		
			discount_contractor = '".$this->db->escape($data['discount_contractor'])."',
			discount_retailer = '".$this->db->escape($data['discount_retailer'])."',
			discount_wholesaler = '".$this->db->escape($data['discount_wholesaler'])."',
			vendor_special_discount = '".$this->db->escape($data['special_offer'])."',
			".$str."
			vendor_special_discount_on = '".$this->db->escape($data['purchaser'])."' WHERE item_id =  $item_id");
     }

     public function getVendorName()
     {
         $result = $this->db->query('SELECT vendor.vendor_id , `user`.`username`, vendor.company_name FROM `vendor`  LEFT JOIN `user` ON (`vendor`.user_id = `user`.user_id) ');

         return $result->rows;
     }

     public function getVendorByName($vendor)
     {
         $result = $this->db->query("SELECT vendor_id FROM `vendor` WHERE company_name = '".$this->db->escape($vendor)."'");

         return isset($result->row['vendor_id']) ? $result->row['vendor_id'] : 0;
     }

     public function importVendor($vendor)
     {
         $this->db->query("INSERT INTO user SET name =  '".$this->db->escape($vendor)."', status = 1");
         $result = $this->db->query("INSERT INTO vendor SET company_name =  '".$this->db->escape($vendor)."', approval_status = 1, user_id = ".$this->db->getLastId());

         return $this->db->getLastId();
     }

     public function getcategory()
     {
         $result = $this->db->query('SELECT * FROM item_category');

         return $result->rows;
     }

     public function getCategoryByName($name)
     {
         $result = $this->db->query("SELECT item_category_id FROM item_category WHERE title = '".$this->db->escape($name)."'");

         return isset($result->row['item_category_id']) ? $result->row['item_category_id'] : 0;
     }

     public function getStockList()
     {
         $result = $this->db->query('SELECT *,  ic.title as item_category From item i LEFT JOIN item_category ic ON (i.item_category = ic.item_category_id)');

         return $result->rows;
     }

     public function getStockTotal($clause = '')
     {
         $result = $this->db->query('SELECT COUNT(i.item_id) as total From item i LEFT JOIN item_category ic ON (i.item_category = ic.item_category_id) '.$clause);

         return $result->row['total'];
     }

     public function getItemDetails($item_id)
     {
         $result = $this->db->query('SELECT ic.*, v.company_name FROM (SELECT i.*, c.item_category_id, c.title as item_category_new FROM item i LEFT JOIN item_category c ON (c.item_category_id = i.item_category)) ic LEFT JOIN vendor v ON v.vendor_id = ic.vendor  WHERE item_id= '.$item_id);

         return $result->row;
     }

     public function addNewCategory($category)
     {
         $this->db->query("INSERT INTO item_category SET title = '".$this->db->escape($category)."'");
         $id = $this->db->getLastId();

         return $id;
     }

     public function getByReference($ref)
     {
         $sql = "SELECT COUNT(item_id) as total FROM item WHERE reference = '".$this->db->escape($ref)."'";
         $result = $this->db->query($sql);

         return isset($result->row['total']) ? $result->row['total'] : 0;
     }

     public function importItem($row)
     {
         if ($this->getByReference($row[5])) {
             return true;
         } else {
             $cId = ($this->getCategoryByName($row[1])) ? $this->getCategoryByName($row[1]) : $this->addNewCategory($row[1]);
             $vId = ($this->getVendorByName($row[4])) ? $this->getVendorByName($row[4]) : $this->importVendor($row[4]);
             $sql = 'INSERT INTO item SET ';
             $sql .= " item_category = '".$cId."' ";
             $sql .= ", item_name = '".$this->db->escape($row[2])."'";
             $sql .= ", brand_name = '".$this->db->escape($row[3])."'";
             $sql .= ", vendor = '".$vId."'";
             $sql .= ", reference = '".$this->db->escape($row[5])."'";
             $sql .= ", packing = '".$this->db->escape($row[6])."'";
             $sql .= ", area = '".$this->db->escape($row[7])."'";
             $sql .= ", purchase_rate = '".$this->db->escape($row[8])."'";

             $this->db->query($sql);

             return $this->db->getLastId();
         }
     }

     public function getVendorList()
     {
         $result = $this->db->query('SELECT vendor.company_name, vendor.vendor_id  FROM vendor LEFT JOIN user ON vendor.user_id = user.user_id');

         return $result->rows;
     }

     public function getCategoryList()
     {
         $result = $this->db->query('SELECT DISTINCT item.item_category, item_category.title FROM item LEFT JOIN item_category ON item.item_category = item_category.item_category_id ORDER BY title');

         return $result->rows;
     }

     public function getRefList()
     {
         $sql = 'SELECT DISTINCT reference FROM `item` ';
         $result = $this->db->query($sql);

         return $result->rows;
     }

     public function validate_email($email, $id = 0)
     {
         $q = "SELECT user_id FROM user WHERE email_id = '".$this->db->escape($email)."'";
         if ($id) {
             $q = "SELECT user_id FROM user WHERE email_id = '".$this->db->escape($email)."' && user_id != $id";
         }
         $result = $this->db->query($q);

         return ($result->num_rows == 1) ? false : true;
     }

     public function createUser($data = [])
     {
         $date = date('Y-m-d h:i:s', time());
         $lastLogin = date('Y-m-d h:i:s', time());
         $last_ip = $_SERVER['REMOTE_ADDR'];
         $user_type = (int) 9;
         $status = (int) 1;
         //$city = rand(1000,9999);
         $password = rand(1000, 9999);

         $this->db->query("INSERT INTO user SET 
			email_id = '".$this->db->escape($data['email'])."',
			password = '".$this->db->escape($password)."', 
			last_ip = '".$this->db->escape($last_ip)."', 
			created = '".$this->db->escape($date)."',
			phone = '".$this->db->escape($data['phone'])."',
			address = '".$this->db->escape($data['address'])."',
			user_type = '".$this->db->escape($user_type)."',
			status = '".$this->db->escape($status)."',
			city = '".$this->db->escape($data['city'])."',
			postal_code = '".$this->db->escape($data['postal_code'])."',
			last_login = '".$this->db->escape($date)."' ");
         $id = $this->db->getLastId();

         //$this->session->login($user_id);

         return $id;
     }

     public function createVendor($user_id, $data = [])
     {
         $vendor_type = (int) 0;
         $approval_status = (int) 1;
         $this->db->query("INSERT INTO vendor SET
			user_id = '".$this->db->escape($user_id)."',
			vendor_type = '".$this->db->escape($vendor_type)."',
			company_name = '".$this->db->escape($data['company_name'])."',
			approval_status = '".$this->db->escape($approval_status)."',
			person_responsible = '".$this->db->escape($data['person_responsible'])."',
			mobile = '".$this->db->escape($data['mobile'])."',
			fax_number = '".$this->db->escape($data['fax'])."',
			category = '".$this->db->escape($data['category'])."',
			franco = '".$this->db->escape($data['franco'])."'
			");
         $vendor_id = $this->db->getLastId();

         return $vendor_id;
     }
 }
