<?php

class ControllerHeader extends Controller
{
    public function load($data)
    {
        if (isset($_GET['lang'])) {
            $this->changeLanguage();
        }
        $this->load->text('common');

        $this->load->model('account');

        $data['notification_count'] = $this->model_account->getNotificationCount($this->session->data['logged_user_id']);

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $lang = ['en', 'fr'];
        unset($lang[array_search($this->session->language, $lang)]);
        $data['lang'] = end($lang);
        if (strpos($_SERVER['REQUEST_URI'], 'lang=')) {
            $url = $_GET;
            $url['lang'] = $data['lang'];
            $data['lang_url'] = urldecode(HTTP_SERVER.'?'.http_build_query($url));
        } else {
            $data['lang_url'] = $_SERVER['REQUEST_URI'].'&lang='.$data['lang'];
        }
        $data['site_title'] = $this->config['site_title'];
        $this->load->view('header', $data);
    }

    public function changeLanguage()
    {
        $rules = ['lang'=>'l:2'];
        $this->form->process_get($rules);
        $this->session->changeLanguage($this->form->data['lang']);
    }
}
