<?php

class ControllerVendors extends Controller
{
    public $limit = 15;

    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $data['page_title'] = 'Purchase Order';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }

    public function createNew()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'New Vendor';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Vendors', 'href' => '?route=vendors/view'];
        $breadcrumb[] = ['title'=>'Create New', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('vendors');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        if ($_POST) {
            $rules = [
                'company_name'       => 'r',
                'email'              => 'r',
                'address'            => 'r',
                'postal_code'        => 'r',
                'person_responsible' => 'r',
                'city'               => 'r',
                'phone'              => 'r',
                 'franco'            => 'r',
                // "fax" => "r",
            ];
            $this->form->process_post($rules);

            $this->load->model('vendor');

            if (!$this->model_vendor->validate_email($this->form->data['email'])) {
                $this->form->error['error_email'] = 'Email already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (!$this->model_vendor->validate_phone($this->form->data['phone'])) {
                $this->form->error['error_phone'] = 'Phone number already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (empty($this->form->error)) {
                $id = $this->model_vendor->createUser($this->form->data);
                $vendor_id = $this->model_vendor->createVendor($id, $this->form->data['company_name'], $this->form->data);
                $this->load->controller('activity');

                $this->controller_activity->logActivity(1, $vendor_id, $this->session->loggedUser(), true);

                header('Location:?route=vendors/view');
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }
        $this->load->view('createNewVendor', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Vendors List';
        $this->load->controller('header');
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Vendors', 'href' => '?route=vendors/view'];
        $breadcrumb[] = ['title'=>'view', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->controller_header->load($header);

        $this->load->text('vendors');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $this->limit;

        $this->load->model('vendor');
        if (isset($_GET['c']) && ($_GET['c'] != '' || $_GET['c'] != 0)) {
            $vendors = $this->model_vendor->getVendorList($start, $this->limit, $_GET['c']);
        } else {
            $vendors = $this->model_vendor->getVendorList($start, $this->limit);
        }
        $vendors_categories = $this->model_vendor->getVendorCategoryList();

        $total_count = $this->model_vendor->getCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $this->limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=vendors/view&page={page}';
        if (isset($_GET['c']) && ($_GET['c'] != '' || $_GET['c'] != 0)) {
            $this->pagination->url = HTTP_SERVER.'?route=vendors/view&c='.$_GET['c'].'&page={page}';
        }
        $data['pagination'] = $this->pagination->render();

        $data['vendors'] = $vendors;
        $data['categories'] = $vendors_categories;
        $this->load->view('viewVendors', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getVendorDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['vendor_id'])) {
                $vendor_id = (int) $_GET['vendor_id'];
                $this->load->model('vendor');

                $vendor_details = $this->model_vendor->getVendorDetails($vendor_id);
                $items = $this->model_vendor->getItemListByVendor($vendor_id);
                $orders = $this->model_vendor->getPurchaseOrderByVendor($vendor_id);

                foreach ($items as $key => $item) {
                    $image = json_decode($items[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $items[$key]['image'] = $image_url;
                    } else {
                        $items[$key]['image'] = 0;
                    }

                    $pdf = json_decode($items[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $items[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $items[$key]['item_pdf'] = 0;
                    }
                }
                echo json_encode(['status' => 'success',
                    'vendor_details'       => $vendor_details,
                    'item_details'         => $items,
                    'purchase_orders'      => $orders, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    /*function getItemListByVendor(){
       header('Content-Type: application/json');
       if($_GET){
           if(isset($_GET['vendor_id'])){
               $vendor_id = $_GET['vendor_id'];
               $this->load->model("vendor");
               $item_details = $this->model_vendor->getItemListByVendor($vendor_id);
               $image = json_decode($item_details["image"]);

               if(count($image) > 0 ) {
                   $image_url = RESOURCE_URL."/".$image[0];
                   $item_details["image"] = $image_url;
               }else{
                   $item_details["image"] = 0;
               }

               $pdf = json_decode($item_details["item_pdf"]);
               if(count($pdf) > 0 ) {
                   $pdf_url = RESOURCE_URL."/".$pdf[0];
                   $item_details["item_pdf"] = $pdf_url;
               }else{
                   $item_details["item_pdf"] = 0;
               }

              echo json_encode(array('status' => 'success',
                   'item_details' => $item_details));


           }else{
               echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
           }
       }
   }*/

    public function editVendor()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        if (!isset($_GET['vendor_id'])) {
            header('location:?route=vendors/index');
            exit;
        }
        $ajax_call = (isset($_GET['ac'])) ? (int) $_GET['ac'] : 0;
        $vendor_id = (int) $_GET['vendor_id'];

        $this->load->model('vendor');
        $vendor_details = $this->model_vendor->getVendorDetails($vendor_id);

        $data = [];

        if ($_POST) {
            $rules = [
                'name'        => 'r',
                'email'       => 'e|r',
                'address'     => 'r',
                'postal_code' => 'r',
                'city'        => 'r',
                'franco'      => 'r',
                'phone'       => 'r',
            ];
            $this->form->process_post($rules);

            $this->load->model('vendor');

            if (!$this->model_vendor->validate_email($this->form->data['email'], $vendor_details['user_id'])) {
                $this->form->error['error_email'] = 'Email already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (!$this->model_vendor->validate_phone($this->form->data['phone'], $vendor_details['user_id'])) {
                $this->form->error['error_phone'] = 'Phone number already registered';
                $data = array_merge($data, $this->form->data);
            }

            if (empty($this->form->error)) {
                $this->model_vendor->updateUser($vendor_details['user_id'], $this->form->data);
                $this->model_vendor->updateVendor($vendor_id, $this->form->data['name'], $this->form->data);
                $this->load->controller('activity');

                $this->controller_activity->logActivity(24, $vendor_id, $this->session->loggedUser(), true);
                if ($ajax_call) {
                    echo json_encode(['status'=>true]);

                    return;
                }
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                if ($ajax_call) {
                    echo json_encode(['status'=>false]);

                    return;
                }
            }
        }

        $header['page_title'] = $vendor_details['company_name'];
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Vendors', 'href' => '?route=vendors/view'];
        $breadcrumb[] = ['title'=>'Edit Vendor', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('vendors');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $vendor_details['name'] = $vendor_details['company_name'];
        unset($vendor_details['company_name']);
        $vendor_details['email'] = $vendor_details['email_id'];
        unset($vendor_details['email_id']);
        $data = array_merge($data, $vendor_details);

        $this->load->view('editVendor', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }
}
