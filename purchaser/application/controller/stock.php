<?php

class ControllerStock extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $data['page_title'] = 'Purchase Order';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }

    public function import()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }

        $data = [];
        $header['page_title'] = 'Import Items';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Stock', 'href' => '?route=stock/stock'];
        $breadcrumb[] = ['title'=>'Import', 'href' => '?route=stock/import'];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->model('stock');

        if (isset($_POST['import'])) {
            $allowedFileType = ['application/vnd.ms-excel', 'text/xls', 'text/xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

            if (in_array($_FILES['file']['type'], $allowedFileType)) {
                $targetPath = DIR_RESOURCES.$_FILES['file']['name'];
                move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

                $Reader = new SpreadsheetReader($targetPath);

                $sheetCount = count($Reader->sheets());
                $error = true;
                for ($i = 0; $i < $sheetCount; $i++) {
                    $Reader->ChangeSheet($i);
                    foreach ($Reader as $Row) {
                        if (isset($Row[2]) && $Row[2] != '' && $Row[2] != 'Item Name') {
                            if ($this->model_stock->importItem($Row)) {
                                $error = false;
                            }
                        }
                    }
                }
                if ($error) {
                    die('There was an error with your excel sheet. Try back later.');
                } else {
                    $this->session->data['status'] = 'Data successfully imported.';
                    header('Location:?route=stock/viewStock');
                    exit;
                }
            } else {
                $type = 'error';
                $message = 'Invalid File Type. Upload Excel File.';
            }
        }
        $data = [];
        $data['page_title'] = 'Import Items';
        $this->load->text('stock');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('import', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function addNewItem()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Add New Item';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Stock', 'href' => '?route=stock/addNewItem'];
        $breadcrumb[] = ['title'=>'Add new item', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('stock');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('stock');

        $data['vendors'] = $this->model_stock->getVendorName();
        $data['categories'] = $this->model_stock->getCategory();
        if ($_POST) {
            $rules = [
                'item_name'     => 'r',
                'category'      => 'r',
                'brand_name'    => 'r',
                'purchase_rate' => 'r',
                'item_area'     => 'r',
                'reference'     => 'r',
                'vendor_name'   => 'r',
                ];
            $file_rules = ['add_image' => 'size_limit:2|ext:jpg,bmp,png',
                                'pdf'  => 'size_limit:2|ext:pdf', ];
            if ($_POST['category'] == 'add_new') {
                $rules['new_category'] = 'r';
            }
            $this->file->process_files($file_rules);
            $this->form->process_post($rules);

            if (empty($this->form->error) && empty($this->file->errors)) {
                $this->file->move_files();
                if ($_POST['category'] == 'add_new') {
                    $this->form->data['category'] = $this->model_stock->addNewCategory($this->form->data['new_category']);
                }
                $this->form->data['add_image'] = json_encode($this->file->files['add_image']);
                $this->form->data['pdf'] = json_encode($this->file->files['pdf']);
                $id = $this->model_stock->addItem($this->form->data);

                $this->load->controller('activity');

                $this->controller_activity->logActivity(4, $id, $this->session->loggedUser(), true);
                $this->session->data['status'] = 'Item details Updated';
                header('location:?route=stock/viewStock');
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->errors);
            }
        }

        $data['margin'] = $this->config['item_margin'];

        $this->load->view('addItemToStock', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function editItem()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $item = (isset($_GET['item_id'])) ? (int) $_GET['item_id'] : 0;
        if (!$item) {
            die('Illegal Operation');
        }

        $data = [];

        $this->load->model('stock');
        $data = $this->model_stock->getItemDetails($item);
        $data['vendor_name'] = $data['vendor'];
        $data['category'] = $data['item_category_id'];
        $data['item_area'] = $data['area'];
        $data['description'] = $data['item_description'];
        $header['page_title'] = 'Edit Item';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Stock', 'href' => '?route=stock/viewStock'];
        $breadcrumb[] = ['title'=>'Edit item', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('stock');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $data['vendors'] = $this->model_stock->getVendorName();
        $data['categories'] = $this->model_stock->getCategory();
        if ($_POST) {
            $rules = [
                'item_name'   => 'r',
                'category'    => 'r',
                'brand_name'  => 'r',
                'item_area'   => 'r',
                'reference'   => 'r',
                'vendor_name' => 'r',
                // "packing" => "r"
                ];
            $file_rules = ['add_image' => 'size_limit:2|ext:jpg,bmp,png',
                                'pdf'  => 'size_limit:2|ext:pdf', ];
            if ($_POST['category'] == 'add_new') {
                $rules['new_category'] = 'r';
            }
            $this->file->process_files($file_rules);
            $this->form->process_post($rules);

            if (empty($this->form->error) && empty($this->file->errors)) {
                $this->file->move_files();
                if ($_POST['category'] == 'add_new') {
                    $this->form->data['category'] = $this->model_stock->addNewCategory($this->form->data['new_category']);
                }
                $this->form->data['add_image'] = json_encode($this->file->files['add_image']);
                $this->form->data['pdf'] = json_encode($this->file->files['pdf']);
                $id = $this->model_stock->editItem($item, $this->form->data);

                $this->load->controller('activity');

                $this->controller_activity->logActivity(40, $item, $this->session->loggedUser(), true);

                header('location:?route=stock/viewStock');
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                $data = array_merge($data, $this->file->errors);
            }
        }

        $this->load->view('editItem', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function viewStock()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Stock';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Stock', 'href' => '?route=stock/viewStock'];
        $breadcrumb[] = ['title'=>'View items', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('stock');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('stock');

        $items = $this->model_stock->getStockList();
        $vendors = $this->model_stock->getVendorList();
        $references = $this->model_stock->getRefList();
        $categories = $this->model_stock->getCategoryList();

        foreach ($items as $key => $item) {
            $image = json_decode($items[$key]['image']);
            if (count($image) > 0) {
                $image_url = RESOURCE_URL.'/'.$image[0];
                $items[$key]['image'] = $image_url;
            } else {
                $items[$key]['image'] = 0;
            }

            $pdf = json_decode($items[$key]['item_pdf']);
            if (count($pdf) > 0) {
                $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                $items[$key]['item_pdf'] = $pdf_url;
            } else {
                $items[$key]['item_pdf'] = 0;
            }
        }
        $data['items'] = $items;
        $data['references'] = $references;
        $data['vendors'] = $vendors;
        $data['categories'] = $categories;
        $this->load->view('viewStock', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function addNewOffer()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $header['page_title'] = 'Add New Special Offer';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Stock', 'href' => '?route=stock/viewStock'];
        $breadcrumb[] = ['title'=>'Add New Special Offer', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $header['style'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.default.min';
        $header['script'][] = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min';
        $this->load->text('viewStock');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('add_new_offer', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('stock');
                $item_details = $this->model_stock->getItemDetails($item_id);
                $image = json_decode($item_details['image']);

                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
           // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }

    public function addVendor()
    {
        $data = [];
        if ($_POST) {
            $this->form->process_post($_POST);

            $this->load->model('stock');

            if (!$this->model_stock->validate_email($this->form->data['email'])) {
                $this->form->error['error_email'] = 'Email already registered';
                $data = array_merge($data, $this->form->data);
            }
            if (empty($this->form->error)) {
                $id = $this->model_stock->createUser($this->form->data);
                $vendor_id = $this->model_stock->createVendor($id, $this->form->data);
                echo '200';
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
                echo json_encode($data);
            }
        } else {
            echo 401 .' Not Found';
        }
    }

    public function addCategory()
    {
        $category = $_POST['category_name'];

        $this->load->model('stock');

        echo $this->model_stock->addNewCategory($category);
    }
}
