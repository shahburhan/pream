<?php

class ControllerPurchaseOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $data['page_title'] = 'Purchase Order';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('purchaseOrder', $data);
    }

    public function newOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'New order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Order', 'href' => '?route=purchase/newOrder'];
        $breadcrumb[] = ['title'=>'New Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchase');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('purchaseOrder');
        //$data["vendors"] = $this->model_purchaseOrder->getVendorName();
        $item_List = $this->model_purchaseOrder->getAllItemsList('');
        $data['payment_mode'] = $this->model_purchaseOrder->getPaymentModes();
        $data['client_addresses'] = $this->model_purchaseOrder->getClientAddress();
        $data['store_address'] = $this->config['store_address'];
        $data['vendors'] = $this->model_purchaseOrder->getVendorsList();
        $data['references'] = $this->model_purchaseOrder->getRefList(1);
        $data['categories'] = $this->model_purchaseOrder->getCategoryList();

        foreach ($item_List as $key => $item) {
            $image = json_decode($item_List[$key]['image']);
            if (count($image) > 0) {
                $image_url = RESOURCE_URL.'/'.$image[0];
                $item_List[$key]['image'] = $image_url;
            } else {
                $item_List[$key]['image'] = 0;
            }

            $pdf = json_decode($item_List[$key]['item_pdf']);
            if (count($pdf) > 0) {
                $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                $item_List[$key]['item_pdf'] = $pdf_url;
            } else {
                $item_List[$key]['item_pdf'] = 0;
            }
        }
        $data['items'] = $item_List;

        if (isset($_GET['ref']) && $_GET['ref'] == 'ro') {
            $id = (int) $_GET['id'];
        }

        if ($_POST) {
            $rules = [
                'order_date'       => 'r',
                'vendor_name'      => 'r',
                'delivery_address' => 'r',
                ];
            $this->form->process_post($rules);

            if (empty($this->form->error)) {
                $id = $this->model_purchaseOrder->newOrder($this->form->data);
                $this->load->controller('activity');
                $this->controller_activity->logActivity(13, $id, $this->session->loggedUser(), true);
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }

        $this->load->view('newOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function viewOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'View Orders';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Order', 'href' => '?route=purchase/viewOrder'];
        $breadcrumb[] = ['title'=>'View Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;

        $header['style'][] = HTTP_SERVER.'/application/view/resources/style/animate';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchase');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('purchaseOrder');
        $orders = $this->model_purchaseOrder->getOrderList($start, $limit);
        $total_count = $this->model_purchaseOrder->orderCount(1);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/viewOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['orders'] = $orders;
        $this->load->view('viewOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function viewRO()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $id = (int) $_GET['id'];

        if (!$id) {
            die('Invalid Request');
        }

        $this->load->model('purchaseOrder');

        if ($_POST) {
            $data['order_id'] = (int) $_GET['id'];
            $data['payment_mode'] = $_POST['payment_mode'];
            $data['tva'] = $_POST['tax'];
            $data['ttc'] = $_POST['ttc'];
            $data['ht'] = $_POST['ht'];
            $data['delivery_address'] = $_POST['delivery_address'];
            $data['delivery_other'] = $_POST['delivery_other'];
            $data['order_date'] = date('Y-m-d', time());
            $data['purchase_rate'] = $_POST['purchase_rate'];

            $this->model_purchaseOrder->RO2PO($data);

            $this->session->data['status'] = 'Request order has been approved';
            header('location:?route=account/overview');
            exit;
        }

        if (!$this->model_purchaseOrder->validateRO($id)) {
            die('Invalid Request');
        }

        $header['page_title'] = (isset($_GET['id'])) ? 'Request Order #'.(int) $_GET['id'] : 'Request Order ';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Order', 'href' => '?route=purchase/viewOrder'];
        $breadcrumb[] = ['title'=>'View RO', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;

        $header['style'][] = HTTP_SERVER.'/application/view/resources/style/animate';
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchase');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $data['details'] = $this->model_purchaseOrder->getRODetails($id);
        $data['items'] = $this->model_purchaseOrder->getROItems($id);
        $data['responsible'] = $this->model_purchaseOrder->raisedBy($data['details']['raised_by']);
        $data['ht_total'] = $this->model_purchaseOrder->RO_HT($id);
        $data['client_addresses'] = $this->model_purchaseOrder->getClientAddress();
        $data['store_address'] = $this->config['store_address'];
        $data['payment_mode'] = $this->model_purchaseOrder->getPaymentModes();
        $data['vendors'] = $this->model_purchaseOrder->getVendorsList();

        $this->load->view('viewRo', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($header);
    }

    public function completeOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Complete Orders';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Purchase Order', 'href' => '?route=purchase/completeOrder'];
        $breadcrumb[] = ['title'=>'Completed Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('purchase');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;
        $this->load->model('purchaseOrder');
        $orders = $this->model_purchaseOrder->getOrderList($start, $limit, ' ', true);
        $total_count = $this->model_purchaseOrder->orderCount(3);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/completeOrder&page={page}';
        $data['pagination'] = $this->pagination->render();
        $data['orders'] = $orders;

        $this->load->view('completedOrder', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function cancelledOrder()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Cancelled Order';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Action', 'href' => '?route=reports/cancelledOrder'];
        $breadcrumb[] = ['title'=>'Cancelled Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('purchase');

        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('purchaseOrder');
        $cancelled_orders = $this->model_purchaseOrder->getCancelledOrderList($start, $limit, true);
        $order_status = $this->model_purchaseOrder->getOrderStatus();
        $order_status_mapped = [];
        foreach ($order_status as $key => $status) {
            $order_status_mapped[$status['order_status_id']] = ucfirst(str_replace('_', ' ', $status['title']));
        }

        $total_count = $this->model_purchaseOrder->orderCount(2);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/cancelledOrder&page={page}';
        $data['pagination'] = $this->pagination->render();

        $data['cancelled_orders'] = $cancelled_orders;
        $data['order_status'] = $order_status_mapped;
        $this->load->view('cancelledOrder', $data);

        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrderList()
    {
        if ($_POST) {
            $clause = trim($this->buildClause());

            $this->load->model('purchaseOrder');
            $client = $this->model_purchaseOrder->getClientAddress();

            foreach ($client as $address) {
                $data[$address['client_id']]['address'] = $address['address'];
                $data[$address['client_id']]['city'] = $address['city'];
                $data[$address['client_id']]['postal'] = $address['postal_code'];
                $data[$address['client_id']]['name'] = $address['company_name'];
            }
            $data[0]['address'] = $this->config['store_address'];
            $data[0]['city'] = '';
            $data[0]['postal'] = '';
            $data[0]['name'] = 'Store';
            if (isset($_POST['c']) && $_POST['c']) {
                $orders = $this->model_purchaseOrder->getOrderList(0, 10, $clause, true);
            } elseif (isset($_POST['can']) && $_POST['can']) {
                $orders = $this->model_purchaseOrder->getOrderList(0, 10, $clause, false, true);
            } else {
                $orders = $this->model_purchaseOrder->getOrderList(0, 10, $clause);
            }

            echo json_encode(['order_list'=>$orders, 'clause'=>$clause, 'client'=>$data]);
        }
    }

    public function getItemDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['item_id'])) {
                $item_id = $_GET['item_id'];
                $this->load->model('purchaseOrder');
                $item_details = $this->model_purchaseOrder->getItemDetails($item_id);
                $image = json_decode($item_details['image']);
                if (count($image) > 0) {
                    $image_url = RESOURCE_URL.'/'.$image[0];
                    $item_details['image'] = $image_url;
                } else {
                    $item_details['image'] = 0;
                }

                $pdf = json_decode($item_details['item_pdf']);
                if (count($pdf) > 0) {
                    $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                    $item_details['item_pdf'] = $pdf_url;
                } else {
                    $item_details['item_pdf'] = 0;
                }

                echo json_encode(['status' => 'success',
                    'item_details'         => $item_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getCategoryList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $vendor = (int) $_GET['vendor_id'];
                $brand = $_GET['brand_name'];
                $categoryList = $this->model_purchaseOrder->getCategoryList($vendor, $brand);
                echo json_encode(['status' => 'success',
                    'category_list'        => $categoryList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getAreaList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $areaList = $this->model_purchaseOrder->getAreaList();
                echo json_encode(['status' => 'success',
                    'area_list'            => $areaList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function buildClause()
    {
        $clause = '';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $brandClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['brand'])) {
            $c = '';
            $fieldName = 'brand_name';
            $filter = $_POST['brand'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = 'order_id';
            if (isset($_POST['list']) && $_POST['list'] == 'item') {
                $fieldName = 'reference';
            }
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$itemClause.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$itemClause.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return $clause;
    }

    public function getVendorsList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['all'])) {
                $this->load->model('purchaseOrder');
                $vendorsList = $this->model_purchaseOrder->getVendorsList();
                echo json_encode(['status'             => 'success',
                                        'vendors_list' => $vendorsList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getItemsList()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $this->load->model('purchaseOrder');
                $this->load->model('stock');
                $clause = '';
                $clause = $this->buildClause();
                $item_List = [];

                $limit = (isset($_POST['limit'])) ? (int) $_POST['limit'] : 15;
                $page = (isset($_POST['page'])) ? (int) $_POST['page'] : 1;
                $start = ($page - 1) * $limit;
                $total_count = $this->model_stock->getStockTotal($clause);
                $this->load->helper('pagination');
                $this->pagination->limit = $limit;
                $this->pagination->total = $total_count;
                $this->pagination->page = $page;
                $this->pagination->url = HTTP_SERVER.'?route=stock/viewStock&page={page}';
                $pagination = $this->pagination->render();

                if (isset($_POST['with_vendor'])) {
                    $item_List = $this->model_purchaseOrder->getAllItemsListWithVendor($clause, $limit, $start);
                } else {
                    $item_List = $this->model_purchaseOrder->getAllItemsList($clause, $limit, $start);
                }

                foreach ($item_List as $key => $item) {
                    $image = json_decode($item_List[$key]['image']);
                    if (count($image) > 0) {
                        $image_url = RESOURCE_URL.'/'.$image[0];
                        $item_List[$key]['image'] = $image_url;
                    } else {
                        $item_List[$key]['image'] = 0;
                    }

                    $pdf = json_decode($item_List[$key]['item_pdf']);
                    if (count($pdf) > 0) {
                        $pdf_url = RESOURCE_URL.'/'.$pdf[0];
                        $item_List[$key]['item_pdf'] = $pdf_url;
                    } else {
                        $item_List[$key]['item_pdf'] = 0;
                    }
                }

                echo json_encode(['status' => 'success',
                    'itemList'             => $item_List,
                    'pagination'           => $pagination,
                    'total_count'          => $total_count,
                    'clause'               => $clause, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function placeOrder()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            if (isset($_POST['all'])) {
                $data = [];
                $this->load->model('purchaseOrder');
                $items = $_POST['items'];
                $purchase_rate = $_POST['purchase_rates'];
                $quantity = $_POST['quantity'];
                $mrp = $_POST['mrp'];
                $htt = 0;
                $ttc = 0;
                $tva = (float) $_POST['tva'];

                for ($i = 0; $i < count($items); $i++) {
                    $p = $purchase_rate[$i]; //Type Removed
                   $q = $quantity[$i]; //Type Removed
                   $m = $mrp[$i]; //Type Removed
                   $htt += $p * $q;
                }

                $ttc = $htt + (($htt * $tva) / 100);
                $_POST['ttc'] = $ttc;
                $_POST['htt'] = $htt;

                $data['htt'] = $htt;
                $data['tva'] = $tva;
                $data['ttc'] = $ttc;
                $data['amount_paid'] = 0;
                $data['order_date'] = date('Y-m-d', time());
                $data['payment_deadline'] = date('Y-m-d', strtotime($data['order_date'].' + 45 days'));
                $data['order_status'] = 1;
                $data['order_type'] = 1;
                $data['vendor_id'] = $_POST['vendor'];
                $data['person_responsible'] = $_POST['person_responsible'];
                $data['payment_mode'] = $_POST['payment_mode'];
                $data['expected_date'] = $_POST['expected_date'];
                $data['delivery_address'] = $_POST['delivery_address'];
                $data['delivery_other'] = $_POST['delivery_other'];
                $data['city'] = $_POST['city'];
                $data['raised_by'] = $this->session->data['logged_user_id'];
                $order_id = $this->model_purchaseOrder->newOrder($data);
                $this->model_purchaseOrder->removeFromQueue(0, true);

                for ($i = 0; $i < count($items); $i++) {
                    $orderData = [];
                    $it = $items[$i];
                    $p = $purchase_rate[$i]; //Type Removed
                    $q = $quantity[$i]; //Type Removed
                    $m = $mrp[$i]; //Type Removed
                    $orderData['order_id'] = $order_id;
                    $orderData['item_id'] = $it;
                    $orderData['quantity'] = $q;
                    $orderData['discount'] = 0;
                    $orderData['rate'] = $p;
                    $orderData['mrp'] = $m;
                    $orderData['order_type'] = $data['order_type'];
                    $this->model_purchaseOrder->addOrderItems($orderData);
                }
                $this->load->controller('activity');
                $this->controller_activity->logActivity(13, $order_id, $this->session->loggedUser(), true);

                $send = (int) $_POST['email'];

                if ($send) {
                    $mailer = Mail::build();
                    $mailer->addAddress('sb.shahburhan@gmail.com', 'Burhan Shah');
                    $mailer->Subject = 'ELEC SBE - Purchase order has been placed';
                    $mailer->Body = 'Purchase order bearing number :'.$order_id.' has been placed.';
                    $mailer->send();
                }

                echo json_encode(['status' => 'success',
                    'order_id'             => $order_id, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $order_id = (int) $_GET['order_id'];
                $this->load->model('purchaseOrder');
                $order_details = $this->model_purchaseOrder->getOrderDetails($order_id);
                $items = $this->model_purchaseOrder->getItemOrderDetails($order_id);
                //if(count(json_decode($message_details['attachment'])) > 0)$message_details['attachment'] = RESOURCE_URL."/".json_decode($message_details['attachment'])[0];
                //else $message_details['attachment'] = 0;
                $payment_modes = $this->model_purchaseOrder->getPaymentModes();
                $bol_details = $this->model_purchaseOrder->fetchBolDetails($order_id);
                $pm_mapped = [];
                foreach ($payment_modes as $payment_mode) {
                    $pm_mapped[$payment_mode['payment_mode_id']] = $payment_mode['title'];
                }

                $client = $this->model_purchaseOrder->getClientAddress();

                $discount = $this->model_purchaseOrder->getDiscount($order_id);

                foreach ($client as $address) {
                    $data[$address['client_id']]['address'] = $address['address'];
                    $data[$address['client_id']]['city'] = $address['city'];
                    $data[$address['client_id']]['postal'] = $address['postal_code'];
                    $data[$address['client_id']]['name'] = $address['company_name'];
                }

                $data[0]['address'] = $this->config['store_address'];
                $data[0]['city'] = '';
                $data[0]['postal'] = '';
                $data[0]['name'] = 'Store';

                echo json_encode(['status' => 'success',
                    'order_details'        => $order_details,
                    'item_details'         => $items, 'payment_mode' => $pm_mapped, 'delivery_address'=>$data, 'discount'=>$discount, 'bol_details'=>$bol_details, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }//else{
           // echo json_encode(array('status' => 'failed', 'message' => "Not enough parameters"));
        //}
    }

    public function cancelOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('purchaseOrder');

            $this->load->controller('activity');
            $this->controller_activity->logActivity(25, $order_id, $this->session->loggedUser(), true);

            return $this->model_purchaseOrder->cancelOrder($order_id);
        }
    }

    public function getRefList()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $com = false;
            $can = false;
            $ref = 0;
            if (isset($_GET['list']) && $_GET['list'] == 'item') {
                $ref = 1;
            }
            if (isset($_GET['all'])) {
                if (isset($_GET['ot'])) {
                    if ($_GET['ot'] == 1) {
                        $com = true;
                    }
                    if ($_GET['ot'] == 2) {
                        $can = true;
                    }
                }
                $vendor = (isset($_GET['vendor_id'])) ? (int) $_GET['vendor_id'] : 0;
                $brand_name = (isset($_GET['brand_name'])) ? $_GET['brand_name'] : '';
                $category = (isset($_GET['category'])) ? $_GET['category'] : '';
                $this->load->model('purchaseOrder');
                $refList = $this->model_purchaseOrder->getRefList($ref, $vendor, $brand_name, $category, $com, $can);
                echo json_encode(['status'         => 'success',
                                        'ref_list' => $refList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getBrandNames()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            $ref = 0;
            if (isset($_GET['vendor_id']) && $_GET['vendor_id']) {
                $vendor_id = (int) $_GET['vendor_id'];
            }
            if (isset($_GET['all']) && $vendor_id) {
                $this->load->model('purchaseOrder');
                $brandList = $this->model_purchaseOrder->getBrandList($vendor_id);
                echo json_encode(['status'           => 'success',
                                        'brand_list' => $brandList, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function checkForMaxStock($item_id, $purchase_quantity)
    {
        $this->load->model('purchaseOrder');
        $quantities = $this->model_purchaseOrder->maxStock($item_id);

        if ($purchase_quantity > ($quantities['max_stock'] - $quantities['current_stock'])) {
            return false;
        } else {
            return true;
        }
    }

    public function addDiscount()
    {
        //Discount Type : 0 - %age
        //                1 - amount
        //Discount On :   0 - Order
        //                1 - Item

        if ($_POST) {
            $rules = [
                'order_id'          => 'r',
                'discount_type'     => 'r',
                'discount_on'       => 'r',
            ];
            $file_rules = [
                'attachment' => 'size_limit:2|ext:jpg,png,pdf',
            ];

            $this->form->process_post($rules);
            $this->file->process_files($file_rules);

            if (empty($this->form->error) && empty($this->file->error)) {
                //add discount
                $this->load->model('purchaseOrder');

                $this->file->move_files();

                if ($this->form->data['discount_on'] == 1) {
                    foreach ($this->form->data['item_discount'] as $k=>$v) {
                        $this->form->data['item_id'] = $k;
                        $this->form->data['discount_amount'] = $v;
                        $this->form->data['attachment'] = json_encode($this->file->files['attachment']);
                        if ($v > 0) {
                            $this->model_purchaseOrder->addDiscount($this->form->data);
                        }
                    }
                } else {
                    $this->form->data['item_id'] = 0;
                    $this->form->data['attachment'] = json_encode($this->file->files['attachment']);
                    $this->model_purchaseOrder->addDiscount($this->form->data);
                }
                $this->session->data['status'] = 'Discount has been added to #'.$this->form->data['order_id'];
                header('location:?route=purchaseOrder/viewOrder');
                exit;
            } else {
                die('Invalid Data Submitted');
            }
        }
    }

    public function changeQuantity()
    {
        $order_id = (int) $_POST['order_id'];
        $item_id = (int) $_POST['item_id'];
        $new_quantity = (int) $_POST['new_quantity'];

        $this->load->model('purchaseOrder');

        if ($new_quantity) {
            $valid = $this->model_purchaseOrder->validateQuantity($order_id, $item_id);
            if ($valid) {
                if ($this->model_purchaseOrder->changeQuantity($order_id, $item_id, $new_quantity)) {
                    echo json_encode(['status'=>true, 'msg'=> 'Item quantity changed.']);
                } else {
                    echo json_encode(['status'=>false]);
                }
            } else {
                echo json_encode(['status'=>false]);
            }
        } else {
            echo json_encode(['status'=>false,  'msg'=> 'Quantity cannot be zero.']);
        }
    }

    public function addToQueue()
    {
        if ($_POST) {
            $item_id = (int) $_POST['item_id'];
            $quantity = (int) $_POST['quantity'];
            $rate = $_POST['rate'];

            $this->load->model('purchaseOrder');

            if ($quantity && $item_id) {
                $this->model_purchaseOrder->addToQueue($item_id, $quantity, $rate);
                echo json_encode(['status'=>true]);
            } else {
                echo json_encode(['status'=>false,  'msg'=> 'Invalid Data.']);
            }
        }
    }

    public function getFromQueue()
    {
        $this->load->model('purchaseOrder');
        if ($_POST) {
            $clause = '';
            $clause = $this->buildClause();
            $queue['queue'] = $this->model_purchaseOrder->getFromQueue($clause);
            $queue['count'] = count($queue['queue']);
            echo json_encode($queue);
        }
    }

    public function removeFromQueue()
    {
        $item_id = (int) $_GET['id'];

        $this->load->model('purchaseOrder');

        $this->model_purchaseOrder->removeFromQueue($item_id);
    }

    public function viewReturnOrder()
    {
        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;
        $where = ' ';

        $this->load->model('returnOrder');
        if ($_POST) {
            $where = $this->buildClause();
        }
        $orders = $this->model_returnOrder->getOrders($start, $limit, $where);
        $reasons = $this->model_returnOrder->getReason();

        foreach ($reasons as $reason) {
            $data[$reason['return_reason_id']] = $reason['title'];
        }

        $total_count = $this->model_returnOrder->orderCount();

        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/viewOrder&page={page}';
        echo json_encode(['order_list'=>$orders, 'reason'=>$data]);
    }
}
