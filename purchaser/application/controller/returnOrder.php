<?php

class ControllerReturnOrder extends Controller
{
    public function index()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $data['page_title'] = 'Purchase Order';
        $this->load->text('common');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('purchaseOrder', $data);
    }

    public function create()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Create Return Requests';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=returnOrder/newOrder'];
        $breadcrumb[] = ['title'=>'New Order', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);
        $this->load->text('returnOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('returnOrder');

        $orders = $this->model_returnOrder->getOrders();
        $data['vendors'] = $this->model_returnOrder->getVendorName();
        $data['reasons'] = $this->model_returnOrder->getReason();
        $data['orders'] = $orders;

        if ($_POST) {
            $rules = [
                'return_date' => 'r|d',
                'vendor_name' => 'r|b',
                'reason'      => 'r|b',
                ];
            $this->form->process_post($rules);
            if (!isset($_POST['selected_item'])) {
                $this->form->error['error_selected_item'] = 'Please select items for return';
            }

            if (empty($this->form->error)) {
                $id = $this->model_returnOrder->createReturnOrder($this->form->data);

                $this->load->controller('activity');
                $this->controller_activity->logActivity(14, $id, $this->session->loggedUser(), true);

                header('Location:?route=returnOrder/view');
                exit;
            } else {
                $data = array_merge($data, $this->form->error);
                $data = array_merge($data, $this->form->data);
            }
        }

        $this->load->view('createReturnRequest', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function returnRequests()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Return Requests';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=returnOrder/returnRequests'];
        $breadcrumb[] = ['title'=>'Requests', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('returnOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }
        $this->load->view('viewReturnRequests', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function returnOldStock()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Old Stock Return';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=returnOrder/returnOldStock'];
        $breadcrumb[] = ['title'=>'Old Stock', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('returnOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->model('returnOrder');

        $data['vendors'] = $this->model_returnOrder->getVendorName();
        $data['reasons'] = $this->model_returnOrder->getReason();
        $items = $this->model_returnOrder->getItemsList();
        $data['items'] = $items;

        $this->load->view('createReturnRequestOldStock', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function getOrderDetails()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['vendor_id'])) {
                $vendor_id = $_GET['vendor_id'];
                $this->load->model('returnOrder');

                $order_details = $this->model_returnOrder->getOrderDetails($vendor_id);
                $items = $this->model_returnOrder->getItemListByVendor($vendor_id);

                echo json_encode(['status' => 'success',
                    'order_details'        => $order_details,
                    'item_details'         => $items, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function getOrder()
    {
        header('Content-Type: application/json');
        if ($_GET) {
            if (isset($_GET['order_id'])) {
                $order_id = $_GET['order_id'];
                $this->load->model('purchaseOrder');
                $this->load->model('returnOrder');
                $order_details = $this->model_purchaseOrder->getOrderDetails($order_id);
                $items = $this->model_purchaseOrder->getItemOrderDetails($order_id);
                //if(count(json_decode($message_details['attachment'])) > 0)$message_details['attachment'] = RESOURCE_URL."/".json_decode($message_details['attachment'])[0];
                //else $message_details['attachment'] = 0;
                $reasons = $this->model_returnOrder->getReason();

                foreach ($reasons as $reason) {
                    $data[$reason['return_reason_id']] = $reason['title'];
                }

                echo json_encode(['status' => 'success',
                    'order_details'        => $order_details,
                    'item_details'         => $items, 'reason'=>$data, ]);
            } else {
                echo json_encode(['status' => 'failed', 'message' => 'Not enough parameters']);
            }
        }
    }

    public function view()
    {
        if (!$this->session->isLoggedIn()) {
            header('Location:?route=account/login');
            exit;
        } elseif (!$this->session->isRightUser()) {
            header('Location:?route=account/unauthorized');
            exit;
        }
        $data = [];
        $header['page_title'] = 'Return Orders';
        $breadcrumb[] = ['title'=>'Home', 'href' => '?route=account/overview'];
        $breadcrumb[] = ['title'=>'Return Order', 'href' => '?route=returnOrder/returnRequests'];
        $breadcrumb[] = ['title'=>'View', 'href' => ''];
        $header['breadcrumb'] = $breadcrumb;
        $this->load->controller('header');
        $this->controller_header->load($header);

        $this->load->text('returnOrder');
        foreach ($this->text as $key => $value) {
            //assign text variables
            $data['text_'.$key] = $value;
        }

        $this->load->view('returnOrdersView', $data);
        $this->load->controller('footer');
        $this->controller_footer->load($data);
    }

    public function viewOrder()
    {
        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;
        $where = ' ';

        $this->load->model('returnOrder');
        if ($_POST) {
            $where = $this->buildClause();
        }
        $orders = $this->model_returnOrder->getOrders($start, $limit, $where);
        $total_count = $this->model_returnOrder->orderCount();
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/viewOrder&page={page}';
        echo json_encode(['order_list'=>$orders]);
    }

    public function getRequestOrders()
    {
        $limit = 10;
        $page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1;
        $start = ($page - 1) * $limit;

        $this->load->model('returnOrder');
        $orders = $this->model_returnOrder->getRequestOrders($start, $limit);
        $total_count = $this->model_returnOrder->orderCount(0);
        $this->load->helper('pagination');
        $this->pagination->limit = $limit;
        $this->pagination->total = $total_count;
        $this->pagination->page = $page;
        $this->pagination->url = HTTP_SERVER.'?route=purchaseOrder/returnRequests&page={page}';
        echo json_encode(['order_list'=>$orders]);
    }

    public function approveOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('returnOrder');

            $this->load->controller('activity');
            $this->controller_activity->logActivity(6, $order_id, $this->session->loggedUser(), true);

            return $this->model_returnOrder->approveOrder($order_id);
        }
    }

    public function disapproveOrder()
    {
        $order_id = (int) $_GET['order_id'];
        if ($order_id) {
            $this->load->model('returnOrder');

            $this->load->controller('activity');
            $this->controller_activity->logActivity(7, $order_id, $this->session->loggedUser(), true);

            return $this->model_returnOrder->disapproveOrder($order_id);
        }
    }

    public function buildClause()
    {
        $clause = ' ';
        $whereClause = '';
        $sortByClause = '';
        $vendorClause = '';
        $itemClause = '';
        if (isset($_POST['sort'])) {
            if ($_POST['sort'] == 'ASC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' ASC';
                }
            }
            if ($_POST['sort'] == 'DESC') {
                if (isset($_POST['sortField'])) {
                    $sortByClause = ' ORDER BY '.$_POST['sortField'].' DESC';
                }
            }
        }

        if (isset($_POST['vendorList'])) {
            $c = '';
            $fieldName = 'vendor';
            $filter = $_POST['vendorList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName.' = '.$filter[$i];
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['areaFilter'])) {
            $c = '';
            $fieldName = 'area';
            $filter = $_POST['areaFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }
        if (isset($_POST['refList'])) {
            $c = '';
            $fieldName = 'order_id';
            if (isset($_POST['list']) && $_POST['list'] == 'item') {
                $fieldName = 'reference';
            }
            $filter = $_POST['refList'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' ) ';
                }
            }
        }

        if (isset($_POST['categoryFilter'])) {
            $c = '';
            $fieldName = 'item_category';
            $filter = $_POST['categoryFilter'];
            for ($i = 0; $i < count($filter); $i++) {
                $c = $c.' '.$fieldName." = '".$filter[$i]."'";
                if ($i != count($filter) - 1) {
                    $c = $c.' '.'OR ';
                }
            }
            if ($c != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$c.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$c.' )';
                }
            }
        }

        if (isset($_POST['itemIdList'])) {
            for ($i = 0; $i < count($_POST['itemIdList']); $i++) {
                $itemClause = $itemClause.' '.'item_id = '.$_POST['itemIdList'][$i];
                if ($i != count($_POST['itemIdList']) - 1) {
                    $itemClause = $itemClause.' '.'OR ';
                }
            }
            if ($itemClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$itemClause.' ) ';
                } else {
                    $whereClause = $whereClause.' AND ( '.$itemClause.' )';
                }
            }
        }

        if (isset($_POST['dateFilter']) && !isset($_POST['list'])) {
            $dateClause = '';
            $today = date('Y-m-d');
            switch ($_POST['dateFilter'][0]) {
                case 'time_1':
                    $interval = 1;
                    break;
                case 'time_2':
                    $interval = 7;
                    break;
                case 'time_3':
                    $interval = 14;
                    break;
                case 'time_4':
                    $interval = 30;
                    break;
            }
            for ($i = 0; $i < count($_POST['dateFilter']); $i++) {
                $dateClause = $dateClause.' '."order_date BETWEEN DATE_SUB('$today', INTERVAL '$interval' DAY) AND '$today' ";
                if ($i != count($_POST['dateFilter']) - 1) {
                    $dateClause = $dateClause.' '.'OR ';
                }
            }
            if ($dateClause != '') {
                if ($whereClause == '') {
                    $whereClause = 'WHERE ( '.$dateClause.' )';
                } else {
                    $whereClause = $whereClause.' AND ( '.$dateClause.' ) ';
                }
            }
        }

        $clause = $whereClause.' '.$sortByClause;

        return $clause;
    }
}
