<?php

//create vendor start
//heading
$text_vendors['title'] = 'Add New Vendor';
//form label
$text_vendors['username'] = 'Username';
$text_vendors['first_name'] = 'First Name';
$text_vendors['last_name'] = 'Last Name';
$text_vendors['password'] = 'Password';
$text_vendors['address'] = 'Address';
$text_vendors['email'] = 'Email';
$text_vendors['postal_code'] = 'Postal Code';
$text_vendors['city'] = 'City';
$text_vendors['phone'] = 'Phone';
$text_vendors['company_name'] = 'Company Name';

//error
$text_vendors['field_required'] = 'This Field Is Required';

//btn
$text_vendors['reset'] = 'Reset';
$text_vendors['create'] = 'Create';

//create vendor end

//-------------------------------//
//view vendor start

$text_vendors['title_vendor'] = 'Vendors';
$text_vendors['search'] = 'Search';
//table headings
$text_vendors['vendor_id'] = 'Vendor Id';
$text_vendors['vendor_name'] = 'Vendor Name';
$text_vendors['vendor_email'] = 'Vendor Email';
$text_vendors['vendor_address'] = 'Vendor Address';
$text_vendors['vendor_contact'] = 'Vendor Contact';
$text_vendors['approval_status'] = 'Approval <br> Status';
$text_vendors['action'] = 'Action';

//action
$text_vendors['edit'] = 'Edit';

//view details
$text_vendors['vendor_details'] = 'Vendor Details';
$text_vendors['po'] = 'PO#';

//item details
$text_vendors['item_id'] = 'Item Id';
$text_vendors['item_name'] = 'Item Name';
$text_vendors['brand_name'] = 'Brand Name';
$text_vendors['buy_rate'] = 'Buy Rate';
$text_vendors['quantity'] = 'Quantity';
$text_vendors['total'] = 'Total';

//view vendor end
