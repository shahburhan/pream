<?php

//--Navigation options
$text_common['overview'] = 'aperçu';
$text_common['po'] = 'Purchase Order';
$text_common['stock'] = 'Stock';
$text_common['return'] = 'Return Order';
$text_common['request'] = 'Request Order';
$text_common['vendor'] = 'Vendors';
$text_common['reports'] = 'Reports';

//--Purchase orders options
$text_common['new_order'] = 'New Order';
$text_common['view_order'] = 'View Order';
$text_common['completed_order'] = 'Completed Order';

//--Stock Options
$text_common['add_new_item'] = 'Add new item';
$text_common['view_stock'] = 'View Stock';

//--Return orders options
$text_common['create_return_order'] = 'Create';
$text_common['return_request'] = 'Return Requests';
$text_common['return_old_stock'] = 'Return old stock to new Vendor';

//--Request order options
$text_common['order_requests'] = 'Requests';
$text_common['order_cancel'] = 'Cancel';
$text_common['order_history'] = 'History';

//--Vendors options
$text_common['create_new_vendors'] = 'Create New';
$text_common['view_vendors'] = 'View';

//--Report/Statistics options
$text_common['sales_report'] = 'Sales Report';
$text_common['item_report'] = 'Item Reports';
$text_common['vendor_report'] = 'Vendor Reports';
