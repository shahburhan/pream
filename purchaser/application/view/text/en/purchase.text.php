<?php

//new order start//

//form label
$text_purchase['new_order_title'] = 'New Purchase Order';
$text_purchase['order_date'] = 'Order Date';
$text_purchase['vendor_name'] = 'Vendor Name';
$text_purchase['delivery_address'] = 'Delivery Address';
$text_purchase['responsible_person'] = 'Responsible Person';

//btn
$text_purchase['create'] = 'Create';
$text_purchase['reset'] = 'Reset';
$text_purchase['add_items'] = 'Add Items';

//table title
$text_purchase['select_item'] = 'Select Items';
$text_purchase['refresh'] = 'Refresh';
$text_purchase['search'] = 'Search';
//table
$text_purchase['item_id'] = 'Item Id';
$text_purchase['item_name'] = 'Item Name';
$text_purchase['brand_name'] = 'Brand Name';
$text_purchase['stock'] = 'Stock';

//item details
$text_purchase['item_details'] = 'Item Details';
$text_purchase['vendor_name'] = 'Vendor Name';
$text_purchase['stock'] = 'Stock';
$text_purchase['current_stock'] = 'Current Stock';
$text_purchase['buy_rate'] = 'Buy Rate';
$text_purchase['profit'] = 'Profit%';
$text_purchase['discount_rate'] = 'Discount Rate';
$text_purchase['packing'] = 'Packing';
$text_purchase['item_area'] = 'Area';
$text_purchase['reference'] = 'Ref#';
$text_purchase['expected_date'] = 'Expected Delivery Date';

//new order end//
//------------------------------------//
//view order start//

$text_purchase['view_po_title'] = 'Purchase Orders';

$text_purchase['search'] = 'Search';
//table headings
$text_purchase['order_id'] = 'PO#';
$text_purchase['order_date'] = 'Order Date';
$text_purchase['vendor_name'] = 'Vendor Name';
$text_purchase['delivery_address'] = 'Delivery Address';
$text_purchase['raised_by'] = 'Raised By';
$text_purchase['total_amount'] = 'Total Amount';
$text_purchase['status'] = 'Status';
$text_purchase['action'] = 'action';

//action
$text_purchase['edit'] = 'Edit';
$text_purchase['delete'] = 'Delete';
$text_purchase['details'] = 'Order Details';

//item
$text_purchase['item_id'] = 'Item Id';
$text_purchase['item_name'] = 'Item Name';
$text_purchase['brand_name'] = 'Brand Name';
$text_purchase['buy_rate'] = 'Buy Rate';
$text_purchase['quantity'] = 'Quantity';
$text_purchase['total'] = 'Total';
$text_purchase['action'] = 'Action';
$text_purchase['reference'] = 'Reference';
$text_purchase['packing'] = 'Packing';

//details
$text_purchase['purchase_details'] = 'Purchase Order Details';
$text_purchase['po'] = 'PO#';
$text_purchase['grand_total'] = 'Grand Total (TTC)';
$text_purchase['delivery_date'] = 'Delivery Date';
$text_purchase['person_responsible'] = 'Person Responsible';
$text_purchase['purchaser'] = 'Purchaser';
$text_purchase['vendor_address'] = 'Vendor Address';

//view order end//

//-----------------------------------//

//complete orders start//
$text_purchase['title'] = 'Completed Orders';
// $text_purchase['order_id'] = "Order Id";
$text_purchase['order_date'] = 'Order Date';
$text_purchase['vendor_name'] = 'Vendor Name';
$text_purchase['delivery_address'] = 'Delivery Address';
$text_purchase['raised_by'] = 'Raised By';
$text_purchase['total_amount'] = 'Total Amount';
$text_purchase['bol'] = 'Bill of Lading';
$text_purchase['cancel'] = 'Cancel Order';

//details

//complete orders end//
