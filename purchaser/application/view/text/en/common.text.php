<?php

//--Navigation options
$text_common['overview'] = 'Overview';
$text_common['po'] = 'Purchase Order';
$text_common['stock'] = 'Item';
$text_common['return'] = 'Return Order';
$text_common['request'] = 'Request Order';
$text_common['vendor'] = 'Vendors';
$text_common['reports'] = 'Reports';
$text_common['notifications'] = 'Notifications';
$text_common['cancelled_order'] = 'Cancelled Order';
//--Purchase orders options
$text_common['new_order'] = 'New Order';
$text_common['view_order'] = 'View Order';
$text_common['completed_order'] = 'Completed Order';

//--Stock Options
$text_common['add_new_item'] = 'Add new item';
$text_common['view_stock'] = 'View Items';
$text_common['add_new_offer'] = 'Add New Special Offer';

//--Return orders options
$text_common['create_return_order'] = 'Create';
$text_common['return_request'] = 'Return Requests';
$text_common['return_old_stock'] = 'Return old stock to Vendor';
$text_common['view_return_orders'] = 'View Return Orders';

//--Request order options
$text_common['order_requests'] = 'Requests';
$text_common['order_cancel'] = 'Cancel';
$text_common['order_history'] = 'History';

//--Vendors options
$text_common['create_new_vendors'] = 'Create New';
$text_common['view_vendors'] = 'View';

//--Report/Statistics options
$text_common['purchase_report'] = 'Purchase Report';
$text_common['item_report'] = 'Item Reports';
$text_common['vendor_report'] = 'Vendor Reports';

//-notifications
$text_common['minimum_stock_alert'] = 'Minimum Stock Alert';
$text_common['residue_stock_alert'] = 'Residue Stock Alert';
$text_common['new_request_order'] = 'New Request Order Alert';
$text_common['ro_cancel'] = 'RO Cancel Alert';
$text_common['po_receipt'] = 'PO Receipt Alert';
$text_common['audit_alert'] = 'Audit Alert';
