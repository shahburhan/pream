<?php

//overview
$text_account['sales_agent'] = 'Sales Agent';
$text_account['an_hour_ago'] = 'An Hour Ago';
$text_account['waylon_dalton'] = 'Waylon Dalton';
$text_account['subject'] = 'Subject';
$text_account['notifications'] = 'Notifications';
$text_account['minimum_stock'] = 'Minimum Stock Alert';
$text_account['stock_update'] = 'Stock Update';
$text_account['order_dispatch'] = 'Order Dispatch';
$text_account['new_item_alert'] = 'New Item Alert';
$text_account['read_more'] = 'Read More';

//login
$text_account['title'] = 'Sign In';
$text_account['email'] = 'Username';
$text_account['password'] = 'Password';
$text_account['subtitle'] = 'Purchaser';

//login button
$text_account['login'] = 'Login';

//Error page start//
$text_account['error_404'] = 'Sorry, the page you are looking for cannot be found.';
//Error page end//

//Unauthorized start//
$text_account['unauthorized_message'] = 'You Are Not Authorized To View This Page.';
//Unauthorized end//

//Activity Text

$text_account['activity_1'] = "A <b class='text-danger'>new vendor</b> has been added";
$text_account['activity_2'] = 'Vendor Approved';
$text_account['activity_3'] = 'new_vendor_disapproved';
$text_account['activity_4'] = "A <b class='text-success'>new item</b> has been added";
$text_account['activity_5'] = "A new <b class='text-warning'>return request</b> has been placed";
$text_account['activity_6'] = "Your return request has been <b class='text-success'>approved</b>";
$text_account['activity_7'] = "Your return request has been <b class='text-danger'>disapproved</b>";
$text_account['activity_8'] = "The item in hand is less than <b class='text-danger'>minimum stock value</b>";
$text_account['activity_9'] = "The item has been <b class='text-danger'>in stock</b> for a long time";
$text_account['activity_10'] = " <b class='text-success'>Purchase order </b> has been received";
$text_account['activity_11'] = "<b class='text-warning'>Audit</b> report has to be submitted";
$text_account['activity_12'] = "Return order has been <b class='text-danger'>cancelled</b>";
$text_account['activity_13'] = "A <b class='text-success'>new </b> purchase order has been placed";
$text_account['activity_14'] = "A <b class='text-danger'>new </b> return order has been placed";
$text_account['activity_15'] = "A new <b class='text-success'>sales agent </b> has been added";
$text_account['activity_16'] = "A new <b class='text-success'>client </b> has been added";
$text_account['activity_17'] = "A new <b class='text-success'>sales order </b> has been placed";
$text_account['activity_18'] = "Sales order <b class='text-danger'>return </b> has been placed";
$text_account['activity_19'] = "A new <b class='text-success'>store keeper</b> has been added";
$text_account['activity_20'] = "A new <b class='text-success'>user </b> has been added";
$text_account['activity_21'] = "<b class='text-warning'>Audit </b> date has been added";
$text_account['activity_22'] = "Purchase order has been <b class='text-success'>delivered</b>";
$text_account['activity_23'] = "<b class='text-success'>BOL</b> has been added to purchase order";
$text_account['activity_24'] = "A new <b class='text-success'>sales agent </b> has been added";
$text_account['activity_25'] = "Purchase Order has been <b class='text-danger'>cancelled</b>";
$text_account['activity_26'] = "<b class='text-warning'>Payment</b> reminder sent to client";
$text_account['activity_27'] = "<b class='text-success'>Payment</b> has been made to vendor.";
$text_account['activity_28'] = "<b class='text-success'>Agent Invoice</b> has been paid";
$text_account['activity_29'] = "Office <b class='text-danger'>expense</b> added in accounts";
$text_account['activity_30'] = "Change in monthly <b class='text-danger'>audit date</b>";
$text_account['activity_31'] = "Store Keeper details <b class='text-success'>updated</b>";
$text_account['activity_32'] = "Sales order <b class='text-success'>dispatched</b>";
$text_account['activity_33'] = "Commission rate <b class='text-success'>added</b> for sales agent";
$text_account['activity_34'] = "Sales agent has been <b class='text-danger'>deactived</b>";
$text_account['activity_35'] = "Client <b class='text-danger'>edited</b>";
$text_account['activity_36'] = "Client <b class='text-danger'> assigned</b> to sales agent";
$text_account['activity_37'] = "Sales order <b class='text-danger'></b>";
$text_account['activity_38'] = "<b class='text-success'>Quotation</b> requested";
$text_account['activity_39'] = "Commission invoice <b class='text-success'>generated</b>";
$text_account['activity_40'] = "Item <b class='text-success'>edited</b>";
$text_account['activity_41'] = 'Order item changed';
