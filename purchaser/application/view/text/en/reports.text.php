<?php

//form label
$text_reports['weekly'] = 'Weekly';
$text_reports['monthly'] = 'Monthly';
$text_reports['quaterly'] = 'Quaterly';
$text_reports['yearly'] = 'Yearly';
$text_reports['completed'] = 'Completed';
$text_reports['pending'] = 'Pending';
$text_reports['cancelled'] = 'Cancelled';
$text_reports['total_orders'] = 'Total Orders';
$text_reports['order_id'] = 'Order Id';
$text_reports['order_date'] = 'Order Date';
$text_reports['vendor_name'] = 'Vendor Name';
$text_reports['raised_by'] = 'Raised By';
