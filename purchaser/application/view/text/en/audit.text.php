<?php

//title
$text_audit['title'] = 'Audit';
//form labels
$text_audit['purchase_order'] = 'Purchase Order';
$text_audit['purchase_approved'] = 'Purchase Approved';
$text_audit['purchase_disapproved'] = 'Purchase Disapproved';
$text_audit['return_order'] = 'Return Order';
$text_audit['return_approved'] = 'Return Approved';
$text_audit['return_disapproved'] = 'Return Disapproved';

//submit

$text_audit['submit'] = 'Submit';
