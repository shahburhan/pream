<?php

//add item start//
//form label
$text_stock['title_add'] = 'Add Item';
$text_stock['title_edit'] = 'Edit Item';
$text_stock['category'] = 'Choose Category';
$text_stock['item_name'] = 'Item Name';
$text_stock['vendor_name'] = 'Vendor Name';
$text_stock['reference'] = 'Reference#';
$text_stock['buy_rate'] = 'Purchase Rate';
$text_stock['sell_rate'] = 'Sell Rate';
$text_stock['brand_name'] = 'Brand Name';
$text_stock['min_stock'] = 'Min Stock';
$text_stock['max_stock'] = 'Max Stock';
$text_stock['remarks'] = 'Remarks';
$text_stock['packing'] = 'Packing';
$text_stock['max_stock'] = 'Max Stock';
$text_stock['add_pdf'] = 'Add Pdf';
$text_stock['item_area'] = 'Area';
$text_stock['item_category'] = 'Category';
$text_stock['item_status'] = 'Item  status';
$text_stock['add_image'] = 'Add Image';
$text_stock['profit'] = 'Profit';
$text_stock['mrp'] = 'MRP';
$text_stock['discount_retailor'] = 'Discount Retailor';
$text_stock['discount_wholesaler'] = 'Discount Wholesaler';
$text_stock['discount_contractor'] = 'Discount Contractor';
$text_stock['net_cost'] = 'Net Cost';
$text_stock['description'] = 'Description';
$text_stock['special_offer'] = 'Special Offer';
$text_stock['on_purchase'] = 'On Purchase Of';
$text_stock['retailor'] = 'Retailor';
$text_stock['contractor'] = 'Contractor';
$text_stock['wholesaler'] = 'Wholesaler';
$text_stock['discount'] = 'Discount';
$text_stock['action'] = 'Action';
$text_stock['field_required'] = 'This Field Is Required';

//btn
$text_stock['reset'] = 'Reset';
$text_stock['add_item'] = 'Add Item';

//add item end//

//-------------------------------------------//

//view items start//

//table headings
$text_stock['title'] = 'View Items';
$text_stock['search'] = 'Search';
$text_stock['s_no'] = 'S.No.';
$text_stock['item_id'] = 'Item Id';
$text_stock['item_name'] = 'Item Name';
$text_stock['brand_name'] = 'Brand Name';
$text_stock['vendor_name'] = 'Vendor Name';
$text_stock['item_location'] = 'Item <br> Location';
$text_stock['minimum_stock'] = 'Minimum <br>Stock <br> Required';
$text_stock['stock_inhand'] = 'Stock <br> Inhand';
$text_stock['so'] = 'Items <br> in <br> SO';
$text_stock['po'] = 'Items <br> in <br> PO';
$text_stock['ro_from_client'] = 'RO <br> From <br> Client';
$text_stock['ro_to_vendor'] = 'RO <br> To <br> Vendor';
$text_stock['total_stock'] = 'Total Stock';
$text_stock['expiry'] = 'Expiry';
$text_stock['last_po'] = 'Last PO';
$text_stock['alert'] = 'Alert';

//item details
$text_stock['item_details'] = 'Item Details';
$text_stock['item_id'] = 'Item Id';
$text_stock['ttc'] = 'T.T.C.';
$text_stock['stock'] = 'Stock';
$text_stock['item_mrp'] = 'MRP';
$text_stock['item_description'] = 'Description';
$text_stock['packing'] = 'Packing';
$text_stock['remarks'] = 'Remarks';
$text_stock['item_area'] = 'Item Area';
$text_stock['current_stock'] = 'Current Stock';
$text_stock['item_expired_stock'] = 'Expired Stock';
$text_stock['item_damaged_stock'] = 'Damaged Stock';
$text_stock['item_in_transit'] = 'On Hold';
$text_stock['min_stock'] = 'Min Stock';
$text_stock['special_offer'] = 'Special Offers';
$text_stock['sale_of'] = 'Sale Of';
$text_stock['purchase_of'] = 'On Purchase Of';
$text_stock['item_image'] = 'Item Image';
$text_stock['discount_expiry'] = 'Vendor Discount Expiry';

//action
$text_stock['edit'] = 'Edit';
$text_stock['delete'] = 'Delete';

//alerts modal
$text_stock['alerts'] = 'Alerts';
$text_stock['field_name'] = 'Field Name';
$text_stock['requested_by'] = 'Requested By';
$text_stock['previous_value'] = 'Previous Value';
$text_stock['new_value'] = 'New Value Suggested';
$text_stock['action'] = 'Action';
$text_stock['approve'] = 'Approve';
$text_stock['disapprove'] = 'Disapprove';

//view items end//
