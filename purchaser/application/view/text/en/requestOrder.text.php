<?php

//request order view start
$text_requestOrder['title'] = 'Request Orders';
$text_requestOrder['search'] = 'Search';

//table headings
$text_requestOrder['order_id'] = 'Order Id';
$text_requestOrder['order_date'] = 'Order Date';
$text_requestOrder['vendor_name'] = 'Vendor Name';
$text_requestOrder['delivery_address'] = 'Delivery Address';
$text_requestOrder['total_amount'] = 'Total Amount';
$text_requestOrder['status'] = 'Status';
$text_requestOrder['raised_by'] = 'Raised By';
$text_requestOrder['action'] = 'Action';

//action
$text_requestOrder['edit'] = 'Edit';
$text_requestOrder['delete'] = 'Delete';
$text_requestOrder['details'] = 'Details';
$text_requestOrder['approve'] = 'Approve';
$text_requestOrder['disapprove'] = 'Disapprove';

//po details
$text_requestOrder['purchase_details'] = 'Purchase Details';
$text_requestOrder['po'] = 'PO#';
$text_requestOrder['grand_total'] = 'Grand Total';
$text_requestOrder['order_date'] = 'Order Date';
$text_requestOrder['vendor_name'] = 'Vendor Name';
$text_requestOrder['vendor_address'] = 'Vendor Address';
$text_requestOrder['delivery_date'] = 'Delivery Date';
$text_requestOrder['person_responsible'] = 'Person Responsible';
$text_requestOrder['purchaser'] = 'Purchaser';

$text_requestOrder['item_id'] = 'Item Id';
$text_requestOrder['item_name'] = 'Item Name';
$text_requestOrder['brand_name'] = 'Brand Name';
$text_requestOrder['buy_rate'] = 'Buy Rate';
$text_requestOrder['quantity'] = 'Quantity';
$text_requestOrder['total'] = 'Total';
$text_requestOrder['action'] = 'Action';

//request order view end

//---------------------------------------------//

//request order history start
$text_requestOrder['title'] = 'Request Orders - History';
$text_requestOrder['search'] = 'Search';
//table headings
$text_requestOrder['order_number'] = 'Order Number';
$text_requestOrder['order_date'] = 'Order Date';
$text_requestOrder['vendor_name'] = 'Vendor Name';
$text_requestOrder['vendor_address'] = 'Vendor Address';
$text_requestOrder['responsible_person'] = 'Responsible Person';
$text_requestOrder['delivery_address'] = 'Delivery Address';
$text_requestOrder['raised_by'] = 'Raised By';
$text_requestOrder['state'] = 'State';

//po details
$text_requestOrder['purchase_details'] = 'Purchase Details';
$text_requestOrder['po'] = 'PO#';
$text_requestOrder['grand_total'] = 'Grand Total';
$text_requestOrder['order_date'] = 'Order Date';
$text_requestOrder['vendor_name'] = 'Vendor Name';
$text_requestOrder['vendor_address'] = 'Vendor Address';
$text_requestOrder['delivery_date'] = 'Delivery Date';
$text_requestOrder['person_responsible'] = 'Person Responsible';
$text_requestOrder['purchaser'] = 'Purchaser';

$text_requestOrder['item_id'] = 'Item Id';
$text_requestOrder['item_name'] = 'Item Name';
$text_requestOrder['brand_name'] = 'Brand Name';
$text_requestOrder['buy_rate'] = 'Buy Rate';
$text_requestOrder['quantity'] = 'Quantity';
$text_requestOrder['total'] = 'Total';

//request order history end//
