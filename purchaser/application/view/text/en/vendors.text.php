<?php

//create vendor start
//heading
$text_vendors['title'] = 'Add New Vendor';
$text_vendors['edit_title'] = 'Update Vendor Details';
//form label
$text_vendors['username'] = 'Username';
$text_vendors['first_name'] = 'First Name';
$text_vendors['last_name'] = 'Last Name';
$text_vendors['password'] = 'Password';
$text_vendors['address'] = 'Address';
$text_vendors['email'] = 'Email';
$text_vendors['postal_code'] = 'Postal Code';
$text_vendors['city'] = 'City';
$text_vendors['phone'] = 'Phone';
$text_vendors['company_name'] = 'Company Name';
$text_vendors['person_responsible'] = 'Person Responsible';
//error
$text_vendors['field_required'] = 'This Field Is Required';

//btn
$text_vendors['reset'] = 'Reset';
$text_vendors['create'] = 'Create';
$text_vendors['update'] = 'Update';

//create vendor end

//-------------------------------//
//view vendor start

$text_vendors['title_vendor'] = 'Vendors';
$text_vendors['search'] = 'Search';
//table headings
$text_vendors['vendor_id'] = 'Vendor Id';
$text_vendors['vendor_name'] = ' Name';
$text_vendors['vendor_email'] = 'Email';
$text_vendors['vendor_address'] = 'Address';
$text_vendors['vendor_contact'] = 'Contact';
$text_vendors['approval_status'] = 'Approval <br> Status';
$text_vendors['action'] = 'Action';

//action
$text_vendors['edit'] = 'Edit';
$text_vendors['details'] = 'Details';
//view details
$text_vendors['vendor_details'] = 'Vendor Details';
$text_vendors['po'] = 'PO#';

//item details
$text_vendors['item_details'] = 'Item Details';
$text_vendors['item_id'] = 'Item Id';
$text_vendors['item_name'] = 'Item Name';
$text_vendors['brand_name'] = 'Brand Name';
$text_vendors['item_category'] = 'Item Category';
$text_vendors['buy_rate'] = 'Buy Rate';
$text_vendors['quantity'] = 'Quantity';
$text_vendors['total'] = 'Total';
$text_vendors['reference'] = 'Ref#';
$text_vendors['action'] = 'Action';

//purchase order
$text_vendors['purchase_order'] = 'Purchase Order';
$text_vendors['order_number'] = 'Order Number';
$text_vendors['order_date'] = 'Order Date';
$text_vendors['order_status'] = 'Order Status';
$text_vendors['delivery_address'] = 'Delivery Address';
$text_vendors['raised_by'] = 'Raised By';
