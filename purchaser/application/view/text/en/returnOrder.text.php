<?php

//create Return order start//
//heading
$text_returnOrder['title'] = 'Select items for return';
$text_returnOrder['title_view'] = 'Return Orders';
$text_returnOrder['title_requests'] = 'Return Requests';
//form label
$text_returnOrder['return_date'] = 'Date Of Return';
$text_returnOrder['vendor_name'] = 'Vendor Name';
$text_returnOrder['reason'] = 'Reason';
$text_returnOrder['description'] = 'Description';

//second heading
$text_returnOrder['heading'] = 'Create Return Order ';
$text_returnOrder['heading_view'] = 'Placed Return Orders';

//table heading
$text_returnOrder['order_id'] = 'RO#';
$text_returnOrder['order_date'] = 'Order Date';
$text_returnOrder['delivery_address'] = 'Delivery Address ';
$text_returnOrder['raised_by'] = 'Raised By ';

//btn
$text_returnOrder['submit'] = 'Submit';
$text_returnOrder['reset'] = 'Reset';
$text_returnOrder['add_po'] = 'Add PO';
$text_returnOrder['search'] = 'Search';

//po details
$text_returnOrder['po_detail'] = 'Purchaser Details';
$text_returnOrder['po_id'] = 'PO#';
$text_returnOrder['vendor_name'] = 'Vendor Name';
$text_returnOrder['stock'] = 'Stock';
$text_returnOrder['vendor_address'] = 'Vendor Address';
$text_returnOrder['delivery_date'] = 'Delivery Date';
$text_returnOrder['person_responsible'] = 'Person Responsible';
$text_returnOrder['delivery_address'] = 'Delivery Address';
$text_returnOrder['purchaser'] = 'Purchaser';
$text_returnOrder['item_id'] = 'Item Id';
$text_returnOrder['item_name'] = 'Item Name';
$text_returnOrder['brand_name'] = 'Brand Name';
$text_returnOrder['buy_rate'] = 'Buy Rate';
$text_returnOrder['quantity'] = 'Quantity';
$text_returnOrder['total_amount'] = 'Total';
$text_returnOrder['item_category'] = 'Item Category';
$text_returnOrder['mrp'] = 'MRP';

//create return order end//
//-------------------------------------------//

//return request start//
//table title
$text_returnOrder['return_title'] = 'Return Requests';

//table headings
$text_returnOrder['return_number'] = 'Return Number';
$text_returnOrder['return_date'] = 'Return Date';
$text_returnOrder['vendor_name'] = 'Vendor Name';
$text_returnOrder['vendor_address'] = 'Vendor Address';
$text_returnOrder['reason'] = 'Reason';
$text_returnOrder['created_by'] = 'Created By';
$text_returnOrder['action'] = 'Action';

//action
$text_returnOrder['approve'] = 'Approve';
$text_returnOrder['disapprove'] = 'Disapprove';
$text_returnOrder['details'] = 'Order Details';
$text_returnOrder['cancel'] = 'Cancel Return';

//return request end//

//--------------------------------------------//

//return Old Stock start//

//title
$text_returnOrder['return_stock_title'] = 'Return Old Stock To Vendor  ';
$text_returnOrder['return_details'] = 'Return Order Details';

//form label
$text_returnOrder['return_date'] = 'Date Of Return';
$text_returnOrder['vendor_name'] = 'Vendor Name';
$text_returnOrder['reason'] = 'Reason';

//btn
$text_returnOrder['add_item'] = 'Add Items';
$text_returnOrder['search'] = 'Search';
$text_returnOrder['submit'] = 'Submit';
$text_returnOrder['reset'] = 'Reset';

//table headings
$text_returnOrder['reference'] = 'Reference';
$text_returnOrder['item_name'] = 'Item Name';
$text_returnOrder['brand_name'] = 'Brand Name';
$text_returnOrder['unit_price'] = 'Unit Price';
$text_returnOrder['quantity_transfer'] = 'Quantity Transfer';
$text_returnOrder['total'] = 'Grand Total';
$text_returnOrder['packing'] = 'Packing';

//item details
$text_returnOrder['item_details'] = 'Item Details';
$text_returnOrder['item_id'] = 'Item Id';
$text_returnOrder['vendor_name'] = 'Vendor Name';
$text_returnOrder['current_stock'] = 'Current Stock';
$text_returnOrder['sell_rate'] = 'Sell Rate';
$text_returnOrder['profit'] = 'Profit%';
$text_returnOrder['discount_rate'] = 'Discount Rate';
$text_returnOrder['ro_number'] = 'RO#';
$text_returnOrder['item_total'] = 'Total';
$text_returnOrder['grand_total'] = 'Grand Total (TTC)';
